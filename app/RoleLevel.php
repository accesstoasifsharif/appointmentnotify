<?php

namespace App;

use App\Role;
use Illuminate\Database\Eloquent\Model;

class RoleLevel extends Model
{
	protected $fillable = [
	'level_name'
	];

	public function roles()
	{
		return $this->hasMany(Role::class);
	}

	public function perspectiveLevels()
	{
		return $this->hasMany(PerspectiveLevel::class);
	}


}
