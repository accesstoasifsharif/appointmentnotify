<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;

class EmergencyContact extends Model
{

	protected $fillable = [
	'person_name',
	'phone_number',
	'email',
	'relation',
	'contact_index',
	'emergencycontactable_id',
	'emergencycontactable_type',
	];

	public function emergencycontactable()
	{
		return $this->morphTo();
	}
}