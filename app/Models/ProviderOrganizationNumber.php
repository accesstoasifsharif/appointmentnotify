<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Traits\ValidateModel;

class ProviderOrganizationNumber extends Model {
	use ValidateModel;
	protected $fillable = [
		'pon_number',
		'ponnumberable_id',
		'ponnumberable_type',
	];
	protected static $validation_rules = [
		'pon_number' => 'required',
	];
	public function ponnumberable() {
		return $this->morphTo();
	}

}