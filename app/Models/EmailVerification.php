<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class EmailVerification extends Model
{
	protected $fillable = [
	'email',
	'token',
	'token_validity',
	];
	
}


