<?php
namespace Model;

use Illuminate\Database\Eloquent\Model;
use Model\Address;
use Model\Avatar;
use Model\ContactInformation;
use Model\NationalProviderIdentification;
use Model\TimeTable;
use Traits\ValidateModel;

class HealthCareFacility extends Model {
	use ValidateModel;
	protected $fillable = [
		'facility_name',
		'creator_id',
		'honor_id',
		'latitude',
		'longitude',
	];

	protected static $validation_rules = [
		'facility_name' => 'required',
	];

	public function address() {
		return $this->morphOne(Address::class, 'addressable');
	}

	public function contact() {
		return $this->morphOne(ContactInformation::class, 'contactable');
	}

	public function timeTable() {
		return $this->morphMany(TimeTable::class, 'timetableable');
	}

	public function avatar() {
		return $this->morphOne(Avatar::class, 'avatarable');
	}

	public function NPI() {
		return $this->morphOne(NationalProviderIdentification::class, 'npinumberable');
	}

}
