<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;
class Avatar extends Model
{
	protected $fillable = [
	'avatar',
	'avatarable_id',
	'avatarable_type'
	];

	public function avatarable()
	{
		return $this->morphTo();
	}

}