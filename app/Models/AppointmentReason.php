<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;

class AppointmentReason extends Model {

	protected $fillable = [

		'reason_name',

	];
}