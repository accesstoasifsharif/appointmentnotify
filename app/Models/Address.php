<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Traits\ValidateModel;

class Address extends Model {
	use ValidateModel;
	protected $fillable = [
		'address_line_1',
		'address_line_2',
		'city_name',
		'zip_code',
		'state_id',
		'country_id',
		'address_type',
		'addressable_id',
		'addressable_type',
	];
	protected static $validation_rules = [
		'address_line_1' => 'required',
		'city_name'      => 'required',
		'zip_code'       => 'required',
		'state_id'       => 'required',
	];

	public function country() {
		return $this->belongsTo(Country::class);
	}

	public function state() {
		return $this->belongsTo(State::class);
	}

	public function addressable() {
		return $this->morphTo();
	}
}