<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class SubscriptionPlanSelect extends Model
{
	protected $table = 'subscription_plan_select';
	
	protected $fillable = [
	'health_care_provider_id',
	'subscription_plan_interval_id',
	'status',
	'end_at'
	];

	public function subscriptionPlanInterval()
	{
		return $this->belongsTo(SubscriptionPlanInterval::class);
	}

	public function healthCareProvider()
	{
		return $this->belongsTo(HealthCareProvider::class);
	}

}