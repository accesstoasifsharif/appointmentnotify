<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;
class IntervalPeriod extends Model
{
	protected $fillable = [
	'period_name',
	'month_multiple'
	];
	
	public function subscriptionPlanInterval()
	{
		return $this->hasMany(SubscriptionPlanInterval::class);
	}

}