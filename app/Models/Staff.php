<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;
use Model\Avatar;

class Staff extends Model {
	protected $fillable = [

		'designation',
		'model_identifier',
		'profile_status',
		'health_care_provider_id',
	];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function personalInformation() {
		return $this->morphOne(PersonalInformation::class, 'personalable');
	}

	public function address() {
		return $this->morphOne(Address::class, 'addressable');
	}

	public function contact() {
		return $this->morphOne(ContactInformation::class, 'contactable');
	}

	public function avatar() {
		return $this->morphOne(Avatar::class, 'avatarable');
	}

}