<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;

class AppointmentReservation extends Model {

	protected $fillable = [
	'site_user_id',
	'an_event_id',
	];
}