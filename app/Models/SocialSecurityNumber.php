<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class SocialSecurityNumber extends Model
{
	protected $fillable = [
	'SSN',
	'ssnumberable_id',
	'ssnumberable_type'
	];

	public function ssnumberable()
	{
		return $this->morphTo();
	}

}