<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class TemporaryStorage extends Model {
	protected $fillable = [
		'email',
		'storage_purpose',
		'storage_description',
		'expiry_date_time',
		'creator_id',
		'creator_model',
	];

}