<?php

namespace Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TimeTable extends Model {
	protected $fillable = [
		'time_from',
		'time_to',
		'day',
		'timetableable_id',
		'timetableable_type',
	];

	protected $times = ['time_from', 'time_to'];

	public function timetableable() {
		return $this->morphTo();
	}

	public function setTimeToAttribute($time) {

		$this->attributes['time_to'] = Carbon::parse(date('H:i:s', strtotime($time)))->format('H:i:s');
	}

	public function getTimeToAttribute() {
		return Carbon::parse($this->attributes['time_to'])->format('g:i A');
	}

	public function setTimeFromAttribute($time) {
		$this->attributes['time_from'] = Carbon::parse(date('H:i:s', strtotime($time)))->format('H:i:s');
	}

	public function getTimeFromAttribute() {
		return Carbon::parse($this->attributes['time_from'])->format('g:i A');
	}

}
