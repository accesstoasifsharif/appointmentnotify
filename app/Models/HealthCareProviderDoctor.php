<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Model\TimeTable;

class HealthCareProviderDoctor extends Model {
	protected $fillable = [
		'hcp_facility_id',
		'doctor_id',
	];

	public function timeTable() {
		return $this->morphMany(TimeTable::class, 'timetableable');
	}
}
