<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Model\TimeTable;

class HealthCareProviderStaff extends Model {
	protected $fillable = [
		'hcp_facility_id',
		'staff_id',
	];

	protected $table = 'health_care_provider_staff';

	public function timeTable() {
		return $this->morphMany(TimeTable::class, 'timetableable');
	}
}
