<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class FeatureRole extends Model
{
	protected $fillable = [
	'provider_type',
	'role_id',
	'feature_id',
	];
}
