<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	protected $fillable = [
	'country_name','area_code'
	];

	public function states()
	{
		return $this->hasMany(State::class);
	}


	public function addresses()
	{
		return $this->hasMany(Address::class);
	}

}
