<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class AccountRecoveryEmail extends Model {
	protected $fillable = ['email_address', 'user_id'];

	public function user() {
		return $this->belongsTo(User::class);
	}
	//
}
