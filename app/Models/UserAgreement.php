<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class UserAgreement extends Model {
	protected $table = 'user_agreement';
	protected $fillable = [
		'agreement_id',
		'user_id',

	];
}
