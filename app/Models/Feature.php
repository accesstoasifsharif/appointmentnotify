<?php

namespace Model;

use App\Role;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
	protected $fillable = [
	'feature_name'
	];
	public function roles()
	{
		return $this->hasMany(Role::class);
	}

}
