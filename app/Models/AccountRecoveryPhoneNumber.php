<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class AccountRecoveryPhoneNumber extends Model {
	protected $fillable = ['phone_number', 'user_id'];

	public function user() {
		return $this->belongsTo(User::class);
	}
}
