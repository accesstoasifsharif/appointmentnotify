<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class AppointmentNote extends Model {
	protected $fillable = ['description', 'appointment_id'];
}
