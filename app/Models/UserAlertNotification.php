<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class UserAlertNotification extends Model
{
	protected $table = 'user_alert_notification';
	
	protected $fillable = [
	'alert_type',
	'alert_name',
	'user_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

}