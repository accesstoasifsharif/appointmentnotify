<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class AppointmentCancelation extends Model {
	protected $fillable = ['an_event_id', 'doctor_id', 'expiry_date_time'];
}
