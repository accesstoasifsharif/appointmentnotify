<?php

namespace Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model {
	protected $table = 'messages';
	public $timestamps = true;
	protected $touches = ['conversation'];

	public $fillable = [
		'message',
		'is_seen',
		'deleted_from_sender',
		'deleted_from_receiver',
		'user_id',
		'conversation_id',
	];

	public function getHumansTimeAttribute() {
		$date = $this->created_at;
		$now = $date->now();

		return $date->diffForHumans($now, true);
	}

	public function conversation() {
		return $this->belongsTo(Conversation::class);
	}

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function sender() {
		return $this->user();
	}
}
