<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model {
	protected $fillable = ['an_event_id'];
}
