<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
	protected $fillable = [
	'name',
	'description',
	'license_type',
	'user_type',
	];

	public function User()
	{
		return $this->belongsToMany(User::class);
	}

}

