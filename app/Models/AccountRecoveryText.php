<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class AccountRecoveryText extends Model {
	protected $fillable = ['recovery_text', 'user_id'];

	public function user() {
		return $this->belongsTo(User::class);
	}
}
