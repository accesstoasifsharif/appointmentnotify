<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Traits\ValidateModel;

class EmployerIdentificationNumber extends Model {
	use ValidateModel;
	protected $fillable = [
		'ein',
		'einumberable_id',
		'einumberable_type',
	];
	protected static $validation_rules = [
		'ein' => 'required',
	];
	public function einumberable() {
		return $this->morphTo();
	}

}