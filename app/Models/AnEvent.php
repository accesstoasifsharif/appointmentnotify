<?php

namespace Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AnEvent extends Model {
	protected $fillable = [
		'calendar_type',
		'date',
		'start_time',
		'end_time',
		'site_user_id',
		'doctor_id',
		'health_care_facility_id',
		'health_care_provider_facility_id',
		'appointment_reason_id',
		'appointment_status',
		'appointment_current_state',
	];
	protected $times = ['start_time', 'end_time'];

	public function setEndTimeAttribute($time) {

		$this->attributes['end_time'] = Carbon::parse(date('H:i:s', strtotime($time)))->format('H:i:s');
	}

	public function getEndTimeAttribute() {
		return Carbon::parse($this->attributes['end_time'])->format('g:i A');
	}

	public function setStartTimeAttribute($time) {
		$this->attributes['start_time'] = Carbon::parse(date('H:i:s', strtotime($time)))->format('H:i:s');
	}

	public function getStartTimeAttribute() {
		return Carbon::parse($this->attributes['start_time'])->format('g:i A');
	}

	/*Date Attribute setter getter*/
	public function setDateAttribute($date) {
		$this->attributes['date'] = Carbon::parse($date)->format('Y-m-d');
	}

	public function getDateAttribute() {
		return Carbon::parse($this->attributes['date'])->format('F j, Y');
	}

}
