<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	protected $fillable = [
	'state_name',
	'postal_code',
	'country_id',
	];

	public function country()
	{
		return $this->belongsTo(Country::class);
	}

	public function addresses()
	{
		return $this->hasMany(Address::class);
	}

}

