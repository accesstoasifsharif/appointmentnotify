<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
	protected $table = 'conversations';
	public $timestamps = true;
	public $fillable = [
	'user_one',
	'user_two',
	'status',
	];

	public function messages()
	{
		return $this->hasMany(Message::class)
		->with('sender');
	}

	public function userone()
	{
		return $this->belongsTo(User::class);
	}

	public function usertwo()
	{
		return $this->belongsTo(User::class);
	}
}
