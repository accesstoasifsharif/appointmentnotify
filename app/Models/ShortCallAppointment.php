<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;

class ShortCallAppointment extends Model {

	protected $table = 'appointment_short_calls';
	protected $fillable = [
		'an_event_id',
		'expiry_date_time',
	];

	public function anEvent() {
		return $this->belongsTo(AnEvent::class);
	}
}
