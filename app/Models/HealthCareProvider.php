<?php

namespace Model;

use App\PerspectiveLevel;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
use Model\Avatar;
use Model\Department;
use Model\Hospital;
use Model\MedicalClinic;
use Model\SinglePractice;
use Model\Staff;
use Model\SubscriptionPlanSelect;

class HealthCareProvider extends Model {
	use Billable;

	protected $fillable = [
		'provider_type',
		'user_id',
		'trial_ends_at',
	];

	protected $dates = ['trial_ends_at'];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function MedicalClinic() {
		return $this->hasOne(MedicalClinic::class);
	}

	public function Hospital() {
		return $this->hasOne(Hospital::class);
	}

	public function Department() {
		return $this->hasOne(Department::class);
	}

	public function hcpDepartments() {
		return $this->hasMany(Department::class, 'other_hcp_id');
	}

	public function SinglePractice() {
		return $this->hasOne(SinglePractice::class);
	}
	public function Staff() {
		return $this->hasOne(Staff::class);
	}
	public function subscriptionPlanSelect() {
		return $this->hasOne(SubscriptionPlanSelect::class);
	}

	public function perspectiveLevels() {
		return $this->hasMany(PerspectiveLevel::class);
	}

	public function healthCareProviderFacility() {
		return $this->hasMany(HealthCareProviderFacility::class);
	}

	public function avatar() {
		return $this->morphOne(Avatar::class, 'avatarable');
	}
	public function getFullNameAttribute() {

		$full_name = '';
		if (!is_null($this->SinglePractice)) {

			$full_name = $this->SinglePractice->doctor->fullName;
			return $full_name;
		}
		if (!is_null($this->MedicalClinic)) {

			$full_name = $this->MedicalClinic->fullName;
			return $full_name;
		}
		if (!is_null($this->Hospital)) {

			$full_name = $this->Hospital->fullName;
			return $full_name;
		}
		if (!is_null($this->Department)) {

			$full_name = $this->Department->fullName;
			return $full_name;
		}
		/*	$full_name = $this->personalInformation->first_name . ' ' . $this->personalInformation->middle_initial . ' ' . $this->personalInformation->last_name;*/
		return 'HealthCareProvider';
	}
}
