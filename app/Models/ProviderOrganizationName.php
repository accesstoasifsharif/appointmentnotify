<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Traits\ValidateModel;

class ProviderOrganizationName extends Model {
	use ValidateModel;
	protected $fillable = [
		'pon_name',
		'ponnameable_id',
		'ponnameable_type',
	];
	protected static $validation_rules = [
		'pon_name' => 'required',
	];
	public function ponnameable() {
		return $this->morphTo();
	}

}