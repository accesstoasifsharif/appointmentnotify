<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class DummyAvatar extends Model
{
	protected $fillable = [
	'avatar',
	'gender',
	'default',
	'user_type'
	];


	public function users()
	{ 
		return $this->belongsToMany(User::class);
	}
}


