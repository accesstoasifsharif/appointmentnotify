<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Traits\ValidateModel;

class ContactPerson extends Model {
	use ValidateModel;
	protected $fillable = [
		'contact_person_name',
		'contactable_id',
		'contactable_type',
	];
	protected static $validation_rules = [
		'contact_person_name' => 'required',

	];

	public function contactable() {
		return $this->morphTo();
	}
}
