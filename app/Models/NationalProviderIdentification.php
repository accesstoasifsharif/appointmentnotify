<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Traits\ValidateModel;

class NationalProviderIdentification extends Model {
	use ValidateModel;
	protected $fillable = [
		'npi',
		'npinumberable_id',
		'npinumberable_type',
	];
	protected static $validation_rules = [
		'npi' => 'required',
	];
	public function npinumberable() {
		return $this->morphTo();
	}

}