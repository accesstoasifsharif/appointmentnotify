<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
	protected $fillable = [
	'name',
	'parent',
	'insurance_type',
	'insurance_category'
	];

}
