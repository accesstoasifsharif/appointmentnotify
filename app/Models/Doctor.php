<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;
use Model\Avatar;
use Model\PersonalInformation;
use Model\SinglePractice;
use Model\SiteUser;
use Model\Specialty;
use Traits\ValidateModel;

class Doctor extends Model {
	use ValidateModel;
	protected $fillable = [
		'specialty_id',
		/*'health_care_provider_id',*/
	];
	protected static $validation_rules = [

		'specialty_id' => 'required',

	];
	public function specialty() {
		return $this->belongsTo(Specialty::class);
	}

	public function siteUsers() {
		return $this->belongsToMany(SiteUser::class);
	}

	public function singlePractice() {
		return $this->hasOne(SinglePractice::class);
	}

	public function EIN() {
		return $this->morphOne(EmployeeIdentificationNumber::class, 'einumberable');
	}

	public function SSN() {
		return $this->morphOne(SocialSecurityNumber::class, 'ssnumberable');
	}

	public function NPI() {
		return $this->morphOne(NationalProviderIdentification::class, 'npinumberable');
	}

	public function personalInformation() {
		return $this->morphOne(PersonalInformation::class, 'personalable');
	}

	public function contact() {
		return $this->morphOne(ContactInformation::class, 'contactable');
	}

	public function getFullNameAttribute() {
		$full_name = $this->personalInformation->first_name . ' ' . $this->personalInformation->middle_name . ' ' . $this->personalInformation->last_name;
		return $full_name;
	}

	public function getGenderPronounAttribute() {
		if ($this->personalInformation->gender == 'male') {return 'his';}

		if ($this->personalInformation->gender == 'female') {return 'her';}

		if ($this->personalInformation->gender == 'other') {return 'his/her';}
	}

	public function avatar() {
		return $this->morphOne(Avatar::class, 'avatarable');
	}

	public function getMyUserAttribute() {
		return $this->singlePractice->healthCareProvider->user;
	}

}