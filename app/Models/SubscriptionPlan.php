<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;
class SubscriptionPlan extends Model
{
	protected $fillable = [
	'plan_name',
	'allowed_user',
	'trail_period',
	'statement_description'
	];

	public function subscriptionPlanInterval()
	{
		return $this->hasMany(SubscriptionPlanInterval::class);
	}

}