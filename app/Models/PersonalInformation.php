<?php

namespace Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Traits\ValidateModel;

class PersonalInformation extends Model {
	use ValidateModel;
	protected $dates = [
		'date_of_birth',
	];
	protected $fillable = [
		'first_name',
		'middle_name',
		'last_name',
		'date_of_birth',
		'psuedo_name',
		'gender',
		'personalable_id',
		'personalable_type',
	];
	protected static $validation_rules = [
		'first_name'  => 'required',
		'last_name'   => 'required',
		'gender'      => 'required',
		'psuedo_name' => 'required|string|min:6|unique:personal_informations',
	];

	public function personalable() {
		return $this->morphTo();
	}

	public function setDateOfBirthAttribute($birth_date) {

		$this->attributes['date_of_birth'] = Carbon::parse($birth_date)->format('Y-m-d');
	}

	public function getDateOfBirthAttribute() {
		return Carbon::parse($this->attributes['date_of_birth'])->format('l F j , Y');
	}
}