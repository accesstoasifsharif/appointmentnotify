<?php

namespace Model;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Model\Avatar;

class SiteUser extends Model {

	protected $table = 'site_users';
	protected $fillable = [
		'user_id',
	];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function doctors() {
		return $this->belongsToMany(Doctor::class);
	}

	public function address() {
		return $this->morphOne(Address::class, 'addressable');
	}

	public function contact() {
		return $this->morphOne(ContactInformation::class, 'contactable');
	}

	public function personalInformation() {
		return $this->morphOne(PersonalInformation::class, 'personalable');
	}

	public function secondaryContact() {
		return $this->morphMany(SecondaryContactNumber::class, 'secondarycontactable');
	}

	public function emergencyContact() {
		return $this->morphMany(EmergencyContact::class, 'emergencycontactable');
	}

	public function avatar() {
		return $this->morphOne(Avatar::class, 'avatarable');
	}

	public function getMyUserAttribute() {
		return $this->user;
	}

}
