<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Model\Address;
use Model\ContactInformation;
use Model\Doctor;
use Model\HealthCareProvider;
use Model\NationalProviderIdentification;
use Model\PersonalInformation;
use Traits\ValidateModel;

class SinglePractice extends Model {
	use ValidateModel;
	protected $fillable = [
		'doctor_id',
		'health_care_provider_id',
	];
	protected static $validation_rules = [
		'doctor_id',
		'health_care_provider_id',
	];
	public function healthCareProvider() {
		return $this->belongsTo(HealthCareProvider::class);
	}

	public function doctor() {
		return $this->belongsTo(Doctor::class);
	}

	public function EIN() {
		return $this->morphOne(EmployerIdentificationNumber::class, 'einumberable');
	}
	public function address() {
		return $this->morphOne(Address::class, 'addressable');
	}

	public function contact() {
		return $this->morphOne(ContactInformation::class, 'contactable');
	}

	public function personalInformation() {
		return $this->morphOne(PersonalInformation::class, 'personalable');
	}

	public function NPI() {
		return $this->morphOne(NationalProviderIdentification::class, 'npinumberable');
	}
}
