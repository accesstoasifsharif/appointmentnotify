<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class SubscriptionPlanInterval extends Model
{
	protected $fillable = [
	'subscription_plan_name',
	'subscription_plan_id',
	'interval_period_id',
	'amount'
	];



	public function subscriptionPlan()
	{
		return $this->belongsTo('Models\SubscriptionPlan');
	}

	public function intervalPeriod()
	{
		return $this->belongsTo('Models\IntervalPeriod');
	}


}