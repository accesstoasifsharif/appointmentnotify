<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;
use Model\Address;
use Model\ContactInformation;
use Model\ContactPerson;
use Model\EmployerIdentificationNumber;
use Model\NationalProviderIdentification;
use Model\ProviderOrganizationName;

class MedicalClinic extends Model {
	protected $fillable = [
		'provider_organization_name',
		'contact_person',
		'health_care_provider_id',
	];

	public function healthCareProvider() {
		return $this->belongsTo(HealthCareProvider::class);
	}

	public function address() {
		return $this->morphOne(Address::class, 'addressable');
	}
	public function EIN() {
		return $this->morphOne(EmployerIdentificationNumber::class, 'einumberable');
	}
	public function contactPerson() {
		return $this->morphOne(ContactPerson::class, 'contactable');
	}
	public function providerOrganizationName() {
		return $this->morphOne(ProviderOrganizationName::class, 'ponnameable');
	}
	public function NPI() {
		return $this->morphOne(NationalProviderIdentification::class, 'npinumberable');
	}
	public function contact() {
		return $this->morphOne(ContactInformation::class, 'contactable');
	}
	public function getFullNameAttribute() {
		$full_name = $this->contactPerson->contact_person_name;
		return $full_name;
	}
}
