<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentHistory extends Model {
	protected $fillable = [
		'calendar_type',
		'date',
		'start_time',
		'end_time',
		'an_event_id',
		'site_user_id',
		'doctor_id',
		'health_care_facility_id',
		'health_care_provider_facility_id',
		'appointment_reason_id',
		'appointment_status',
		'appointment_current_state',
	];
}
