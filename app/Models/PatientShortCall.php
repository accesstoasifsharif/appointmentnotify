<?php

namespace Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PatientShortCall extends Model {

	protected $fillable = [
		'doctor_id',
		'health_care_provider_facility_id',
		'site_user_id',
		'site_user_email',
		'time_span',
		'expiry_date_time',
	];

	public function setExpiryDateTimeAttribute($weeks) {
		$week = preg_replace('/[^0-9]/', '', $weeks);
		$this->attributes['expiry_date_time'] = Carbon::now()->addWeek($week);
	}

	public function doctor() {
		return $this->belongsTo(Doctor::class);
	}

	public function healthCareFacility() {
		return $this->belongsTo(HealthCareFacility::class);
	}

	public function siteUser() {
		return $this->belongsTo(SiteUser::class);
	}

}