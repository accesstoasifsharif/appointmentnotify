<?php
namespace Model;

use Illuminate\Database\Eloquent\Model;
use Model\HealthCareProvider;
use Model\HealthCareFacility;

class HealthCareProviderFacility extends Model {

	protected $fillable = [
	'health_care_facility_id',
	'health_care_provider_id'
	];


	public function healthCareProvider()
	{
		return $this->belongsTo(HealthCareProvider::class);
	}

	public function healthCareFacility()
	{
		return $this->belongsTo(HealthCareFacility::class);
	}




}
