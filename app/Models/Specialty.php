<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;
use Traits\ValidateModel;

class Specialty extends Model {
	use ValidateModel;
	protected $fillable = [
		'specialty_name',
	];

	protected static $validation_rules = [
		'specialty_id' => 'required',

	];

	public function doctor() {
		return $this->hasOne(Doctor::class);
	}
}