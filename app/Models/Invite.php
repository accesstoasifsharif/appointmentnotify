<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;

class Invite extends Model {

	protected $fillable = [
		'email', 'token',
	];

}
