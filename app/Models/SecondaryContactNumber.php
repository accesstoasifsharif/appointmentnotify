<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class SecondaryContactNumber extends Model
{
	protected $fillable = [
	'phone_number',
	'contact_type',
	'secondary_contactable_id',
	'secondary_contactable_type'
	];

	public function secondarycontactable()
	{
		return $this->morphTo();
	}

}