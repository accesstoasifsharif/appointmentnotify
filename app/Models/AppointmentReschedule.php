<?php

namespace Model;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class AppointmentReschedule extends Model {
	protected $fillable = [
		'an_event_id',
		'doctor_id',
		'prefer_time_1',
		'prefer_time_2',
		'prefer_date_1',
		'prefer_date_2',
		'reason',

	];
	protected $times = ['prefer_time_1', 'prefer_time_2'];

	public function setPreferTime1Attribute($time) {
		$flag_1 = str_contains($time, '(');
		if ($flag_1) {
			$time_convert = strstr($time, '(', true);
			$this->attributes['prefer_time_1'] = Carbon::parse($time_convert)->format('H:i:s');
		} else {
			$this->attributes['prefer_time_1'] = $time;
		}

		$flag1 = str_contains($time, 'AM');
		$flag2 = str_contains($time, 'PM');

		if ($flag1 || $flag2) {
			$this->attributes['prefer_time_1'] = DateTime::createFromFormat('h:i A', $time);
		} else {
			$this->attributes['prefer_time_1'] = $time;
		}
	}

	public function getPreferTime1Attribute() {
		return Carbon::parse($this->attributes['prefer_time_1'])->format('Y m d H:i:s');
	}

	public function setPreferTime2Attribute($time) {
		$flag_1 = str_contains($time, '(');
		if ($flag_1) {
			$time_convert = strstr($time, '(', true);
			$this->attributes['prefer_time_2'] = Carbon::parse($time_convert)->format('H:i:s');
		} else {
			$this->attributes['prefer_time_2'] = $time;
		}

		$flag1 = str_contains($time, 'AM');
		$flag2 = str_contains($time, 'PM');
		if ($flag1 || $flag2) {
			$this->attributes['prefer_time_2'] = DateTime::createFromFormat('h:i A', $time);
		} else {
			$this->attributes['prefer_time_2'] = $time;
		}

	}

	public function getPreferTime2Attribute() {
		return Carbon::parse($this->attributes['prefer_time_2'])->format('Y m d H:i:s');
	}

}
