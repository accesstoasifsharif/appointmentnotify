<?php

namespace Model;
use Illuminate\Database\Eloquent\Model;
use Traits\ValidateModel;

class ContactInformation extends Model {
	use ValidateModel;
	protected $fillable = [
		'phone_number',
		'fax_number',
		'email',
		'url',
		'contactable_id',
		'contactable_type',
	];
	protected static $validation_rules = [
		'phone_number' => 'required',

		'email' => 'string',
	];

	public function contactable() {
		return $this->morphTo();
	}
}