<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $fillable = ['zip','city','state','latitude','longitude','timezone','dst'];
}
