<?php

namespace App;
use App\PerspectiveLevel;
use App\User;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Model\Avatar;
use Model\HealthCareProvider;
use Model\SiteUser;

class User extends Authenticatable {
	use HasApiTokens, Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password', 'user_type', 'profile_status',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function admin() {
		return $this->hasOne('App\Admin');
	}

	public function save(array $options = []) {
		$this->slugAttribute();
		parent::save($options);
	}

	public static function create(array $attributes = []) {
		return (new static )->newQuery()->create($attributes);
	}

	public function slugAttribute() {
		$this->attributes['slug'] = uniqid();
	}

	public static function findBySlugOrId($slug) {
		return User::whereSlug($slug)->orWhere('id', $slug)->first();
	}

	public function siteUser() {
		return $this->hasOne(SiteUser::class);
	}

	public function recoveryEmail() {
		return $this->hasOne(AccountRecoveryEmail::class);
	}

	public function recoveryText() {
		return $this->hasOne(AccountRecoveryText::class);
	}

	public function recoveryPhoneNumber() {
		return $this->hasOne(AccountRecoveryPhoneNumber::class);
	}

	public function healthCareProvider() {
		return $this->hasOne(HealthCareProvider::class);
	}

	public function messages() {
		return $this->hasMany(Message::class);
	}

	public function otherReason() {
		return $this->hasMany(AppointmentOtherReason::class);
	}

	public function staff() {
		return $this->hasOne(Staff::class);
	}

	public function avatar() {
		return $this->morphOne(Avatar::class, 'avatarable');
	}

	public function conversation() {
		return $this->hasMany(Conversation::class);
	}

	public function dummyAvatar() {
		return $this->belongsToMany(DummyAvatar::class);
	}

	public function agreement() {
		return $this->belongsToMany(Agreement::class);
	}

	public function perspectiveLevels() {
		return $this->hasMany(PerspectiveLevel::class);
	}
	public function getFullNameAttribute() {

		$full_name = '';
		if (!is_null($this->siteUser)) {
			$full_name = $this->siteUser->personalInformation->first_name . ' ' . $this->siteUser->personalInformation->middle_name . ' ' . $this->siteUser->personalInformation->last_name;
			return $full_name;
		}
		if (!is_null($this->healthCareProvider)) {

			$full_name = $this->healthCareProvider->fullName;
			return $full_name;
		}
		return 'hard code name';
	}
	public function getUserNameAttribute() {
		if (!$this->name == '') {
			return $this->name;
		} else {
			return $this->slug;
		}

	}
	public function getMyUserAttribute() {
		return $this;
	}

}
