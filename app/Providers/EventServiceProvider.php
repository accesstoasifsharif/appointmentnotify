<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider {
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'App\Events\Event' => [
			'App\Listeners\EventListener',
		],
		'App\Events\SiteUser\OrderShipped' => [
			'App\Listeners\SiteUser\SendShipmentNotification',
			'App\Listeners\SiteUser\SendShipmentNotificationByEmail',
			'App\Listeners\EventListener@sendEmail',
			'App\Listeners\EventListener@handle',
		],

		'App\Events\HealthCareProvider\AppointmentBookedAndApprovedByProvider' => [
			'App\Listeners\SiteUser\AppointmentBookedAndApprovedByProvider',
		],

		'App\Events\HealthCareProvider\RescheduleAppointmentRequestRejectedByProvider' => [
			'App\Listeners\SiteUser\RescheduleAppointmentRequestRejectedByProvider',
		],

		'App\Events\HealthCareProvider\ShortCallAppointmentPublishByProvider' => [
			'App\Listeners\SiteUser\ShortCallAppointmentPublishByProvider',
		],

		'App\Events\HealthCareProvider\ReserveAppointmentRequestApproved' => [
			'App\Listeners\SiteUser\ReserveAppointmentRequestApproved',
		],

		'App\Events\HealthCareProvider\RescheduleAppointmentRequestApproved' => [
			'App\Listeners\SiteUser\RescheduleAppointmentRequestApproved',
		],

		'App\Events\HealthCareProvider\ReserveAppointmentRequestRescheduled' => [
			'App\Listeners\SiteUser\ReserveAppointmentRequestRescheduled',
		],

		'App\Events\HealthCareProvider\RescheduleAppointmentRequestRescheduled' => [
			'App\Listeners\SiteUser\RescheduleAppointmentRequestRescheduled',
		],

		'App\Events\SiteUser\AddPatientToShortCallListBySiteUser' => [
			'App\Listeners\HealthCareProvider\AddPatientToShortCallListBySiteUser',
		],

		'App\Events\SiteUser\AppointmentRescheduleRequestBySiteUser' => [
			'App\Listeners\HealthCareProvider\AppointmentRescheduleRequestBySiteUser',
		],

		'App\Events\SiteUser\AppointmentReservationRequestBySiteUser' => [
			'App\Listeners\HealthCareProvider\AppointmentReservationRequestBySiteUser',
		],

		'App\Events\MessageConversation' => [
			'App\Listeners\MessageConversation',
		],

	];

	protected $subscribe = [
		'App\Listeners\PostDeploymentEventListener',
	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot() {
		parent::boot();

		//
	}
}
