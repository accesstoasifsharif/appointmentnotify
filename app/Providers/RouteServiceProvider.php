<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider {
	/**
	 * This namespace is applied to your controller routes.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'App\Http\Controllers';
	protected $namespace_admin = '';
	protected $namespace_home = 'Home';
	protected $namespace_siteUser = '';
	protected $namespace_healthCareProvider = '';

	protected $api_namespace_postfix = '\ApiControllers';

	protected $prefix_siteUser = '';
	protected $prefix_healthCareProvider = '';
	protected $namespace_common_api = 'Common';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @return void
	 */
	public function boot() {
		$this->namespace_admin = adminNamespace();
		$this->namespace_siteUser = siteUserNamespace();
		$this->namespace_healthCareProvider = healthCareProviderNamespace();

		$this->prefix_siteUser = siteUserPrefix();
		$this->prefix_healthCareProvider = healthCareProviderPrefix();

		parent::boot();
	}

	/**
	 * Define the routes for the application.
	 *
	 * @return void
	 */
	public function map() {
		$this->mapApiRoutes();

		$this->mapWebRoutes();

		//
	}

	/**
	 * Define the "web" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapWebRoutes() {

		Route::group([
			'middleware' => 'web',
			'namespace'  => $this->namespace,
		], function ($router) {
			require base_path('routes/web.php');
		});

		Route::group([
			'middleware' => 'web',
			'namespace'  => $this->namespace_siteUser,
			'prefix'     => $this->prefix_siteUser,
		], function ($router) {
			require base_path('routes/siteuser.php');
		});

		Route::group([
			'middleware' => 'web',
			'namespace'  => $this->namespace_healthCareProvider,
			'prefix'     => $this->prefix_healthCareProvider,
		], function ($router) {
			require base_path('routes/healthcareprovider.php');
		});

		Route::group([
			'middleware' => 'web',
			'namespace'  => $this->namespace_admin,
			'prefix'     => adminPrefix(),
		], function ($router) {
			require base_path('routes/admin.php');
		});
		Route::group([
			'middleware' => 'web',
			'namespace'  => $this->namespace_admin,
		], function ($router) {
			require base_path('routes/practice.php');
		});
	}

	/**
	 * Define the "api" routes for the application.
	 *
	 * These routes are typically stateless.
	 *
	 * @return void
	 */
	protected function mapApiRoutes() {
		Route::prefix('api')
			->middleware('api')
			->namespace($this->namespace_home . $this->api_namespace_postfix)
			->group(base_path('routes/api.php'));

		Route::prefix('api')
			->middleware('api')
			->namespace($this->namespace_siteUser . $this->api_namespace_postfix)
			->group(base_path('routes/api.siteuser.php'));

		Route::prefix('api')
			->middleware('api')
			->namespace($this->namespace_healthCareProvider . $this->api_namespace_postfix)
			->group(base_path('routes/api.healthcareprovider.php'));

		Route::prefix('api')
			->middleware('api')
			->namespace($this->namespace_admin . $this->api_namespace_postfix)
			->group(base_path('routes/api.admin.php'));

		Route::prefix('api')
			->middleware('api')
			->namespace($this->namespace_common_api . $this->api_namespace_postfix)
			->group(base_path('routes/api.common.php'));
	}

}