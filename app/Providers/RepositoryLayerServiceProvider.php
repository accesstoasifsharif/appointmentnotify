<?php

namespace App\Providers;
use File;
use Illuminate\Support\ServiceProvider;

class RepositoryLayerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {

		$contractNamespace = '/Repositories/Contracts/';
		$implNamespace = '/Repositories/';

		$contractBasePath = glob(app_path() . $contractNamespace)[0];
		$implBasePath = glob(app_path() . $implNamespace)[0];

		/*
			getting all folder name from repositories/contract folder
		*/
		$folders = [];
		$allFolders = glob(app_path() . $contractNamespace . '/*', GLOB_ONLYDIR);

		foreach ($allFolders as $s) {
			array_push($folders, pathinfo($s)['basename']);
		}
		/* traversing all folders to register contracts */
		foreach ($folders as $singleFolder) {
			/* traversing all files to withing implementations */

			foreach (File::allFiles($contractBasePath . $singleFolder) as $filename) {
				$singleFileName = pathinfo($filename . '')['filename'];
				/*if ('SaveNotes' == $singleFileName) {
					dd('Repository\\Contracts\\' . $singleFolder . '\\' . $singleFileName,
						'Repository\\' . $singleFolder . '\\' . $singleFileName);
					}*/
				$this->app->bind(

					'Repository\\Contracts\\' . $singleFolder . '\\' . $singleFileName,
					'Repository\\' . $singleFolder . '\\' . $singleFileName
				);
			}
		}

	}
}
