<?php

namespace App\Providers;
use App;
use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

//use App\Permission;
//use App\RolePolicies;

class AuthServiceProvider extends ServiceProvider {
	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies = [
		'App\Model' => 'App\Policies\ModelPolicy',
	];

	/**
	 * Register any authentication / authorization services.
	 *
	 * @return void
	 */
	public function boot() {
		$this->registerPolicies();

		//

		if (!App::runningInConsole()) {

			/*  try{*/
			/* foreach($this->getPermissions() as $permission){

			Gate::define($permission->name,function ($user) use ($permission)
			{
			return $user->hasRole($permission->roles);
			});

			}*/

			/*       } catch (\Exception $e) {
			return [];
			}*/
			Passport::routes();
			Passport::tokensExpireIn(Carbon::now()->addDays(15));
			Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
		}

	}

/* protected function getPermissions()
{
return Permission::with('roles')->get();
}*/
}
