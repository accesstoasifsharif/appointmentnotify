<?php

namespace App;

use App\BusinessType;
use App\RoleLevel;
use Illuminate\Database\Eloquent\Model;
use Model\Feature;

class Role extends Model
{
	protected $fillable = [
	'name',
	'label',
	'business_type_id',
	'role_level_id',
	'feature_id'
	];


	public function businessType()
	{
		return $this->belongsTo(BusinessType::class);
	}


	public function roleLevel()
	{
		return $this->belongsTo(RoleLevel::class);
	}

	public function feature()
	{
		return $this->belongsTo(Feature::class);
	}



}
