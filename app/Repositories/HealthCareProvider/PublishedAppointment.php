<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Repository\Contracts\HealthCareProvider\PublishedAppointment as PublishedAppointmentContract;

class PublishedAppointment implements PublishedAppointmentContract {
	public function getSpecificDoctorSpecificHCPFacilityAppointments($doctor_id, $hcp_facility_id, $hcp_id) {
		$appointments = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id


			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Public') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Publish') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id


			");

		return $appointments;
	}

	public function getSpecificDoctorAllHCPFacilityAppointments($doctor_id, $hcp_id) {
		$appointments = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id



			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Public') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Publish') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $appointments;
	}

	public function getAllDoctorSpecificHCPFacilityAppointments($hcp_facility_id, $hcp_id) {
		$appointments = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id

			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Public') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Publish') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $appointments;
	}

	public function getAllDoctorAllHCPFacilityAppointments($hcp_id) {

		$appointments = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Public') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Publish') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $appointments;

	}

	public function getSpecificDoctorSpecificHCPFacilityAppointmentsDetail($doctor_id, $hcp_facility_id, $hcp_id) {

		if ($hcp_facility_id == 0) {
			$appointments = DB::select("
				select
				an_events.id as an_event_id,
				an_events.date as date,
				an_events.start_time as start_time,
				health_care_facilities.facility_name as facility_name,
				appointment_reasons.reason_name

				from an_events
				join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
				join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
				join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
				left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
				join doctors on doctors.id = an_events.doctor_id
				where health_care_providers.id = " . $hcp_id . "
				And doctors.id = " . $doctor_id . "
				And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Public') . "
				And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Publish') . "
				order by an_events.date
				");

			return $appointments;
		} else {
			$appointments = DB::select("
				select
				an_events.id as an_event_id,
				an_events.date as date,
				an_events.start_time as start_time,
				health_care_facilities.facility_name as facility_name,
				appointment_reasons.reason_name

				from an_events
				join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
				join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
				join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
				join doctors on doctors.id = an_events.doctor_id
				left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
				where health_care_providers.id = " . $hcp_id . "
				And doctors.id = " . $doctor_id . "
				And health_care_provider_facilities.id = " . $hcp_facility_id . "
				And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Publish') . "
				And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Publish') . "
				order by an_events.date

				");

			return $appointments;
		}

	}

}
