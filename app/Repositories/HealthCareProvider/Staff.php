<?php

namespace Repository\HealthCareProvider;
use Illuminate\Support\Facades\DB;
use Model\HealthCareProvider;
use Model\Staff as StaffModel;
use Repository\Contracts\HealthCareProvider\Staff as StaffContract;

class Staff implements StaffContract {
	public function getStaffByHCP(HealthCareProvider $hcp) {
		if (!isset($hcp->Staff)) {
			return null;
		}
		return $hcp->Staff;
	}
	public function createStaff(HealthCareProvider $hcp) {
		return \Model\Staff::create(
			[
				'health_care_provider_id' => $hcp->id,
			]);
	}
	public function getStaffByStaffId($staff_id) {
		return StaffModel::find($staff_id);
	}

	public function getStaffInfoByStaffId($staff_id) {
		return StaffModel::with('personalInformation')->find($staff_id);
	}

	public function addOrUpdateStaffInfo(StaffModel $staff) {
		$staff->save();
		return $staff;
	}

	public function removeStaff(StaffModel $staff) {
		$staff->delete();
		return 'true';
	}

	public function getStaffBySearchQuery($query) {
		$result = DB::select("
			select
			staff.id as target_id,
			staff.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from staff
			join personal_informations on personal_informations.personalable_id = staff.id
			where (personal_informations.first_name like '%" . $query . "%'
			OR personal_informations.last_name like '%" . $query . "%')
			AND personal_informations.personalable_type like '%Staff%'
			");
		return $result;
	}

}
