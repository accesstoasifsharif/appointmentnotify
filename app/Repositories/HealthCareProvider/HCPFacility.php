<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Model\HealthCareProviderFacility;
use Repository\Contracts\HealthCareProvider\HCPFacility as HCPFacilityContract;

class HCPFacility implements HCPFacilityContract {

	public function getHCPFacilityByFacilityAndProviderId($facility_id, $hcp_id) {
		$hcp_facility = HealthCareProviderFacility::where('health_care_facility_id', $facility_id)->where('health_care_provider_id', $hcp_id)->first();
		return $hcp_facility;
	}

	public function getHCPFacilityByHCPFacilityId($HCPfacility_id) {
		return HealthCareProviderFacility::find($HCPfacility_id);
	}

	public function addHCPFacility(HealthCareProviderFacility $hcp_facility) {
		$hcp_facility->save();
		return 'true';
	}
	public function addOrUpdateHCPFacility($hcp_facility_data_array) {
		return HealthCareProviderFacility::create($hcp_facility_data_array);

	}

	public function deleteHCPFacility(HealthCareProviderFacility $hcp_facility) {
		$hcp_facility->delete();
		return 'false';
	}

	public function getAllHCPFacilities($hcp_id) {
		$result = DB::select("select
			DISTINCT
			health_care_provider_facilities.id as hcp_facility_id,
			health_care_facilities.id as facility_id,
			health_care_facilities.model_identifier as model_identifier,
			health_care_facilities.facility_name,
			addresses.zip_code,
			addresses.address_line_1,
			addresses.address_line_2,
			addresses.city_name,
			states.postal_code
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_facilities on health_care_provider_facilities.health_care_facility_id = health_care_facilities.id
			join addresses on health_care_facilities.id = addresses.addressable_id
			join states on addresses.state_id = states.id
			join contact_informations on health_care_facilities.id = contact_informations.contactable_id
			where health_care_providers.id = " . $hcp_id . "
			AND addresses.addressable_type like '%HealthCareFacility%'
			AND contact_informations.contactable_type like '%HealthCareFacility%'
			");
		return $result;
	}

	public function getAllHCPFacilitiesBySearchQuery($query) {
		$result = DB::select("select
			DISTINCT
			health_care_facilities.id as facility_id,
			health_care_facilities.facility_name,
			addresses.zip_code,
			addresses.address_line_1,
			addresses.address_line_2,
			addresses.city_name,
			states.postal_code
			from health_care_facilities
			join addresses on health_care_facilities.id = addresses.addressable_id
			join states on addresses.state_id = states.id
			join contact_informations on health_care_facilities.id = contact_informations.contactable_id

			Where health_care_facilities.facility_name like '%" . $query . "%'
			AND addresses.addressable_type like '%HealthCareFacility%'
			AND contact_informations.contactable_type like '%HealthCareFacility%'
			");
		return $result;
	}

}
