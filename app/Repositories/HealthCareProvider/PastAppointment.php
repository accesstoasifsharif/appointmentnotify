<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Repository\Contracts\HealthCareProvider\PastAppointment as PastAppointmentContract;

class PastAppointment implements PastAppointmentContract {
	public function getSpecificDoctorSpecificHCPFacilityAppointments($doctor_id, $hcp_facility_id, $hcp_id) {
		$appointments = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id


			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "

			And personal_informations.personalable_type like '%Doctor%'
			group by appointment_histories.doctor_id

			");

		return $appointments;
	}

	public function getSpecificDoctorAllHCPFacilityAppointments($doctor_id, $hcp_id) {
		$appointments = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And personal_informations.personalable_type like '%Doctor%'
			group by appointment_histories.doctor_id

			");

		return $appointments;
	}

	public function getAllDoctorSpecificHCPFacilityAppointments($hcp_facility_id, $hcp_id) {
		$appointments = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id

			where health_care_providers.id = " . $hcp_id . "

			And personal_informations.personalable_type like '%Doctor%'
			group by appointment_histories.doctor_id

			");

		return $appointments;
	}

	public function getAllDoctorAllHCPFacilityAppointments($hcp_id) {

		$appointments = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And personal_informations.personalable_type like '%Doctor%'
			group by appointment_histories.doctor_id

			");

		return $appointments;

	}

	public function getSpecificDoctorSpecificHCPFacilityAppointmentsDetail($doctor_id, $hcp_facility_id, $hcp_id) {

		if ($hcp_facility_id == 0) {
			$appointments = DB::select("
				select
				appointment_histories.id as an_event_id,
				appointment_histories.date as date,
				appointment_histories.start_time as start_time,
				health_care_facilities.facility_name as facility_name,
				appointment_reasons.reason_name

				from appointment_histories
				join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
				join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
				join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
				left join appointment_reasons on appointment_reasons.id =  appointment_histories.appointment_reason_id
				join doctors on doctors.id = appointment_histories.doctor_id
				where health_care_providers.id = " . $hcp_id . "
				And doctors.id = " . $doctor_id . "
				order by appointment_histories.date
				");

			return $appointments;
		} else {
			$appointments = DB::select("
				select
				appointment_histories.id as appointment_history_id,
				appointment_histories.date as date,
				appointment_histories.start_time as start_time,
				health_care_facilities.facility_name as facility_name,
				appointment_reasons.reason_name

				from appointment_histories
				join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
				join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
				join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
				join doctors on doctors.id = appointment_histories.doctor_id
				left join appointment_reasons on appointment_reasons.id =  appointment_histories.appointment_reason_id
				where health_care_providers.id = " . $hcp_id . "
				And doctors.id = " . $doctor_id . "
				And health_care_provider_facilities.id = " . $hcp_facility_id . "
				order by appointment_histories.date
				");

			return $appointments;
		}

	}

}
