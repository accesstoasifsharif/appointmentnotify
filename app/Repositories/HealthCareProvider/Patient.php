<?php

namespace Repository\HealthCareProvider;
use Facades\Service\Common\DataTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Repository\Contracts\HealthCareProvider\Patient as PatientContract;

class Patient implements PatientContract {
	public function getAllPatientsSpecificPerspective($hcp_id, $request) {
		$default_per_page = 5;

		$per_page = Input::get('per_page');
		$page = Input::get('page');
		$per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;

		$result = DB::select("
			select
			 *, doctors.model_identifier as doctor_img
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
			join doctors on health_care_provider_doctors.doctor_id = doctors.id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			AND personal_informations.personalable_type like '%Doctor%'
			group by health_care_provider_doctors.doctor_id
			");
		$data = DataTable::getPaginate($result, $per_page, $page);

		return $data;

	}
	public function allPatientsByDoctorId($doctorId, $hcp_id, $request) {

		// return DB::select("select * from site_users
		// 	join doctor_site_user on doctor_site_user.site_user_id
		// 	join doctors on doctors.id = doctor_site_user.doctor_id
		// 	where doctors.id = " . $doctorId . "
		// 	// And  health_care_providers.id = " . $hcp_id . "
		// 	");
		$default_per_page = 5;

		$per_page = Input::get('per_page');
		$page = Input::get('page');
		$per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;

		$result = DB::select("select Distinct
			site_users.id as target_id,
			site_users.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,users.email as email,personal_informations.gender as gender,personal_informations.date_of_birth as date_of_birth,contact_informations.phone_number as phone_number

			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
			join doctors on doctors.id = health_care_provider_doctors.doctor_id
			join doctor_site_user on doctor_site_user.doctor_id = doctors.id
			join site_users on site_users.id = doctor_site_user.site_user_id
			join personal_informations on personal_informations.personalable_id = site_users.id
			join contact_informations on contact_informations.contactable_id = site_users.id
			join users on users.id = site_users.user_id
			where health_care_providers.id = " . $hcp_id . "
			AND doctors.id = " . $doctorId . "
			AND personal_informations.personalable_type like '%SiteUser%'
			AND contact_informations.contactable_type like '%SiteUser%'
			");
		$data = DataTable::getPaginate($result, $per_page, $page);

		return $data;
	}
}

// doctors.id as target_id,
// 			doctors.model_identifier as model_identifier,
// 			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
// 			health_care_provider_doctors.id as hcp_doctor_id