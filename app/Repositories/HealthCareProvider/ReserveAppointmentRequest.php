<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Model\AppointmentReservation;
use Repository\Contracts\HealthCareProvider\ReserveAppointmentRequest as ReserveAppointmentRequestContract;

class ReserveAppointmentRequest implements ReserveAppointmentRequestContract {

	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequests($doctor_id, $hcp_facility_id, $hcp_id) {
		$requests = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_reservations
			join an_events on an_events.id = appointment_reservations.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $requests;
	}

	public function getSpecificDoctorAllHCPFacilityAppointmentRequests($doctor_id, $hcp_id) {
		$requests = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_reservations
			join an_events on an_events.id = appointment_reservations.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $requests;
	}

	public function getAllDoctorSpecificHCPFacilityAppointmentRequests($hcp_facility_id, $hcp_id) {
		$requests = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_reservations
			join an_events on an_events.id = appointment_reservations.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $requests;
	}

	public function getAllDoctorAllHCPFacilityAppointmentRequests($hcp_id) {
		$requests = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_reservations
			join an_events on an_events.id = appointment_reservations.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $requests;
	}

	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequestsDetail($doctor_id, $hcp_facility_id, $hcp_id) {
		if ($hcp_facility_id == 0) {
			$appointments = DB::select("
				select
				appointment_reservations.id as appointment_reservation_id,
				an_events.date as date,
				an_events.start_time as start_time,
				health_care_facilities.facility_name as facility_name,
				appointment_reasons.reason_name

				from appointment_reservations
				join an_events on an_events.id = appointment_reservations.an_event_id
				join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
				join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
				join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
				left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
				join doctors on doctors.id = an_events.doctor_id
				where health_care_providers.id = " . $hcp_id . "
				And doctors.id = " . $doctor_id . "
				And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
				order by an_events.date
				");

			return $appointments;
		} else {
			$appointments = DB::select("
				select
				appointment_reservations.id as appointment_reservation_id,
				an_events.date as date,
				an_events.start_time as start_time,
				health_care_facilities.facility_name as facility_name,
				appointment_reasons.reason_name

				from appointment_reservations
				join an_events on an_events.id = appointment_reservations.an_event_id
				join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
				join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
				join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
				join doctors on doctors.id = an_events.doctor_id
				left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
				where health_care_providers.id = " . $hcp_id . "
				And doctors.id = " . $doctor_id . "
				And health_care_provider_facilities.id = " . $hcp_facility_id . "
				And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
				order by an_events.date
				");

			return $appointments;
		}
	}

	public function getReserveAppointmentByReserveAppointmentId($reserve_appointment_id) {
		return AppointmentReservation::find($reserve_appointment_id);
	}

	public function removeReserveAppointment(AppointmentReservation $reserve_appointment) {
		$reserve_appointment->delete();
		return 'true';
	}

}
