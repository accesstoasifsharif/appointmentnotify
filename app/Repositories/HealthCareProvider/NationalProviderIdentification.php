<?php

namespace Repository\HealthCareProvider;

use Repository\Contracts\HealthCareProvider\NationalProviderIdentification as NationalProviderIdentificationContract;

class NationalProviderIdentification implements NationalProviderIdentificationContract {

	public function addOrUpdateNPI($relation, $npi) {
		if (isset($relation->npi)) {
			$relation->NPI()->delete();
		}
		$relation->NPI()->save($npi);
		return 1;
	}

	public function removeNPI($relation) {
		$relation->NPI()->delete();
		return 1;
	}
	public function getNPIDetails($relation) {
		return $relation->NPI;

	}
}
