<?php

namespace Repository\HealthCareProvider;
use Model\Doctor as DoctorModel;
use Model\HealthCareProvider;
use Repository\Contracts\HealthCareProvider\SinglePractice as SinglePracticeContract;

class SinglePractice implements SinglePracticeContract {
	public function getSinglePracticeByHCP(HealthCareProvider $hcp) {
		if (!isset($hcp->SinglePractice)) {
			return null;
		}
		return $hcp->SinglePractice;
	}
	public function createSinglePractice(HealthCareProvider $hcp, DoctorModel $dr) {
		return \Model\SinglePractice::create(
			[
				'health_care_provider_id' => $hcp->id,
				'doctor_id'               => $dr->id,
			]);
	}

}
