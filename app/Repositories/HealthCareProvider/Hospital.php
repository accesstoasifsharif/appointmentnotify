<?php

namespace Repository\HealthCareProvider;
use Model\HealthCareProvider;
use Repository\Contracts\HealthCareProvider\Hospital as HospitalContract;

class Hospital implements HospitalContract {
	public function getHospitalByHCP(HealthCareProvider $hcp) {
		if (!isset($hcp->Hospital)) {
			return null;
		}
		return $hcp->Hospital;

	}
	public function createHospital(HealthCareProvider $hcp) {

		return \Model\Hospital::create(
			[
				'health_care_provider_id' => $hcp->id,
			]);
	}

}
