<?php

namespace Repository\HealthCareProvider;
use Model\HealthCareProvider;
use Repository\Contracts\HealthCareProvider\MedicalClinic as MedicalClinicContract;

class MedicalClinic implements MedicalClinicContract {
	public function getMedicalClinicByHCP(HealthCareProvider $hcp) {
		if (!isset($hcp->MedicalClinic)) {
			return null;
		}
		return $hcp->MedicalClinic;

	}
	public function createMedicalClinic(HealthCareProvider $hcp) {
		return \Model\MedicalClinic::create(
			[
				'health_care_provider_id' => $hcp->id,
			]);

	}

}
