<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Model\HealthCareProviderDoctor;
use Repository\Contracts\HealthCareProvider\HCPDoctor as HCPDoctorContract;

class HCPDoctor implements HCPDoctorContract {

	public function getHCPDoctorByDoctorAndProviderId($doctor_id, $hcp_id) {
		return DB::select("
			select health_care_provider_doctors.id as hcp_doctor_id
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
			join doctors on health_care_provider_doctors.doctor_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			AND doctors.id = " . $doctor_id . "
			");
	}

	public function getHCPDoctorByHCPDoctorId($HCPDoctor_id) {
		return HealthCareProviderDoctor::find($HCPDoctor_id);
	}

	public function getHCPDoctorByDoctorIdAndHCPFacilityId($doctor_id, $hcp_facility_id) {

		return HealthCareProviderDoctor::where('hcp_facility_id', $hcp_facility_id)->where('doctor_id', $doctor_id)->first();

	}

	public function addHCPDoctor(HealthCareProviderDoctor $hcp_doctor) {
		$hcp_doctor->save();
		return 'true';
	}
	public function addOrUpdateHCPDoctor($hcp_doctor_array_data) {
		return HealthCareProviderDoctor::create($hcp_doctor_array_data);
	}

	public function deleteHCPDoctor(HealthCareProviderDoctor $hcp_doctor) {
		$hcp_doctor->delete();
		return 'true';
	}

	public function getAllHCPDoctorsByHCPId($hcp_id) {
		$result = DB::select("
			select
			DISTINCT doctors.id as target_id,
			doctors.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_doctors.id as hcp_doctor_id
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
			join doctors on health_care_provider_doctors.doctor_id = doctors.id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			AND personal_informations.personalable_type like '%Doctor%'
			group by health_care_provider_doctors.doctor_id
			");
		return $result;
	}

	public function getAllHCPDoctorsOfSpecificHCPFacilityOfHCP($hcp_id, $hcp_facility_id) {
		$result = DB::select("
			select
			DISTINCT doctors.id as target_id,
			doctors.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name

			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
			join doctors on health_care_provider_doctors.doctor_id = doctors.id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			AND personal_informations.personalable_type like '%Doctor%'

			");
		return $result;
	}

	public function getAllHCPDoctorsOfHCPBySearchQuery($hcp_id, $query) {
		$result = DB::select("
			select DISTINCT doctors.id as target_id,
			doctors.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
			join doctors on health_care_provider_doctors.doctor_id = doctors.id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			AND (personal_informations.first_name like '%" . $query . "%'
			OR personal_informations.last_name like '%" . $query . "%')
			AND personal_informations.personalable_type like '%Doctor%'
			");

		return $result;
	}

	public function getAllHCPDoctorsOfSpecificHCPFacilityOfHCPBySearchQuery($hcp_id, $query, $hcp_facility_id) {
		$result = DB::select("
			select
			DISTINCT doctors.id as target_id,
			doctors.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
			join doctors on health_care_provider_doctors.doctor_id = doctors.id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			AND (personal_informations.first_name like '%" . $query . "%'
			OR personal_informations.last_name like '%" . $query . "%')
			AND personal_informations.personalable_type like '%Doctor%'
			");
		return $result;
	}

}
