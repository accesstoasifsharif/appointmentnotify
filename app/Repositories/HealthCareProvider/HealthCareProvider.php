<?php

namespace Repository\HealthCareProvider;
use App\User;
use Repository\Contracts\HealthCareProvider\HealthCareProvider as HealthCareProviderContract;

class HealthCareProvider implements HealthCareProviderContract {

	public function getHealthCareProviderByUser(User $user) {
		//	if (isset($user->healthCareProvider)) {
		$user_new = User::with('healthCareProvider')->find($user->id);
//		HealthCareProvider::find();
		return $user_new->healthCareProvider;
		//	}
		//	return null;
	}
	public function getUserByHealthCareProvider($hcp) {

		return $hcp->user;

	}
	public function createHealthCareProvider(User $user, $providerType) {
		$hcp = \Model\HealthCareProvider::create(
			[
				'user_id'       => $user->id,
				'provider_type' => $providerType,
			]);

		$hcp->user()->associate($user);
		$hcp->save();
		$user->save();
		return $hcp;

	}

	public function addHCPPerspectiveLevel($user_id, $hcp_id, $level_id) {
		return \App\PerspectiveLevel::create(
			[
				'user_id'                 => $user_id,
				'health_care_provider_id' => $hcp_id,
				'role_level_id'           => $level_id,
			]);
	}
}
