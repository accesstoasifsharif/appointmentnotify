<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Model\AppointmentReschedule;
use Repository\Contracts\HealthCareProvider\RescheduleAppointmentRequest as RescheduleAppointmentRequestContract;

class RescheduleAppointmentRequest implements RescheduleAppointmentRequestContract {

	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequests($doctor_id, $hcp_facility_id, $hcp_id) {
		$requests = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $requests;
	}

	public function getSpecificDoctorAllHCPFacilityAppointmentRequests($doctor_id, $hcp_id) {
		$requests = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $requests;
	}

	public function getAllDoctorSpecificHCPFacilityAppointmentRequests($hcp_facility_id, $hcp_id) {
		$requests = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $requests;
	}

	public function getAllDoctorAllHCPFacilityAppointmentRequests($hcp_id) {
		$requests = DB::select("
			select
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name,

			count(*) as total_appointments
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id

			");

		return $requests;
	}

	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequestsDetail($doctor_id, $hcp_facility_id, $hcp_id) {
		if ($hcp_facility_id == 0) {
			$appointments = DB::select("
				select
				appointment_reschedules.id as appointment_reschedule_id,
				appointment_reschedules.prefer_time_1 as time_1,
				appointment_reschedules.prefer_time_2 as time_2,
				appointment_reschedules.prefer_date_1 as date_1,
				appointment_reschedules.prefer_date_2 as date_2,
				health_care_facilities.facility_name as facility_name,
				appointment_reasons.reason_name

				from appointment_reschedules
				join an_events on an_events.id = appointment_reschedules.an_event_id
				join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
				join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
				join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
				left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
				join doctors on doctors.id = an_events.doctor_id
				where health_care_providers.id = " . $hcp_id . "
				And doctors.id = " . $doctor_id . "
				And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
				order by an_events.date
				");

			return $appointments;
		} else {
			$appointments = DB::select("
				select
				appointment_reschedules.id as appointment_reschedule_id,
				appointment_reschedules.prefer_time_1 as time_1,
				appointment_reschedules.prefer_time_2 as time_2,
				appointment_reschedules.prefer_date_1 as date_1,
				appointment_reschedules.prefer_date_2 as date_2,
				health_care_facilities.facility_name as facility_name,
				appointment_reasons.reason_name
				from appointment_reschedules
				join an_events on an_events.id = appointment_reschedules.an_event_id
				join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
				join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
				join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
				join doctors on doctors.id = an_events.doctor_id
				left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
				where health_care_providers.id = " . $hcp_id . "
				And doctors.id = " . $doctor_id . "
				And health_care_provider_facilities.id = " . $hcp_facility_id . "
				And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
				order by an_events.date
				");

			return $appointments;
		}
	}

	public function getRescheduleAppointmentByRescheduleAppointmentId($reschedule_appointment_id) {
		return AppointmentReschedule::find($reschedule_appointment_id);
	}

	public function removeRescheduleAppointment(AppointmentReschedule $reschedule_appointment) {
		$reschedule_appointment->delete();
		return 'true';
	}

}
