<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Model\Doctor as DoctorModel;
use Repository\Contracts\HealthCareProvider\Doctor as DoctorContract;

class Doctor implements DoctorContract {
	public function getDoctorByEmail($email) {
		return DoctorModel::where($email);
	}
	public function getDoctorByDoctorId($doctor_id) {
		return DoctorModel::find($doctor_id);
	}

	public function getDoctorInfoByDoctorId($doctor_id) {
		return DoctorModel::with('personalInformation')->with('specialty')->with('contact')->find($doctor_id);
	}

	public function addOrUpdateDoctorInfo(DoctorModel $doctor) {
		$doctor->save();
		return $doctor;
	}
	public function addOrUpdateDoctor($doctor, $dataArray) {
		if (is_null($doctor)) {
			return DoctorModel::create($dataArray);
		}
		$doctor->update(['specialty_id' => $dataArray['specialty_id']]);
		return $doctor;
	}

	public function removeDoctor(DoctorModel $doctor) {
		$doctor->delete();
		return $doctor;
	}

	public function getDoctorBySearchQuery($query) {
		$result = DB::select("
			select
			doctors.id as target_id,
			doctors.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name

			from doctors
			join personal_informations on personal_informations.personalable_id = doctors.id
			where (personal_informations.first_name like '%" . $query . "%'
			OR personal_informations.last_name like '%" . $query . "%')
			AND personal_informations.personalable_type like '%Doctor%'
			");
		return $result;
	}

}
