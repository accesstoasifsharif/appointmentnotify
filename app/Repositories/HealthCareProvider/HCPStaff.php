<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Model\HealthCareProviderStaff;
use Repository\Contracts\HealthCareProvider\HCPStaff as HCPStaffContract;

class HCPStaff implements HCPStaffContract {

	public function getHCPStaffByStaffAndProviderId($staff_id, $hcp_id) {
		return DB::select("
			select health_care_provider_staff.id as hcp_staff_id
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_staff on health_care_provider_staff.hcp_facility_id = health_care_provider_facilities.id
			join staff on health_care_provider_staff.doctor_id = staff.id
			where health_care_providers.id = " . $hcp_id . "
			AND staff.id = " . $staff_id . "
			");
	}

	public function getHCPStaffByHCPStaffId($HCPStaff_id) {
		return HealthCareProviderStaff::find($HCPStaff_id);
	}

	public function getHCPStaffByStaffIdAndHCPFacilityId($staff_id, $hcp_facility_id) {

		return HealthCareProviderStaff::where('hcp_facility_id', $hcp_facility_id)->where('staff_id', $staff_id)->first();

	}

	public function addHCPStaff(HealthCareProviderStaff $hcp_staff) {
		$hcp_staff->save();
		return 'true';
	}

	public function deleteHCPStaff(HealthCareProviderStaff $hcp_staff) {
		$hcp_staff->delete();
		return 'true';
	}

	public function getAllHCPStaffsByHCPId($hcp_id) {
		$result = DB::select("
			select
			DISTINCT staff.id as target_id,
			staff.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_staff on health_care_provider_staff.hcp_facility_id = health_care_provider_facilities.id
			join staff on health_care_provider_staff.staff_id = staff.id
			join personal_informations on personal_informations.personalable_id = staff.id
			where health_care_providers.id = " . $hcp_id . "
			AND personal_informations.personalable_type like '%Staff%'
			");
		return $result;
	}

	public function getAllHCPStaffsOfSpecificHCPFacilityOfHCP($hcp_id, $hcp_facility_id) {
		$result = DB::select("
			select
			DISTINCT staff.id as target_id,
			staff.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_staff on health_care_provider_staff.hcp_facility_id = health_care_provider_facilities.id
			join staff on health_care_provider_staff.staff_id = staff.id
			join personal_informations on personal_informations.personalable_id = staff.id
			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			AND personal_informations.personalable_type like '%Staff%'
			");
		return $result;
	}

	public function getAllHCPStaffsOfHCPBySearchQuery($hcp_id, $query) {

		$result = DB::select("
			select
			DISTINCT staff.id as target_id,
			staff.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_staff on health_care_provider_staff.hcp_facility_id = health_care_provider_facilities.id
			join staff on health_care_provider_staff.staff_id = staff.id
			join personal_informations on personal_informations.personalable_id = staff.id
			where health_care_providers.id = " . $hcp_id . "

			AND (personal_informations.first_name like '%" . $query . "%'
			OR personal_informations.last_name like '%" . $query . "%')
			AND personal_informations.personalable_type like '%Staff%'
			");
		return $result;

	}

	public function getAllHCPStaffsOfSpecificHCPFacilityOfHCPBySearchQuery($hcp_id, $query, $hcp_facility_id) {
		$result = DB::select("
			select
			DISTINCT staff.id as target_id,
			staff.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from health_care_providers
			join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
			join health_care_provider_staff on health_care_provider_staff.hcp_facility_id = health_care_provider_facilities.id
			join staff on health_care_provider_staff.staff_id = staff.id
			join personal_informations on personal_informations.personalable_id = staff.id
			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			AND (personal_informations.first_name like '%" . $query . "%'
			OR personal_informations.last_name like '%" . $query . "%')
			AND personal_informations.personalable_type like '%Staff%'
			");
		return $result;
	}

}
