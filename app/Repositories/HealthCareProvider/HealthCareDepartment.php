<?php

namespace Repository\HealthCareProvider;

use Repository\Contracts\HealthCareProvider\HealthCareDepartment as HCDepartmentContract;

class HealthCareDepartment implements HCDepartmentContract {

	public function getAllHCDepartments($hcp_id) {
		return $hcp_id;
	}

	public function getAllHCDepartmentsBySearchQuery($query, $hcp_id) {
		return $hcp_id;
	}

}
