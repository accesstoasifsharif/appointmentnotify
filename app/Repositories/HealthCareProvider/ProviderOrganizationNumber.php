<?php

namespace Repository\HealthCareProvider;

use Repository\Contracts\HealthCareProvider\ProviderOrganizationNumber as ProviderOrganizationNumberContract;

class ProviderOrganizationNumber implements ProviderOrganizationNumberContract {

	public function addOrUpdatePON($relation, $pon) {

		if (isset($relation->PONumber)) {
			$relation->PONumber()->delete();
		}
		$relation->PONumber()->save($pon);
		return 1;

	}
	public function removePON($relation) {

		$relation->PONumber()->delete();
		return 1;
	}
	public function getPONDetails($relation) {

		return $relation->PONumber;
	}

}
