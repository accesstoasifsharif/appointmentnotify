<?php

namespace Repository\HealthCareProvider;

use Repository\Contracts\HealthCareProvider\EmployerIdentificationNumber as EmployerIdentificationNumberContract;

class EmployerIdentificationNumber implements EmployerIdentificationNumberContract {

	public function addOrUpdateEIN($relation, $ein) {
		if (isset($relation->EIN)) {
			$relation->EIN()->delete();
		}
		$relation->EIN()->save($ein);
		return 1;
	}
	public function removeEIN($relation) {
		$relation->EIN()->delete();
		return 1;
	}
	public function getEINDetails($relation) {
		return $relation->EIN;
	}

}
