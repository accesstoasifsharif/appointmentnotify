<?php

namespace Repository\HealthCareProvider;

use Model\ShortCallAppointment as ShortCallAppointmentModel;
use Repository\Contracts\HealthCareProvider\ShortCallAppointment as ShortCallAppointmentContract;

class ShortCallAppointment implements ShortCallAppointmentContract {

	public function getShortCallAppointmentByShortCallAppointmentId($short_call_appointment_id) {
		return ShortCallAppointmentModel::find(short_call_appointment_id);
	}

	public function addOrUpdateShortCallAppointment(ShortCallAppointmentModel $short_call_appointment) {
		$short_call_appointment->save();
		return 'true';
	}

	public function removeShortCallAppointment(ShortCallAppointmentModel $short_call_appointment) {
		$short_call_appointment->delete();
		return 'true';
	}

}
