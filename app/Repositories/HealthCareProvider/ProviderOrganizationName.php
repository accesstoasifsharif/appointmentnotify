<?php
namespace Repository\HealthCareProvider;

use Repository\Contracts\HealthCareProvider\ProviderOrganizationName as ProviderOrganizationNameContract;

class ProviderOrganizationName implements ProviderOrganizationNameContract {

	public function addOrUpdatePON($relation, $pon) {

		if (isset($relation->providerOrganizationName)) {
			$relation->providerOrganizationName()->delete();
		}
		$relation->providerOrganizationName()->save($pon);
		return 1;

	}
	public function removePON($relation) {

		$relation->providerOrganizationName()->delete();
		return 1;
	}
	public function getPONDetails($relation) {

		return $relation->providerOrganizationName;
	}

}
