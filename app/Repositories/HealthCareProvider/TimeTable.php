<?php

namespace Repository\HealthCareProvider;

use Model\TimeTable as TimeTableModel;
use Repository\Contracts\HealthCareProvider\TimeTable as TimeTableContract;

class TimeTable implements TimeTableContract {

	// param  $relation + TimeTable $timeTable
	// return TimeTable new created or updated Object
	public function addOrUpdateTimeTableInfo($relation, $time_Table) {
		if (isset($relation->timeTable)) {
			$relation->timeTable()->delete();
		}
		$relation->timeTable()->saveMany($time_Table);
		return 1;
	}

	// param $relation
	// return Delete Success Message
	public function removeTimeTable($relation) {
		$relation->timeTable()->delete();
		return 1;
	}

// return TimeTable of Relation
	public function getCompleteTimeTableOfRelation($relation) {

		$days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
		$array_day = $days;
		$array_time = $relation->timeTable;
		$array_day_time = array();

		for ($i = 0; $i < count($days); $i++) {
			for ($j = 0; $j < $array_time->count(); $j++) {
				if ($array_time[$j]->day == $array_day[$i]) {
					$array_day_time[$i] = $array_time[$j];
					break;
				} else {
					$array_day_time[$i] = ['day' => $array_day[$i]];
				}
			}
		}

		return $array_day_time;
	}

	public function getCompleteTimeTableFormate() {

		$days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
		$array_day = $days;

		$array_day_time = array();
		for ($i = 0; $i < count($array_day); $i++) {
			$array_day_time[$i] = ['day' => $array_day[$i]];
		}
		return $array_day_time;
	}

	public function timeTableMakerForDB($timeTabelData) {
		$time_array = $timeTabelData['data'];
		$day_selected = $timeTabelData['selected'];
		$count = count($time_array);

		$time_list = array();

		for ($i = 0; $i < $count; $i++) {
			if ($day_selected[$i]) {
				if (isset($time_array[$i]['time_from']) && isset($time_array[$i]['time_to'])) {
					$timeTable = new TimeTableModel();
					$timeTable->time_from = $time_array[$i]['time_from'];
					$timeTable->time_to = $time_array[$i]['time_to'];
					$timeTable->day = $time_array[$i]['day'];
					array_push($time_list, $timeTable);
				}
			}
		}
		return $time_list;
	}

}