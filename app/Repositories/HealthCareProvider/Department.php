<?php

namespace Repository\HealthCareProvider;
use Model\HealthCareProvider;
use Repository\Contracts\HealthCareProvider\Department as DepartmentContract;

class Department implements DepartmentContract {
	public function getDepartmentByHCP(HealthCareProvider $hcp) {

		if (!isset($hcp->Department)) {
			return null;
		}
		return $hcp->Department;
	}
	public function createDepartment(HealthCareProvider $hcp) {

		return \Model\Department::create(
			[
				'health_care_provider_id' => $hcp->id,
			]);
	}

}
