<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Model\HealthCareFacility as HCFacility;
use Repository\Contracts\HealthCareProvider\HealthCareFacility as HealthCareFacilityContract;

class HealthCareFacility implements HealthCareFacilityContract {

	public function getHealthCareFacilityById($facility_id) {
		return HCFacility::find($facility_id);
	}

	public function addOrUpdateHealthCareFacility(HCFacility $facility) {

		$facility->save();
		return $facility;
	}

	public function deleteHealthCareFacility(HCFacility $facility) {
		$facility->delete();
		return 1;
	}
	public function allHealthCareFacilities() {
		return HCFacility::all();
	}

	public function getFacilityDetailByFacilityId($facility_id) {
		$result = DB::select("select
			DISTINCT
			national_provider_identifications.NPI as facility_npi,
			health_care_facilities.id as facility_id,
			health_care_facilities.facility_name,
			addresses.address_line_1,addresses.address_line_2,
			addresses.city_name,addresses.zip_code,
			addresses.state_id,addresses.country_id,
			contact_informations.phone_number,
			contact_informations.fax_number,
			contact_informations.email,
			contact_informations.url,
			health_care_facilities.honor_id,
			health_care_facilities.creator_id
			from health_care_facilities
			join addresses on health_care_facilities.id = addresses.addressable_id
			join contact_informations on health_care_facilities.id = contact_informations.contactable_id
			join national_provider_identifications on health_care_facilities.id = national_provider_identifications.npinumberable_id
			where  health_care_facilities.id = " . $facility_id . "
			AND addresses.addressable_type like '%HealthCareFacility%'
			AND contact_informations.contactable_type like '%HealthCareFacility%'
			AND national_provider_identifications.npinumberable_type like '%HealthCareFacility%'
			");
		return $result;
	}

	// public function getExistingFacilityBySearchQuery($query) {

	// }

}
