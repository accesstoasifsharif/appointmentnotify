<?php

namespace Repository\HealthCareProvider;

use DB;
use Repository\Contracts\HealthCareProvider\HCPPatient as HCPPatientContract;

class HCPPatient implements HCPPatientContract {

	public function getAllHCPDoctorPatients($hcp_id, $hcp_doctor_id) {
		if ($hcp_doctor_id != '0') {
			return DB::select("select
				site_users.id as target_id,
				site_users.model_identifier as model_identifier,
				doctors.id as doctor_id,
				doctors.model_identifier as doctor_identifier,
				CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
				from health_care_provider_doctors
				join doctors on doctors.id = health_care_provider_doctors.doctor_id
				join doctor_site_user on doctor_site_user.doctor_id = doctors.id
				join site_users on site_users.id = doctor_site_user.site_user_id
				join personal_informations on personal_informations.personalable_id = site_users.id
				where health_care_provider_doctors.id = " . $hcp_doctor_id . "
				AND personal_informations.personalable_type like '%SiteUser%'
				");
		} else {
			return DB::select("select Distinct
				site_users.id as target_id,
				site_users.model_identifier as model_identifier,
				CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
				from health_care_providers
				join health_care_provider_facilities on health_care_provider_facilities.health_care_provider_id = health_care_providers.id
				join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
				join doctors on doctors.id = health_care_provider_doctors.doctor_id
				join doctor_site_user on doctor_site_user.doctor_id = doctors.id
				join site_users on site_users.id = doctor_site_user.site_user_id
				join personal_informations on personal_informations.personalable_id = site_users.id
				where health_care_providers.id = " . $hcp_id . "
				AND personal_informations.personalable_type like '%SiteUser%'
				");
		}

	}

	public function getPatientDetail($patient_id) {
		$result = DB::select("select
			*,
			site_users.id as target_id,
			site_users.model_identifier as model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from site_users
			join personal_informations on personal_informations.personalable_id = site_users.id
			left join contact_informations on contact_informations.contactable_id = site_users.id
			where site_users.id = " . $patient_id . "
			AND personal_informations.personalable_type like '%SiteUser%'
			AND contact_informations.contactable_type like '%SiteUser%'
			");

		return $result;
	}

}
