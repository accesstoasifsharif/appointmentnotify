<?php

namespace Repository\HealthCareProvider;

use Illuminate\Support\Facades\DB;
use Repository\Contracts\HealthCareProvider\CalendarAppointment as CalendarAppointmentContract;

class CalendarAppointment implements CalendarAppointmentContract {

	public function getSpecificDoctorSpecificHCPFacilityUpcomingAppointmentsMonthView($doctor_id, $hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.date as date,
			an_events.start_time as start_time,
			an_events.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			count(*) as total_appointments
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join personal_informations on personal_informations.personalable_id = doctors.id


			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id , an_events.date

			");
		return $this->upcomingAppointmentColor($data, 'month');
	}

	public function getSpecificDoctorAllHCPFacilityUpcomingAppointmentsMonthView($doctor_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.date as date,
			an_events.start_time as start_time,
			an_events.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,

			count(*) as total_appointments
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id



			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id , an_events.date

			");

		return $this->upcomingAppointmentColor($data, 'month');
	}

	public function getAllDoctorSpecificHCPFacilityUpcomingAppointmentsMonthView($hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.date as date,
			an_events.start_time as start_time,
			an_events.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,

			count(*) as total_appointments
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id

			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id , an_events.date
			");
		return $this->upcomingAppointmentColor($data, 'month');
	}

	public function getAllDoctorAllHCPFacilityUpcomingAppointmentsMonthView($hcp_id) {

		$data = DB::select("
			select
			an_events.date as date,
			an_events.start_time as start_time,
			an_events.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,

			count(*) as total_appointments
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id , an_events.date
			");

		return $this->upcomingAppointmentColor($data, 'month');
	}

	public function getSpecificDoctorSpecificHCPFacilityPastAppointmentsMonthView($doctor_id, $hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,


			count(*) as total_appointments
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id


			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "

			And personal_informations.personalable_type like '%Doctor%'
			group by appointment_histories.doctor_id ,appointment_histories.date
			");

		return $this->pastAppointmentColor($data, 'month');
	}

	public function getSpecificDoctorAllHCPFacilityPastAppointmentsMonthView($doctor_id, $hcp_id) {
		$data = DB::select("
			select
			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,


			count(*) as total_appointments
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And personal_informations.personalable_type like '%Doctor%'
			group by appointment_histories.doctor_id ,appointment_histories.date
			");

		return $this->pastAppointmentColor($data, 'month');
	}

	public function getAllDoctorSpecificHCPFacilityPastAppointmentsMonthView($hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,


			count(*) as total_appointments
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id

			where health_care_providers.id = " . $hcp_id . "

			And personal_informations.personalable_type like '%Doctor%'
			group by appointment_histories.doctor_id ,appointment_histories.date
			");

		return $this->pastAppointmentColor($data, 'month');
	}

	public function getAllDoctorAllHCPFacilityPastAppointmentsMonthView($hcp_id) {
		$data = DB::select("
			select

			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,


			count(*) as total_appointments
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And personal_informations.personalable_type like '%Doctor%'
			group by appointment_histories.doctor_id ,appointment_histories.date
			");

		return $this->pastAppointmentColor($data, 'month');
	}

	public function getSpecificDoctorSpecificHCPFacilityRescheduleAppointmentsMonthView($doctor_id, $hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.date as date,
			an_events.start_time as start_time,
			an_events.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,


			count(*) as total_appointments
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id , an_events.date
			");

		return $this->rescheduleAppointmentColor($data, 'month');
	}

	public function getSpecificDoctorAllHCPFacilityRescheduleAppointmentsMonthView($doctor_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.date as date,
			an_events.start_time as start_time,
			an_events.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,


			count(*) as total_appointments
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id , an_events.date
			");

		return $this->rescheduleAppointmentColor($data, 'month');
	}

	public function getAllDoctorSpecificHCPFacilityRescheduleAppointmentsMonthView($hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.date as date,
			an_events.start_time as start_time,
			an_events.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,


			count(*) as total_appointments
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id , an_events.date
			");

		return $this->rescheduleAppointmentColor($data, 'month');
	}

	public function getAllDoctorAllHCPFacilityRescheduleAppointmentsMonthView($hcp_id) {
		$data = DB::select("
			select

			an_events.date as date,
			an_events.start_time as start_time,
			an_events.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,


			count(*) as total_appointments
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			group by an_events.doctor_id , an_events.date
			");

		return $this->rescheduleAppointmentColor($data, 'month');
	}

	protected function upcomingAppointmentColor($data_array, $view) {
		for ($i = 0; $i < count($data_array); $i++) {
			$data_array[$i]->color = config('appointmentnotify.an_event_type.upcoming.color');
			$data_array[$i]->an_event_type = config('appointmentnotify.an_event_type.upcoming.event_type');
			$data_array[$i]->view_type = $view;
		}
		return $data_array;
	}

	protected function pastAppointmentColor($data_array, $view) {
		for ($i = 0; $i < count($data_array); $i++) {
			$data_array[$i]->color = config('appointmentnotify.an_event_type.past.color');
			$data_array[$i]->an_event_type = config('appointmentnotify.an_event_type.past.event_type');
			$data_array[$i]->view_type = $view;
		}
		return $data_array;
	}
	protected function rescheduleAppointmentColor($data_array, $view) {
		for ($i = 0; $i < count($data_array); $i++) {
			$data_array[$i]->color = config('appointmentnotify.an_event_type.reschedule.color');
			$data_array[$i]->an_event_type = config('appointmentnotify.an_event_type.reschedule.event_type');
			$data_array[$i]->view_type = $view;
		}
		return $data_array;
	}

	public function getSpecificDoctorSpecificHCPFacilityUpcomingAppointmentsDayView($doctor_id, $hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id


			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'
			");
		return $this->upcomingAppointmentColor($data, 'day');
	}

	public function getSpecificDoctorAllHCPFacilityUpcomingAppointmentsDayView($doctor_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id



			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'
			");

		return $this->upcomingAppointmentColor($data, 'day');
	}

	public function getAllDoctorSpecificHCPFacilityUpcomingAppointmentsDayView($hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id

			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'

			");
		return $this->upcomingAppointmentColor($data, 'day');
	}

	public function getAllDoctorAllHCPFacilityUpcomingAppointmentsDayView($hcp_id) {
		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'
			");
		return $this->upcomingAppointmentColor($data, 'day');
	}

	public function getSpecificDoctorSpecificHCPFacilityPastAppointmentsDayView($doctor_id, $hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			appointment_histories.id as appointment_history_id,
			appointment_histories.an_event_id as an_event_id,
			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id


			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "

			And personal_informations.personalable_type like '%Doctor%'

			");

		return $this->pastAppointmentColor($data, 'day');
	}

	public function getSpecificDoctorAllHCPFacilityPastAppointmentsDayView($doctor_id, $hcp_id) {
		$data = DB::select("
			select
			appointment_histories.id as appointment_history_id,
			appointment_histories.an_event_id as an_event_id,
			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And personal_informations.personalable_type like '%Doctor%'
			");

		return $this->pastAppointmentColor($data, 'day');
	}

	public function getAllDoctorSpecificHCPFacilityPastAppointmentsDayView($hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			appointment_histories.id as appointment_history_id,
			appointment_histories.an_event_id as an_event_id,
			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And personal_informations.personalable_type like '%Doctor%'

			");

		return $this->pastAppointmentColor($data, 'day');
	}

	public function getAllDoctorAllHCPFacilityPastAppointmentsDayView($hcp_id) {
		$data = DB::select("
			select
			appointment_histories.id as appointment_history_id,
			appointment_histories.an_event_id as an_event_id,
			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And personal_informations.personalable_type like '%Doctor%'

			");

		return $this->pastAppointmentColor($data, 'day');
	}

	public function getSpecificDoctorSpecificHCPFacilityRescheduleAppointmentsDayView($doctor_id, $hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'

			");

		return $this->rescheduleAppointmentColor($data, 'day');
	}

	public function getSpecificDoctorAllHCPFacilityRescheduleAppointmentsDayView($doctor_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And doctors.id = " . $doctor_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			");

		return $this->rescheduleAppointmentColor($data, 'day');
	}

	public function getAllDoctorSpecificHCPFacilityRescheduleAppointmentsDayView($hcp_facility_id, $hcp_id) {
		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And health_care_provider_facilities.id = " . $hcp_facility_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'

			");

		return $this->rescheduleAppointmentColor($data, 'day');
	}

	public function getAllDoctorAllHCPFacilityRescheduleAppointmentsDayView($hcp_id) {
		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id

			join personal_informations on personal_informations.personalable_id = doctors.id
			where health_care_providers.id = " . $hcp_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'

			");

		return $this->rescheduleAppointmentColor($data, 'day');
	}

	public function getDoctorHCPFacilityDateUpcomingAppointmentsDetailPreview($doctor_id, $date) {
		$data = DB::select("
			select


			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			health_care_facilities.facility_name as facility_name,
			appointment_reasons.reason_name,
			specialties.specialty_name as specialty_name


			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join doctors on doctors.id = an_events.doctor_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			join specialties on specialties.id = doctors.specialty_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
			left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
			Where an_events.doctor_id = " . $doctor_id . "
			And an_events.date = '" . $date . "'
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'
			order by an_events.start_time
			");
		return $data;
	}

	public function getDoctorHCPFacilityDatePastAppointmentsDetailPreview($doctor_id, $date) {
		$data = DB::select("
			select
			appointment_histories.id as appointment_history_id,
			appointment_histories.an_event_id as an_event_id,
			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			health_care_facilities.facility_name as facility_name,
			appointment_reasons.reason_name,
			specialties.specialty_name as specialty_name

			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = appointment_histories.doctor_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			join specialties on specialties.id = doctors.specialty_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
			left join appointment_reasons on appointment_reasons.id =  appointment_histories.appointment_reason_id
			Where appointment_histories.doctor_id = " . $doctor_id . "
			And appointment_histories.date = '" . $date . "'
			And personal_informations.personalable_type like '%Doctor%'
			order by appointment_histories.start_time

			");
		return $data;
	}

	public function getDoctorHCPFacilityDateRescheduleAppointmentsDetailPreview($doctor_id, $date) {

		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			appointment_reschedules.id as appointment_reschedule_id,
			appointment_reschedules.prefer_time_1 as time_1,
			appointment_reschedules.prefer_time_2 as time_2,
			appointment_reschedules.prefer_date_1 as date_1,
			appointment_reschedules.prefer_date_2 as date_2,
			health_care_facilities.facility_name as facility_name,
			appointment_reasons.reason_name,
			specialties.specialty_name as specialty_name

			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
			left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			Where an_events.doctor_id = " . $doctor_id . "
			And an_events.date = '" . $date . "'
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			order by an_events.start_time
			");
		return $data;
	}

	public function getDoctorHCPFacilityUpcomingAppointmentDetailPreview($an_event_id) {

		$data = DB::select("
			select
			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name as specialty_name,
			health_care_facilities.facility_name as facility_name,
			appointment_reasons.reason_name
			from an_events
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			Where an_events.id = " . $an_event_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Approved') . "
			And personal_informations.personalable_type like '%Doctor%'
			");
		return $data;
	}

	public function getDoctorHCPFacilityPastAppointmentDetailPreview($an_event_id) {

		$data = DB::select("
			select
			appointment_histories.id as appointment_history_id,
			appointment_histories.an_event_id as an_event_id,
			appointment_histories.date as date,
			appointment_histories.start_time as start_time,
			appointment_histories.end_time as end_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name as specialty_name
			from appointment_histories
			join health_care_provider_facilities on health_care_provider_facilities.id = appointment_histories.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
			join doctors on doctors.id = appointment_histories.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			Where appointment_histories.an_event_id = " . $an_event_id . "
			And personal_informations.personalable_type like '%Doctor%'
			");
		return $data;
	}

	public function getDoctorHCPFacilityRescheduleAppointmentDetailPreview($an_event_id) {

		$data = DB::select("
			select
			appointment_reschedules.id as appointment_reschedule_id,
			appointment_reschedules.prefer_time_1 as time_1,
			appointment_reschedules.prefer_time_2 as time_2,
			appointment_reschedules.prefer_date_1 as date_1,
			appointment_reschedules.prefer_date_2 as date_2,
			health_care_facilities.facility_name as facility_name,
			appointment_reasons.reason_name,

			an_events.id as an_event_id,
			an_events.end_time as end_time,
			an_events.date as date,
			an_events.start_time as start_time,
			doctors.id as doctor_id,
			doctors.model_identifier as doctor_model_identifier,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name,
			health_care_provider_facilities.id as hcp_facility_id,
			specialties.specialty_name as specialty_name
			from appointment_reschedules
			join an_events on an_events.id = appointment_reschedules.an_event_id
			join health_care_provider_facilities on health_care_provider_facilities.id = an_events.health_care_provider_facility_id
			join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
			join doctors on doctors.id = an_events.doctor_id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			left join appointment_reasons on appointment_reasons.id =  an_events.appointment_reason_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
			Where an_events.id = " . $an_event_id . "
			And an_events.appointment_status = " . config('appointmentnotify.appointment_status.Private') . "
			And an_events.appointment_current_state = " . config('appointmentnotify.appointment_current_state.Pending') . "
			And personal_informations.personalable_type like '%Doctor%'
			");
		return $data;
	}

}
