<?php

namespace Repository\Common;

use Model\PatientShortCall as PatientShortCallModel;
use Repository\Contracts\Common\PatientShortCall as PatientShortCallContract;

class PatientShortCall implements PatientShortCallContract {

	public function addOrUpdatePatientShortCall(PatientShortCallModel $short_call) {
		$short_call->save();
		return 'true';
	}

	public function checkExistance($doctor_id, $hcp_facility_id, $site_user_id) {
		$result = PatientShortCallModel::where('doctor_id', $doctor_id)->where('health_care_provider_facility_id', $hcp_facility_id)->where('site_user_id', $site_user_id)->get();
		if (count($result) > 0) {
			return true;
		} else {
			return false;
		}

	}
}
