<?php

namespace Repository\Common;
use Repository\Contracts\Common\PersonalInfo as PersonalformationContract;

class PersonalInfo implements PersonalformationContract {

	public function addOrUpdatePersonalInfo($relation, $personalInfo) {

		if (isset($relation->personalInformation)) {
			$relation->personalInformation()->delete();
		}
		$relation->personalInformation()->save($personalInfo);
		return 1;
	}
	public function removePersonalInfo($relation) {
		$relation->personalInformation()->delete();
		return 1;

	}

	public function getPersonalInfoDetails($relation) {
		return $relation->personalInformation;

	}

}
