<?php
namespace Repository\Common;

use Carbon\Carbon;
use Model\Invite;
use Repository\Contracts\Common\SignUpInvite as SignUpInviteContract;

class SignUpInvite implements SignUpInviteContract {

	public function createInvite($email) {
		$invite = Invite::where('email', $email)->first();
		if (!is_null($invite)) {
			Invite::where('email', $email)->delete();
		}
		$token = generateToken();

		Invite::create(['email' => $email, 'token' => getHashedToken($token)]);
		return $token;
	}

	public function getInvite($credentials) {

		$invite = Invite::where('email', $credentials['email'])->get()->first();

		return $invite;

	}
	public function tokenExists($invite, $token) {

		//$invite = Invite::where('email', $email)->first();

		return $invite &&
		!$this->tokenExpired($invite->created_at) &&
		verifyTokenHash($token, $invite->token);
	}

	public function deleteTokens($invite) {
		return $invite->delete();

	}
	/*
	return bool if invite exists or not
	 */
	public function inviteExist($email) {

		$invite = Invite::where('email', $email)->first();
		if (!is_null($invite)) {
			return true;
		}
		return false;
	}
	/*
	return bool if invite is expired or not
	 */
	public function inviteExpired($email) {

		return true;
	}
	public function createNewToken() {
	}

	public function tokenExpired($createdAt) {
		$expires = 1;
		return Carbon::parse($createdAt)->addWeeks($expires)->isPast();
	}

}
