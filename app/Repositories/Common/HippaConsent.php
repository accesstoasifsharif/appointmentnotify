<?php

namespace Repository\Common;
use Illuminate\Support\Facades\DB;
use Model\UserAgreement;
use Repository\Contracts\Common\HippaConsent as HippaConsentContract;

class HippaConsent implements HippaConsentContract {

	public function getAgreement($siteUserId) {

		$userId = DB::select("select user_id from user_agreement
			where user_id = " . $siteUserId . " ");
		// return $userId[0]->user_id;
		// return response()->json($userId);
		$u_id = $userId[0]->user_id;
		if (isset($u_id)) {
			return "true";
		} else {
			return "false";
		}
	}

	public function upDateHippaConsent($data_array, $siteUserId) {
		$check = DB::select("select * from user_agreement");
		// return $check;
		if ($check) {
			return 0;
		} else {
			if ($data_array->input('value')) {

				$licenseType = $data_array->input('hippaAgreement');
				$agreement_id = DB::select("select id from agreements
					where license_type LIKE '%$licenseType%' ");
				$agreement_id = $agreement_id[0]->id;

				$agreement = new UserAgreement();
				$agreement->agreement_id = $agreement_id;

				$agreement->user_id = $siteUserId;

				$agreement->save();
				return 1;
			} else {
				UserAgreement::where('user_id', $siteUserId)->delete();
				return "ok";
			}
		}

	}
	public function getLicenseAgreement($data_array) {
		$licenseType = $data_array->input('licenseType');
		$userType = $data_array->input('userType');

		return DB::select("select * from agreements

			where  user_type = " . $userType . "
			AND license_type LIKE '%$licenseType%' ");
	}
}