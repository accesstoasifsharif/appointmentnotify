<?php

namespace Repository\Common;

use Model\AnEvent as AnEventModel;
use Repository\Contracts\Common\AnEvent as AnEventContract;

class AnEvent implements AnEventContract {

	public function getAnEventByAnEventId($an_event_id) {
		return AnEventModel::find($an_event_id);
	}

	public function addOrUpdateAnEvent(AnEventModel $an_event) {
		$an_event->save();
		return 'true';
	}
	public function removeAnEvent(AnEventModel $an_event) {
		$an_event->delete();
		return 'true';
	}
}
