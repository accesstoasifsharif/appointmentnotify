<?php

namespace Repository\Common;

use Repository\Contracts\Common\ContactPerson as ContactPersonContract;

class ContactPerson implements ContactPersonContract {
	public function addOrUpdateContactPersonInfo($relation, $contactPerson) {
		if (isset($relation->contactPerson)) {
			$relation->contactPerson()->delete();
		}

		$relation->contactPerson()->save($contactPerson);
		return 1;
	}
	public function removeContactPerson($relation) {
		return $relation->contactPerson()->delete();
	}
	public function getContactPersonDetails($relation) {

		return $relation->contactPerson;
	}

}
