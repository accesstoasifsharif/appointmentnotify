<?php
namespace Repository\Common;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Repository\Contracts\Common\ResetPassword as ResetPasswordContract;

class ResetPassword implements ResetPasswordContract {
	public function resetPassword($data_array, $siteUserId) {

		$hashedPassword = User::find($siteUserId)->password;

		if (Hash::check($data_array->current_password, $hashedPassword)) {

			DB::table('users')
				->where('id', $siteUserId)
				->update(['password' => bcrypt($data_array->new_password)]);
			return "password updated";

		} else {

			return "not matched";

		}
	}
}
