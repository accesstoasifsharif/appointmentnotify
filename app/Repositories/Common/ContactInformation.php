<?php

namespace Repository\Common;

use Model\ContactInformation as ContactInformationModel;
use Model\Doctor as DcotorModel;
use Repository\Contracts\Common\ContactInformation as ContactInformationContract;

class ContactInformation implements ContactInformationContract {
	public function getDoctorContactInfoByEmails($emails_array) {

		foreach ($emails_array as $value) {
			$contactInfo = ContactInformationModel::whereEmail($value)->whereContactableType(DcotorModel::class)->first();
			if (!is_null($contactInfo)) {
				return $contactInfo;
			}

		}
		return null;
	}
	public function addOrUpdateContactInfo($relation, $contact) {

		if (isset($relation->contact)) {

			$relation->contact()->delete();

		}
		$relation->contact()->save($contact);

		return 1;

	}
	public function removeContact($relation) {
		$relation->contact()->delete();
		return 1;
	}
	public function getContactDetails($relation) {
		return $relation->contact;

	}

}
