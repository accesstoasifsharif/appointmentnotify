<?php
namespace Repository\Common;
use Model\Conversation as ConversationModel;
use Repository\Contracts\Common\Conversation as ConversationContract;

class Conversation implements ConversationContract {

	public function newConversation(ConversationModel $conversation) {
		$conversation->save();
		return $conversation;
	}

	public function getConversationMessages($conversation) {
		return ConversationModel::with('messages')->find($conversation);
	}

	public function getConversationMessagesPreview($conversation) {
		$message_preview = ConversationModel::with(["messages" => function ($query) {
			$query->orderBy('created_at', 'desc')->first();
		}])->find($conversation);
		if (isset($message_preview)) {
			return $message_preview;
		} else {
			return '0';
		}
	}

	public function getConversationAmongTwoUsers($user1, $user2) {

		$conversation = ConversationModel::where('user_one', $user1)
			->where('user_two', $user2);
		return $conversation;
	}

}
