<?php

namespace Repository\Common;

use Repository\Contracts\Common\Address as AddressContract;

class Address implements AddressContract {

	public function addOrUpdateAddressInfo($relation, $address) {
		if (isset($relation->address)) {
			$relation->address()->delete();
		}
		$address->country_id = 1;
		$relation->address()->save($address);
		return 1;
	}

	public function removeAddress($relation) {
		$relation->address()->delete();
		return 1;
	}
	public function getAddressDetails($relation) {
		return $relation->address;
	}

}
