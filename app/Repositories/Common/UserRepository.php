<?php

namespace Repository\Common;

use Repository\Contracts\Common\UserRepository as UserRepositoryContract;

class UserRepository implements UserRepositoryContract {
	public function updateUserProfileStatus($user, $status) {

		$user->profile_status = $status;
		$user->save();
		return 1;

	}

}
