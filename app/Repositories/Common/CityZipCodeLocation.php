<?php

namespace Repository\Common;
use Illuminate\Support\Facades\DB;
use Model\City;
use Repository\Contracts\Common\CityZipCodeLocation as CityZipCodeLocationContract;

class CityZipCodeLocation implements CityZipCodeLocationContract {
	public function getNearestZipCodeList($zip_code, $distance) {
		// return $zip_code . $distance;

		$row = City::whereZip($zip_code)->first();
		$lat1 = $row['latitude'];
		$lon1 = $row['longitude'];
		$d = $distance;
		$r = 3959;
		$latN = rad2deg(asin(sin(deg2rad($lat1)) * cos($d / $r) + cos(deg2rad($lat1)) * sin($d / $r) * cos(deg2rad(0))));
		$latS = rad2deg(asin(sin(deg2rad($lat1)) * cos($d / $r) + cos(deg2rad($lat1)) * sin($d / $r) * cos(deg2rad(180))));
		$lonE = rad2deg(deg2rad($lon1) + atan2(sin(deg2rad(90)) * sin($d / $r) * cos(deg2rad($lat1)), cos($d / $r) - sin(deg2rad($lat1)) * sin(deg2rad($latN))));
		$lonW = rad2deg(deg2rad($lon1) + atan2(sin(deg2rad(270)) * sin($d / $r) * cos(deg2rad($lat1)), cos($d / $r) - sin(deg2rad($lat1)) * sin(deg2rad($latN))));

		$zip_list = DB::select(" select zip from cities
			where ( latitude <= " . $latN . " AND latitude >= " . $latS . " AND longitude <= " . $lonE . " AND longitude >= " . $lonW . ")
			And (latitude != " . $lat1 . " AND longitude != " . $lon1 . ")
			And city != ''
			ORDER BY zip desc
			");
		$base_zip = DB::select(" select zip from cities
			where zip = " . $zip_code . "
			");
		$array_test = array();
		// $array_zip = array_merge($base_zip,$zip_list);
		// return $array_zip;
		array_push($array_test, $base_zip);
		array_push($array_test, $zip_list);
		// return $array_test;

		return $this->makeZipCodeFinalArray($array_test);
		return $zip_list;
	}
	protected function makeZipCodeFinalArray($array_test) {
		$array_final = array();
		for ($i = 0; $i < count($array_test); $i++) {
			for ($j = 0; $j < count($array_test[$i]); $j++) {
				array_push($array_final, $array_test[$i][$j]);
			}
		}
		return $array_final;
	}

}
