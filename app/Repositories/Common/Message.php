<?php

namespace Repository\Common;

use Model\Message as MessageModel;
use Repository\Contracts\Common\Message as MessageContract;

class Message implements MessageContract {
	public function newMessage(MessageModel $message) {
		$message->save();
		return $message;
	}
}
