<?php

namespace Repository\SiteUser;
use Illuminate\Support\Facades\DB;
use Repository\Contracts\SiteUser\SiteUserCircledProvider as SiteUserCircledProviderContract;

class SiteUserCircledProvider implements SiteUserCircledProviderContract {

	public function getAllSiteUserCircledProvider($site_user_id) {
		return DB::select("select
			site_users.model_identifier as site_user_identifier,
			doctors.model_identifier as doctor_identifier,
			site_users.id as site_user_id,
			doctors.id as doctor_id,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from site_users
			join doctor_site_user on doctor_site_user.site_user_id = site_users.id
			join doctors on doctors.id = doctor_site_user.doctor_id
			join personal_informations  on personal_informations.personalable_id = doctors.id
			where personal_informations.personalable_type like '%Doctor%'
			AND site_users.id = $site_user_id");
	}

	public function getAllVerifiedSiteUserCircledProvider($site_user_id) {

		return DB::select("select
			site_users.model_identifier as site_user_identifier,
			doctors.model_identifier as doctor_identifier,
			site_users.id as site_user_id,
			doctors.id as doctor_id,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as full_name
			from site_users
			join doctor_site_user on doctor_site_user.site_user_id = site_users.id
			join doctors on doctors.id = doctor_site_user.doctor_id
			join personal_informations  on personal_informations.personalable_id = doctors.id
			where personal_informations.personalable_type like '%Doctor'
			AND doctors.verify_status = '1'
			AND site_users.id = $site_user_id");

	}

}
