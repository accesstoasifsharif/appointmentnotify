<?php

namespace Repository\SiteUser;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Repository\Contracts\SiteUser\UpcomingAppointment as UpcomingAppointmentContract;

class UpcomingAppointment implements UpcomingAppointmentContract {

	public function allDistinctUpcomingAppointmentsBySiteUserId($siteUser_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Approved');

		$allDistinctUpcomingAppointmentsBySiteUserId = DB::select("
			SELECT *, appointments.id as appointment_id ,doctors.model_identifier as doctor_img FROM site_users
			JOIN an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			left JOIN appointments on appointments.an_event_id = an_events.id
			JOIN health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			WHERE site_users.id = " . $siteUser_id . "
			AND personal_informations.personalable_type  like  '%Doctor'
			and an_events.appointment_status =  " . $app_status . "
			and an_events.appointment_current_state =  " . $app_current_state . "
			");

		return $allDistinctUpcomingAppointmentsBySiteUserId;
	}
	public function getAllUpComingAppointemntsBySiteUserId($siteUser_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Approved');

		// $default_per_page = 5;

		// $per_page = Input::get('per_page');
		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;

		$all_appointments = DB::select("
			SELECT * ,count(*) as total,doctors.model_identifier as doctor_img ,an_events.id as upcoming_appointment_id FROM site_users
			JOIN an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			-- left JOIN appointments on appointments.an_event_id = an_events.id
			join health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			WHERE personal_informations.personalable_type  like  '%Doctor'
			AND site_users.id = " . $siteUser_id . "
			AND appointment_status = " . $app_status . "
			AND appointment_current_state = " . $app_current_state . "
			group by an_events.doctor_id");
		// $data = DataTable::getPaginate($all_appointments, $per_page, $page);

		return $all_appointments;
	}

	public function getUpcomingAppointmentDetailByDoctorId($id, $siteUser_id) {
		// $page = Input::get('page');
		// $per_page = 1;

		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Approved');
		$all_detail_appointments = DB::select("
			SELECT * , an_events.id as appointment_id,doctors.model_identifier as doctor_img FROM site_users
			JOIN an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			left JOIN appointments on appointments.an_event_id = an_events.id
			JOIN health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			JOIN personal_informations on personal_informations.personalable_id = doctors.id
			JOIN addresses on addresses.addressable_id = health_care_facilities.id

			Where site_users.id = " . $siteUser_id . "
			AND doctors.id = " . $id . "
			AND appointment_status = " . $app_status . "
			AND appointment_current_state = " . $app_current_state . "
			And personal_informations.personalable_type  like  '%Doctor%'
			AND addresses.addressable_type  like  '%HealthCareFacility%'
			");
		// $data = DataTable::getPaginate($all_detail_appointments, $per_page, $page);
		$doctorDetail = DB::select("select *, doctors.model_identifier as doctor_img,doctors.id as doctor_id from doctors
			join personal_informations on personal_informations.personalable_id =  doctors.id
			join contact_informations on contact_informations.contactable_id = doctors.id
			join specialties on doctors.specialty_id = specialties.id
			where doctors.id = $id
			and personal_informations.personalable_type like '%Doctor'
			and contact_informations.contactable_type like '%Doctor'");

		return $doctor_and_appointmetn_detail = ['doctorDetail' => $doctorDetail, 'data' => $all_detail_appointments];

	}

}