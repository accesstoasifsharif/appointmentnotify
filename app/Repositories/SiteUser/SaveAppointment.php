<?php

namespace Repository\SiteUser;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Model\AnEvent;
use Model\Appointment;
use Model\AppointmentReservation;
use Repository\Contracts\SiteUser\SaveAppointment as SaveAppointmentContract;

class SaveAppointment implements SaveAppointmentContract {
	public function saveAppointment($data_array, $siteUser_id) {

		$doctor_id = $data_array->input('doctor');
		$facility_id = $data_array->input('facility');
		$reason_id = $data_array->input('reason');
		$start_time = $data_array->input('startTime');
		$end_time = $data_array->input('endTime');
		$app_date = $data_array->input('date');
		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Pending');

		$converted_date = Carbon::parse($app_date)->format('Y-m-d');

		$hcp_facility = DB::select("select hcp_facility_id from
			doctors
			join health_care_provider_doctors on health_care_provider_doctors.doctor_id= doctors.id
			join health_care_provider_facilities on health_care_provider_facilities.id = health_care_provider_doctors.hcp_facility_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.id
			where doctors.id = " . $doctor_id . " AND health_care_facilities.id= " . $facility_id . "
			");

		$h_facility = $hcp_facility[0]->hcp_facility_id;

		$new_event = new AnEvent();
		$new_event->calendar_type = '0';
		$new_event->date = $converted_date;
		$new_event->start_time = $start_time;
		$new_event->end_time = $end_time;
		$new_event->site_user_id = $siteUser_id;
		$new_event->doctor_id = $doctor_id;
		$new_event->health_care_facility_id = $facility_id;
		$new_event->health_care_provider_facility_id = $h_facility;
		$new_event->appointment_reason_id = $reason_id;
		$new_event->appointment_status = $app_status;
		$new_event->appointment_current_state = $app_current_state;
		$new_event->save();
		$last_event_entry = DB::getPdo()->lastInsertId();
		$new_appointment = new Appointment();
		$new_appointment->an_event_id = $last_event_entry;
		$new_appointment->save();
		$new_reserve_appointment = new AppointmentReservation();
		$new_reserve_appointment->site_user_id = $siteUser_id;
		$new_reserve_appointment->an_event_id = $last_event_entry;
		$new_reserve_appointment->save();

		return $new_event;
		return "Entry saved in appointment table , AppointmentReservation and an_event table!";
	}

}