<?php

namespace Repository\SiteUser;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Model\AnEvent;
use Model\AppointmentCancelation;
use Repository\Contracts\SiteUser\CancelAppointment as CancelAppointmentContract;

class CancelAppointment implements CancelAppointmentContract {
	public function cancelUpComingAppointemnt($appointment_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Suspend');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Suspend');

		// $event_id = ::find($appointment_id)->an_event_id;

		$cancel_app = AnEvent::find($appointment_id);

		$cancel_app->update([
			'appointment_status' => $app_status,
			'appointment_current_state' => $app_current_state,
		]);
		$cancel_appointment = new AppointmentCancelation();
		$cancel_appointment->an_event_id = $appointment_id;
		$cancel_appointment->doctor_id = $cancel_app->doctor_id;
		$cancel_appointment->expiry_date_time = Config::get(Config_File_Name . '.APP_APPOINTMENT_CANCELATION_REVOKE_TIME.RevokeTimeLimit');
		$cancel_appointment->save();
		return "appointment statuses changed";

	}
	public function getAllCancelledAppointmentBySiteUserId($siteUser_id) {

		// $default_per_page = 5;

		// $per_page = Input::get('per_page');
		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;

		$app_status = Config::get(Config_File_Name . '.appointment_status.Suspend');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Suspend');
		$all_appointments = DB::select("
			SELECT * ,count(*) as total, doctors.model_identifier as doctor_img FROM site_users
			join an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id

			JOIN   appointment_cancelations on   appointment_cancelations.an_event_id = an_events.id
			join health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			WHERE personal_informations.personalable_type  like  '%Doctor'
			AND appointment_status = " . $app_status . "
			AND appointment_current_state = " . $app_current_state . "
			group by  appointment_cancelations.doctor_id

			");
		// $data = DataTable::getPaginate($all_appointments, $per_page, $page);

		return $all_appointments;

	}
	public function getAllCancelledAppointmentsDetailByDoctorId($doctorId, $siteUser_id) {

		// $default_per_page = 5;

		// $per_page = Input::get('per_page');
		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;

		$app_status = Config::get(Config_File_Name . '.appointment_status.Suspend');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Suspend');
		$all_appointments = DB::select("
			SELECT * , appointment_cancelations.id as appointment_id  , doctors.model_identifier as doctor_img FROM site_users
			join an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id

			JOIN   appointment_cancelations on   appointment_cancelations.an_event_id = an_events.id
			join health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			JOIN addresses on addresses.addressable_id = health_care_facilities.id
			WHERE personal_informations.personalable_type  like  '%Doctor'
			AND appointment_status = " . $app_status . "
			AND appointment_current_state = " . $app_current_state . "
			AND doctors.id = " . $doctorId . "
			AND site_users.id = " . $siteUser_id . "
			AND addresses.addressable_type  like  '%HealthCareFacility%'

			");
		$doctorDetail = DB::select("select *, doctors.id as doctor_id , doctors.model_identifier as doctor_img from doctors
			join personal_informations on personal_informations.personalable_id =  doctors.id
			join contact_informations on contact_informations.contactable_id = doctors.id
			join specialties on doctors.specialty_id = specialties.id
			where doctors.id = $doctorId
			and personal_informations.personalable_type like '%Doctor'
			and contact_informations.contactable_type like '%Doctor'");
		// $data = DataTable::getPaginate($all_appointments, $per_page, $page);
		return $doctor_and_appointmetn_detail = ['doctorDetail' => $doctorDetail, 'data' => $all_appointments];

	}
	public function revokeCancelledAppointment($appointment_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Approved');
		$event_id = AppointmentCancelation::find($appointment_id)->an_event_id;
		$cancel_appointment = AppointmentCancelation::find($appointment_id);
		$cancel_app = AnEvent::find($event_id);

		$cancel_app->update([
			'appointment_status' => $app_status,
			'appointment_current_state' => $app_current_state,
		]);
		$cancel_appointment->delete();

		return "Status Changed";
	}
}