<?php

namespace Repository\SiteUser;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Repository\Contracts\SiteUser\ReservedAppointments as ReservedAppointmentsContract;

class ReservedAppointments implements ReservedAppointmentsContract {
	public function allDistinctReservedAppointmentsBySiteUserId($siteUser_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Pending');

		$allDistinctReservedAppointmentsBySiteUserId = DB::select("
			SELECT *, appointment_reservations.id as appointment_id ,doctors.model_identifier as doctor_img FROM site_users
			JOIN an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id

			join personal_informations on personal_informations.personalable_id = doctors.id

			JOIN appointment_reservations on appointment_reservations.an_event_id = an_events.id
			JOIN health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			WHERE site_users.id = " . $siteUser_id . "
			AND personal_informations.personalable_type  like  '%Doctor'
			and an_events.appointment_status =  " . $app_status . "
			and an_events.appointment_current_state =  " . $app_current_state . "
			");

		return $allDistinctReservedAppointmentsBySiteUserId;
	}

	public function getAggregatedReservedAppointmentsBySiteUserId($siteUser_id) {
		/*return $siteUser_id;*/
		// $default_per_page = 5;

		// $per_page = Input::get('per_page');
		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;

		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Pending');
		$aggregated_appointments = DB::select("
			SELECT * ,count(*) as total , doctors.model_identifier as doctor_img FROM site_users
			join an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id

			JOIN  appointment_reservations on  appointment_reservations.an_event_id = an_events.id
			join health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			WHERE personal_informations.personalable_type  like  '%Doctor%'
			AND appointment_status = " . $app_status . "
			AND appointment_current_state = " . $app_current_state . "
			AND site_users.id = " . $siteUser_id . "
			group by doctors.id

			");

		// $data = DataTable::getPaginate($aggregated_appointments, $per_page, $page);

		return $aggregated_appointments;
	}
	public function getAllReservedAppointmentsDetailByDoctorId($doctorId, $siteUser_id) {

		// $default_per_page = 5;

		// $per_page = Input::get('per_page');

		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;
		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Pending');
		$all_detail_appointments = DB::select("
			SELECT * , appointment_reservations.id as appointment_id  , doctors.model_identifier as doctor_img FROM site_users
			JOIN an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id

			JOIN appointment_reservations on appointment_reservations.an_event_id = an_events.id
			JOIN health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			JOIN addresses on addresses.addressable_id = health_care_facilities.id

			Where site_users.id = " . $siteUser_id . "
			AND doctors.id = " . $doctorId . "
			AND appointment_status = " . $app_status . "
			AND appointment_current_state = " . $app_current_state . "
			And personal_informations.personalable_type  like  '%Doctor%'
			AND addresses.addressable_type  like  '%HealthCareFacility%'
			");
		$doctorDetail = DB::select("select * , doctors.model_identifier as doctor_img , doctors.id as doctor_id from doctors
			join personal_informations on personal_informations.personalable_id =  doctors.id
			join contact_informations on contact_informations.contactable_id = doctors.id
			join specialties on doctors.specialty_id = specialties.id
			where doctors.id = $doctorId
			and personal_informations.personalable_type like '%Doctor'
			and contact_informations.contactable_type like '%Doctor'");
		// $data = DataTable::getPaginate($all_detail_appointments, $per_page, $page);
		return $doctor_and_appointmetn_detail = ['doctorDetail' => $doctorDetail, 'data' => $all_detail_appointments];

	}
}