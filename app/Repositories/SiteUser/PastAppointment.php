<?php

namespace Repository\SiteUser;
use Illuminate\Support\Facades\DB;
use Repository\Contracts\SiteUser\PastAppointment as PastAppointmentContract;

class PastAppointment implements PastAppointmentContract {

	public function allDistinctPastAppointmentsBySiteUserId($siteUser_id) {
		return DB::select("
			Select * , appointment_histories.id as appointment_id , doctors.model_identifier as doctor_img from site_users
			JOIN appointment_histories on appointment_histories.site_user_id = site_users.id
			JOIN doctors ON doctors.id = appointment_histories.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id

			JOIN health_care_facilities on health_care_facilities.id =appointment_histories.health_care_facility_id
			WHERE site_users.id = " . $siteUser_id . "
			AND personal_informations.personalable_type  like  '%Doctor'
			");
	}
	public function getAllPastAppointemntsBySiteUserId($siteUser_id) {
		// $default_per_page = 5;

		// $per_page = Input::get('per_page');
		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;
		$past_appointments = DB::select("SELECT * ,count(*) as total , doctors.model_identifier as doctor_img FROM site_users
			JOIN an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			JOIN appointment_histories on appointment_histories.an_event_id = an_events.id
			join health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			WHERE personal_informations.personalable_type  like  '%Doctor'
			AND site_users.id = " . $siteUser_id . "
			group by an_events.doctor_id
			");
		// $data = DataTable::getPaginate($past_appointments, $per_page, $page);

		return $past_appointments;

	}
	// JOIN avatars on avatarable_id = doctors.id
	public function getPastAppointmentDetailByDoctorId($doctor_id, $siteUser_id) {
		// $default_per_page = 5;

		// $per_page = Input::get('per_page');

		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;
		$all_detail_appointments = DB::select("
			SELECT *  , doctors.model_identifier as doctor_img, appointment_histories.id as appointment_id FROM site_users
			join appointment_histories on appointment_histories.site_user_id = site_users.id
			JOIN doctors ON doctors.id = appointment_histories.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			JOIN an_events on an_events.id = appointment_histories.an_event_id

			JOIN health_care_facilities on health_care_facilities.id =appointment_histories.health_care_facility_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			JOIN addresses on addresses.addressable_id = health_care_facilities.id

			Where site_users.id = " . $siteUser_id . "
			AND doctors.id = " . $doctor_id . "
			And personal_informations.personalable_type  like  '%Doctor%'
			AND addresses.addressable_type  like  '%HealthCareFacility%'
			");
		$doctorDetail = DB::select("select *, doctors.id as doctor_id , doctors.model_identifier as doctor_img from doctors
			join personal_informations on personal_informations.personalable_id =  doctors.id
			join contact_informations on contact_informations.contactable_id = doctors.id
			join specialties on doctors.specialty_id = specialties.id
			where doctors.id = $doctor_id
			and personal_informations.personalable_type like '%Doctor'
			and contact_informations.contactable_type like '%Doctor'");
		// $data = DataTable::getPaginate($all_detail_appointments, $per_page, $page);
		return $doctor_and_appointmetn_detail = ['doctorDetail' => $doctorDetail, 'data' => $all_detail_appointments];

	}

}
