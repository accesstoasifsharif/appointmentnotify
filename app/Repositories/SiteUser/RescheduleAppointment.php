<?php

namespace Repository\SiteUser;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Model\AnEvent;
use Model\AppointmentReschedule;
use Repository\Contracts\SiteUser\RescheduleAppointment as RescheduleAppointmentContract;

class RescheduleAppointment implements RescheduleAppointmentContract {

	public function saveRescheduleTime($data_array) {

		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Pending');

		$reschedule_app = AnEvent::find($data_array->an_event_id);
		return $data_array->an_event_id;
		$reschedule_app->update([
			'appointment_status' => $app_status,
			'appointment_current_state' => $app_current_state,
		]);

		$date_1 = Carbon::parse($data_array->date_1)->format('Y-m-d');
		if (isset($data_array->date_2)) {
			$date_2 = Carbon::parse($data_array->date_2)->format('Y-m-d');
		} else {
			$date_2 = $data_array->date_2;

		}
		$time_1 = $data_array->time_1;
		$time_2 = $data_array->time_2;

		// return;

		$new_reschedule_appointment = new AppointmentReschedule();
		$new_reschedule_appointment->an_event_id = $data_array->an_event_id;
		$new_reschedule_appointment->doctor_id = $data_array->doctor_id;
		$new_reschedule_appointment->prefer_time_1 = $time_1;
		$new_reschedule_appointment->prefer_time_2 = $time_2;
		$new_reschedule_appointment->prefer_date_1 = $date_1;
		$new_reschedule_appointment->prefer_date_2 = $date_2;
		$new_reschedule_appointment->reason = "";
		$new_reschedule_appointment->save();

		return $reschedule_app;

	}

	public function getAggregatedRescheduledAppointmentBySiteUserId(
		$siteUser_id) {
		// $default_per_page = 5;

		// $per_page = Input::get('per_page');
		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;
		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Pending');
		$all_appointments = DB::select("
			SELECT * ,count(*) as total , doctors.model_identifier as doctor_img FROM site_users
			join an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id

			JOIN  appointment_reschedules on  appointment_reschedules.an_event_id = an_events.id
			join health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			WHERE personal_informations.personalable_type  like  '%Doctor%'
			AND appointment_status = " . $app_status . "
			AND appointment_current_state = " . $app_current_state . "
			AND site_users.id = " . $siteUser_id . "
			group by appointment_reschedules.doctor_id

			");

		// $data = DataTable::getPaginate($all_appointments, $per_page, $page);

		return $all_appointments;

	}
	public function getRescheduledAppointmentDetailByDoctorId($doctor_id, $siteUser_id) {

		// $default_per_page = 5;

		// $per_page = Input::get('per_page');
		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;

		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Pending');
		$all_detail_appointments = DB::select("
			SELECT * , appointment_reschedules.id as appointment_id  , doctors.model_identifier as doctor_img FROM site_users
			JOIN an_events on an_events.site_user_id = site_users.id
			JOIN doctors ON doctors.id = an_events.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id

			JOIN appointment_reschedules on appointment_reschedules.an_event_id = an_events.id
			JOIN health_care_facilities on health_care_facilities.id =an_events.health_care_facility_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			JOIN addresses on addresses.addressable_id = health_care_facilities.id

			Where site_users.id = " . $siteUser_id . "
			AND doctors.id = " . $doctor_id . "
			AND appointment_status = " . $app_status . "
			AND appointment_current_state = " . $app_current_state . "
			And personal_informations.personalable_type  like  '%Doctor%'
			AND addresses.addressable_type  like  '%HealthCareFacility%'
			");
		$doctorDetail = DB::select("select * , doctors.model_identifier as doctor_img , doctors.id as doctor_id from doctors
			join personal_informations on personal_informations.personalable_id =  doctors.id
			join contact_informations on contact_informations.contactable_id = doctors.id
			join specialties on doctors.specialty_id = specialties.id
			where doctors.id = $doctor_id
			and personal_informations.personalable_type like '%Doctor'
			and contact_informations.contactable_type like '%Doctor'");
		// $data = DataTable::getPaginate($all_detail_appointments, $per_page, $page);
		return $doctor_and_appointmetn_detail = ['doctorDetail' => $doctorDetail, 'data' => $all_detail_appointments];

	}
}