<?php

namespace Repository\SiteUser;
use Repository\Contracts\SiteUser\EmergencyContact as EmergencyContactContract;

class EmergencyContact implements EmergencyContactContract {
	public function addOrUpdateEmergencyContact($data_array, $siteUserId) {
		return $data_array->full_name;
	}
}