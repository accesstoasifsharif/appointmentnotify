<?php

namespace Repository\SiteUser;

use Illuminate\Support\Facades\DB;
use Repository\Contracts\SiteUser\ShortCallAppointment as ShortCallAppointmentContract;

class ShortCallAppointment implements ShortCallAppointmentContract {
	public function getAggregatedShortCallAppointmentsBySiteUserId($siteUser_id) {

		// $default_per_page = 5;

		// $per_page = Input::get('per_page');
		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;

		$aggregated_appointments = DB::select("
			SELECT * ,count(*) as total , doctors.model_identifier as doctor_img FROM site_users
			JOIN  patient_short_calls on  patient_short_calls.site_user_id = site_users.id

			JOIN doctors ON doctors.id = patient_short_calls.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id


			join health_care_provider_facilities on health_care_provider_facilities.id =patient_short_calls.health_care_provider_facility_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.	health_care_facility_id
			WHERE personal_informations.personalable_type  like  '%Doctor%'
			AND site_users.id = " . $siteUser_id . "
			group by doctors.id

			");
		// $data = DataTable::getPaginate($aggregated_appointments, $per_page, $page);

		return $aggregated_appointments;

	}
	public function getShortCallAppointmentDetailByDoctorId($doctor_id, $siteUser_id) {
		// $default_per_page = 5;

		// $per_page = Input::get('per_page');

		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;
		$all_detail_appointments = DB::select("

			SELECT * ,count(*) as total , doctors.model_identifier as doctor_img FROM site_users
			JOIN  patient_short_calls on  patient_short_calls.site_user_id = site_users.id

			JOIN doctors ON doctors.id = patient_short_calls.doctor_id
			JOIN specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id


			join health_care_provider_facilities on health_care_provider_facilities.id =patient_short_calls.health_care_provider_facility_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.	health_care_facility_id
			JOIN addresses on addresses.addressable_id = health_care_facilities.id
			Where site_users.id = " . $siteUser_id . "
			AND doctors.id = " . $doctor_id . "
			And personal_informations.personalable_type  like  '%Doctor%'
			AND addresses.addressable_type  like  '%HealthCareFacility%'

			");

		$doctorDetail = DB::select("select * , doctors.model_identifier as doctor_img , doctors.id as doctor_id from doctors
			join personal_informations on personal_informations.personalable_id =  doctors.id
			join contact_informations on contact_informations.contactable_id = doctors.id
			join specialties on doctors.specialty_id = specialties.id
			where doctors.id = $doctor_id
			and personal_informations.personalable_type like '%Doctor'
			and contact_informations.contactable_type like '%Doctor'");
		// $data = DataTable::getPaginate($all_detail_appointments, $per_page, $page);
		return $doctor_and_appointmetn_detail = ['doctorDetail' => $doctorDetail, 'data' => $all_detail_appointments];

	}
}