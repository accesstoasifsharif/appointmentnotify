<?php

namespace Repository\SiteUser;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Repository\Contracts\SiteUser\AppointmentTimeLine as AppointmentTimeLineContract;

class AppointmentTimeLine implements AppointmentTimeLineContract {

	protected function getAllAppointmentsOfSiteUser($site_user_id) {
		$appointments = DB::select("select
			*
			from an_events
			where an_events.site_user_id = " . $site_user_id . "
			order by an_events.date desc
			");
		return $appointments;
	}

	public function getYearListOfAppointments($site_user) {
		$appointments_list = $this->getAllAppointmentsOfSiteUser($site_user->id);
		$appointment_years_list = array();
		for ($i = 0; $i < count($appointments_list); $i++) {
			$year = ['year' => Carbon::parse($appointments_list[$i]->date)->format('Y')];
			array_push($appointment_years_list, $year);
		}
		$appointment_years = array();
		foreach ($appointment_years_list as $current) {
			if (!in_array($current, $appointment_years)) {
				$appointment_years[] = $current;
			}
		}
		return $appointment_years;
	}

	public function getYearMonthListOfAppointments($year, $site_user) {
		$appointments_list = $this->getAllAppointmentsOfSiteUser($site_user->id);
		$unique_year_months = array();

		for ($i = 0; $i < count($appointments_list); $i++) {
			$year_convert = Carbon::parse($appointments_list[$i]->date)->format('Y');

			if (strpos($year_convert, $year) !== false) {
				$year_month = [
					'year' => $year,
					'month' => Carbon::parse($appointments_list[$i]->date)->format('M'),
					// ,'month_number'=>Carbon::parse($appointments_list[$i]->date)->format('m')
				];
				array_push($unique_year_months, $year_month);
			}

		}

		$appointment_year_months = array();
		foreach ($unique_year_months as $current) {
			if (!in_array($current, $appointment_year_months)) {
				$appointment_year_months[] = $current;
			}
		}

		return $appointment_year_months;
	}

	public function getAppointmentListOfMonthOfYear($year, $month, $site_user) {
		$appointments_list = $this->getAllAppointmentsOfSiteUser($site_user->id);
		$unique_year_appointment = array();

		for ($i = 0; $i < count($appointments_list); $i++) {
			$year_convert = Carbon::parse($appointments_list[$i]->date)->format('Y');
			$month_convert = Carbon::parse($appointments_list[$i]->date)->format('M');

			if (strpos($year_convert, $year) !== false && strpos($month_convert, $month) !== false) {
				array_push($unique_year_appointment, $appointments_list[$i]->id);
			}
		}

		return $this->appointmentListMaker($unique_year_appointment);
	}

	protected function appointmentListMaker($appoint_id_array) {
		$unique_appointments = array();
		for ($i = 0; $i < count($appoint_id_array); $i++) {
			array_push($unique_appointments, $this->getAppointmentDataById($appoint_id_array[$i]));

		}

		$appointment_attach_array = array();
		for ($i = 0; $i < count($unique_appointments); $i++) {
			for ($j = 0; $j < count($unique_appointments[$i]); $j++) {
				array_push($appointment_attach_array, $unique_appointments[$i][$j]);
			}
		}
		return $appointment_attach_array;
	}

	protected function getAppointmentDataById($an_event_id) {
		$an_events = DB::select("select
			an_events.id as an_event_id,
			an_events.date,
			an_events.start_time,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
			specialties.specialty_name,
			health_care_facilities.facility_name,
			addresses.address_line_1,
			addresses.address_line_2,
			addresses.city_name,
			addresses.zip_code,
			states.postal_code
			from an_events
			join doctors on doctors.id = an_events.doctor_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			join specialties on specialties.id = doctors.specialty_id
			join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
			join addresses on addresses.addressable_id = health_care_facilities.id
			join states on states.id = addresses.state_id
			where an_events.id = " . $an_event_id . "
			And personal_informations.personalable_type like '%Doctor%'
			And addresses.addressable_type like '%HealthCareFacility%'
			");
		return $an_events;
	}

}