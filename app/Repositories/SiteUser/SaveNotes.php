<?php

namespace Repository\SiteUser;

use Illuminate\Support\Facades\DB;
use Model\AppointmentNote;
use Repository\Contracts\SiteUser\SaveNotes as SaveNoteContract;

class SaveNotes implements SaveNoteContract {

	public function saveNote($data_array) {

		$note_id = $data_array->id;

		$description = $data_array->note;

		$savenote = DB::select("
			select * from appointment_notes
			WHERE appointment_notes.appointment_id = " . $note_id . "
			");

		if ($savenote) {

			DB::table('appointment_notes')
				->where('appointment_notes.appointment_id', $note_id)
				->update(['description' => $description]);
			return "note is updated";
		} else {

			$newnote = new AppointmentNote();
			$newnote->description = $description;
			$newnote->appointment_id = $note_id;
			$newnote->save();
			return "note has been saved!";
		}
	}
	public function getNoteByAppointmentId($appointment_id) {
		$note = DB::select(
			"select * from appointment_notes
			where appointment_notes.appointment_id = " . $appointment_id . "
			");
		return $note;
	}
}