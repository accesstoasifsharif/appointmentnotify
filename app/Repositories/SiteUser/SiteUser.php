<?php

namespace Repository\SiteUser;
use App\User;
use Repository\Contracts\SiteUser\SiteUser as SiteUserContract;

class SiteUser implements SiteUserContract {

	public function getSiteUser(User $user) {

		if (!isset($user->siteUser)) {
			return \Model\SiteUser::create(['user_id' => $user->id]);

		}

		return $user->siteUser;
	}

}
