<?php

namespace Repository\SiteUser;

use Model\AppointmentReason;
use Repository\Contracts\SiteUser\AppointmentsReasons as AppointmentReasonContract;

class AppointmentsReasons implements AppointmentReasonContract {
	public function getAppointmentReasons() {

		$reasons = AppointmentReason::all();
		return $reasons;
	}
}