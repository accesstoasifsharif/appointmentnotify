<?php

namespace Repository\Home;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Model\AnEvent;
use Model\AppointmentReservation;
use Model\Specialty;
use Repository\Contracts\Home\HomeAppointments as HomeAppointmentsContract;

class HomeAppointments implements HomeAppointmentsContract {

	public function getAllSpecialtyList() {
		return Specialty::all();
	}
	protected function getFacilityListInZipList($zip_array) {
		$facility_array = array();
		for ($i = 0; $i < count($zip_array); $i++) {
			$facility_list = DB::select("select health_care_facilities.id as unique_facility_id from health_care_facilities
				join addresses on addresses.addressable_id = health_care_facilities.id
				where addresses.zip_code = " . $zip_array[$i]->zip . "
				And addresses.addressable_type like '%HealthCareFacility%'
				");
			if (!empty($facility_list)) {
				array_push($facility_array, $facility_list);
			}
		}
		return $this->makeFacilityList($facility_array);
	}

	protected function makeFacilityList($facility_array) {
		$facility_attach_array = array();
		for ($i = 0; $i < count($facility_array); $i++) {
			for ($j = 0; $j < count($facility_array[$i]); $j++) {
				array_push($facility_attach_array, $facility_array[$i][$j]);
			}
		}

		$facility_final_array = array();
		foreach ($facility_attach_array as $current) {
			if (!in_array($current, $facility_final_array)) {
				$facility_final_array[] = $current;
			}
		}
		return $facility_final_array;
	}

// A
	public function showAppointmentsForProviderView($search_query, $speciality_id, $selectGender, $zip_list, $page, $doctor_id) {
		$facility_list = $this->getFacilityListInZipList($zip_list);

		if ($speciality_id != "") {
			return $this->getAppointmentsForProviderViewWithSpecialty($search_query, $selectGender, $speciality_id, $zip_list, $facility_list, $page, $doctor_id);
		} else {
			return $this->getAppointmentsForProviderViewWithoutSpecialty($search_query, $selectGender, $zip_list, $facility_list, $page, $doctor_id);
		}
	}
// 1
	protected function getAppointmentsForProviderViewWithSpecialty($search_query, $selectGender, $speciality_id, $zip_list, $facility_list, $page, $doctor_id) {
		if ($selectGender == "all") {
			//ok
			return $this->getAppointmentsForProviderViewWithSpecialtyWithoutGender($search_query, $speciality_id, $zip_list, $facility_list, $page, $doctor_id);
		} else {
			//ok
			return $this->getAppointmentsForProviderViewWithSpecialtyWithGender($search_query, $selectGender, $speciality_id, $zip_list, $facility_list, $page, $doctor_id);
		}

	}

// 1.1
	protected function getAppointmentsForProviderViewWithSpecialtyWithGender($search_query, $selectGender, $speciality_id, $zip_list, $facility_list, $page, $doctor_id) {
		//ok
		if ($search_query == '' && $zip_list != null) {
			//1.1.2
			return $this->getAppointmentsForProviderViewWithSpecialtyWithGenderWithoutDoctorWithZip($speciality_id, $selectGender, $facility_list, $page, $doctor_id);
		}
		//ok
		if ($search_query != '' && $zip_list != null) {
			//1.1.4
			return $this->getAppointmentsForProviderViewWithSpecialtyWithGenderWithDoctorWithZip($speciality_id, $selectGender, $search_query, $facility_list, $page, $doctor_id);
		}
	}

// 1.2
	protected function getAppointmentsForProviderViewWithSpecialtyWithoutGender($search_query, $speciality_id, $zip_list, $facility_list, $page, $doctor_id) {
		if ($search_query == '' && $zip_list != null) {
			//1.2.2'
			return $this->getAppointmentsForProviderViewWithSpecialtyWithoutGenderWithoutDoctorWithZip($speciality_id, $facility_list, $page, $doctor_id);
		}
		if ($search_query != '' && $zip_list != null) {
			//1.2.4
			return $this->getAppointmentsForProviderViewWithSpecialtyWithoutGenderWithDoctorWithZip($speciality_id, $search_query, $facility_list, $page, $doctor_id);
		}
	}

// 1.1.2
	protected function getAppointmentsForProviderViewWithSpecialtyWithGenderWithoutDoctorWithZip($speciality_id, $selectGender, $facility_list, $page, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					doctors.id as doctor_id,
					doctors.model_identifier as doctor_identifier,
					an_events.id as appointment_id,
					an_events.health_care_facility_id as facility_id,
					specialties.id as specialty_id,
					specialties.specialty_name as specialty_name,
					count(*) as total_appointments,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					1 as total_facility
					from an_events
					join doctors on doctors.id = an_events.doctor_id
					join specialties on specialties.id = doctors.specialty_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And doctors.specialty_id = " . $speciality_id . "
					And personal_informations.gender like '" . $selectGender . "'
					And personal_informations.personalable_type like '%Doctor%'
					And an_events.appointment_status like '" . $app_status . "'
					And an_events.appointment_current_state like $app_current_state
					group by doctors.id
					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}

			$appointments_data = $this->makeProviderViewAppointmentList($appointment_array, $facility_list);
			$paginate = HOME_PAGE_PAGINATE;
			$offSet = ($page * $paginate) - $paginate;
			$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
			$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
			$appointments_data = $appointments_data->toArray();
			return $appointments_data;
		} else {
			return $this->getAppointmentDetailOfSpecificDoctor($facility_list, $doctor_id);

		}
	}

// 1.1.4
	protected function getAppointmentsForProviderViewWithSpecialtyWithGenderWithDoctorWithZip($speciality_id, $selectGender, $search_query, $facility_list, $page, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					doctors.id as doctor_id,
					doctors.model_identifier as doctor_identifier,
					an_events.id as appointment_id,
					an_events.health_care_facility_id as facility_id,
					specialties.id as specialty_id,
					specialties.specialty_name as specialty_name,
					count(*) as total_appointments,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					1 as total_facility
					from an_events
					join doctors on doctors.id = an_events.doctor_id
					join specialties on specialties.id = doctors.specialty_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And doctors.specialty_id = " . $speciality_id . "
					And personal_informations.gender like '" . $selectGender . "'
					And personal_informations.personalable_type like '%Doctor%'
					And (personal_informations.first_name like '%" . $search_query . "%'
					OR personal_informations.last_name like '%" . $search_query . "%')
					And an_events.appointment_status like '" . $app_status . "'
					And an_events.appointment_current_state like $app_current_state
					group by doctors.id
					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}

			// $appointments_data = $this->makeProviderViewAppointmentList($appointment_array,$facility_list);

			$appointment_cover_specific_specialty_name = $this->getAppointmentForProviderViewIfNameFieldSpecialtyWithGender($selectGender, $facility_list, $search_query);

			$appointments_unsorted_array = array_merge($appointment_array, $appointment_cover_specific_specialty_name);

			$appointments_data = $this->makeProviderViewAppointmentList($appointments_unsorted_array, $facility_list);

			$paginate = HOME_PAGE_PAGINATE;
			$offSet = ($page * $paginate) - $paginate;
			$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
			$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
			$appointments_data = $appointments_data->toArray();
			return $appointments_data;
		} else {
			return $this->getAppointmentDetailOfSpecificDoctor($facility_list, $doctor_id);

		}
	}

// 1.2.2
	protected function getAppointmentsForProviderViewWithSpecialtyWithoutGenderWithoutDoctorWithZip($speciality_id, $facility_list, $page, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		// return $facility_list;
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					doctors.id as doctor_id,
					doctors.model_identifier as doctor_identifier,
					an_events.id as appointment_id,
					an_events.health_care_facility_id as facility_id,
					specialties.id as specialty_id,
					specialties.specialty_name as specialty_name,
					count(*) as total_appointments,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					1 as total_facility
					from an_events
					join doctors on doctors.id = an_events.doctor_id
					join specialties on specialties.id = doctors.specialty_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And doctors.specialty_id = " . $speciality_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And an_events.appointment_status like '" . $app_status . "'
					And an_events.appointment_current_state like $app_current_state

					group by doctors.id
					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			// return $appointment_array;
			$appointments_data = $this->makeProviderViewAppointmentList($appointment_array, $facility_list);
			$paginate = HOME_PAGE_PAGINATE;
			$offSet = ($page * $paginate) - $paginate;
			$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
			/*paginate*/
			$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
			$appointments_data = $appointments_data->toArray();
			return $appointments_data;
		} else {
			return $this->getAppointmentDetailOfSpecificDoctor($facility_list, $doctor_id);

		}
	}

// 1.2.4
	protected function getAppointmentsForProviderViewWithSpecialtyWithoutGenderWithDoctorWithZip($speciality_id, $search_query, $facility_list, $page, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					doctors.id as doctor_id,
					doctors.model_identifier as doctor_identifier,
					an_events.id as appointment_id,
					an_events.health_care_facility_id as facility_id,
					specialties.id as specialty_id,
					specialties.specialty_name as specialty_name,
					count(*) as total_appointments,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					1 as total_facility
					from an_events
					join doctors on doctors.id = an_events.doctor_id
					join specialties on specialties.id = doctors.specialty_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And doctors.specialty_id = " . $speciality_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And (personal_informations.first_name like '%" . $search_query . "%'
					OR personal_informations.last_name like '%" . $search_query . "%')
					And an_events.appointment_status like '" . $app_status . "'
					And an_events.appointment_current_state like $app_current_state
					group by doctors.id
					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}

			// $appointments_data = $this->makeProviderViewAppointmentList($appointment_array,$facility_list);

			$appointment_cover_specific_specialty_name = $this->getAppointmentForProviderViewIfNameFieldSpecialty($facility_list, $search_query);

			$appointments_unsorted_array = array_merge($appointment_array, $appointment_cover_specific_specialty_name);

			$appointments_data = $this->makeProviderViewAppointmentList($appointments_unsorted_array, $facility_list);

			$paginate = HOME_PAGE_PAGINATE;
			$offSet = ($page * $paginate) - $paginate;
			$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
			$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
			$appointments_data = $appointments_data->toArray();
			return $appointments_data;
		} else {
			return $this->getAppointmentDetailOfSpecificDoctor($facility_list, $doctor_id);

		}
	}

	protected function makeProviderViewAppointmentList($appointment_array, $facility_list) {
		// return $appointment_array;

		$appointment_attach_array = array();
		for ($i = 0; $i < count($appointment_array); $i++) {
			for ($j = 0; $j < count($appointment_array[$i]); $j++) {
				array_push($appointment_attach_array, $appointment_array[$i][$j]);
			}
		}
		// return $appointment_attach_array;

		$appointment_final_array = array();
		foreach ($appointment_attach_array as $current) {
			if (!in_array($current, $appointment_final_array, true)) {
				$appointment_final_array[] = $current;
			}
		}

		// return $appointment_final_array;

		$appointment_final_array2 = array();
		$uniquekeys = array();

		for ($i = 0; $i < count($appointment_final_array); $i++) {
			if (!in_array($appointment_final_array[$i]->doctor_id, $uniquekeys)) {
				$uniquekeys[] = $appointment_final_array[$i]->doctor_id;
				$appointment_final_array2[] = $appointment_final_array[$i];
			} else {
				$index = $this->getArrayKeyMatchIndex($appointment_final_array2, $appointment_final_array[$i]->doctor_id);
				$appointment_final_array2[$index]->total_appointments = $appointment_final_array2[$index]->total_appointments + $appointment_final_array[$i]->total_appointments;
				$appointment_final_array2[$index]->total_facility++;
			}
		}

		for ($i = 0; $i < count($appointment_final_array2); $i++) {
			// $appointment_final_array2[$i]->facilities = array();
			$doctor_facility_list = $this->getAppointmentFacilitiesOfDoctor($appointment_final_array2[$i]->doctor_id, $facility_list);
			$appointment_final_array2[$i]->facilities = $doctor_facility_list;
		}

		return $appointment_final_array2;
	}

// return the index at key matched...
	protected function getArrayKeyMatchIndex($array, $key) {
		for ($i = 0; $i < count($array); $i++) {
			if ($array[$i]->doctor_id == $key) {return $i;}
		}
	}

	protected function getAppointmentFacilitiesOfDoctor($doctor_id, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$doctor_facility = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$doctor_facility_list = DB::select("select
				doctors.id as doctor_id,
				doctors.model_identifier as doctor_identifier,
				an_events.health_care_facility_id as facility_id,
				health_care_facilities.latitude as facility_lat,
				health_care_facilities.longitude as facility_lng,
				addresses.zip_code,
				addresses.address_line_1,
				addresses.address_line_2,
				addresses.city_name,
				addresses.zip_code,
				states.postal_code,
				health_care_facilities.facility_name
				from an_events
				join doctors on doctors.id = an_events.doctor_id
				join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
				join addresses on addresses.addressable_id = health_care_facilities.id
				join states on states.id = addresses.state_id
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And an_events.doctor_id = " . $doctor_id . "
				And an_events.appointment_status like '" . $app_status . "'
				And an_events.appointment_current_state like $app_current_state
				And addresses.addressable_type like '%HealthCareFacility%'
				group by doctors.id
				");
			if (!empty($doctor_facility_list)) {
				array_push($doctor_facility, $doctor_facility_list);
			}
		}

		$doctor_facility_attach_array = array();
		for ($i = 0; $i < count($doctor_facility); $i++) {
			for ($j = 0; $j < count($doctor_facility[$i]); $j++) {
				array_push($doctor_facility_attach_array, $doctor_facility[$i][$j]);
			}
		}

		$doctor_facility_final_array = array();
		foreach ($doctor_facility_attach_array as $current) {
			if (!in_array($current, $doctor_facility_final_array, true)) {
				$doctor_facility_final_array[] = $current;
			}
		}

		return $doctor_facility_final_array;
	}

	// 2
	protected function getAppointmentsForProviderViewWithoutSpecialty($search_query, $selectGender, $zip_list, $facility_list, $page, $doctor_id) {
		if ($selectGender == "all") {
			return $this->getAppointmentsForProviderViewWithoutSpecialtyWithoutGender($search_query, $zip_list, $facility_list, $page, $doctor_id);
		} else {
			return $this->getAppointmentsForProviderViewWithoutSpecialtyWithGender($search_query, $selectGender, $zip_list, $facility_list, $page, $doctor_id);
		}
	}

// 2.1
	protected function getAppointmentsForProviderViewWithoutSpecialtyWithGender($search_query, $selectGender, $zip_list, $facility_list, $page, $doctor_id) {
		if ($search_query == '' && $zip_list != null) {
			//2.1.2
			return $this->getAppointmentsForProviderViewWithoutSpecialtyWithGenderWithoutDoctorWithZip($selectGender, $facility_list, $page, $doctor_id);
		}

		if ($search_query != '' && $zip_list != null) {
			//2.1.4
			return $this->getAppointmentsForProviderViewWithoutSpecialtyWithGenderWithDoctorWithZip($selectGender, $search_query, $facility_list, $page, $doctor_id);
		}
	}

// 2.2
	protected function getAppointmentsForProviderViewWithoutSpecialtyWithoutGender($search_query, $zip_list, $facility_list, $page, $doctor_id) {
		// return $zip_list;
		if ($search_query == '' && $zip_list != null) {
			//2.2.2
			return $this->getAppointmentsForProviderViewWithoutSpecialtyWithoutGenderWithoutDoctorWithZip($facility_list, $page, $doctor_id);
		}

		if ($search_query != '' && $zip_list != null) {
			//2.2.4
			return $this->getAppointmentsForProviderViewWithoutSpecialtyWithoutGenderWithDoctorWithZip($search_query, $facility_list, $page, $doctor_id);
		}
	}

	//2.1.2
	protected function getAppointmentsForProviderViewWithoutSpecialtyWithGenderWithoutDoctorWithZip($selectGender, $facility_list, $page, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					doctors.id as doctor_id,
					doctors.model_identifier as doctor_identifier,
					an_events.id as appointment_id,
					an_events.health_care_facility_id as facility_id,
					specialties.id as specialty_id,
					specialties.specialty_name as specialty_name,
					count(*) as total_appointments,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					1 as total_facility
					from an_events
					join doctors on doctors.id = an_events.doctor_id
					join specialties on specialties.id = doctors.specialty_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And personal_informations.gender like '" . $selectGender . "'
					And an_events.appointment_status like '" . $app_status . "'
					And an_events.appointment_current_state like $app_current_state
					group by doctors.id
					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}

			$appointments_data = $this->makeProviderViewAppointmentList($appointment_array, $facility_list);
			$paginate = HOME_PAGE_PAGINATE;
			$offSet = ($page * $paginate) - $paginate;
			$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
			$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
			$appointments_data = $appointments_data->toArray();
			return $appointments_data;
		} else {
			return $this->getAppointmentDetailOfSpecificDoctor($facility_list, $doctor_id);

		}
	}

	//2.1.4
	protected function getAppointmentsForProviderViewWithoutSpecialtyWithGenderWithDoctorWithZip($selectGender, $search_query, $facility_list, $page, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					doctors.id as doctor_id,
					doctors.model_identifier as doctor_identifier,
					an_events.id as appointment_id,
					an_events.health_care_facility_id as facility_id,
					specialties.id as specialty_id,
					specialties.specialty_name as specialty_name,
					count(*) as total_appointments,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					1 as total_facility
					from an_events
					join doctors on doctors.id = an_events.doctor_id
					join specialties on specialties.id = doctors.specialty_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And personal_informations.gender like '" . $selectGender . "'
					And personal_informations.personalable_type like '%Doctor%'
					And (personal_informations.first_name like '%" . $search_query . "%'
					OR personal_informations.last_name like '%" . $search_query . "%')
					And an_events.appointment_status like '" . $app_status . "'
					And an_events.appointment_current_state like $app_current_state
					group by doctors.id
					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}

			$appointment_cover_specific_specialty_name = $this->getAppointmentForProviderViewIfNameFieldSpecialtyWithGender($selectGender, $facility_list, $search_query);

			$appointments_unsorted_array = array_merge($appointment_array, $appointment_cover_specific_specialty_name);

			$appointments_data = $this->makeProviderViewAppointmentList($appointments_unsorted_array, $facility_list);

			// $appointments_data = $this->makeProviderViewAppointmentList($appointment_array,$facility_list);

			$paginate = HOME_PAGE_PAGINATE;
			$offSet = ($page * $paginate) - $paginate;
			$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
			$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
			$appointments_data = $appointments_data->toArray();
			return $appointments_data;
		} else {
			return $this->getAppointmentDetailOfSpecificDoctor($facility_list, $doctor_id);

		}
	}

	//2.2.2
	protected function getAppointmentsForProviderViewWithoutSpecialtyWithoutGenderWithoutDoctorWithZip($facility_list, $page, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					doctors.id as doctor_id,
					doctors.model_identifier as doctor_identifier,
					an_events.id as appointment_id,
					an_events.health_care_facility_id as facility_id,
					specialties.id as specialty_id,
					specialties.specialty_name as specialty_name,
					count(*) as total_appointments,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					CONCAT_WS('',`address_line_1`,' ' ,`address_line_2`,' ' ,`city_name`,' ' ,`zip_code`) as doctor_address,
					1 as total_facility
					from an_events
					join doctors on doctors.id = an_events.doctor_id
					join specialties on specialties.id = doctors.specialty_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					join addresses on addresses.addressable_id = doctors.id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And addresses.addressable_type like '%Doctor%'
					And an_events.appointment_status like '" . $app_status . "'
					And an_events.appointment_current_state like $app_current_state
					group by doctors.id
					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			$appointments_data = $this->makeProviderViewAppointmentList($appointment_array, $facility_list);
			$paginate = HOME_PAGE_PAGINATE;
			$offSet = ($page * $paginate) - $paginate;
			$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
			$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
			$appointments_data = $appointments_data->toArray();
			return $appointments_data;
		} else {
			return $this->getAppointmentDetailOfSpecificDoctor($facility_list, $doctor_id);

		}
	}

	//2.2.4
	protected function getAppointmentsForProviderViewWithoutSpecialtyWithoutGenderWithDoctorWithZip($search_query, $facility_list, $page, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					doctors.id as doctor_id,
					doctors.model_identifier as doctor_identifier,
					an_events.id as appointment_id,
					an_events.health_care_facility_id as facility_id,
					specialties.id as specialty_id,
					specialties.specialty_name as specialty_name,
					count(*) as total_appointments,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					1 as total_facility
					from an_events
					join doctors on doctors.id = an_events.doctor_id
					join specialties on specialties.id = doctors.specialty_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "

					And personal_informations.personalable_type like '%Doctor%'
					And (personal_informations.first_name like '%" . $search_query . "%'
					OR personal_informations.last_name like '%" . $search_query . "%')

					And an_events.appointment_status like '" . $app_status . "'
					And an_events.appointment_current_state like $app_current_state
					group by doctors.id
					");
				// OR specialties.specialty_name like  '%".$search_query."%'

				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}

			$appointment_cover_specific_specialty_name = $this->getAppointmentForProviderViewIfNameFieldSpecialty($facility_list, $search_query);

			$appointments_unsorted_array = array_merge($appointment_array, $appointment_cover_specific_specialty_name);

			$appointments_data = $this->makeProviderViewAppointmentList($appointments_unsorted_array, $facility_list);

			$paginate = HOME_PAGE_PAGINATE;
			$offSet = ($page * $paginate) - $paginate;
			$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
			$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
			$appointments_data = $appointments_data->toArray();
			return $appointments_data;
		} else {
			return $this->getAppointmentDetailOfSpecificDoctor($facility_list, $doctor_id);
		}
	}
	protected function getAppointmentDetailOfSpecificDoctor($facility_list, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$appointment_array = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$appointment_list = DB::select("select
				an_events.id as unique_appointment_id,
				an_events.date as appointment_date,
				an_events.start_time as appointment_time,
				health_care_facilities.facility_name
				from an_events
				join doctors on doctors.id = an_events.doctor_id
				join specialties on specialties.id = doctors.specialty_id
				join personal_informations on personal_informations.personalable_id = doctors.id
				join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And an_events.doctor_id = " . $doctor_id . "
				And personal_informations.personalable_type like '%Doctor%'
				And an_events.appointment_status like '" . $app_status . "'
				And an_events.appointment_current_state like $app_current_state
				");
			if (!empty($appointment_list)) {
				array_push($appointment_array, $appointment_list);
			}
		}
		return $this->makeProviderViewAppointmentDetailList($appointment_array);
	}

	protected function makeProviderViewAppointmentDetailList($appointment_array) {
		$appointment_attach_array = array();
		for ($i = 0; $i < count($appointment_array); $i++) {
			for ($j = 0; $j < count($appointment_array[$i]); $j++) {
				array_push($appointment_attach_array, $appointment_array[$i][$j]);
			}
		}

		return $appointment_attach_array;
	}

	public function showDoctorDetailByDoctorId($doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$doctor_detail = DB::select("select
			users.id as unique_user_id,
			specialties.specialty_name,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name
			from doctors
			join specialties on specialties.id = doctors.specialty_id
			join single_practices on single_practices.doctor_id = doctors.id
			join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
			join users on users.id = health_care_providers.user_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where doctors.id = " . $doctor_id . "
			And personal_informations.personalable_type like '%Doctor%'
			");

		return $doctor_detail;
	}

	public function checkAppointmentExistanceByAppointmentId($appointment_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$appointment = AnEvent::where('appointment_status', $app_status)->where('appointment_current_state', $app_current_state)->find($appointment_id);
		if (isset($appointment)) {
			return 'true';
		} else {
			return 'false';
		}
	}

	public function bookAnAppointment($appointment_id, $siteUser_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Private');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Pending');
		$an_event = AnEvent::find($appointment_id);
		$an_event->appointment_status = $app_status;
		$an_event->appointment_current_state = $app_current_state;
		$an_event->save();
		$appointment_reservation = new AppointmentReservation();
		$appointment_reservation->site_user_id = $siteUser_id;
		$appointment_reservation->an_event_id = $appointment_id;
		$appointment_reservation->save();
		return "appointment reserve request send successfully...";
	}

	public function checkAppointmentExistance($appointment_id) {
		return $this->appointmentRepo->checkAppointmentExistanceByAppointmentId($appointment_id);
	}

	public function getSingleAppointmentDetailById($appointment_id) {
		$appointment_detail = DB::select("select
			an_events.id as appointment_id,
			an_events.date as appointment_date,
			CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
			health_care_facilities.facility_name

			from an_events
			join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
			join doctors on doctors.id = an_events.doctor_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			where an_events.id = " . $appointment_id . "
			And personal_informations.personalable_type like '%Doctor%'
			");

		return $appointment_detail;
	}

	protected function getAppointmentForProviderViewIfNameFieldSpecialty($facility_list, $search_query) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$appointment_array = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$appointment_list = DB::select("select
				doctors.id as doctor_id,
				doctors.model_identifier as doctor_identifier,
				an_events.id as appointment_id,
				an_events.health_care_facility_id as facility_id,
				specialties.id as specialty_id,
				specialties.specialty_name as specialty_name,
				count(*) as total_appointments,
				CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
				1 as total_facility
				from an_events
				join doctors on doctors.id = an_events.doctor_id
				join specialties on specialties.id = doctors.specialty_id
				join personal_informations on personal_informations.personalable_id = doctors.id
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And specialties.specialty_name like '%" . $search_query . "%'
				And personal_informations.personalable_type like '%Doctor%'
				And an_events.appointment_status like '" . $app_status . "'
				And an_events.appointment_current_state like $app_current_state
				group by doctors.id
				");
			if (!empty($appointment_list)) {
				array_push($appointment_array, $appointment_list);
			}
		}
		return $appointment_array;
	}

	protected function getAppointmentForProviderViewIfNameFieldSpecialtyWithGender($selectGender, $facility_list, $search_query) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$appointment_array = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$appointment_list = DB::select("select
				doctors.id as doctor_id,
				doctors.model_identifier as doctor_identifier,
				an_events.id as appointment_id,
				an_events.health_care_facility_id as facility_id,
				specialties.id as specialty_id,
				specialties.specialty_name as specialty_name,
				count(*) as total_appointments,
				CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
				1 as total_facility
				from an_events
				join doctors on doctors.id = an_events.doctor_id
				join specialties on specialties.id = doctors.specialty_id
				join personal_informations on personal_informations.personalable_id = doctors.i
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And specialties.specialty_name like '%" . $search_query . "%'
				And personal_informations.personalable_type like '%Doctor%'
				And personal_informations.gender like '" . $selectGender . "'
				And an_events.appointment_status like '" . $app_status . "'
				And an_events.appointment_current_state like $app_current_state
				group by doctors.id
				");
			if (!empty($appointment_list)) {
				array_push($appointment_array, $appointment_list);
			}
		}
		return $appointment_array;
	}

	// Action methods for facility view
	public function showAppointmentsForFacilityViewWithGender($gender, $facility_id) {
		return $gender;
		return $facility_id;
	}

	public function showAppointmentsForFacilityView($search_query, $zip_list, $specialty_id, $page, $doctor_id, $gender, $facility_id) {
		$facility_list = $this->getFacilityListInZipList($zip_list);

		if ($specialty_id != "") {
			//B:: 1
			return $this->getAppointmentsForFacilityViewWithSpecialty($search_query, $gender, $specialty_id, $zip_list, $page, $facility_id, $facility_list, $doctor_id);
		} else {
			//B:: 2
			return $this->getAppointmentsForFacilityViewWithoutSpecialty($search_query, $gender, $zip_list, $page, $facility_id, $facility_list, $doctor_id);
		}
	}
//B:: 1
	protected function getAppointmentsForFacilityViewWithSpecialty($search_query, $gender, $specialty_id, $zip_list, $page, $facility_id, $facility_list, $doctor_id) {
		if ($search_query != "") {
			//f1
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithNameField($search_query, $gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		} else {
			//f2
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithoutNameField($gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}
	}

//B:: 2
	protected function getAppointmentsForFacilityViewWithoutSpecialty($search_query, $gender, $zip_list, $page, $facility_id, $facility_list, $doctor_id) {
		if ($search_query != "") {
			//f3
			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithNameField($search_query, $gender, $zip_list, $page, $facility_id, $facility_list, $doctor_id);
		} else {
			//f4
			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameField($gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}
	}

//f1
	protected function getAppointmentsForFacilityViewWithSpecialtyWithNameField($search_query, $gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($facility_id == "") {
			//f1.1
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithoutFacilityId($search_query, $specialty_id, $zip_list, $page, $facility_list);
		} else {
			//f1.2
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithFacilityId($search_query, $gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}
	}

//f1.1
	protected function getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithoutFacilityId($search_query, $specialty_id, $zip_list, $page, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');

		$doctor_array = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$facility_id_list = DB::select("select
				doctors.id as doctor_id,
				health_care_facilities.model_identifier as health_care_facility_identifier,
				health_care_facilities.id as unique_facility_id,
				1 as total_doctors
				from an_events
				join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
				join health_care_provider_facilities on health_care_provider_facilities.health_care_facility_id = health_care_facilities.id
				join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
				join doctors on doctors.id = health_care_provider_doctors.doctor_id
				join specialties on specialties.id = doctors.specialty_id
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And specialties.id = " . $specialty_id . "
				And health_care_facilities.facility_name like '%" . $search_query . "%'
				And an_events.appointment_status like $app_status
				And an_events.appointment_current_state like $app_current_state

				group by doctors.id
				");
			if (!empty($facility_id_list)) {
				array_push($doctor_array, $facility_id_list);
			}
		}

		$facility_data_list = $this->makeFacilityViewAppointmentList($doctor_array);
		// return $facility_data_list;

		$appointment_array = array();
		for ($i = 0; $i < count($facility_data_list); $i++) {
			$appointment_list = DB::select("select
				an_events.doctor_id as doctor_id,
				health_care_facilities.id as facility_id,
				health_care_facilities.model_identifier as health_care_facility_identifier,
				health_care_facilities.facility_name as facility_name,
				addresses.address_line_1,addresses.address_line_2,addresses.city_name,
				addresses.zip_code,states.postal_code,
				health_care_facilities.latitude as facility_lat,
				health_care_facilities.longitude as facility_lng,
				count(*) as total_appointments,
				1 as total_doctors
				from an_events
				join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
				join health_care_provider_facilities on health_care_provider_facilities.health_care_facility_id = health_care_facilities.id
				join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
				join doctors on doctors.id = health_care_provider_doctors.doctor_id

				join specialties on specialties.id = doctors.specialty_id

				join addresses on addresses.addressable_id = health_care_facilities.id
				join states on states.id = addresses.state_id
				where an_events.health_care_facility_id = " . $facility_data_list[$i]->unique_facility_id . "
				And specialties.id = " . $specialty_id . "
				And an_events.doctor_id = " . $facility_data_list[$i]->doctor_id . "
				And an_events.appointment_status like $app_status
				And an_events.appointment_current_state like $app_current_state
				And addresses.addressable_type like '%HealthCareFacility%'

				group by doctors.id
				");
			if (!empty($appointment_list)) {
				array_push($appointment_array, $appointment_list);
			}
		}

		$appointments_data = $this->makeFacilityViewAppointmentListModified($appointment_array);

		$paginate = HOME_PAGE_PAGINATE;
		$offSet = ($page * $paginate) - $paginate;
		$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
		$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
		$appointments_data = $appointments_data->toArray();
		return $appointments_data;

		return "getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithoutFacilityId";
	}

//f1.2
	protected function getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithFacilityId($search_query, $gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($gender == "all") {
			//f1.2.1
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithFacilityIdWithoutGender($search_query, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		} else {
			//f1.2.2
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithFacilityIdWithGender($search_query, $gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}

		return "getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithFacilityId";
	}

//f1.2.1
	protected function getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithFacilityIdWithoutGender($search_query, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					users.id as unique_user_id,
					doctors.id as doctor_id,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					specialties.specialty_name,
					count(*) as total_appointments
					from an_events

					join doctors on doctors.id = an_events.doctor_id
					join single_practices on single_practices.doctor_id = doctors.id
					join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
					join users on users.id = health_care_providers.user_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					join specialties on specialties.id = doctors.specialty_id
					join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And health_care_facilities.id = " . $facility_id . "
					And specialties.id = " . $specialty_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And an_events.appointment_status like $app_status
					And an_events.appointment_current_state like $app_current_state
					group by doctors.id

					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			return $this->makeFacilityViewAppointmentDetailList($appointment_array);
		} else {
			return $this->getAppointmentDetailOfSpecificFacility($facility_list, $facility_id, $doctor_id);
		}

		return "getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithFacilityIdWithoutGender";
	}

//f1.2.2
	protected function getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithFacilityIdWithGender($search_query, $gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					users.id as unique_user_id,
					doctors.id as doctor_id,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					specialties.specialty_name,
					count(*) as total_appointments
					from an_events

					join doctors on doctors.id = an_events.doctor_id
					join single_practices on single_practices.doctor_id = doctors.id
					join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
					join users on users.id = health_care_providers.user_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					join specialties on specialties.id = doctors.specialty_id
					join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And health_care_facilities.id = " . $facility_id . "
					And specialties.id = " . $specialty_id . "
					And personal_informations.gender like '" . $gender . "'
					And personal_informations.personalable_type like '%Doctor%'
					And an_events.appointment_status like $app_status
					And an_events.appointment_current_state like $app_current_state
					group by an_events.doctor_id

					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			return $this->makeFacilityViewAppointmentDetailList($appointment_array);
		} else {
			return $this->getAppointmentDetailOfSpecificFacility($facility_list, $facility_id, $doctor_id);
		}

		return "getAppointmentsForFacilityViewWithSpecialtyWithNameFieldWithFacilityIdWithGender";
	}

//f2
	protected function getAppointmentsForFacilityViewWithSpecialtyWithoutNameField($gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($facility_id == "") {
			//f2.1
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithoutFacilityId($specialty_id, $zip_list, $page, $facility_list);
		} else {
			//f2.2
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithFacilityId($gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}
	}

//f2.1
	protected function getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithoutFacilityId($specialty_id, $zip_list, $page, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$doctor_array = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$doctor_list = DB::select("select
				doctors.id as doctor_id,
				health_care_facilities.model_identifier as health_care_facility_identifier,
				health_care_facilities.id as unique_facility_id
				from an_events
				join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
				join health_care_provider_facilities on health_care_provider_facilities.health_care_facility_id = health_care_facilities.id
				join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
				join doctors on doctors.id = health_care_provider_doctors.doctor_id

				join specialties on specialties.id = doctors.specialty_id

				join addresses on addresses.addressable_id = health_care_facilities.id
				join states on states.id = addresses.state_id
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And specialties.id = " . $specialty_id . "
				And an_events.appointment_status like $app_status
				And an_events.appointment_current_state like $app_current_state
				And addresses.addressable_type like '%HealthCareFacility%'

				group by doctors.id
				");
			if (!empty($doctor_list)) {
				array_push($doctor_array, $doctor_list);
			}
		}
		$doctor_data = $this->makeFacilityViewAppointmentList($doctor_array);
		// return $doctor_data;

		$appointment_array = array();
		for ($i = 0; $i < count($doctor_data); $i++) {
			$appointment_list = DB::select("select
				an_events.doctor_id as doctor_id,
				health_care_facilities.model_identifier as health_care_facility_identifier,
				health_care_facilities.id as facility_id,
				health_care_facilities.facility_name as facility_name,
				addresses.address_line_1,addresses.address_line_2,addresses.city_name,
				addresses.zip_code,states.postal_code,
				health_care_facilities.latitude as facility_lat,
				health_care_facilities.longitude as facility_lng,
				count(*) as total_appointments,
				1 as total_doctors
				from an_events
				join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
				join health_care_provider_facilities on health_care_provider_facilities.health_care_facility_id = health_care_facilities.id
				join health_care_provider_doctors on health_care_provider_doctors.hcp_facility_id = health_care_provider_facilities.id
				join doctors on doctors.id = health_care_provider_doctors.doctor_id

				join specialties on specialties.id = doctors.specialty_id

				join addresses on addresses.addressable_id = health_care_facilities.id
				join states on states.id = addresses.state_id
				where an_events.health_care_facility_id = " . $doctor_data[$i]->unique_facility_id . "
				And specialties.id = " . $specialty_id . "
				And an_events.doctor_id = " . $doctor_data[$i]->doctor_id . "
				And an_events.appointment_status like $app_status
				And an_events.appointment_current_state like $app_current_state
				And addresses.addressable_type like '%HealthCareFacility%'

				group by doctors.id
				");
			if (!empty($appointment_list)) {
				array_push($appointment_array, $appointment_list);
			}
		}
		$appointments_data = $this->makeFacilityViewAppointmentListModified($appointment_array);

		$paginate = HOME_PAGE_PAGINATE;
		$offSet = ($page * $paginate) - $paginate;
		$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
		$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
		$appointments_data = $appointments_data->toArray();
		return $appointments_data;

		return "getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithoutFacilityId";
	}

	protected function makeFacilityViewAppointmentListModified($appointment_array) {
		$appointment_attach_array = array();
		for ($i = 0; $i < count($appointment_array); $i++) {
			for ($j = 0; $j < count($appointment_array[$i]); $j++) {
				array_push($appointment_attach_array, $appointment_array[$i][$j]);
			}
		}

		$appointment_final_array = array();
		foreach ($appointment_attach_array as $current) {
			if (!in_array($current, $appointment_final_array, true)) {
				$appointment_final_array[] = $current;
			}
		}

		$appointment_final_array2 = array();
		$uniquekeys1 = array();

		for ($i = 0; $i < count($appointment_final_array); $i++) {
			if (!in_array($appointment_final_array[$i]->doctor_id, $uniquekeys1)) {
				$uniquekeys1[] = $appointment_final_array[$i]->doctor_id;
				$appointment_final_array2[] = $appointment_final_array[$i];
			}

		}

		$appointment_final_array3 = array();
		$uniquekeys2 = array();

		for ($i = 0; $i < count($appointment_final_array2); $i++) {
			if (!in_array($appointment_final_array2[$i]->facility_id, $uniquekeys2)) {
				$uniquekeys2[] = $appointment_final_array2[$i]->facility_id;
				$appointment_final_array3[] = $appointment_final_array2[$i];
			} else {
				$index = $this->getArray2KeyMatchIndex($appointment_final_array3, $appointment_final_array2[$i]->facility_id);
				$appointment_final_array3[$index]->total_appointments = $appointment_final_array3[$index]->total_appointments + $appointment_final_array2[$i]->total_appointments;
				$appointment_final_array3[$index]->total_doctors++;
			}
		}
		return $appointment_final_array3;
	}

	protected function getArray2KeyMatchIndex($array, $key) {
		for ($i = 0; $i < count($array); $i++) {
			if ($array[$i]->facility_id == $key) {return $i;}
		}
	}

//f2.2
	protected function getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithFacilityId($gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($gender == "all") {
			//f2.2.1
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithFacilityIdWithoutGender($specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		} else {
			//f2.2.2
			return $this->getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithFacilityIdWithGender($gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}

		return "getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithFacilityId";
	}

//f2.2.1
	protected function getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithFacilityIdWithoutGender($specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					users.id as unique_user_id,
					doctors.id as doctor_id,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					specialties.specialty_name,
					count(*) as total_appointments
					from an_events

					join doctors on doctors.id = an_events.doctor_id
					join single_practices on single_practices.doctor_id = doctors.id
					join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
					join users on users.id = health_care_providers.user_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					join specialties on specialties.id = doctors.specialty_id
					join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And health_care_facilities.id = " . $facility_id . "
					And specialties.id = " . $specialty_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And an_events.appointment_status like $app_status
					And an_events.appointment_current_state like $app_current_state
					group by an_events.doctor_id
					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			return $this->makeFacilityViewAppointmentDetailList($appointment_array);
		} else {
			return $this->getAppointmentDetailOfSpecificFacility($facility_list, $facility_id, $doctor_id);
		}
		return "getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithFacilityIdWithoutGender";
	}

//f2.2.2
	protected function getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithFacilityIdWithGender($gender, $specialty_id, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					users.id as unique_user_id,
					doctors.id as doctor_id,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					specialties.specialty_name,
					count(*) as total_appointments
					from an_events
					join doctors on doctors.id = an_events.doctor_id
					join single_practices on single_practices.doctor_id = doctors.id
					join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
					join users on users.id = health_care_providers.user_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					join specialties on specialties.id = doctors.specialty_id
					join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And health_care_facilities.id = " . $facility_id . "
					And specialties.id = " . $specialty_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And personal_informations.gender like '" . $gender . "'
					And an_events.appointment_status like $app_status
					And an_events.appointment_current_state like $app_current_state
					group by an_events.doctor_id

					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			return $this->makeFacilityViewAppointmentDetailList($appointment_array);
		} else {
			return $this->getAppointmentDetailOfSpecificFacility($facility_list, $facility_id, $doctor_id);
		}
		return "getAppointmentsForFacilityViewWithSpecialtyWithoutNameFieldWithFacilityIdWithGender";
	}

//f3
	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithNameField($search_query, $gender, $zip_list, $page, $facility_id, $facility_list, $doctor_id) {
		if ($facility_id == "") {
			//f3.1
			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithoutFacilityId($search_query, $zip_list, $page, $facility_list);
		} else {
			//f3.2
			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithFacilityId($search_query, $gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}
	}

//f3.1
	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithoutFacilityId($search_query, $zip_list, $page, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$appointment_array = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$appointment_list = DB::select("select
				an_events.doctor_id as doctor_id,
				health_care_facilities.id as facility_id,
				health_care_facilities.model_identifier as health_care_facility_identifier,
				health_care_facilities.facility_name as facility_name,
				addresses.address_line_1,addresses.address_line_2,addresses.city_name,
				addresses.zip_code,states.postal_code,
				health_care_facilities.latitude as facility_lat,
				health_care_facilities.longitude as facility_lng,
				count(*) as total_appointments,
				1 as total_doctors
				from an_events
				join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id

				join addresses on addresses.addressable_id = health_care_facilities.id
				join states on states.id = addresses.state_id
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And health_care_facilities.facility_name like '%" . $search_query . "%'
				And an_events.appointment_status like $app_status
				And an_events.appointment_current_state like $app_current_state
				And addresses.addressable_type like '%HealthCareFacility%'
				group by an_events.health_care_facility_id
				");
			if (!empty($appointment_list)) {
				array_push($appointment_array, $appointment_list);
			}
		}

		$appointments_data = $this->makeFacilityViewAppointmentList($appointment_array);

		$paginate = HOME_PAGE_PAGINATE;
		$offSet = ($page * $paginate) - $paginate;
		$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
		$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
		$appointments_data = $appointments_data->toArray();
		return $appointments_data;

		return "getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithoutFacilityId";
	}

// protected function makeFacilityViewAppointmentListNew($new_merged_data)
	// {

//         $appointment_attach_array = array();
	//         for ($i=0; $i < count($appointment_array); $i++) {
	//             for ($j=0; $j < count($appointment_array[$i]); $j++) {
	//                 array_push($appointment_attach_array,$appointment_array[$i][$j]);
	//             }
	//         }
	//  	// return $appointment_attach_array;

//         $appointment_final_array  = array();
	//         foreach ($appointment_attach_array as $current) {
	//             if ( ! in_array($current, $appointment_final_array,true)) {
	//                 $appointment_final_array[] = $current;
	//             }
	//         }

//         // return $appointment_final_array;

//         $appointment_final_array2  = array();
	//         $uniquekeys = array();

// 		for ($i=0; $i < count($appointment_final_array) ; $i++) {
	// 			if (!in_array($appointment_final_array[$i]->facility_id, $uniquekeys)) {
	// 		            $uniquekeys[] = $appointment_final_array[$i]->facility_id;
	// 		            $appointment_final_array2[] = $appointment_final_array[$i];
	// 		    }
	// 			else
	// 	        {
	// 	        	$index = $this->getArrayKeyMatchIndexFacilityView($appointment_final_array2 , $appointment_final_array[$i]->facility_id);
	// 	        	$appointment_final_array2[$index]->total_appointments = $appointment_final_array2[$index]->total_appointments + $appointment_final_array[$i]->total_appointments;
	// 	        	$appointment_final_array2[$index]->total_facility ++;
	// 	        }
	// 	    }
	// }

// 	// return the index at key matched...
	//     protected function getArrayKeyMatchIndexFacilityView($array , $key)
	//     {
	//     	for ($i=0; $i < count($array); $i++) {
	//     		if($array[$i]->facility_id == $key)
	//     		{return $i;}
	//     	}
	//     }

//f3.2
	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithFacilityId($search_query, $gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($gender == "all") {
			//f3.1.1
			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithFacilityIdWithoutGender($search_query, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		} else {
			//f3.1.2
			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithFacilityIdWithGender($search_query, $gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}

		return "getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithFacilityId";
	}

//f3.1.1
	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithFacilityIdWithoutGender($search_query, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');

		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					users.id as unique_user_id,
					doctors.id as doctor_id,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					specialties.specialty_name,
					count(*) as total_appointments
					from an_events

					join doctors on doctors.id = an_events.doctor_id
					join single_practices on single_practices.doctor_id = doctors.id
					join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
					join users on users.id = health_care_providers.user_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					join specialties on specialties.id = doctors.specialty_id
					join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And health_care_facilities.id = " . $facility_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And an_events.appointment_status like $app_status
					And an_events.appointment_current_state like $app_current_state
					group by an_events.doctor_id

					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			return $this->makeFacilityViewAppointmentDetailList($appointment_array);
		} else {
			return $this->getAppointmentDetailOfSpecificFacility($facility_list, $facility_id, $doctor_id);
		}
		return "getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithFacilityIdWithoutGender";
	}

//f3.1.2
	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithFacilityIdWithGender($search_query, $gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					users.id as unique_user_id,
					doctors.id as doctor_id,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					specialties.specialty_name,
					count(*) as total_appointments
					from an_events

					join doctors on doctors.id = an_events.doctor_id
					join single_practices on single_practices.doctor_id = doctors.id
					join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
					join users on users.id = health_care_providers.user_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					join specialties on specialties.id = doctors.specialty_id
					join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And health_care_facilities.id = " . $facility_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And personal_informations.gender like '" . $gender . "'
					And an_events.appointment_status like $app_status
					And an_events.appointment_current_state like $app_current_state
					group by an_events.doctor_id

					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			return $this->makeFacilityViewAppointmentDetailList($appointment_array);
		} else {
			return $this->getAppointmentDetailOfSpecificFacility($facility_list, $facility_id, $doctor_id);
		}
		return "getAppointmentsForFacilityViewWithoutSpecialtyWithNameFieldWithFacilityIdWithGender";
	}

//f4
	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameField($gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($facility_id == "") {
			//f4.1
			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithoutFacilityId($zip_list, $page, $facility_list);
		} else {
			//f4.2
			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithFacilityId($gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}
	}

//f4.1

	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithoutFacilityId($zip_list, $page, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');

		$appointment_array = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$appointment_list = DB::select("select
				an_events.doctor_id as doctor_id,
				health_care_facilities.id as facility_id,
				health_care_facilities.model_identifier as health_care_facility_identifier,
				health_care_facilities.facility_name as facility_name,
				addresses.address_line_1,addresses.address_line_2,addresses.city_name,
				addresses.zip_code,states.postal_code,
				health_care_facilities.latitude as facility_lat,
				health_care_facilities.longitude as facility_lng,
				count(*) as total_appointments,
				1 as total_doctors
				from an_events
				join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
				join addresses on addresses.addressable_id = health_care_facilities.id
				join states on states.id = addresses.state_id
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And an_events.appointment_status like $app_status
				And an_events.appointment_current_state like $app_current_state
				And addresses.addressable_type like '%HealthCareFacility%'
				group by an_events.health_care_facility_id
				");
			if (!empty($appointment_list)) {
				array_push($appointment_array, $appointment_list);
			}
		}
		$doctor_array = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$doctor_list = DB::select("select
				an_events.doctor_id as doctor_id,
				an_events.health_care_facility_id as facility_id
				from an_events
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And an_events.appointment_status like $app_status
				And an_events.appointment_current_state like $app_current_state
				group by doctor_id
				");
			if (!empty($doctor_list)) {
				array_push($doctor_array, $doctor_list);
			}
		}

		$appointments_data = $this->makeFacilityViewAppointmentList($appointment_array, $doctor_array);

		$paginate = HOME_PAGE_PAGINATE;
		$offSet = ($page * $paginate) - $paginate;
		$itemsForCurrentPage = array_slice($appointments_data, $offSet, $paginate, true);
		$appointments_data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($appointments_data), $paginate, $page);
		$appointments_data = $appointments_data->toArray();
		return $appointments_data;

		return "getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithoutFacilityId";

	}

//f4.2
	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithFacilityId($gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		if ($gender == "all") {
			//f4.2.1

			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithFacilityIdWithoutGender($zip_list, $page, $facility_id, $doctor_id, $facility_list);
		} else {
			//f4.2.2
			return $this->getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithFacilityIdWithGender($gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list);
		}
	}

//f4.2.1
	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithFacilityIdWithoutGender($zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');

		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					users.id as unique_user_id,
					doctors.id as doctor_id,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					specialties.specialty_name,
					count(*) as total_appointments
					from an_events

					join doctors on doctors.id = an_events.doctor_id
					join single_practices on single_practices.doctor_id = doctors.id
					join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
					join users on users.id = health_care_providers.user_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					join specialties on specialties.id = doctors.specialty_id
					join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And health_care_facilities.id = " . $facility_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And an_events.appointment_status like $app_status
					And an_events.appointment_current_state like $app_current_state
					group by an_events.doctor_id

					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			return $this->makeFacilityViewAppointmentDetailList($appointment_array);
		} else {
			return $this->getAppointmentDetailOfSpecificFacility($facility_list, $facility_id, $doctor_id);
		}
		return "getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithFacilityIdWithoutGender";
	}

//f4.2.2
	protected function getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithFacilityIdWithGender($gender, $zip_list, $page, $facility_id, $doctor_id, $facility_list) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		if ($doctor_id == "") {
			$appointment_array = array();
			for ($i = 0; $i < count($facility_list); $i++) {
				$appointment_list = DB::select("select
					users.id as unique_user_id,
					doctors.id as doctor_id,
					CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name,
					specialties.specialty_name,
					count(*) as total_appointments
					from an_events

					join doctors on doctors.id = an_events.doctor_id
					join single_practices on single_practices.doctor_id = doctors.id
					join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
					join users on users.id = health_care_providers.user_id
					join personal_informations on personal_informations.personalable_id = doctors.id
					join specialties on specialties.id = doctors.specialty_id
					join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
					where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
					And health_care_facilities.id = " . $facility_id . "
					And personal_informations.personalable_type like '%Doctor%'
					And personal_informations.gender like '" . $gender . "'
					And an_events.appointment_status like $app_status
					And an_events.appointment_current_state like $app_current_state
					group by an_events.doctor_id

					");
				if (!empty($appointment_list)) {
					array_push($appointment_array, $appointment_list);
				}
			}
			return $this->makeFacilityViewAppointmentDetailList($appointment_array);
		} else {
			return $this->getAppointmentDetailOfSpecificFacility($facility_list, $facility_id, $doctor_id);
		}
		return "getAppointmentsForFacilityViewWithoutSpecialtyWithoutNameFieldWithFacilityIdWithGender";
	}

	protected function makeFacilityViewAppointmentList($appointment_array, $doctor_array) {

		// return $doctor_array;

		$appointment_attach_array = array();
		for ($i = 0; $i < count($appointment_array); $i++) {
			for ($j = 0; $j < count($appointment_array[$i]); $j++) {
				array_push($appointment_attach_array, $appointment_array[$i][$j]);
			}
		}
		// return $appointment_attach_array;

		$appointment_final_array = array();
		foreach ($appointment_attach_array as $current) {
			if (!in_array($current, $appointment_final_array, true)) {
				$appointment_final_array[] = $current;
			}
		}
		return $appointment_final_array;
		$facility_appointment_final_array = array();
		$facility_uniquekeys = array();

		for ($i = 0; $i < count($appointment_final_array); $i++) {
			if (!in_array($appointment_final_array[$i]->doctor_id, $facility_uniquekeys)) {
				$facility_appointment_final_array[$i] = $appointment_final_array[$i];
				$facility_uniquekeys[$i] = $appointment_final_array[$i]->doctor_id;
			} else {
				$index = $this->getArrayKeyMatchIndexForFacility($facility_appointment_final_array, $appointment_final_array[$i]->doctor_id);
				$facility_appointment_final_array[$index]->total_doctors++;
			}
		}
		return $facility_appointment_final_array;
	}
	protected function getArrayKeyMatchIndexForFacility($array, $key) {
		for ($i = 0; $i < count($array); $i++) {
			if ($array[$i]->doctor_id == $key) {return $i;}
		}
	}

	protected function makeFacilityViewAppointmentDetailList($appointment_array) {
		$appointment_attach_array = array();
		for ($i = 0; $i < count($appointment_array); $i++) {
			for ($j = 0; $j < count($appointment_array[$i]); $j++) {
				array_push($appointment_attach_array, $appointment_array[$i][$j]);
			}
		}
		return $appointment_attach_array;
	}
	protected function getAppointmentDetailOfSpecificFacility($facility_list, $facility_id, $doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$appointment_array = array();
		for ($i = 0; $i < count($facility_list); $i++) {
			$appointment_list = DB::select("select
				an_events.id as unique_appointment_id,
				an_events.date as appointment_date,
				an_events.start_time as appointment_time,
				health_care_facilities.facility_name
				from an_events
				join doctors on doctors.id = an_events.doctor_id
				join specialties on specialties.id = doctors.specialty_id
				join single_practices on single_practices.doctor_id = doctors.id
				join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
				join users on users.id = health_care_providers.user_id
				join personal_informations on personal_informations.personalable_id = doctors.id
				join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
				where an_events.health_care_facility_id = " . $facility_list[$i]->unique_facility_id . "
				And an_events.doctor_id = " . $doctor_id . "
				And an_events.health_care_facility_id = " . $facility_id . "
				And personal_informations.personalable_type like '%Doctor%'
				And an_events.appointment_status like $app_status
				And an_events.appointment_current_state like $app_current_state
				");
			if (!empty($appointment_list)) {
				array_push($appointment_array, $appointment_list);
			}
		}
		return $this->makeProviderViewAppointmentDetailList($appointment_array);
	}

	public function showFacilityDetailByFacilityId($facility_id) {
		$facility_detail = DB::select("select
			*
			from health_care_facilities
			join addresses on addresses.addressable_id =  health_care_facilities.id
			where health_care_facilities.id = " . $facility_id . "
			and addresses.addressable_type like '%HealthCareFacility%'
			");
		return $facility_detail;
	}

}
