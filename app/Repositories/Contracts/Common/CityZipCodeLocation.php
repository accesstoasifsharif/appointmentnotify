<?php

namespace Repository\Contracts\Common;

interface CityZipCodeLocation {

	public function getNearestZipCodeList($zip_code, $distance);
}
