<?php

namespace Repository\Contracts\Common;

use Model\AnEvent as AnEventModel;

interface AnEvent {

	public function getAnEventByAnEventId($an_event_id);
	public function addOrUpdateAnEvent(AnEventModel $an_event);
	public function removeAnEvent(AnEventModel $an_event);

}
