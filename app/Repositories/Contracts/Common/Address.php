<?php

namespace Repository\Contracts\Common;

interface Address {

	public function addOrUpdateAddressInfo($relation, $address);
	public function removeAddress($relation);
	public function getAddressDetails($relation);

}
