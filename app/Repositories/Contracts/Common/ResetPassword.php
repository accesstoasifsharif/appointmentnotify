<?php

namespace Repository\Contracts\Common;

interface ResetPassword {
	public function resetPassword($data_array, $siteUserId);
}