<?php

namespace Repository\Contracts\Common;

use Model\PatientShortCall as PatientShortCallModel;

interface PatientShortCall {

	public function addOrUpdatePatientShortCall(PatientShortCallModel $short_call);
	public function checkExistance($doctor_id, $hcp_facility_id, $site_user_id);

}
