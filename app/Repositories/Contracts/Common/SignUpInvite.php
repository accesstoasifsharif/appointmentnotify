<?php

namespace Repository\Contracts\Common;

interface SignUpInvite {

	public function createInvite($email);

}