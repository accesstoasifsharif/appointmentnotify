<?php

namespace Repository\Contracts\Common;

interface ContactInformation {
	public function addOrUpdateContactInfo($relation, $contact);
	public function removeContact($relation);
	public function getContactDetails($relation);
	public function getDoctorContactInfoByEmails($emails_array);
}
