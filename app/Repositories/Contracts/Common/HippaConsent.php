<?php

namespace Repository\Contracts\Common;

interface HippaConsent {
	public function upDateHippaConsent($data_array, $siteUserId);
	public function getLicenseAgreement($data_array);
	public function getAgreement($siteUserId);
}