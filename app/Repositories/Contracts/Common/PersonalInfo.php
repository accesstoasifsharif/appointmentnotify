<?php

namespace Repository\Contracts\Common;

interface PersonalInfo {

	public function addOrUpdatePersonalInfo($relation, $personalInfo);
	public function removePersonalInfo($relation);

	public function getPersonalInfoDetails($relation);

}
