<?php

namespace Repository\Contracts\Common;

interface UserRepository {

	public function updateUserProfileStatus($user, $status);

}
