<?php

namespace Repository\Contracts\Common;

interface ContactPerson {

	public function addOrUpdateContactPersonInfo($relation, $contactPerson);
	public function removeContactPerson($relation);
	public function getContactPersonDetails($relation);

}
