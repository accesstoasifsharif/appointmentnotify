<?php

namespace Repository\Contracts\Home;

interface HomeAppointments {

	public function showAppointmentsForProviderView($search_query, $speciality_id, $selectGender, $zip_list, $page, $doctor_id);
	public function showAppointmentsForFacilityView($search_query, $zip_list, $specialty_id, $page, $doctor_id, $gender, $facility_id);
	public function showAppointmentsForFacilityViewWithGender($gender, $facility_id);
	public function getAllSpecialtyList();
	public function bookAnAppointment($appointment_id, $siteUser_id);
	public function checkAppointmentExistanceByAppointmentId($appointment_id);
}
