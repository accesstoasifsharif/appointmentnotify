<?php

namespace Repository\Contracts\HealthCareProvider;

interface TimeTable {

	public function timeTableMakerForDB($timeTabelData);
	public function addOrUpdateTimeTableInfo($relation, $time_Table);
	public function removeTimeTable($relation);
	public function getCompleteTimeTableOfRelation($relation);
	public function getCompleteTimeTableFormate();

}
