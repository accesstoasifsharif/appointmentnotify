<?php

namespace Repository\Contracts\HealthCareProvider;

interface EmployerIdentificationNumber {

	public function addOrUpdateEIN($relation, $ein);
	public function removeEIN($relation);
	public function getEINDetails($relation);

}
