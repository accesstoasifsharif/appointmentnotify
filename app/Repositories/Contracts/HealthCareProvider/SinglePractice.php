<?php

namespace Repository\Contracts\HealthCareProvider;
use Model\Doctor as DoctorModel;
use Model\HealthCareProvider;

interface SinglePractice {
	public function getSinglePracticeByHCP(HealthCareProvider $hcp);
	public function createSinglePractice(HealthCareProvider $hcp, DoctorModel $dr);

}
