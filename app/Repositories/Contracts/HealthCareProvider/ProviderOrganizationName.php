<?php

namespace Repository\Contracts\HealthCareProvider;

interface ProviderOrganizationName {

	public function addOrUpdatePON($relation, $pon);
	public function removePON($relation);
	public function getPONDetails($relation);

}
