<?php

namespace Repository\Contracts\HealthCareProvider;

interface Patient {
	public function getAllPatientsSpecificPerspective($hcp_id, $request);
	public function allPatientsByDoctorId($doctorId, $hcp_id, $request);
}