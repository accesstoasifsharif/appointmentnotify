<?php

namespace Repository\Contracts\HealthCareProvider;

use Model\HealthCareFacility as HCFacility;

interface HealthCareFacility {

	public function getHealthCareFacilityById($facility_id);
	public function addOrUpdateHealthCareFacility(HCFacility $facility);
	public function deleteHealthCareFacility(HCFacility $facility);
	public function allHealthCareFacilities();

	public function getFacilityDetailByFacilityId($facility_id);
	// public function getExistingFacilityBySearchQuery($query);

}
