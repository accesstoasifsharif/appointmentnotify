<?php

namespace Repository\Contracts\HealthCareProvider;

interface PublishedAppointment {
	public function getSpecificDoctorSpecificHCPFacilityAppointments($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityAppointments($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityAppointments($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityAppointments($hcp_id);
	public function getSpecificDoctorSpecificHCPFacilityAppointmentsDetail($doctor_id, $hcp_facility_id, $hcp_id);
}
