<?php

namespace Repository\Contracts\HealthCareProvider;

use Model\AppointmentReschedule;

interface RescheduleAppointmentRequest {

	public function getRescheduleAppointmentByRescheduleAppointmentId($reschedule_appointment_id);
	public function removeRescheduleAppointment(AppointmentReschedule $reschedule_appointment);

	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequests($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityAppointmentRequests($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityAppointmentRequests($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityAppointmentRequests($hcp_id);
	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequestsDetail($doctor_id, $hcp_facility_id, $hcp_id);
}
