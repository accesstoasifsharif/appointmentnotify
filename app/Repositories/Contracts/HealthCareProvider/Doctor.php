<?php

namespace Repository\Contracts\HealthCareProvider;

use Model\Doctor as DoctorModel;

interface Doctor {

	public function getDoctorByEmail($email);
	public function getDoctorByDoctorId($doctor_id);
	public function addOrUpdateDoctorInfo(DoctorModel $doctor);
	public function addOrUpdateDoctor($doctor, $dataArray);
	public function removeDoctor(DoctorModel $doctor);

	public function getDoctorBySearchQuery($query);

}
