<?php

namespace Repository\Contracts\HealthCareProvider;
use Model\HealthCareProvider;
use Model\Staff as StaffModel;

interface Staff {
	public function getStaffByHCP(HealthCareProvider $hcp);
	public function createStaff(HealthCareProvider $hcp);
	public function getStaffByStaffId($staff_id);
	public function addOrUpdateStaffInfo(StaffModel $staff);
	public function removeStaff(StaffModel $staff);

	public function getStaffBySearchQuery($query);

}
