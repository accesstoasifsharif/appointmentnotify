<?php

namespace Repository\Contracts\HealthCareProvider;
use Model\HealthCareProviderDoctor;

interface HCPDoctor {

	public function getHCPDoctorByDoctorAndProviderId($doctor_id, $hcp_id);
	public function getHCPDoctorByHCPDoctorId($HCPDoctor_id);
	public function addHCPDoctor(HealthCareProviderDoctor $hcp_doctor);
	public function addOrUpdateHCPDoctor($hcp_doctor_array_data);
	public function deleteHCPDoctor(HealthCareProviderDoctor $hcp_doctor);

	public function getAllHCPDoctorsByHCPId($hcp_id);
	public function getAllHCPDoctorsOfSpecificHCPFacilityOfHCP($hcp_id, $hcp_facility_id);
	public function getAllHCPDoctorsOfHCPBySearchQuery($hcp_id, $query);
	public function getAllHCPDoctorsOfSpecificHCPFacilityOfHCPBySearchQuery($hcp_id, $query, $hcp_facility_id);

}
