<?php

namespace Repository\Contracts\HealthCareProvider;
use Model\HealthCareProvider;

interface Hospital {
	public function getHospitalByHCP(HealthCareProvider $hcp);
	public function createHospital(HealthCareProvider $hcp);

}
