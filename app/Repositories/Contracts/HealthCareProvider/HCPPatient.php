<?php

namespace Repository\Contracts\HealthCareProvider;

interface HCPPatient {

	public function getAllHCPDoctorPatients($hcp_id, $hcp_doctor_id);
	public function getPatientDetail($patient_id);
}
