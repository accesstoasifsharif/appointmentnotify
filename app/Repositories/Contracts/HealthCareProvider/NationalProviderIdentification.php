<?php

namespace Repository\Contracts\HealthCareProvider;

interface NationalProviderIdentification {

	public function addOrUpdateNPI($relation, $npi);
	public function removeNPI($relation);
	public function getNPIDetails($relation);

}
