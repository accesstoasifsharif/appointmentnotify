<?php

namespace Repository\Contracts\HealthCareProvider;
use Model\HealthCareProvider;

interface Department {
	public function getDepartmentByHCP(HealthCareProvider $hcp);
	public function createDepartment(HealthCareProvider $hcp);

}
