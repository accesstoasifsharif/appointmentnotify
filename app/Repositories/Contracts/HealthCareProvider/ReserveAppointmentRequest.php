<?php

namespace Repository\Contracts\HealthCareProvider;

use Model\AppointmentReservation;

interface ReserveAppointmentRequest {

	public function getReserveAppointmentByReserveAppointmentId($reserve_appointment_id);
	public function removeReserveAppointment(AppointmentReservation $reserve_appointment);

	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequests($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityAppointmentRequests($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityAppointmentRequests($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityAppointmentRequests($hcp_id);
	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequestsDetail($doctor_id, $hcp_facility_id, $hcp_id);
}
