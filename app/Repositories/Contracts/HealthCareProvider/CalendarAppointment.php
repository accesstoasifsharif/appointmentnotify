<?php

namespace Repository\Contracts\HealthCareProvider;

interface CalendarAppointment {

	public function getSpecificDoctorSpecificHCPFacilityUpcomingAppointmentsMonthView($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityUpcomingAppointmentsMonthView($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityUpcomingAppointmentsMonthView($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityUpcomingAppointmentsMonthView($hcp_id);

	public function getSpecificDoctorSpecificHCPFacilityPastAppointmentsMonthView($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityPastAppointmentsMonthView($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityPastAppointmentsMonthView($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityPastAppointmentsMonthView($hcp_id);

	public function getSpecificDoctorSpecificHCPFacilityRescheduleAppointmentsMonthView($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityRescheduleAppointmentsMonthView($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityRescheduleAppointmentsMonthView($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityRescheduleAppointmentsMonthView($hcp_id);

	public function getSpecificDoctorSpecificHCPFacilityUpcomingAppointmentsDayView($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityUpcomingAppointmentsDayView($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityUpcomingAppointmentsDayView($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityUpcomingAppointmentsDayView($hcp_id);

	public function getSpecificDoctorSpecificHCPFacilityPastAppointmentsDayView($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityPastAppointmentsDayView($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityPastAppointmentsDayView($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityPastAppointmentsDayView($hcp_id);

	public function getSpecificDoctorSpecificHCPFacilityRescheduleAppointmentsDayView($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityRescheduleAppointmentsDayView($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityRescheduleAppointmentsDayView($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityRescheduleAppointmentsDayView($hcp_id);

	public function getDoctorHCPFacilityDateUpcomingAppointmentsDetailPreview($doctor_id, $date);
	public function getDoctorHCPFacilityDatePastAppointmentsDetailPreview($doctor_id, $date);
	public function getDoctorHCPFacilityDateRescheduleAppointmentsDetailPreview($doctor_id, $date);

	public function getDoctorHCPFacilityUpcomingAppointmentDetailPreview($an_event_id);
	public function getDoctorHCPFacilityPastAppointmentDetailPreview($an_event_id);
	public function getDoctorHCPFacilityRescheduleAppointmentDetailPreview($an_event_id);

}
