<?php

namespace Repository\Contracts\HealthCareProvider;
use Model\HealthCareProvider;

interface MedicalClinic {
	public function getMedicalClinicByHCP(HealthCareProvider $hcp);
	public function createMedicalClinic(HealthCareProvider $hcp);

}
