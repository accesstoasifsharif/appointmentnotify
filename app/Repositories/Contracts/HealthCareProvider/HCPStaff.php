<?php

namespace Repository\Contracts\HealthCareProvider;

use Model\HealthCareProviderStaff;

interface HCPStaff {

	public function getHCPStaffByStaffAndProviderId($staff_id, $hcp_id);
	public function getHCPStaffByHCPStaffId($HCPStaff_id);
	public function addHCPStaff(HealthCareProviderStaff $hcp_staff);
	public function deleteHCPStaff(HealthCareProviderStaff $hcp_staff);

	public function getAllHCPStaffsByHCPId($hcp_id);
	public function getAllHCPStaffsOfSpecificHCPFacilityOfHCP($hcp_id, $hcp_facility_id);
	public function getAllHCPStaffsOfHCPBySearchQuery($hcp_id, $query);
	public function getAllHCPStaffsOfSpecificHCPFacilityOfHCPBySearchQuery($hcp_id, $query, $hcp_facility_id);

}
