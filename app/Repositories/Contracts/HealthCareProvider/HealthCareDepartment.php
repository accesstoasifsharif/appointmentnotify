<?php

namespace Repository\Contracts\HealthCareProvider;

interface HealthCareDepartment {

	public function getAllHCDepartments($hcp_id);
	public function getAllHCDepartmentsBySearchQuery($query, $hcp_id);

}
