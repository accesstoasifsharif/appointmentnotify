<?php

namespace Repository\Contracts\HealthCareProvider;
use App\User;

interface HealthCareProvider {
	public function getHealthCareProviderByUser(User $user);
	public function getUserByHealthCareProvider($hcp);
	public function createHealthCareProvider(User $user, $providerType);
	public function addHCPPerspectiveLevel($user_id, $hcp_id, $level_id);

}
