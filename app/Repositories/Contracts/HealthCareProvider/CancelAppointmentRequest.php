<?php

namespace Repository\Contracts\HealthCareProvider;

interface CancelAppointmentRequest {

	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequests($doctor_id, $hcp_facility_id, $hcp_id);
	public function getSpecificDoctorAllHCPFacilityAppointmentRequests($doctor_id, $hcp_id);
	public function getAllDoctorSpecificHCPFacilityAppointmentRequests($hcp_facility_id, $hcp_id);
	public function getAllDoctorAllHCPFacilityAppointmentRequests($hcp_id);
	public function getSpecificDoctorSpecificHCPFacilityAppointmentRequestsDetail($doctor_id, $hcp_facility_id, $hcp_id);

}
