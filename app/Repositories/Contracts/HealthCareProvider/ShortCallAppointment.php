<?php

namespace Repository\Contracts\HealthCareProvider;

use Model\ShortCallAppointment as ShortCallAppointmentModel;

interface ShortCallAppointment {
	public function getShortCallAppointmentByShortCallAppointmentId($short_call_appointment_id);
	public function addOrUpdateShortCallAppointment(ShortCallAppointmentModel $short_call_appointment);
	public function removeShortCallAppointment(ShortCallAppointmentModel $short_call_appointment);
}
