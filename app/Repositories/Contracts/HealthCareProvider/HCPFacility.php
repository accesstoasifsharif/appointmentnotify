<?php

namespace Repository\Contracts\HealthCareProvider;

use Model\HealthCareProviderFacility;

interface HCPFacility {

	public function getHCPFacilityByFacilityAndProviderId($facility_id, $hcp_id);
	public function getHCPFacilityByHCPFacilityId($HCPfacility_id);
	public function addHCPFacility(HealthCareProviderFacility $hcp_facility);
	public function addOrUpdateHCPFacility($hcp_facility_data_array);
	public function deleteHCPFacility(HealthCareProviderFacility $hcp_facility);

	public function getAllHCPFacilities($hcp_id);
	public function getAllHCPFacilitiesBySearchQuery($query);

}
