<?php

namespace Repository\Contracts\SiteUser;
use App\User;

interface SiteUser {

	public function getSiteUser(User $user);

}
