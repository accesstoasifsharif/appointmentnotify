<?php

namespace Repository\Contracts\SiteUser;

interface AppointmentTimeLine {

	public function getYearListOfAppointments($site_user);
	public function getYearMonthListOfAppointments($year, $site_user);
	public function getAppointmentListOfMonthOfYear($year, $month, $site_user);

}
