<?php

namespace Repository\Contracts\SiteUser;

interface UpcomingAppointment {

	public function getAllUpComingAppointemntsBySiteUserId($siteUser_id);
	public function allDistinctUpcomingAppointmentsBySiteUserId($siteUser_id);
	public function getUpcomingAppointmentDetailByDoctorId($id, $siteUser_id);
	/*public function getNoteByAppointmentId($appointment_id);*/

}
