<?php

namespace Repository\Contracts\SiteUser;

interface SiteUserCircledProvider {

	public function getAllSiteUserCircledProvider($site_user_id);
	public function getAllVerifiedSiteUserCircledProvider($site_user_id);
}
