<?php

namespace Repository\Contracts\SiteUser;

interface ReservedAppointments {
	public function allDistinctReservedAppointmentsBySiteUserId($siteUser_id);
	public function getAggregatedReservedAppointmentsBySiteUserId($siteUser_id);
	public function getAllReservedAppointmentsDetailByDoctorId($doctorId, $siteUser_id);
}