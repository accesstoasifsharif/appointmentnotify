<?php

namespace Repository\Contracts\SiteUser;

interface EmergencyContact {
	public function addOrUpdateEmergencyContact($data_array, $siteUserId);

}
