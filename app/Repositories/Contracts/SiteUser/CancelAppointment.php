<?php

namespace Repository\Contracts\SiteUser;

interface CancelAppointment {
	public function cancelUpComingAppointemnt($appointment_id);
	public function getAllCancelledAppointmentBySiteUserId($siteUser_id);
	public function getAllCancelledAppointmentsDetailByDoctorId($doctor_id, $siteUser_id);
	public function revokeCancelledAppointment($appointment_id);

}
