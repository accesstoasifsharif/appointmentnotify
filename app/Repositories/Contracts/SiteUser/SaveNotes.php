<?php

namespace Repository\Contracts\SiteUser;

interface SaveNotes {

	public function saveNote($data_array);
	public function getNoteByAppointmentId($appointment_id);

}
