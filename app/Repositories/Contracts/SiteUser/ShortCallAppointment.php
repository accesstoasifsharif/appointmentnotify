<?php

namespace Repository\Contracts\SiteUser;

interface ShortCallAppointment {
	public function getAggregatedShortCallAppointmentsBySiteUserId($siteUser_id);
	public function getShortCallAppointmentDetailByDoctorId($doctor_id, $siteUser_id);

}