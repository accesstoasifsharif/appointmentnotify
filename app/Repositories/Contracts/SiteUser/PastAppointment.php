<?php

namespace Repository\Contracts\SiteUser;

interface PastAppointment {

	public function getPastAppointmentDetailByDoctorId($doctor_id, $siteUser_id);
	public function getAllPastAppointemntsBySiteUserId($siteUser_id);
	public function allDistinctPastAppointmentsBySiteUserId($siteUser_id);

	/*public function getNoteByAppointmentId($appointment_id);*/

}
