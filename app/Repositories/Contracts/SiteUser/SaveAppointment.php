<?php

namespace Repository\Contracts\SiteUser;

interface SaveAppointment {

	public function saveAppointment($data_array, $siteUser_id);

}