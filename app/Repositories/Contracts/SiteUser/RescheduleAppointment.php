<?php

namespace Repository\Contracts\SiteUser;

interface RescheduleAppointment {
	public function saveRescheduleTime($data_array);
	public function getAggregatedRescheduledAppointmentBySiteUserId(
		$siteUser_id);
	public function getRescheduledAppointmentDetailByDoctorId($doctorId, $siteUser_id);

}