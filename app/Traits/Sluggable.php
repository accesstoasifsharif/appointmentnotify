<?php

namespace App\Http\Traits;


trait Sluggable
{

    protected $config = ['source' => null, 'slug_column' => 'slug','separator' => '-','key_column' => 'id', 'unique' => true,];

    protected static $primary_key = 'id';


    abstract public function sluggable($array);

    public static function findBySlugOrId($slug)
    {   
        $class = get_class();
        $model = new $class;
        return $model::whereSlug($slug)->orWhere(self::$primary_key,$slug)->first();
    }




    public function save(array $options = [])
    {
        $this->slug();
        parent::save($options);
    }

    public static function create(array $attributes = [])
    {
        return (new static)->newQuery()->create($attributes);
    }

    public function slug()
    {   
        $this->config = $this->getConfiguration($this->sluggable());
        $this->sluggify();
    }

    public function getConfiguration(array $overrides = [])
    {
        static $defaultConfig = null;
        if ($defaultConfig === null) {
            $defaultConfig = $this->config;
        }
        return array_merge($defaultConfig, $overrides);
    }


    protected function sluggify()
    {
        $attribs = $this->getSluggableAttributes();
        $var = implode($attribs, ',');
        $source_column = str_replace(',', ' ', $var);
        $slug = $this->generateSlug($source_column);
        $this->saveSlug($slug);
    }

    public function getSluggableAttributes()
    {   
        $slug_attributes = [];
        for ($i=0; $i < count($this->config['source']); $i++) { 
            array_push($slug_attributes, $this->attributes[$this->config['source'][$i]]);
        }
        return $slug_attributes; 

    }

    protected function generateSlug($value)
    {

        $temp = str_slug($value, $this->config['separator']);
        if ($this->config['unique']) {
            return $this->generateUniqueSlug($temp);
        } else {
            return $temp;
        }

    }


    protected function generateUniqueSlug($temp)
    {
        if(!$this->all()->where('slug',$temp)->isEmpty()){
            $i = 1;
            $newslug = $temp . $this->config['separator'] . $i;
            while(!$this->all()->where('slug',$newslug)->isEmpty()){
                $i++;
                $newslug = $temp . $this->config['separator'] . $i;
            }
            $temp =  $newslug;
        }
        return $temp;
    }


    public function saveSlug($slug)
    {
        $this->attributes[$this->config['slug_column']] = $slug;
    }

}

