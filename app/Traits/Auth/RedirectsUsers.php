<?php

namespace App\Http\Traits\Auth;


trait RedirectsUsers
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    
/*    public function redirectPathHealthCareProvider()
    {
        return property_exists($this, 'redirectToHealthCareProviderLogin')? $this->redirectToHealthCareProviderLogin : '/healthcareprovider';
    }

    public function redirectPathSiteUser()
    {
        return property_exists($this, 'redirectToSiteUserLogin') ? $this->redirectToSiteUserLogin : '/siteuser';
    }

    */

    public function redirectPath()
    {
        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }

}

