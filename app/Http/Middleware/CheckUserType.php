<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Facades\Service\Common\UserContext;

class CheckUserType {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \the user type allowed by the caller controller  $userTypeAllowd
	@param  \ the user type of athunticated user $authUserType
	 * @return mixed
	 */
	public function handle($request, Closure $next, $userContext) {

		if (Auth::user()->user_type != UserContext::getUserTypeIdByName($userContext)) {

			return redirect()->route(UserContext::getRedirectUrlByUserTypeId(Auth::user()->user_type));
		}

		return $next($request);
	}
}
