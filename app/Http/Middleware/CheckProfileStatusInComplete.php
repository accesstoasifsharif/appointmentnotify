<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use Facades\Service\Common\UserContext;
class CheckProfileStatusInComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,  $userContext)
    {

        if(Auth::user()->profile_status ==  UserContext::statusComplete()  )
        {  
            return redirect()->back();
        } 

        return $next($request);
    }
}
