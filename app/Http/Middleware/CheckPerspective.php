<?php

namespace App\Http\Middleware;

use Closure;

class CheckPerspective {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {

		$perspective = $request->input('perspective_slug');
		if ($request->ajax()) {
			if (!$perspective) {
				return response()->json([
					'errors' => 'Cookie Not found',
				], 555);
			}
		}
		return $next($request);
	}
}
