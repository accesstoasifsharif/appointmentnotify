<?php

namespace Home\ApiControllers;
use App\Http\Controllers\Controller;
use Auth;
use Facades\Service\Common\CityZipCodeLocationService;
use Facades\Service\Home\HomeAppointmentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

// use Facades\Service\Home\HomeAppointmentService;
class HomeAppointmentsController extends Controller {

	public function __construct() {
		$this->middleware('auth:api', ['only' => ['bookAnAppointment']]);
	}

	public function getAllSpecialtyList() {
		return HomeAppointmentService::getAllSpecialtyList();
	}

	protected function getNearestZipCode($zip_code, $distance) {
		// return Auth::User();
		if ($zip_code == "") {
			if (Auth::User() == null) {
				$zip_code = 10314;
			} else {
				$zip_code = Auth::User()->siteUser->address->zip_code;
			}
			return CityZipCodeLocationService::getNearestZipCodeList($zip_code, $distance);
		} else {
			return CityZipCodeLocationService::getNearestZipCodeList($zip_code, $distance);
		}
	}
	public function showAppointmentsForProviderView(Request $request) {
		$search_query = $request->search_query;
		$zip_code = $request->zip_code;
		$distance = $request->distance;
		$speciality_id = $request->specialty_id;
		$selectGender = $request->gender;
		$page = $request->page;
		$doctor_id = $request->doctor_id;
		$zip_list = $this->getNearestZipCode($zip_code, $distance);
		return HomeAppointmentService::showAppointmentsForProviderView($search_query, $speciality_id, $selectGender, $zip_list, $page, $doctor_id);
		return CityZipCodeLocationService::getNearestZipCodeList();
	}
	public function showAppointmentsForFacilityView(Request $request) {
		$search_query = $request->search_query;
		$zip_code = $request->zip_code;
		$distance = $request->distance;
		$specialty_id = $request->specialty_id;
		$page_no = $request->page;
		$doctor_id = $request->doctor_id;
		$gender = $request->gender;
		$facility_id = $request->facility_id;

		if ($page_no != '') {$page = $page_no;} else { $page = 1;}
		$zip_list = $this->getNearestZipCode($zip_code, $distance);
		return HomeAppointmentService::showAppointmentsForFacilityView($search_query, $zip_list, $specialty_id, $page, $doctor_id, $gender, $facility_id);
	}
	public function showAppointmentsForFacilityViewWithGender(Request $request) {
		$gender = $request->gender;
		$facility_id = $request->facility_id;
		return HomeAppointmentService::showAppointmentsForFacilityViewWithGender($gender, $facility_id);
	}

	public function checkAppointmentExistanceByAppointmentId($appointment_id) {
		return HomeAppointmentService::checkAppointmentExistanceByAppointmentId($appointment_id);
	}
	public function bookAnAppointment($appointment_id) {
		$user = Auth::user();
		return HomeAppointmentService::bookAnAppointment($appointment_id, $user->siteUser->id);
	}
	public function getSingleDoctorFacilitiesByDoctorId($doctor_id) {
		$app_status = Config::get(Config_File_Name . '.appointment_status.Public');
		$app_current_state = Config::get(Config_File_Name . '.appointment_current_state.Publish');
		$facility_list = DB::select("select
			health_care_facilities.id as unique_facility_id,
			health_care_facilities.facility_name,
			health_care_facilities.latitude,
			health_care_facilities.longitude
			from an_events
			join health_care_facilities on health_care_facilities.id = an_events.health_care_facility_id
			where an_events.doctor_id = " . $doctor_id . "
			And an_events.appointment_current_state like $app_current_state
			And an_events.appointment_status like $app_status
			group by an_events.health_care_facility_id
			");
		return $facility_list;

	}

}
