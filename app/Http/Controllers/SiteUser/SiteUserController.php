<?php

namespace SiteUser;
use App\Http\Controllers\BaseController;
use Auth;
use Facades\App\Http\ModelValidator;
use Facades\Service\Home\HomeAppointmentService;
use Illuminate\Http\Request;
use Model\DummyAvatar;
use Session;

class SiteUserController extends BaseController {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {

		parent::__construct(SiteUser);

	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($id = null) {
		return redirect('/siteuser/' . Auth::User()->slug . '/dashboard');
	}

	public function dashboard(Request $request, $id) {
		// return Cookie::get('appointment_book_id');
		// Cookie::make('name', $id, 60);
		// return Cookie::get('name');
		// dd(Cookie::get('appointment_book_id'));
		$appointment_id = Session::get('appointment_id');
		if (isset($appointment_id)) {
			$user = Auth::user();
			HomeAppointmentService::bookAnAppointment($appointment_id, $user->siteUser->id);
			Session::forget('appointment_id');
			return redirect('/');
		} else {
			$selected_perspective_slug = $id;
			// $features = collect(config('appointmentnotify.site_user_features'));
			$features = config('appointmentnotify.site_user_features');
			return view('siteuser.index', compact('selected_perspective_slug', 'features'));
		}
	}

	// Healthcare Provider Wizard
	public function wizard() {
		return view('siteuser.registration_wizard');
	}

	public function storeProfile(Request $request) {
		$rules = ModelValidator::getValidationRules(['ContactInformation', 'Address', 'PersonalInformation']);

		$this->validate($request, $rules);

		return $rules;

	}

	public function dummyAvatars() {
		return DummyAvatar::where('user_type', 0)->get();
	}

	/* Provider Tab Actions (Asif Sharif) */
	public function Providers(Request $request, $id) {
		$selected_perspective_slug = $id;
		$features = config('appointmentnotify.site_user_features');

		return view('siteuser.siteuser_providers', compact('selected_perspective_slug', 'features'));
	}
	public function Calendar(Request $request, $id) {
		$selected_perspective_slug = $id;
		$features = config('appointmentnotify.site_user_features');

		return view('siteuser.siteuser_calendar', compact('selected_perspective_slug', 'features'));
	}
	public function accountSetting(Request $request, $id) {
		$selected_perspective_slug = $id;
		$features = config('appointmentnotify.site_user_features');

		return view('siteuser.siteuser_account_setting', compact('selected_perspective_slug', 'features'));
	}

	public function appointments(Request $request, $id) {
		$selected_perspective_slug = $id;
		$features = config('appointmentnotify.site_user_features');
		return view('siteuser.siteuser_appointments', compact('selected_perspective_slug', 'features'));
	}
	public function siteUserMessaging(Request $request, $id) {
		$selected_perspective_slug = $id;
		$features = config('appointmentnotify.site_user_features');

		return view('siteuser.siteuser_messaging', compact('selected_perspective_slug', 'features'));
	}
}
