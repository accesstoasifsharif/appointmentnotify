<?php

namespace SiteUser\ApiControllers;
use App\Http\Controllers\Controller;
use Auth;
use Facades\Service\Common\CityZipCodeLocationService;
use Facades\Service\Common\DataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ProviderController extends Controller {

	public function getUser() {
		return Auth::user();
	}
	protected function getNearestZipCode($zip_code, $distance) {
		if ($zip_code == "") {
			$zip_code = Auth::User()->siteUser->address->zip_code;
			return CityZipCodeLocationService::getNearestZipCodeList($zip_code, $distance);
		} else {
			return CityZipCodeLocationService::getNearestZipCodeList($zip_code, $distance);
		}
	}
	public function getAllDoctorsFromZipList($zip_array, $page) {
		// return $zip_array;
		$facility_array = $this->getFacilityListInZipList($zip_array);

		$doctor_array = $this->getDoctorListInFacilityList($facility_array, $page);

		return $doctor_array;
	}
	protected function getFacilityListInZipList($zip_array) {
		$facility_array = array();
		for ($i = 0; $i < count($zip_array); $i++) {
			$facility_list = DB::select("select health_care_facilities.id as unique_facility_id from health_care_facilities
				join addresses on addresses.addressable_id = health_care_facilities.id
				where addresses.zip_code = " . $zip_array[$i]->zip . "
				And addresses.addressable_type like '%HealthCareFacility%'
				");
			if (!empty($facility_list)) {
				array_push($facility_array, $facility_list);
			}
		}
		return $this->makeFacilityList($facility_array);
	}
	protected function makeFacilityList($facility_array) {
		$facility_attach_array = array();
		for ($i = 0; $i < count($facility_array); $i++) {
			for ($j = 0; $j < count($facility_array[$i]); $j++) {
				array_push($facility_attach_array, $facility_array[$i][$j]);
			}
		}

		$facility_final_array = array();
		foreach ($facility_attach_array as $current) {
			if (!in_array($current, $facility_final_array)) {
				$facility_final_array[] = $current;
			}
		}
		return $facility_final_array;
	}
	protected function getDoctorListInFacilityList($facility_array, $page) {
		$doctor_array = array();
		// return $facility_array;
		for ($i = 0; $i < count($facility_array); $i++) {
			$doctor_list = DB::select("select CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name ,specialties.specialty_name, doctors.id as unique_doctor_id, doctors.model_identifier as doctor_identifier from doctors

				join health_care_provider_doctors on health_care_provider_doctors.doctor_id = doctors.id
				join health_care_provider_facilities on health_care_provider_facilities.id = health_care_provider_doctors.hcp_facility_id
				join health_care_providers on health_care_providers.id = health_care_provider_facilities.health_care_provider_id
				join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
				join personal_informations on personal_informations.personalable_id = doctors.id
				join specialties on specialties.id = doctors.specialty_id
				where health_care_facilities.id = " . $facility_array[$i]->unique_facility_id . "
				And personal_informations.personalable_type like '%Doctor%'
				");
			if (!empty($doctor_list)) {
				array_push($doctor_array, $doctor_list);
			}
		}
		$doctor_final_array = $this->makeDoctorsList($doctor_array);
		$paginate = HOME_PAGE_PAGINATE;
		$offSet = ($page * $paginate) - $paginate;
		$itemsForCurrentPage = array_slice($doctor_final_array, $offSet, $paginate, true);
		$doctor_final_array = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($doctor_final_array), $paginate, $page);
		$doctor_final_array = $doctor_final_array->toArray();
		return $doctor_final_array;
	}

	protected function makeDoctorsList($doctor_array) {
		$doctor_attach_array = array();
		for ($i = 0; $i < count($doctor_array); $i++) {
			for ($j = 0; $j < count($doctor_array[$i]); $j++) {
				array_push($doctor_attach_array, $doctor_array[$i][$j]);
			}
		}

		$doctor_final_array = array();
		foreach ($doctor_attach_array as $current) {
			if (!in_array($current, $doctor_final_array)) {
				$doctor_final_array[] = $current;
			}
		}
		// return $doctor_final_array;
		return $doctor_final_array;
	}
	protected function getNearestDoctorListWithDoctorName($doctor_name, $page) {
		$doctor_list = DB::select("select  CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name ,doctors.model_identifier as doctor_identifier, specialties.specialty_name, doctors.id as unique_doctor_id,personal_informations.personalable_type from doctors
			join personal_informations on personal_informations.personalable_id = doctors.id
			join specialties on specialties.id = doctors.specialty_id
			join single_practices on single_practices.doctor_id = doctors.id
			join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
			join users on users.id = health_care_providers.user_id
			Where personal_informations.personalable_type like '%Doctor%'
			And personal_informations.first_name like '%" . $doctor_name . "%'
			OR personal_informations.last_name like '%" . $doctor_name . "%'
			OR specialties.specialty_name like '%" . $doctor_name . "%'
			");
		// return $doctor_list;
		$paginate = HOME_PAGE_PAGINATE;
		$offSet = ($page * $paginate) - $paginate;
		$itemsForCurrentPage = array_slice($doctor_list, $offSet, $paginate, true);
		$doctor_list = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($doctor_list), $paginate, $page);
		$doctor_list = $doctor_list->toArray();
		return $doctor_list;

	}

	public function providerDetail(Request $request) {
		$page = $request->page;
		$searchDoctor = $request->searchDoctor;
		$distance = $request->distance;
		$zipcode = $request->zipcode;
		$zip_list = $this->getNearestZipCode($zipcode, $distance);
		if (($searchDoctor == 'undefined' || $searchDoctor == '') && ($zipcode == 'undefined' || $zipcode == '')) {
			return $this->getAllDoctorsFromZipList($zip_list, $page);
			// return  " 0    0";
		}
		if (($searchDoctor == 'undefined' || $searchDoctor == '') && ($zipcode != 'undefined' || $zipcode != '')) {
			return $this->getAllDoctorsFromZipList($zip_list, $page);
			// return  " 0    1";
		}
		if (($searchDoctor != 'undefined' || $searchDoctor != '') && ($zipcode == 'undefined' || $zipcode == '')) {
			return $this->getNearestDoctorListWithDoctorName($searchDoctor, $page);
			// return  " 1    0";
		}

		// $default_per_page = 5;
		// $query = Input::get('query');
		// $per_page = Input::get('per_page');
		// $page = Input::get('page');
		// $per_page = ($per_page == 'undefined') ? $default_per_page : $per_page;
		// if ($query != '') {
		// 	$query_array = DB::select("select *,doctors.id from doctors
		// 		join personal_informations on personal_informations.personalable_id  = doctors.id
		// 		join specialties on doctors.specialty_id = specialties.id
		// 		where personal_informations.first_name like '%" . $query . "%'
		// 		and personal_informations.personalable_type like '%Doctor'");
		// 	$data = DataTable::getPaginate($query_array, $per_page, $page);
		// } else {
		// 	$query_array = DB::select("select *,doctors.id from doctors
		// 		join personal_informations on personal_informations.personalable_id =  doctors.id
		// 		join specialties on doctors.specialty_id = specialties.id
		// 		where personal_informations.personalable_type like '%Doctor'");
		// 	$data = DataTable::getPaginate($query_array, $per_page, $page);
		// }
		// return $data;
	}

	public function getDoctorById($doctor_id) {
		$user = Auth::user();
		$user->siteUser->doctors()->attach($doctor_id);
		return "Provider Added";
	}

	public function deleteDoctorById($doctor_id) {
		$user = Auth::user();
		$user->siteUser->doctors()->detach($doctor_id);
		return "Provider Deleted";
	}

	public function getSingleDoctorInfoById($doctor_id) {
		// $doctor = Doctor::find($doctor_id);
		$doctorDetail = DB::select("select *, doctors.id as doctor_id from doctors
			join personal_informations on personal_informations.personalable_id =  doctors.id
			join contact_informations on contact_informations.contactable_id = doctors.id
			join specialties on doctors.specialty_id = specialties.id
			where doctors.id = $doctor_id
			and personal_informations.personalable_type like '%Doctor'
			and contact_informations.contactable_type like '%Doctor'");

		// return json_encode(json_encode($doctorDetail[0], JSON_FORCE_OBJECT));
		$facilities_of_doctor = DB::select("select *, doctors.id as doctor_id from doctors
			join health_care_provider_doctors on health_care_provider_doctors.doctor_id = doctors.id
			join health_care_provider_facilities on health_care_provider_facilities.id = health_care_provider_doctors.hcp_facility_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
			join contact_informations on contact_informations.contactable_id = health_care_facilities.id
			join addresses on addresses.addressable_id = health_care_facilities.id
			where doctors.id = $doctor_id
			and contact_informations.contactable_type like '%HealthCareFacility'
			and addresses.addressable_type like '%HealthCareFacility'");
		$doctorDetailWithFacility = [
			'doctorDetail' => $doctorDetail,
			'doctorFacilityDetail' => $facilities_of_doctor];
		return $doctorDetailWithFacility;
	}

	public function getAllSiteUserProvidersBySiteUserId(Request $request) {
		$siteUser_id = Auth::user()->siteUser->id;
		$query = $request->input('query');
		$per_page = $request->input('per_page');
		$page = $request->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;

		if ($query != '') {
			$query_array = DB::select("select DISTINCT doctors.model_identifier,doctors.id, specialties.specialty_name,  CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name from doctor_site_user
				join site_users on site_users.id = doctor_site_user.site_user_id
				join doctors on doctors.id = doctor_site_user.doctor_id
				join specialties on specialties.id = doctors.specialty_id
				join personal_informations on personal_informations.personalable_id = doctors.id
				WHERE doctor_site_user.site_user_id = " . $siteUser_id . "
				and personal_informations.first_name like '%" . $query . "%'
				or specialties.specialty_name like '%" . $query . "%'
				and personal_informations.personalable_type like '%Doctor'");
		} else {
			$query_array = DB::select("select  doctors.model_identifier,doctors.id, specialties.specialty_name,  CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name from doctor_site_user
				join site_users on site_users.id = doctor_site_user.site_user_id
				join doctors on doctors.id = doctor_site_user.doctor_id
				join specialties on specialties.id = doctors.specialty_id
				join personal_informations on personal_informations.personalable_id = doctors.id
				WHERE doctor_site_user.site_user_id = " . $siteUser_id . "
				and personal_informations.personalable_type like '%Doctor'");
		}
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}
	public function allSiteUserProviders() {
		$siteUser_id = Auth::user()->siteUser->id;
		$query_array = DB::select("select  doctors.model_identifier,doctors.id, specialties.specialty_name,  CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name from site_users
			join doctor_site_user on doctor_site_user.site_user_id = site_users.id
			join doctors on doctor_site_user.doctor_id = doctors.id
			join specialties on specialties.id = doctors.specialty_id
			join personal_informations on personal_informations.personalable_id = doctors.id
			WHERE site_users.id = " . $siteUser_id . "
			and personal_informations.personalable_type like '%Doctor'");
		$doctor_list = DB::select("select  CONCAT_WS('', `first_name`,' ' ,`middle_name`,' ', `last_name`) as doctor_name ,doctors.model_identifier as doctor_identifier, specialties.specialty_name, doctors.id as unique_doctor_id,personal_informations.personalable_type from doctors
			join personal_informations on personal_informations.personalable_id = doctors.id
			join specialties on specialties.id = doctors.specialty_id
			join single_practices on single_practices.doctor_id = doctors.id
			join health_care_providers on health_care_providers.id = single_practices.health_care_provider_id
			join users on users.id = health_care_providers.user_id
			Where personal_informations.personalable_type like '%Doctor%'
			");
		// return $doctor_list;
		// return $query_array;
		$allDoctor = [
			'doctorBySiteUserId' => $query_array,
			'allDoctors' => $doctor_list];
		return $allDoctor;

	}
	public function getFacilityTimeTable($facility_id) {
		return DB::select("select *, health_care_facilities.id as facility_id from time_tables
			join health_care_facilities on health_care_facilities.id = time_tables.timetableable_id
			where health_care_facilities.id = $facility_id
			and time_tables.timetableable_type like '%HealthCareFacility'");
	}

}
