<?php

namespace SiteUser\ApiControllers;
use App\Http\Controllers\Controller;
use Auth;
use Facades\Service\SiteUser\AppointmentService;
use Illuminate\Http\Request;

class HomeController extends Controller {

	public function getAppointmentReasons() {

		return AppointmentService::getAppointmentReasons();

	}

	public function saveAppointment(Request $data_array) {
		return AppointmentService::saveAppointment($data_array, Auth::User());
	}
	public function getAllUpComingAppointemntsBySiteUserId() {
		return AppointmentService::getAllUpComingAppointemntsBySiteUserId(Auth::User());

	}
	/*asif sharif*/
	public function allDistinctUpcomingAppointmentsBySiteUserId() {
		return AppointmentService::allDistinctUpcomingAppointmentsBySiteUserId(Auth::User());
	}
	public function allDistinctPastAppointmentsBySiteUserId() {
		return AppointmentService::allDistinctPastAppointmentsBySiteUserId(Auth::User());
	}
	/*asif sharif*/
	public function getUpcomingAppointmentDetailByDoctorId($id) {
		return AppointmentService::getUpcomingAppointmentDetailByDoctorId($id, Auth::User());

	}

	public function getAllPastAppointemntsBySiteUserId() {
		return AppointmentService::getAllPastAppointemntsBySiteUserId(Auth::User());

	}
	public function getDetailOfPastAppointmentByDoctorId($id) {

		return AppointmentService::getDetailOfPastAppointmentByDoctorId($id, Auth::User());

	}
	public function getNoteByAppointmentId($appointment_id) {
		return AppointmentService::getNoteByAppointmentId($appointment_id);

	}
	public function saveNote(Request $data_array) {
		return AppointmentService::saveNote($data_array);

	}

	public function saveRescheduleTime(Request $data_array) {

		return AppointmentService::saveRescheduleTime($data_array);
	}
	public function cancelUpComingAppointemnt($appointment_id) {
		return AppointmentService::cancelUpComingAppointemnt($appointment_id);
	}
	public function getAggregatedRescheduledAppointmentBySiteUserId() {
		return AppointmentService::getAggregatedRescheduledAppointmentBySiteUserId(Auth::User());
	}
	public function getAllCancelledAppointmentBySiteUserId() {
		return AppointmentService::getAllCancelledAppointmentBySiteUserId(Auth::User());
// return "raza";
	}
	public function getRescheduledAppointmentDetailByDoctorId($doctor_id) {

		return AppointmentService::getRescheduledAppointmentDetailByDoctorId($doctor_id, Auth::User());

	}
	public function getAllCancelledAppointmentsDetailByDoctorId($doctor_id) {
		return AppointmentService::getAllCancelledAppointmentsDetailByDoctorId($doctor_id, Auth::User());
	}
	public function revokeCancelledAppointment($appointment_id) {
		return AppointmentService::revokeCancelledAppointment($appointment_id);
	}
	public function allDistinctReservedAppointmentsBySiteUserId() {
		return AppointmentService::allDistinctReservedAppointmentsBySiteUserId(Auth::User());
	}
	public function getAggregatedReservedAppointmentsBySiteUserId() {

		return AppointmentService::getAggregatedReservedAppointmentsBySiteUserId(Auth::User());
	}
	public function getAllReservedAppointmentsDetailByDoctorId($doctor_id) {
		/*return $doctor_id;*/
		return AppointmentService::getAllReservedAppointmentsDetailByDoctorId($doctor_id, Auth::User());
	}
	public function getAggregatedShortCallAppointmentsBySiteUserId() {
		/*return Auth::User();*/
		return AppointmentService::getAggregatedShortCallAppointmentsBySiteUserId(Auth::User());
	}
	public function getShortCallAppointmentDetailByDoctorId($doctor_id) {
		return AppointmentService::getShortCallAppointmentDetailByDoctorId($doctor_id, Auth::User());
	}
}
