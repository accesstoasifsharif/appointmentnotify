<?php
namespace SiteUser\ApiControllers;
use App\Events\SiteUser\AddPatientToShortCallListBySiteUser;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Model\PatientShortCall;
use Model\SiteUser;

class ShortCallListController extends Controller {
	public function index() {
		return shortCallListTime();
	}
	public function getAllDoctorsOfAuthenticatedUser() {

		$user = Auth::User();
		$site_user_id = $user->siteUser->id;

		$docs = DB::select("select  *,
			site_users.model_identifier as site_user_identifier,
			doctors.model_identifier as doctor_identifier,
			site_users.id as site_user_id,
			doctors.id as doctor_id
			from site_users
			join doctor_site_user on doctor_site_user.site_user_id = site_users.id
			join doctors on doctors.id = doctor_site_user.doctor_id
			join personal_informations  on personal_informations.personalable_id = doctors.id
			join specialties on specialties.id =  doctors.specialty_id
			join users on users.id = site_users.user_id
			where personal_informations.personalable_type like '%Doctor'
			AND site_users.id = $site_user_id");
		return $docs;

	}
	public function getFacilityByDoctorId($id) {

		$facilities_of_doctor = DB::select(" select health_care_facilities.facility_name ,health_care_facilities.id  from doctors
			join health_care_provider_doctors on health_care_provider_doctors.doctor_id = doctors.id
			join health_care_provider_facilities on health_care_provider_facilities.id = health_care_provider_doctors.hcp_facility_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.health_care_facility_id
			where doctors.id = $id
			GROUP BY facility_name,health_care_facilities.id");
		return $facilities_of_doctor;
	}
	public function saveShortCallList(Request $request) {
		$user = Auth::User();
		$site_user_id = $user->siteUser->id;
		// $site_user_email = $user->siteUser->site_user_email;
		$doctor_id = $request->input('doctor');
		$facility_id = $request->input('facility');
		$hcp_facility = DB::select("select hcp_facility_id from
			doctors
			join health_care_provider_doctors on health_care_provider_doctors.doctor_id= doctors.id
			join health_care_provider_facilities on health_care_provider_facilities.id = health_care_provider_doctors.hcp_facility_id
			join health_care_facilities on health_care_facilities.id = health_care_provider_facilities.id
			where doctors.id = " . $doctor_id . " AND health_care_facilities.id= " . $facility_id . "
			");
		$h_facility = $hcp_facility[0]->hcp_facility_id;
		$checkShortCall = PatientShortCall::where('doctor_id', '=', $doctor_id)->where('site_user_id', '=', $site_user_id)->where('health_care_provider_facility_id', '=', $h_facility)->first();

		if (isset($checkShortCall)) {
			return "Already Added!.";
		} else {
			$shortcall = new PatientShortCall();
			$shortcall->doctor_id = $doctor_id;
			$shortcall->health_care_provider_facility_id = $h_facility;
			$shortcall->site_user_id = $site_user_id;
			// $shortcall->site_user_email = $site_user_email;
			$shortcall->time_span = $request->input('time');
			$shortcall->expiry_date_time = $request->input('time');
			$shortcall->save();
			event(new AddPatientToShortCallListBySiteUser($shortcall));
			return "You Are Added To Short Call List." . $shortcall;
		}

	}
}
