<?php

namespace SiteUser\ApiControllers;
use App\Http\Controllers\Controller;
use Auth;
use Facades\Service\SiteUser\EmergencyContactService;
use Illuminate\Http\Request;

class SiteUserEmergencyContactController extends Controller {
	public function emergencyContactData() {
		// $user = Auth::User();
		// return $this->service->emergencyContactData($user->siteUser);
	}
	public function addOrUpDateEmergencyContact(Request $data_array) {

		// $user = Auth::User();
		return EmergencyContactService::addOrUpDateEmergencyContact($data_array, Auth::User());
	}
}
