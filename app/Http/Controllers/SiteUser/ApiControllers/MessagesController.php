<?php

namespace SiteUser\ApiControllers;
use App\Http\Controllers\Controller;
use Auth;
use Facades\Service\SiteUser\SiteUserProvidersConversationService;

class MessagesController extends Controller {

	public function getSiteUserProvidersConverstion() {
		return SiteUserProvidersConversationService::getSiteUserProvidersConverstion(Auth::User()->siteUser->id);
	}
}
