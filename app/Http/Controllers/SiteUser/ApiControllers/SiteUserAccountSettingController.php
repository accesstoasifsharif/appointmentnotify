<?php

namespace SiteUser\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Carbon\Carbon;
use Facades\Service\Common\ModelValidator;
use Facades\Service\Common\UserProfile as UserProfileService;
use Facades\Service\SiteUser\SiteUser as SiteUserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Model\AccountRecoveryEmail;
use Model\AccountRecoveryPhoneNumber;
use Model\AccountRecoveryText;
use Model\Address;
use Model\ContactInformation;
use Model\PersonalInformation;
use Model\SiteUser;

class SiteUserAccountSettingController extends Controller {

	public function getSiteUserProfile() {
		return UserProfileService::getProfileByRelation(SiteUserService::getSiteUserByUser(Auth::user()));
	}
	public function getSiteUserAddress() {
		return UserProfileService::getAddresseByRelation(SiteUserService::getSiteUserByUser(Auth::user()));
	}
	public function getSiteUserContactInfo() {
		return UserProfileService::getContactInfoByRelation(SiteUserService::getSiteUserByUser(Auth::user()));
	}
	public function getSiteUserPersonalInfo() {
		return UserProfileService::getPersonalInfoByRelation(SiteUserService::getSiteUserByUser(Auth::user()));
	}

	public function updateSiteUserPersonalInfo(Request $request) {

		$this->validate($request, ModelValidator::getValidationRules(
			['PersonalInformation']
		));

		return UserProfileService::updatePersonalInfo(SiteUserService::getSiteUserByUser(Auth::user()), $request);
	}

	public function updateSiteUserAddress(Request $request) {
		$this->validate($request, ModelValidator::getValidationRules(
			['Address']
		));
		return UserProfileService::updateAddress(SiteUserService::getSiteUserByUser(Auth::user()), $request);
	}
	public function updateSiteUserContactInfo(Request $request) {
		$this->validate($request, ModelValidator::getValidationRules(
			['ContactInformation']
		));
		return UserProfileService::updateContactInfo(SiteUserService::getSiteUserByUser(Auth::user()), $request);
	}
	public function saveSiteUserProfile(Request $request) {

		$this->validate($request, ModelValidator::getValidationRules(
			['PersonalInformation', 'ContactInformation', 'Address']
		));
		UserProfileService::updateProfileByRelation(SiteUserService::getSiteUserByUser(Auth::user()), $request);

		UserProfileService::updateUserProfileStatusToComplete(Auth::user());

		return ['success' => '1', 'redirectUrl' => siteUserRedirectUrl()];
	}
	public function getSiteUserInfo() {
		$authencateUserId = Auth::user()->siteUser->id;

		$getSiteUserInfo = DB::select("select *,
			personal_informations.id as personal_information_id,
			site_users.id as site_user_id,
			avatars.id as avatar_id,
			contact_informations.id as contact_information_id,
			addresses.id as addresse_id,
			addresses.state_id as state_id,
			countries.id as country_id
			from site_users
			join personal_informations on personal_informations.personalable_id = site_users.id
			join avatars on avatars.avatarable_id = site_users.id
			join contact_informations on contact_informations.contactable_id = site_users.id
			join addresses on addresses.addressable_id = site_users.id
			join states on states.id = addresses.state_id
			join countries on countries.id = states.country_id
			where site_users.id = $authencateUserId
			and personal_informations.personalable_type like '%SiteUser'
			and avatars.avatarable_type like '%SiteUser'
			and contact_informations.contactable_type like '%SiteUser'");
		return $getSiteUserInfo;
	}
	public function updateSiteUserInfo(Request $request) {

		$siteUser = Auth::user()->siteUser->id;
		$app_date = $request->date_of_birth;
		$converted_date = Carbon::parse($app_date)->format('Y-m-d');
		$siteUserPersonalInfo = PersonalInformation::find($request->input('personal_information_id'));
		$siteUserAddressInfo = Address::find($request->input('addresse_id'));
		$siteUserContactInfo = ContactInformation::find($request->input('contact_information_id'));

		$siteUserPersonalInfo->first_name = $request->first_name;
		$siteUserPersonalInfo->middle_name = $request->middle_name;
		$siteUserPersonalInfo->last_name = $request->last_name;
		$siteUserPersonalInfo->psuedo_name = $request->psuedo_name;
		$siteUserPersonalInfo->gender = $request->gender;
		$siteUserPersonalInfo->date_of_birth = $converted_date;
		$siteUserPersonalInfo->save();

		$siteUserContactInfo->phone_number = $request->phone_number;
		$siteUserContactInfo->fax_number = $request->fax_number;
		$siteUserContactInfo->email = $request->email;
		$siteUserContactInfo->save();

		$siteUserAddressInfo->address_line_1 = $request->address_line_1;
		$siteUserAddressInfo->address_line_2 = $request->address_line_2;
		$siteUserAddressInfo->city_name = $request->city_name;
		$siteUserAddressInfo->state_id = $request->state_id;
		$siteUserAddressInfo->country_id = 1;
		$siteUserAddressInfo->zip_code = $request->zip_code;
		$siteUserAddressInfo->save();
		return 200;
	}

	/*recovery setting actions*/
	public function saveRecoveryOptions(Request $request) {
		return 200;
	}
	public function getAllRecoveryOtionsByUserId() {
		// return Auth:user();

		$recoveryEmail = DB::select("select email_address as recovery_email, users.id as user_id from users
			join account_recovery_emails on account_recovery_emails.user_id = users.id
			where users.id =  " . Auth::User()->id . "");
		$recoveryPhoneNumber = DB::select("select phone_number as recovery_phone, users.id as user_id from users
			join account_recovery_phone_numbers on account_recovery_phone_numbers.user_id = users.id
			where users.id = " . Auth::User()->id . "");
		$recoveryText = DB::select("select recovery_text, users.id as user_id from users
			join account_recovery_texts on account_recovery_texts.user_id = users.id
			where users.id = " . Auth::User()->id . "");
		$recoveryOptions = [
			'recovery_email' => $recoveryEmail,
			'recovery_phone' => $recoveryPhoneNumber,
			'recovery_text' => $recoveryText,
		];
		return $recoveryOptions;

	}
	public function updateRecoveryOptions(Request $request) {
		// return $request;
		$recoveryOptions = DB::select("select *, users.id from users
			join account_recovery_emails on account_recovery_emails.user_id = users.id
			join account_recovery_phone_numbers on account_recovery_phone_numbers.user_id = users.id
			join account_recovery_texts on account_recovery_texts.user_id = users.id
			where users.id = " . Auth::User()->id . "");
		// return $recoveryOptions;
		if ($recoveryOptions) {

			DB::table('account_recovery_emails')
				->where('user_id', Auth::User()->id)
				->update([
					'email_address' => $request->recovery_email,
				]);
			DB::table('account_recovery_phone_numbers')
				->where('user_id', Auth::User()->id)
				->update([
					'phone_number' => $request->recovery_phone,
				]);
			DB::table('account_recovery_texts')
				->where('user_id', Auth::User()->id)
				->update([
					'recovery_text' => $request->recovery_text,
				]);
			return "recovery setting updated";

		} else {
			$recoveryEmail = new AccountRecoveryEmail();
			$recoveryEmail->email_address = $request->recovery_email;
			$recoveryEmail->user_id = Auth::User()->id;
			$recoveryEmail->save();

			$recoveryPhoneNumber = new AccountRecoveryPhoneNumber();
			$recoveryPhoneNumber->phone_number = $request->recovery_phone;
			$recoveryPhoneNumber->user_id = Auth::User()->id;
			$recoveryPhoneNumber->save();

			$recoveryText = new AccountRecoveryText();
			$recoveryText->recovery_text = $request->recovery_text;
			$recoveryText->user_id = Auth::User()->id;
			$recoveryText->save();

			return "recovery setting saved";
		}

	}
}
