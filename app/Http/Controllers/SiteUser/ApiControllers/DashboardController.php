<?php

namespace SiteUser\ApiControllers;
use App\Http\Controllers\Controller;
use Auth;
use Facades\Service\SiteUser\AppointmentTimeLineService;

class DashboardController extends Controller {

	public function yearListOfAppointments() {
		return AppointmentTimeLineService::getYearListOfAppointments(Auth::User()->siteUser);
	}

	public function yearMonthListOfAppointments($year) {
		return AppointmentTimeLineService::getYearMonthListOfAppointments($year, Auth::User()->siteUser);
	}

	public function appointmentListOfMonthOfYear($year, $month) {
		return AppointmentTimeLineService::getAppointmentListOfMonthOfYear($year, $month, Auth::User()->siteUser);
	}
}
