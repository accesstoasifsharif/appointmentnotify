<?php

namespace HealthCareProvider;
use App\BusinessType;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Auth;
use Carbon\Carbon;
use Facades\Service\Common\DataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Model\HealthCareProvider;
use Model\HealthCareProviderFacility;
use Session;

class HealthCareProviderController extends BaseController {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	public function __construct() {

		parent::__construct(HealthCareProvider);

	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($id = null) {
		return redirect('/healthcareprovider/' . Auth::User()->slug . '/dashboard');
	}

	public function saveOrUpdateSession($current_route_name, $id) {
		$user_by_slug = User::findBySlugOrId($id);
		$selected_perspective_slug = $user_by_slug->slug;
		$selected_perspective_name = $user_by_slug->name;
		$feature_roles = $this->findAvailableFeatures(Auth::User(), $user_by_slug);
		$my_perspectives = $this->findMyPerspectives(Auth::User(), $user_by_slug);
		$session_data = ['selected_perspective_slug' => $selected_perspective_slug, 'selected_perspective_name' => $selected_perspective_name, 'feature_roles' => $feature_roles, 'my_perspectives' => $my_perspectives, 'current_route_name' => $current_route_name];
		Session::put("session_data", $session_data);

	}

	// Healthcare Provider Wizard
	public function wizard() {
		$redirectUrl = '/';
		if (session('redirectUrl')) {
			$redirectUrl = session('redirectUrl');
		}
		//dd('session url', session('redirectUrl'));
		return view('healthcareprovider.registration_wizard', compact('redirectUrl'));

	}

	public function findAvailableFeatures($auth_user, $user_by_slug) {

		$hcp_id = $user_by_slug->healthCareProvider->id;
		$user_perspective = User::with(['perspectiveLevels' => function ($query) use ($hcp_id) {
			$query->where('health_care_provider_id', $hcp_id);
		}])->find($auth_user->id);
		$perspective = $user_perspective->perspectiveLevels->first();
		$hcp_type = HealthCareProvider::find($perspective->health_care_provider_id)->provider_type;
		$business_type = BusinessType::where('config_id', $hcp_type)->first();
		$role_level = $perspective->role_level_id;
		$feature_roles = Role::where('role_level_id', $role_level)->where('business_type_id', $business_type->id)->with('feature')->get();

		return $feature_roles;
	}

	public function findMyPerspectives($auth_user, $user_by_slug) {
		$hcp_id = $user_by_slug->healthCareProvider->id;
		$my_perspectives = User::with('perspectiveLevels')->find($auth_user->id);
		return $my_perspectives->perspectiveLevels;
	}

	public function healthCareProviderDashboard(Request $request, $id = null) {
		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");
		return view('healthcareprovider.dashboard', compact('feature_roles', 'my_perspectives', 'selected_perspective_slug', 'current_route_name', 'selected_perspective_name'));
	}

	public function healthCareProviderFacility(Request $request, $id = null) {
		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");
		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		return view('healthcareprovider.facility', compact('selected_perspective_slug', 'feature_roles', 'my_perspectives', 'current_route_name', 'selected_perspective_name'));
	}

	public function healthCareProviderHospital(Request $request, $id = null) {
		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");

		$user_by_slug = User::findBySlugOrId(Session::get("session_data.selected_perspective_slug"));

		$my_hospitals = HealthCareProvider::with('Hospital.children', 'Hospital.parent')->find($user_by_slug->healthCareProvider->id);

		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		return view('healthcareprovider.hospital', compact('selected_perspective_slug', 'current_route_name', 'feature_roles', 'my_perspectives', 'my_hospitals', 'selected_perspective_name'));
	}

	// public function healthCareProviderStoreHospital(Request $request, $id = null) {
	// 	$user = new User();
	// 	$user->email = $request->input('email');
	// 	$user->name = $request->input('hospital_name');
	// 	$user->password = bcrypt($request->input('password'));
	// 	$user->user_type = UserContext::getUserTypeIdByName($this->userContext);
	// 	$user->save();

	// 	$hcp = new HealthCareProvider();
	// 	$hcp->user_id = $user->id;
	// 	$hcp->provider_type = config('appointmentnotify.' . $this->userContext . '.type.hospital');
	// 	$hcp->save();

	// 	$user_by_slug = User::findBySlugOrId(Session::get("session_data.selected_perspective_slug"));

	// 	$hospital = new Hospital();
	// 	$hospital->hospital_name = $request->input('hospital_name');
	// 	$hospital->health_care_provider_id = $hcp->id;
	// 	$hospital->parent_id = $user_by_slug->healthCareProvider->id;
	// 	$hospital->save();

	// 	$perspective = new PerspectiveLevel();
	// 	$perspective->user_id = $user->id;
	// 	$perspective->health_care_provider_id = $hcp->id;
	// 	$perspective->role_level_id = 1;
	// 	$perspective->save();

	// 	$perspective = new PerspectiveLevel();
	// 	$perspective->user_id = Auth::User()->id;
	// 	$perspective->health_care_provider_id = $hcp->id;
	// 	$perspective->role_level_id = 1;
	// 	$perspective->save();

	// 	return redirect('/healthcareprovider/' . Session::get("session_data.selected_perspective_slug") . '/hospital');
	// }

	public function healthCareProviderDepartment(Request $request, $id) {

		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");
		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		return view('healthcareprovider.department', compact('selected_perspective_slug', 'feature_roles', 'my_perspectives', 'current_route_name', 'selected_perspective_name'));

/*		$this->saveOrUpdateSession($request->route()->getName(), $id);
$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");

$feature_roles = Session::get("session_data.feature_roles");
$my_perspectives = Session::get("session_data.my_perspectives");
$current_route_name = Session::get("session_data.current_route_name");
$user_by_slug = User::findBySlugOrId(Session::get("session_data.selected_perspective_slug"));

if ($user_by_slug->healthCareProvider->provider_type == config('appointmentnotify.' . $this->userContext . '.type.hospital')) {
$my_departments = HealthCareProvider::with('Hospital', 'hcpDepartments')->find($user_by_slug->healthCareProvider->id);

} else if ($user_by_slug->healthCareProvider->provider_type == config('appointmentnotify.' . $this->userContext . '.type.department')) {
$my_departments = HealthCareProvider::with('Department.children')->find($user_by_slug->healthCareProvider->id);
// return $my_departments;
} else if ($user_by_slug->healthCareProvider->provider_type == config('appointmentnotify.' . $this->userContext . '.type.medical_clinic')) {
$my_departments = HealthCareProvider::with('hcpDepartments.children')->find($user_by_slug->healthCareProvider->id);
} else {
return 200;
}

return view('healthcareprovider.department', compact('selected_perspective_slug', 'current_route_name', 'feature_roles', 'my_perspectives', 'my_departments'));*/
	}

	// public function healthCareProviderCreateDepartment($id = null) {
	// 	$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
	// 	$provider_type = Session::get("session_data.provider_type");
	// 	$feature_roles = Session::get("session_data.feature_roles");
	// 	$my_perspectives = Session::get("session_data.my_perspectives");

	// 	return view('healthcareprovider.createdepartment', compact('selected_perspective_slug', 'provider_type', 'feature_roles', 'my_perspectives'));
	// }

	// public function healthCareProviderStoreDepartment(Request $request, $id = null) {

	// 	$user = new User();
	// 	$user->email = $request->input('email');
	// 	$user->name = $request->input('department_name');
	// 	$user->password = bcrypt($request->input('password'));
	// 	$user->user_type = UserContext::getUserTypeIdByName($this->userContext);
	// 	$user->save();

	// 	$hcp = new HealthCareProvider();
	// 	$hcp->user_id = $user->id;
	// 	$hcp->provider_type = config('appointmentnotify.' . $this->userContext . '.type.department');
	// 	$hcp->save();

	// 	$user_by_slug = User::findBySlugOrId(Session::get("session_data.selected_perspective_slug"));

	// 	$department = new Department();
	// 	$department->department_name = $request->input('department_name');
	// 	$department->health_care_provider_id = $hcp->id;

	// 	if ($user_by_slug->healthCareProvider->provider_type == config('appointmentnotify.' . $this->userContext . '.type.department')) {
	// 		$department->parent_id = $user_by_slug->healthCareProvider->Department->id;
	// 	} else {
	// 		$department->parent_id = 0;
	// 		$department->other_hcp_id = $user_by_slug->healthCareProvider->id;
	// 	}

	// 	$department->save();

	// 	$perspective = new PerspectiveLevel();
	// 	$perspective->user_id = $user->id;
	// 	$perspective->health_care_provider_id = $hcp->id;
	// 	$perspective->role_level_id = 1;
	// 	$perspective->save();

	// 	if ($user_by_slug->healthCareProvider->provider_type == config('appointmentnotify.' . $this->userContext . '.type.department')) {
	// 		$perspective = new PerspectiveLevel();
	// 		$perspective->user_id = Auth::User()->id;
	// 		$perspective->health_care_provider_id = $hcp->id;
	// 		$perspective->role_level_id = 2;
	// 		$perspective->save();
	// 	}

	// 	return redirect('/healthcareprovider/' . Session::get("session_data.selected_perspective_slug") . '/department');
	// }

	public function healthCareProviderStaff(Request $request, $id) {

		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");

		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");
		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		return view('healthcareprovider.staff', compact('selected_perspective_slug', 'feature_roles', 'my_perspectives', 'current_route_name', 'selected_perspective_name'));
	}

	public function healthCareProviderAppointment(Request $request, $id) {
		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");
		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		return view('healthcareprovider.appointment', compact('selected_perspective_slug', 'feature_roles', 'my_perspectives', 'current_route_name', 'selected_perspective_name'));
	}

	public function healthCareProviderCalendar(Request $request, $id) {
		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");
		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		return view('healthcareprovider.calender', compact('selected_perspective_slug', 'feature_roles', 'my_perspectives', 'current_route_name', 'selected_perspective_name'));
	}

	public function healthCareProviderMessage(Request $request, $id) {
		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");
		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		return view('healthcareprovider.message', compact('selected_perspective_slug', 'feature_roles', 'my_perspectives', 'current_route_name', 'selected_perspective_name'));
	}

	public function healthCareProviderPatient(Request $request, $id) {
		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");
		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		return view('healthcareprovider.patient', compact('selected_perspective_slug', 'feature_roles', 'my_perspectives', 'current_route_name', 'selected_perspective_name'));
	}

	public function allUsers() {
		$query = Input::get('query');
		$per_page = Input::get('per_page');
		$page = Input::get('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;

		/* this block of code will run if you want to fetch data by Model facade query */

		if ($query != '') {
			/*condition applied if search query passed from datatable*/
			$data = User::where('name', 'LIKE', "%$query%")->paginate($per_page);
		} else {
			$data = User::paginate($per_page);
		}

		/* this block of code will run if you want to fetch data by custom data base query */
		if ($query != '') {
			/*condition applied if search query passed from datatable*/
			$query_array = DB::select("select * from users where users.name like '%" . $query . "%'");
			$data = DataTable::getPaginate($query_array, $per_page, $page);
		} else {
			$query_array = DB::select("select * from users");
			$data = DataTable::getPaginate($query_array, $per_page, $page);
		}

		return $data;
	}

	public function healthCareProviderAccountSettings(Request $request, $id) {
		$this->saveOrUpdateSession($request->route()->getName(), $id);
		$selected_perspective_slug = Session::get("session_data.selected_perspective_slug");
		$feature_roles = Session::get("session_data.feature_roles");
		$my_perspectives = Session::get("session_data.my_perspectives");
		$current_route_name = Session::get("session_data.current_route_name");
		$selected_perspective_name = Session::get("session_data.selected_perspective_name");
		return view('healthcareprovider.account_setting_' . getProviderTypeByHCPTypeId(Auth::user()->healthCareProvider->provider_type)

			, compact('selected_perspective_slug', 'feature_roles', 'my_perspectives', 'current_route_name', 'selected_perspective_name')
		);
	}
	public function timeConvert() {
		$time = '11:53 PM';
		$convert = Carbon::parse($time)->format('H:i:s');
		return $time . "......to......" . $convert;
	}

}
