<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Facades\Service\HealthCareProvider\PatientService;
use Illuminate\Http\Request;

class PatientController extends Controller {
	public function getCurrentHealthCareProvider($perspective) {
		$hcp = User::findBySlugOrId($perspective)->healthCareProvider;
		return $hcp;
	}

	public function getAllPatientsSpecificPerspective(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));

		return PatientService::getAllPatientsSpecificPerspective($hcp->id, $request);
	}
	public function allPatientsByDoctorId(Request $request) {
		$doctorId = $request->input('doctor_id');
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return PatientService::allPatientsByDoctorId($doctorId, $hcp->id, $request);
	}
}
