<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Facades\Service\Common\ModelValidator;
use Facades\Service\Common\UserProfile as UserProfileService;
use Facades\Service\HealthCareProvider\HealthCareProvider as HCPService;
use Facades\Service\HealthCareProvider\HealthCareProviderProfile as HCPProfileService;
use Illuminate\Http\Request;

class MedicalClinicProfileController extends Controller {

	public function saveOrUpdateMCProfile(Request $request) {

		$this->validate($request, ModelValidator::getValidationRules(
			['NationalProviderIdentification', 'Address', 'EmployerIdentificationNumber', 'ProviderOrganizationName', 'ContactPerson', 'ContactInformation']
		));
		$medicalClinic = HCPService::getMedicalClinicByUser(Auth::user());

		$medicalClinic = HCPProfileService::saveOrUpdateMedicalClinicProfileByRelation($medicalClinic, $request->all());

		UserProfileService::updateUserProfileStatusToComplete(Auth::user());

		return ['success' => '1', 'redirectUrl' => healthCareProviderRedirectUrl()];
	}
}
