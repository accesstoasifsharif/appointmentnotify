<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Facades\Service\HealthCareProvider\CalendarService;
use Illuminate\Http\Request;

class HealthCareProviderCalendarController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function getCurrentHealthCareProvider($perspective) {
		$hcp = User::findBySlugOrId($perspective)->healthCareProvider;
		return $hcp;
	}

	public function getAllAppointments(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return CalendarService::getAllAppointmentsForCalendar($request, $hcp->id);
	}

	public function getDetailPreviewOfMonthViewAppointments(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return CalendarService::getDetailPreviewOfMonthViewAppointments($request, $hcp->id);
	}
	public function getDetailPreviewOfDayViewAppointment(Request $request) {
		return CalendarService::getDetailPreviewOfDayViewAppointment($request);
	}

}
