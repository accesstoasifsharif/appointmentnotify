<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Facades\Service\HealthCareProvider\HCDepartmentService;
use Illuminate\Http\Request;

class HealthCareDepartmentController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function getCurrentHealthCareProvider($perspective) {
		$hcp = User::findBySlugOrId($perspective)->healthCareProvider;
		return $hcp;
	}

	public function getAllHCDepartments(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return HCDepartmentService::getAllHCDepartmentsByHCPId($request, $hcp->id);
	}

}
