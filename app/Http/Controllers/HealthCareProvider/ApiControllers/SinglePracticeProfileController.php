<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Facades\Service\Common\ModelValidator;
use Facades\Service\Common\UserProfile as UserProfileService;
use Facades\Service\HealthCareProvider\DoctorService;
use Facades\Service\HealthCareProvider\HCPDoctorAndStaffService;
use Facades\Service\HealthCareProvider\HCPFacilityService;
use Facades\Service\HealthCareProvider\HealthCareProvider as HCPService;
use Facades\Service\HealthCareProvider\HealthCareProviderProfile as HCPProfileService;
use Illuminate\Http\Request;

class SinglePracticeProfileController extends Controller {

	public function saveOrUpdateSPProfile(Request $request) {

		$this->validate($request, ModelValidator::getValidationRules(
			['NationalProviderIdentification', 'Address', 'PersonalInformation', 'ContactInformation', 'Doctor', 'HealthCareFacility']
		));
		//return Auth::user();
		//return Auth::user()->healthCareProvider;
		$doctor = DoctorService::addOrUpdateDoctor(DoctorService::getDoctorByEmails(array_merge($request->all(), ['user_email' => Auth::user()->email])), $request->all());

		//return 'hello' . $doctor;
		$singlePractice = HCPService::getSinglePracticeByUser(Auth::user(), $doctor);

		HCPProfileService::saveOrUpdateSinglePracticeProfileByRelation($singlePractice, $request->all());
		//return HCPService::getHealthCareProviderByUser(Auth::user());
		$hcpFacility = HCPFacilityService::addOrUpdateHealthCareProviderFacility(HCPService::getHealthCareProviderByUser(Auth::user()), $request->all());

		HCPDoctorAndStaffService::addOrUpdateHCPDoctor($hcpFacility, $doctor);

		UserProfileService::updateUserProfileStatusToComplete(Auth::user());

		return ['success' => '1', 'redirectUrl' => healthCareProviderRedirectUrl()];
	}
}
