<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use Facades\Service\HealthCareProvider\AppointmentAndRequestOperationService;
use Illuminate\Http\Request;

class AppointmentAndRequestOperationController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	// public function getCurrentHealthCareProvider($perspective) {
	// 	$hcp = User::findBySlugOrId($perspective)->healthCareProvider;
	// 	return $hcp;
	// }

	public function rescheduleExistingAnEvent(Request $request) {
		return AppointmentAndRequestOperationService::rescheduleExistingAnEvent($request);
	}

	public function reserveRequestApproval(Request $request) {
		return AppointmentAndRequestOperationService::reserveRequestApproval($request);
	}

	public function reserveRequestReschedule(Request $request) {
		return AppointmentAndRequestOperationService::reserveRequestReschedule($request);
	}

	public function rescheduleRequestApproval(Request $request) {
		return AppointmentAndRequestOperationService::rescheduleRequestApproval($request);
	}

	public function rescheduleRequestReschedule(Request $request) {
		return AppointmentAndRequestOperationService::rescheduleRequestReschedule($request);
	}

	public function publishNewAppointment(Request $request) {
		return AppointmentAndRequestOperationService::publishNewAppointment($request);
	}

	public function publishedAppointmentReschedule(Request $request) {
		return AppointmentAndRequestOperationService::publishedAppointmentReschedule($request);
	}

	public function publishedAppointmentCancel(Request $request) {
		return AppointmentAndRequestOperationService::publishedAppointmentCancel($request);
	}

	public function publishedAppointmentPatientAssign(Request $request) {
		return AppointmentAndRequestOperationService::publishedAppointmentPatientAssign($request);
	}

	public function publishedAppointmentToShortCallList(Request $request) {
		return AppointmentAndRequestOperationService::publishedAppointmentToShortCallList($request);
	}

	public function rescheduleRequestCancel(Request $request) {
		return AppointmentAndRequestOperationService::rescheduleRequestCancel($request);
	}

	public function dumpCanceledAppointment(Request $request) {
		return AppointmentAndRequestOperationService::dumpCanceledAppointment($request);
	}

	public function publishCanceledAppointment(Request $request) {
		return AppointmentAndRequestOperationService::publishCanceledAppointment($request);
	}

	public function putCanceledAppointmentToShortCallPool(Request $request) {
		return AppointmentAndRequestOperationService::putCanceledAppointmentToShortCallPool($request);
	}
}
