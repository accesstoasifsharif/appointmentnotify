<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Facades\Service\HealthCareProvider\AppointmentService;
use Illuminate\Http\Request;

class AppointmentController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function getCurrentHealthCareProvider($perspective) {
		$hcp = User::findBySlugOrId($perspective)->healthCareProvider;
		return $hcp;
	}

	public function getAllUpcomingAppointments(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentService::getAllUpcomingAppointments($request, $hcp->id);
	}
	public function getAllUpcomingAppointmentsDetail(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentService::getAllUpcomingAppointmentsDetail($request, $hcp->id);
	}

	public function getAllPublishedAppointments(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentService::getAllPublishedAppointments($request, $hcp->id);
	}
	public function getAllPublishedAppointmentsDetail(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentService::getAllPublishedAppointmentsDetail($request, $hcp->id);
	}

	public function getAllPastAppointments(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentService::getAllPastAppointments($request, $hcp->id);
	}
	public function getAllPastAppointmentsDetail(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentService::getAllPastAppointmentsDetail($request, $hcp->id);
	}

}
