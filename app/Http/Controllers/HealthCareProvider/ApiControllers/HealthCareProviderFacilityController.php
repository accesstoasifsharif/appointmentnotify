<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Facades\Service\HealthCareProvider\HCPFacilityService;
use Illuminate\Http\Request;

class HealthCareProviderFacilityController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function getCurrentHealthCareProvider($perspective) {
		$hcp = User::findBySlugOrId($perspective)->healthCareProvider;
		return $hcp;
	}

	public function getHCPFacilities(Request $request) {
		$perspective = $request->input('perspective_slug');
		$hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPFacilityService::allFacilitiesByHCPId($request, $hcp->id);
	}

	public function getHCPFacilityDetail($facility_id) {
		return HCPFacilityService::getFacilityDetailByFacilityId($facility_id);
	}

	public function storeHCPFacility(Request $request) {

		// $rules = ModelValidator::getValidationRules(['ContactInformation', 'Address']);

		// $this->validate($request, $rules);

		$perspective = $request->input('perspective_slug');
		$hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPFacilityService::storeHCPFacility($request, $hcp);
	}

	public function getHCPFacilityTimeTableDetail($facility_id = null) {
		return HCPFacilityService::getHCPFacilityTimeTableDetail($facility_id);
	}

	public function deleteHCPFacility(Request $request, $facility_id) {
		$perspective = $request->input('perspective_slug');
		$hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPFacilityService::deleteHCPFacility($facility_id, $hcp->id);
	}

	public function searchExistingFacility(Request $request) {
		$perspective = $request->input('perspective_slug');
		// $hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPFacilityService::searchExistingFacility($request);

	}

	public function getAllHCPFacilities(Request $request) {
		$perspective = $request->input('perspective_slug');
		$hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPFacilityService::getAllHCPFacilities($hcp->id);
	}

}
