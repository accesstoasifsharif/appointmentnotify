<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Facades\Service\Common\ModelValidator;
use Facades\Service\Common\UserProfile as UserProfileService;
use Facades\Service\HealthCareProvider\HealthCareProvider as HCPService;
use Facades\Service\HealthCareProvider\HealthCareProviderProfile as HCPProfileService;
use Illuminate\Http\Request;

class DepartmentProfileController extends Controller {

	public function saveOrUpdateDepartmentProfile(Request $request) {

		$this->validate($request, ModelValidator::getValidationRules(
			['NationalProviderIdentification', 'Address', 'EmployerIdentificationNumber', 'ProviderOrganizationName', 'ContactPerson', 'ContactInformation']
		));

		$department = HCPService::getDepartmentByUser(Auth::user());

		$department = HCPProfileService::saveOrUpdateDepartmentProfileByRelation($department, $request->all());

		UserProfileService::updateUserProfileStatusToComplete(Auth::user());

		return ['success' => '1', 'redirectUrl' => healthCareProviderRedirectUrl()];
	}
}
