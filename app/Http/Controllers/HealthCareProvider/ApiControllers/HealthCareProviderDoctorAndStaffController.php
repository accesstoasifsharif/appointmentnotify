<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Facades\Service\HealthCareProvider\HCPDoctorAndStaffService;
use Illuminate\Http\Request;

class HealthCareProviderDoctorAndStaffController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function getCurrentHealthCareProvider($perspective) {
		$hcp = User::findBySlugOrId($perspective)->healthCareProvider;
		return $hcp;
	}

	public function getHCPDoctorAndStaff(Request $request) {
		$perspective = $request->input('perspective_slug');
		$hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPDoctorAndStaffService::allDoctorAndStaffByHCPId($request, $hcp->id);
	}

	public function getAllHCPDoctorAndStaff(Request $request) {
		$perspective = $request->input('perspective_slug');
		$hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPDoctorAndStaffService::getAllHCPDoctorAndStaff($hcp->id);
	}

	public function deleteHCPDoctorOrStaff(Request $request) {
		$perspective = $request->input('perspective_slug');
		$hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPDoctorAndStaffService::deleteHCPDoctorOrStaff($request, $hcp->id);
	}

	// public function getHCPDoctorOrStaffTimeTableDetail(Request $request) {
	// 	$perspective = $request->input('perspective_slug');
	// 	$hcp = $this->getCurrentHealthCareProvider($perspective);
	// 	return HCPDoctorAndStaffService::getHCPDoctorOrStaffTimeTableDetail($request, $hcp->id);
	// }

	public function searchAllDoctorOrStaff(Request $request) {
		return HCPDoctorAndStaffService::getAllStaffOrDoctorBySearchQuery($request);
	}

	public function getTimeTableOfDoctorOrStaffOfHCPFacility(Request $request) {
		return HCPDoctorAndStaffService::getTimeTableOfDoctorOrStaffOfHCPFacility($request);
	}

	public function addOrUpdateHCPAllStaff(Request $request) {
		$perspective = $request->input('perspective_slug');
		$hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPDoctorAndStaffService::addOrUpdateHCPAllStaff($request, $hcp->id);
	}

	public function getSelectedDoctorStaffDetail(Request $request) {
		return HCPDoctorAndStaffService::getSelectedDoctorStaffDetail($request);
	}

	public function getAllHCPDoctorsOfHCPFacility(Request $request) {
		$perspective = $request->input('perspective_slug');
		$hcp = $this->getCurrentHealthCareProvider($perspective);
		return HCPDoctorAndStaffService::getAllHCPDoctorsOfHCPFacility($request, $hcp->id);
	}

}
