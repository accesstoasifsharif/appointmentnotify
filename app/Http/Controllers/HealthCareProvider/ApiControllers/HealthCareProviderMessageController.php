<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Facades\Service\HealthCareProvider\HCPConversationService;
use Illuminate\Http\Request;

class HealthCareProviderMessageController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function getCurrentHealthCareProvider($perspective) {
		$hcp = User::findBySlugOrId($perspective)->healthCareProvider;
		return $hcp;
	}

	public function getHCPAllStaffConversationPreview(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return HCPConversationService::getHCPAllStaffConversationPreview($request, $hcp->id, Auth::User()->id);
	}

	public function getPatientsConversationPreview(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return HCPConversationService::getPatientsConversationPreview($request, $hcp->id, Auth::User()->id);
	}

	// public function hcpConversationWithSpecificUser(Request $request) {
	// 	$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
	// 	return HCPConversationService::hcpConversationWithSpecificUser($request, $hcp->id, Auth::User()->id);
	// }

}
