<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use App\User;
use Facades\Service\HealthCareProvider\AppointmentRequestService;
use Illuminate\Http\Request;

class AppointmentRequestController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function getCurrentHealthCareProvider($perspective) {
		$hcp = User::findBySlugOrId($perspective)->healthCareProvider;
		return $hcp;
	}

	public function getAllReserveAppointmentRequests(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentRequestService::getAllReserveAppointmentRequests($request, $hcp->id);
	}
	public function getAllReserveAppointmentRequestsDetail(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentRequestService::getAllReserveAppointmentRequestsDetail($request, $hcp->id);
	}

	public function getAllRescheduleAppointmentRequests(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentRequestService::getAllRescheduleAppointmentRequests($request, $hcp->id);
	}
	public function getAllRescheduleAppointmentRequestsDetail(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentRequestService::getAllRescheduleAppointmentRequestsDetail($request, $hcp->id);
	}

	public function getAllCancelAppointmentRequests(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentRequestService::getAllCancelAppointmentRequests($request, $hcp->id);
	}
	public function getAllCancelAppointmentRequestsDetail(Request $request) {
		$hcp = $this->getCurrentHealthCareProvider($request->input('perspective_slug'));
		return AppointmentRequestService::getAllCancelAppointmentRequestsDetail($request, $hcp->id);
	}

}
