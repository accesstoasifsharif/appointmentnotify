<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use Facades\Service\HealthCareProvider\HCPPatientService;
use Illuminate\Http\Request;

class HealthCareProviderPatientController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function getHCPPatient(Request $request) {
		return HCPPatientService::getPatientDetailByPatientId($request);
	}
}
