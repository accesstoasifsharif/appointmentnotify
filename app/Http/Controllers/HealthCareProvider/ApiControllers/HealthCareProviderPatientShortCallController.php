<?php

namespace HealthCareProvider\ApiControllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Facades\Service\HealthCareProvider\PatientShortCallService;

class HealthCareProviderPatientShortCallController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function addPatientToShortCall(Request $request)
	{
		return PatientShortCallService::addPatientToShortCall($request);	
	}
}
