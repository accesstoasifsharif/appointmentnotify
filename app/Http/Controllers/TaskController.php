<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
	public function create()
	{
		return view('task.create',['tasks' => Task::all()]);
	}
	public function store()
	{
		$this->validate(request(), [
			'title' => 'required',
			'description' => 'required' 
		]);
		
		Task::forceCreate([
			'title' => request('title'),
			'description' => request('description')
		]);

		return redirect('test/task');


		// return ;
	}
}














