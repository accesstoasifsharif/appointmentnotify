<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class WebpackController extends Controller
{
	public function index()
	{
		return view ('webpack.index');
	}

	public function dummyUserCreate()
	{
		$user = new User();
		$user->name = "HealthCareProvider";
		$user->user_type = config('appointmentnotify.user_type.healthcareprovider');
		$user->email = "provider@cherry.com";
		$user->password = bcrypt('cherry');
		$user->save();

		$user = new User();
		$user->name = "SiteUser";
		$user->user_type = config('appointmentnotify.user_type.siteuser');
		$user->email = "siteuser@cherry.com";
		$user->password = bcrypt('cherry');
		$user->save();

		return 200;
	}
	public function vueComponentOne()
	{
		return view ('webpack.vuecomponentone');
	}
	public function vueComponentTwo()
	{
		return view ('webpack.vuecomponenttwo');
	}
	public function vueComponentThree()
	{
		return view ('webpack.vuecomponentthree');
	}
	public function vueComponentFour()
	{
		return view ('webpack.vuecomponentfour');
	}
	public function vueComponentFive()
	{
		return view ('webpack.vuecomponentfive');
	}



}





