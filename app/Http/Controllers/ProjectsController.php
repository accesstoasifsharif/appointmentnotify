<?php

namespace App\Http\Controllers;
use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
	public function index()
	{

		$projects = Project::all();
		return view('projects.index',compact('projects'));     
	}
	public function store(Request $request)
	{

		$this->validate(request(),[

			'name'=> 'required',
			'description' => 'required'			]

			);


		$request->name;
		$request->description;
		$project= new Project();
		$project->name= $request['name'];
		$project->description= $request['description'];
    // add other fields
		$project->save();
		return redirect('test/projects');
		//return "form submitted";
	}
}
