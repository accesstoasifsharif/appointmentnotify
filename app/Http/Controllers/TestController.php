<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TestController extends Controller {
	protected $hasher;

	public function __construct(HasherContract $hasher) {
		$this->hasher = $hasher;
	}

	public function index() {
		Auth::logout();
		return "in test";
	}

	public function ValidateUserCredentials(Request $request) {

		$this->validate($request, [
			'email'    => 'required|string',
			'password' => 'required|string',
		]);

		return ['message' => 'inputs are valid ! ..'];

	}

	public function partial() {
		return view('webpack.partial');
	}
	public function secondPartial() {
		return view('webpack.secondpartial');
	}
	public function vuePractice() {
		return view('webpack.vuePractice');
	}

	public function namedRoute(Request $request) {
		$user = User::findBySlugOrId($request->input('slug'));
		return $user;

		return view('webpack.nameroute', compact('articles'));
	}
	public function communicatonapp() {
		return view('webpack.communicaton');
	}
	public function adnanTest() {

		//	return config(Config_File_Name . '.' . HealthCareProvider . '.hcp_type.' . SinglePractice);
		return \Model\PersonalInformation::class;
	}

	public function adnanTest1() {
		Str::contains('@callback', '@');
		Str::upper('hello');
		//return SiteUser;
		//config('appointmentnotify.'.SiteUser.'.user_type')
		camel_case('foo bar'); // fooBar
		kebab_case('foo bar'); // foo-bar
		snake_case('foo bar'); // foo_bar
		studly_case('foo_bar'); // FooBar
		title_case('a nice title uses the correct case'); // A Nice Title Uses The Correct Case

		str_plural('child'); // children
		str_singular('cars'); // car
		str_slug('Laravel 5 Framework', '-'); // laravel-5-framework

		ucwords('welcome to the php world'); // Welcome To The Php World
		ucfirst('welcome to the php world'); //  Welcome to the php world
		strtoupper('welcome to cloudways'); //  WELCOME TO CLOUDWAYS
		strtolower('WELCOME TO CLOUDWAYS'); // welcome to cloudways

		//    config('appointmentnotify.healthcareprovider.user_type');
		return view('welcome');

	}
	public function hashCheck() {

		return redirect()->route('test.red')
			->with('status', 'profile updated..');
	}
	public function red() {

		return dd(session('status'));
	}

}
