<?php

namespace Admin;
use App\Http\Controllers\Controller;

class AdminController extends Controller {
	protected $userContext = Admin;
	public function __construct() {

		$this->middleware('auth');
		$this->middleware('check.user.type:' . $this->userContext);

	}

	public function dashboard() {
		return view('admin.dashboard');

	}

	public function test() {
		dd(config('auth.guards.api.provider'));
		//	$config = $this->app['config']['auth.providers.'.$provider];

		//    $this->customProviderCreators[$config['driver']])
		//	dd( $config );
		return view('admin.test');
	}
}
