<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\Auth\AuthenticatesAdmins;

class AdminLoginController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	 */

	use AuthenticatesAdmins;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */

	protected $redirectTo = '/admin';
	/* protected $redirectToHealthCareProviderLogin = '/healthcareprovider';
	protected $redirectToSiteUserLogin = '/siteuser';*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest')->except('logout');
	}
}
