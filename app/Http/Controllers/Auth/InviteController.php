<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\User;
use Facades\Service\Common\UserContext;
use Facades\Service\Common\UserInvite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class InviteController extends Controller {
	public function __construct() {
		$this->middleware('guest')->except('logout');
	}

	public function doctorInvite(Request $request, $token = null) {

		return view('auth.invite.reset')->with(
			['token' => $token, 'email' => $request->email]
		);
	}
	public function test() {

		return UserInvite::sendRegisterEmailInviteDoctor('mrmoghal@gmail.com');
	}
	public function setPassword(Request $request) {

		$this->validate($request, $this->rules(), $this->validationErrorMessages());

		// Here we will attempt to reset the user's password. If it is successful we
		// will update the password on an actual user model and persist it to the
		// database. Otherwise we will parse the error and return the response.
		$response = UserInvite::setPassword(
			$this->credentials($request), function ($credentials) {
				//	dd('credientials', $credentials);
				$this->setUser($credentials);
			}
		);

		// If the password was successfully reset, we will redirect the user back to
		// the application's home authenticated view. If there is an error we can
		// redirect them back to where they came from with their error message.
		//	dd(\Service\Common\UserInvite::$INVITE_PASSWORD_SET);
		return $response == \Service\Common\UserInvite::$INVITE_PASSWORD_SET
		? $this->sendResetResponse($response)
		: $this->sendResetFailedResponse($request, $response);
	}
	/**
	 * Get the password reset validation rules.
	 *
	 * @return array
	 */
	protected function rules() {
		return [
			'token'    => 'required',
			'email'    => 'required|email',
			'password' => 'required|confirmed|min:6',
		];
	}

	/**
	 * Get the password reset validation error messages.
	 *
	 * @return array
	 */
	protected function validationErrorMessages() {
		return [];
	}

	/**
	 * Get the password reset credentials from the request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	protected function credentials(Request $request) {
		return $request->only(
			'email', 'password', 'password_confirmation', 'token'
		);
	}
	protected function setUser($credentials) {

		$user = User::create([
			'email'          => $credentials['email']/*. Str::random(2)*/,
			'password'       => bcrypt($credentials['password']),
			'remember_token' => Str::random(60),
			'user_type'      => 1,
		]);

		$this->guard()->login($user);
	}
	/**
	 * Get the response for a successful password reset.
	 *
	 * @param  string  $response
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function sendResetResponse($response) {
		$wUrl = UserContext::getWizardRedirectUrlByUserTypeId(Auth::user()->user_type);
		session(['redirectUrl' => '/doctor']);
		return redirect()->route($wUrl);
	}

	/**
	 * Get the response for a failed password reset.
	 *
	 * @param  \Illuminate\Http\Request
	 * @param  string  $response
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function sendResetFailedResponse(Request $request, $response) {
		return redirect()->back()
			->withInput($request->only('email'))
			->withErrors(['email' => trans($response)]);
	}
	protected function guard() {
		return Auth::guard();
	}
}
