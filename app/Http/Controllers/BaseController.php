<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

class BaseController extends Controller {
	protected $userContext = '';

	public function __construct($userContext) {
		$this->userContext = $userContext;

		$this->middleware('auth');

		$this->middleware('check.user.type:' . $this->userContext);

		$this->middleware('check.profile.status.complete:' . $this->userContext)->except('wizard');

		$this->middleware('check.profile.status.incomplete:' . $this->userContext)->only('wizard');

	}
}
