<?php

namespace Common\ApiControllers;
use App\Http\Controllers\Controller;
use Auth;
use Facades\Service\Common\UserService;
use Illuminate\Http\Request;

class UserController extends Controller {
	public function resetPassword(Request $data_array) {

		return UserService::resetPassword($data_array, Auth::User());
	}
	public function upDateHippaConsent(Request $data_array) {

		return UserService::upDateHippaConsent($data_array, Auth::User());

	}
	public function getLicenseAgreement(Request $data_array) {
		//
		return UserService::getLicenseAgreement($data_array);
	}
	public function getAgreement() {
		return UserService::getAgreement(Auth::User());
	}
}
