<?php

namespace Common\ApiControllers;
use App\Http\Controllers\Controller;
use Auth;
use Facades\Service\Common\UserNotificationService;

class NotificationApiController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	public function allNotifications() {
		return UserNotificationService::getNotifications(Auth::User());
	}

	public function markNotificationAsRead($notification_id) {
		return UserNotificationService::markNotificationAsRead($notification_id);
	}

	public function markNotificationAsUnRead($notification_id) {
		return UserNotificationService::markNotificationAsUnRead($notification_id);
	}

}
