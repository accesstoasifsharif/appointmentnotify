<?php

namespace Common\ApiControllers;
use App\Events\MessageConversation;
use App\Http\Controllers\Controller;
use Facades\Service\Common\MessageService;
use Facades\Service\Common\UserData;
use Illuminate\Http\Request;

class MessageApiController extends Controller {

	public function __construct() {
		$this->middleware('check.perspective');
	}

	// return all messages with specific user_id of authenticated user
	public function specificUserConverstion(Request $request) {
		$user_1 = UserData::getUserByModelIdAndModelIdentifier($request->input('user1_id'), $request->input('user1_model'));
		$user_2 = UserData::getUserByModelIdAndModelIdentifier($request->input('user2_id'), $request->input('user2_model'));
		$conversation = MessageService::getSpecificUserConverstion($user_1->id, $user_2->id, $request);
		if ($conversation != '0') {
			$conversation->left_user = $user_1->id;
			$conversation->right_user = $user_2->id;
		}
		return $conversation;
	}

	public function storeMessage(Request $request) {
		$user_1 = UserData::getUserByModelIdAndModelIdentifier($request->input('sender_id'), $request->input('sender_identifier'));
		$user_2 = UserData::getUserByModelIdAndModelIdentifier($request->input('reciever_id'), $request->input('reciever_identifier'));
		$message = MessageService::sendMessageByUserId($user_1->id, $user_2->id, $request);
		event(new MessageConversation($user_1, $user_2, $message));
		return $message;
	}

	// public function getConversationPreview(Request $request) {
	// 	return MessageService::getConversationPreview($request);
	// }

}
