<?php

namespace Common\ApiControllers;
use App\Http\Controllers\Controller;
use Facades\Service\Common\ModelAvatar;
use Illuminate\Http\Request;
use Model\Country;
use Model\Specialty;
use Model\State;

class CommonApiController extends Controller {

	public function allCountries() {
		return Country::all();
	}

	public function allStates() {
		return State::all();
	}

	public function getAvatarByModelAndId(Request $request) {
		return ModelAvatar::getAvatarByModelAndId($request);
	}

	public function allSpecialties() {
		return Specialty::all();
	}

	public function shortCallDuration()
	{
		return config('appointmentnotify.APP_SHORT_CALL_LIST_TIME_SPAN');
	}




}
