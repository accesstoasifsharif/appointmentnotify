<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

class HomeController extends Controller {

	public function __construct() {
		// $this->middleware('auth');
	}
	/*homepage action method*/
	public function homePage() {
		return view('home.homepage');
	}

}
