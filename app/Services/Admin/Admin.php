<?php

namespace Service\Admin;
use Route;

class Admin  
{

	protected $file = Config_File_Name;
	public function routes()
	{
        // Authentication Routes...
		
		Route::get('admin/login','Auth\AdminLoginController@showLoginForm' );
		Route::post('admin/login','Auth\AdminLoginController@login' )->name('admin.login');
		Route::post('admin/logout','Auth\AdminLoginController@logout' )->name('admin.logout');
		
	}
	

}
