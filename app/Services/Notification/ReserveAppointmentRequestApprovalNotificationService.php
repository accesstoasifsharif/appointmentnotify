<?php

namespace Service\Notification;

class ReserveAppointmentRequestApprovalNotificationService {

	public function toArray($notifiable) {
		return $this->toDefault($notifiable);
	}

	public function toDatabase($notifiable) {
		return $this->toDefault($notifiable);
	}

	public function toBroadcast($notifiable) {
		return $this->toDefault($notifiable);
	}

	protected function toDefault($notifiable) {
		$notification_text = "Your appointment reservation request is approved by " . $this->sender_user->full_name;

		return [
			'send_from' => $this->sender_user->id,
			'send_to' => $this->reciever_user->id,
			'notification_message' => $notification_text,
			'appointment_type' => 'reserve_request',
		];
	}

}
