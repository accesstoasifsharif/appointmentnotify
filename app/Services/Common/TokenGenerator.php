<?php

namespace Service\Common;

use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Support\Str;

class TokenGenerator {

	protected $hasher;

	public function __construct(HasherContract $hasher) {
		$this->hasher = $hasher;
	}

	public function generateToken() {

		$key = app()['config']['app.key'];
		if (Str::startsWith($key, 'base64:')) {
			$key = base64_decode(substr($key, 7));
		}
		$token = hash_hmac('sha256', Str::random(40), $key);
		return $token;

	}
	public function getHashedToken($token) {
		return $this->hasher->make($token);

	}
	public function verifyTokenHash($token, $hashedToken) {
		return password_verify($token, $hashedToken);

	}

}
