<?php

namespace Service\Common;

use Illuminate\Pagination\LengthAwarePaginator;

class DataTable {
	public function getPaginate($query_array, $per_page, $page) {
		$offSet = ($page * $per_page) - $per_page;
		$itemsForCurrentPage = array_slice($query_array, $offSet, $per_page, true);
		$query_array = new LengthAwarePaginator($itemsForCurrentPage, count($query_array), $per_page, $page);
		$paginated_data = $query_array->toArray();
		return $paginated_data;

	}
}
