<?php

namespace Service\Common;
use App\Mail\DoctorInvited;
use Closure;
use Illuminate\Support\Facades\Mail;
use Model\Invite;
use Repository\Contracts\Common\SignUpInvite as SignUpInviteContract;

class UserInvite {

	public static $INVITE_LINK_SENT = 'invite.sent';

	public static $INVITE_PASSWORD_SET = 'invite.passwordset';

	public static $INVALID_EMAIL = 'invite.email';

	public static $INVALID_TOKEN = 'invite.token';

	protected $signUpInvite = '';

	public function __construct(SignUpInviteContract $sc) {

		$this->signUpInvite = $sc;

	}

	public function sendRegisterEmailInviteDoctor($email) {

		$token = $this->signUpInvite->createInvite($email);

		Mail::to($email)->send(new DoctorInvited(inviteUrl($token, 'doctor.invite')));
		return 'this is the invite to doctor' . $email;
	}

	public function setPassword(array $credentials, Closure $callback) {

		// If the responses from the validate method is not a user instance, we will
		// assume that it is a redirect and simply return it from this method and
		// the user is properly redirected having an error message on the post.
		$invite = $this->validateSetPassword($credentials);

		if (!$invite instanceof Invite) {
			return $invite;
		}

		//	$password = $credentials['password'];

		// Once the reset has been validated, we'll call the given callback with the
		// new password. This gives the user an opportunity to store the password
		// in their persistent storage. Then we'll delete the token and return.
		$callback($credentials);

		$this->signUpInvite->deleteTokens($invite);

		return self::$INVITE_PASSWORD_SET;
	}
	protected function validateSetPassword(array $credentials) {

		if (is_null($invite = $this->signUpInvite->getInvite($credentials))) {
			//	dd('invite', $invite);
			return self::$INVALID_EMAIL;
		}

		/*if (!$this->validateNewPassword($credentials)) {
		return static::INVALID_PASSWORD;
		}*/
		if (!$this->signUpInvite->tokenExists($invite, $credentials['token'])) {

			return self::$INVALID_TOKEN;
		}

		return $invite;
	}
	/**
	 *create account for the given token.
	 *
	 * @param  array  $credentials
	 * @param  \Closure  $callback
	 * @return mixed
	 */
	/* public function reset(array $credentials, Closure $callback)
{
// If the responses from the validate method is not a user instance, we will
// assume that it is a redirect and simply return it from this method and
// the user is properly redirected having an error message on the post.
$user = $this->validateReset($credentials);

if (! $user instanceof CanResetPasswordContract) {
return $user;
}

$password = $credentials['password'];

// Once the reset has been validated, we'll call the given callback with the
// new password. This gives the user an opportunity to store the password
// in their persistent storage. Then we'll delete the token and return.
$callback($user, $password);

$this->tokens->delete($user);

return static::PASSWORD_RESET;
}*/

}
