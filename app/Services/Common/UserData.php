<?php

namespace Service\Common;

use App\User;
use Facades\Service\Common\UserContext;
use Model\Doctor;
use Model\SiteUser;

class UserData {

	public function getUserByDoctorId($doctor_id) {

		if (isset(Doctor::find($doctor_id)->singlePractice->healthCareProvider->user)) {
			return Doctor::find($doctor_id)->singlePractice->healthCareProvider->user;
		}
		return null;
	}

	public function getUserBySiteUserId($siteUser_id) {
		return SiteUser::find($siteUser_id)->user;
	}

	public function getUserByModelIdAndModelIdentifier($model_id, $model_identifier) {
		$model = UserContext::getModelNameByModelIdentifier($model_identifier);
		if ($model == 'User') {
			$class = 'App\\' . $model;
		} else {
			$class = 'Model\\' . $model;
		}
		$model = new $class;
		return $model::find($model_id)->myUser;
	}

	public function getSiteUserByUserEmailAddress($email) {
		$user = User::whereEmail($email)->first();
		return $user->siteUser;
	}

}
