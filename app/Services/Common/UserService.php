<?php

namespace Service\Common;
use Repository\Contracts\Common\HippaConsent;
use Repository\Contracts\Common\ResetPassword;
use Repository\Contracts\SiteUser\SiteUser;

class UserService {
	protected $resetPasswordContract = '';
	protected $siteUserContract = '';
	protected $hippaConsentContract = '';

	public function __construct(ResetPassword $rp, SiteUser $sc,
		HippaConsent $hc) {
		$this->resetPasswordContract = $rp;
		$this->siteUserContract = $sc;
		$this->hippaConsentContract = $hc;
	}

	public function resetPassword($data_array, $user) {
		// $siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->resetPasswordContract->resetPassword($data_array, $user->id);

	}
	public function upDateHippaConsent($data_array, $user) {

		return $this->hippaConsentContract->upDateHippaConsent($data_array, $user->id);

	}
	public function getLicenseAgreement($data_array) {

		return $this->hippaConsentContract->getLicenseAgreement($data_array);

	}
	public function getAgreement($user) {

		return $this->hippaConsentContract->getAgreement($user->id);

	}
}