<?php
namespace Service\Common;
use Model\Conversation as ConversationModel;
use Model\Message as MessageModel;
use Repository\Common\Conversation;
use Repository\Contracts\Common\Message;

class MessageService {
	protected $Mcontract = '';
	protected $Ccontract = '';
	protected $messageRepo;
	protected $auth_user_id;

	public function __construct(Message $Mcon, Conversation $Ccon) {
		$this->Mcontract = $Mcon;
		$this->Ccontract = $Ccon;
	}

	public function getSpecificUserConverstion($user1_id, $user2_id) {
		$conversation = $this->isExistsAmongTwoUsers($user1_id, $user2_id);
		if ($conversation) {
			return $this->Ccontract->getConversationMessages($conversation);
		} else if ($conversation = $this->isExistsAmongTwoUsers($user2_id, $user1_id)) {
			return $this->Ccontract->getConversationMessages($conversation);
		}
		return "0";
	}

	public function getSpecificUserConverstionPreview($user1_id, $user2_id) {
		$conversation = $this->isExistsAmongTwoUsers($user1_id, $user2_id);
		if ($conversation) {
			return $this->Ccontract->getConversationMessagesPreview($conversation);
		} else if ($conversation = $this->isExistsAmongTwoUsers($user2_id, $user1_id)) {
			return $this->Ccontract->getConversationMessagesPreview($conversation);
		}
		return "0";
	}

	protected function isExistsAmongTwoUsers($user1, $user2) {

		$conversation = $this->Ccontract->getConversationAmongTwoUsers($user1, $user2);
		if ($conversation->exists()) {
			return $conversation->first()->id;
		}
		return false;
	}

	public function sendMessageByUserId($user_1, $user_2, $data_array) {
		$message = $data_array->input('message_body');

		if ($conversationId = $this->isConversationExists($user_1, $user_2)) {
			$message = $this->makeMessage($user_1, $conversationId, $message);
			return $message;
		}
		if ($conversationId = $this->isConversationExists($user_2, $user_1)) {
			$message = $this->makeMessage($user_1, $conversationId, $message);
			return $message;
		}
		$convId = $this->newConversation($user_1, $user_2);
		$message = $this->makeMessage($user_1, $convId, $message);
		return $message;
	}

	protected function getSerializeUser($user1, $user2) {
		$user = [];
		$user['one'] = ($user1 < $user2) ? $user1 : $user2;
		$user['two'] = ($user1 < $user2) ? $user2 : $user1;

		return $user;
	}

	protected function isConversationExists($user_1, $user_2) {

		if (empty($user_1) || empty($user_2)) {
			return false;
		}
		// $user = $this->getSerializeUser($user_1, $user_2);
		return $this->isExistsAmongTwoUsers($user_1, $user_2);
	}

	protected function newConversation($user_1, $user_2) {
		$conversationId = $this->isConversationExists($user_1, $user_2);
		// $user = $this->getSerializeUser($this->auth_user_id, $receiverId);
		if ($conversationId === false) {
			$conversation = new ConversationModel();
			$conversation->user_one = $user_1;
			$conversation->user_two = $user_2;
			$conversation->status = 1;
			$this->Ccontract->newConversation($conversation);
			if ($conversation) {
				return $conversation->id;
			}
		}
		return $conversationId;
	}

	protected function makeMessage($user_1, $conversationId, $message_body) {
		$message = new MessageModel();
		$message->message = $message_body;
		$message->conversation_id = $conversationId;
		$message->user_id = $user_1;
		$message->is_seen = 0;
		$this->Mcontract->newMessage($message);
		return $message;
	}

}