<?php

namespace Service\Common;

use Illuminate\Notifications\DatabaseNotification;

class UserNotificationService {

	public function getNotifications($user) {
		return $user->notifications;
	}

	public function markNotificationAsRead($notification_id) {
		$notification = DatabaseNotification::find($notification_id);
		$notification->markAsRead();
		return 'read';
	}

	public function markNotificationAsUnRead($notification_id) {
		$notification = DatabaseNotification::find($notification_id);
		$notification->read_at = null;
		$notification->save();
		return 'unread';
	}

}
