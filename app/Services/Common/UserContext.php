<?php

namespace Service\Common;

class UserContext {

	protected $file = Config_File_Name;

	public function getUserTypeIdByName($contextName) {

		foreach (config($this->file . '.user_type_name') as $singleUserType) {
			if ($contextName == $singleUserType) {
				return config($this->file . '.' . $contextName . '.user_type_id');
			}
		}
		return '-1';
	}
	public function getUserTypeNameById($id) {
		foreach (config($this->file . '.user_type_name') as $singleUserType) {
			if ($id == config($this->file . '.' . $singleUserType . '.user_type_id')) {
				return $singleUserType;
			}
		}
		return '-1';
	}

	public function getRedirectUrlByUserTypeId($id) {
		foreach (config($this->file . '.user_type_name') as $singleUserType) {
			if ($id == config($this->file . '.' . $singleUserType . '.user_type_id')) {
				return config($this->file . '.' . $singleUserType . '.redirectUrl');
			}
		}
		return '-1';
	}
	public function getWizardRedirectUrlByUserTypeId($id) {

		foreach (config($this->file . '.user_type_name') as $singleUserType) {

			if ($id == config($this->file . '.' . $singleUserType . '.user_type_id')) {

				return config($this->file . '.' . $singleUserType . '.profileWizardUrl');
			}
		}
		return '-1';
	}
	public function statusComplete() {
		return config($this->file . '.profile_status.complete');
	}
	public function statusInComplete() {
		return config($this->file . '.profile_status.incomplete');
	}

	public function getModelNameByModelIdentifier($model_identifier) {
		foreach (config($this->file . '.model_identifiers') as $key => $value) {
			if ($value == $model_identifier) {
				return $key;
			}
		}
	}

}
