<?php
namespace Service\Common;

use Repository\Contracts\Common\CityZipCodeLocation;

class CityZipCodeLocationService {

	protected $cityZipCodeLocationContract = '';

	public function __construct(CityZipCodeLocation $city_zip_code_location) {
		$this->cityZipCodeLocationContract = $city_zip_code_location;
	}
	public function getNearestZipCodeList($zip_code, $distance) {
		// $siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->cityZipCodeLocationContract->getNearestZipCodeList($zip_code, $distance);
	}

}