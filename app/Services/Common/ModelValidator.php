<?php

namespace Service\Common;

Class ModelValidator {

	public function getValidationRules(array $models) {

		$rules = [];
		foreach ($models as $singleModel) {

			$class = 'Model\\' . $singleModel;
			$model = new $class;
			$model_rules = $model::getRules();
			$rules = array_merge($rules, $model_rules);

		}

		return $rules;
	}

}
