<?php

namespace Service\Common;

use App\User;
use Facades\Service\Common\UserContext;
use Model\Doctor;
use Model\DummyAvatar;
use Model\HealthCareFacility;
use Model\SiteUser;
use Model\Staff;

class ModelAvatar {
	public function getAvatarByModelAndId($data_array) {
		$model_identifier = $data_array->input('model');
		$model_id = $data_array->input('modelId');
		$model = UserContext::getModelNameByModelIdentifier($model_identifier);

		switch ($model) {
		case 'SiteUser':{
				return $this->getSiteUserAvatar($model_id);
				break;
			}
		case 'Doctor':{
				return $this->getDoctorAvatar($model_id);
				break;
			}
		case 'HealthCareFacility':{
				return $this->getHealthCareFacilityAvatar($model_id);
				break;
			}
		case 'Staff':{
				return $this->getStaffAvatar($model_id);
				break;
			}

		default:{

				return $this->getUserAvatar($model_id);
				break;
			}
		}
	}

	protected function getSiteUserAvatar($site_user_id) {
		$avatar = SiteUser::find($site_user_id)->avatar;
		if (isset($avatar)) {
			return $avatar->avatar;
		} else {
			$dummy_avatars = DummyAvatar::all()->shuffle();
			return $dummy_avatars[0]->avatar;
		}
	}

	protected function getDoctorAvatar($doctor_id) {

		$doctor = Doctor::find($doctor_id);
		if (isset($doctor->avatar)) {
			return $doctor->avatar->avatar;
		} else {
			$dummy_avatars = DummyAvatar::where('default', '0')->where('user_type', config('appointmentnotify.model_identifiers.Doctor'))->where('gender', $doctor->personalInformation->gender)->get();
			return $dummy_avatars[0]->avatar;
		}

	}

	protected function getHealthCareFacilityAvatar($hc_facility) {

		$avatar = HealthCareFacility::find($hc_facility)->avatar;
		if (isset($avatar)) {
			return $avatar->avatar;
		} else {
			$dummy_avatars = DummyAvatar::where('user_type', config('appointmentnotify.model_identifiers.HealthCareFacility'))->get();
			return $dummy_avatars[0]->avatar;
		}
	}

	protected function getStaffAvatar($staff_id) {
		$avatar = Staff::find($staff_id)->avatar;
		if (isset($avatar)) {
			return $avatar->avatar;
		} else {
			$dummy_avatars = DummyAvatar::all()->shuffle();
			return $dummy_avatars[0]->avatar;
		}
	}

	protected function getUserAvatar($user_id) {
		$user = User::find($user_id);
		if (isset($user->siteUser)) {
			return $this->getSiteUserAvatar($user->siteUser->id);
		} else {
			if (isset($user->healthCareProvider->singlePractice)) {
				return $this->getDoctorAvatar($user->healthCareProvider->singlePractice->doctor_id);
			}
		}
	}

}
