<?php

namespace Service\Common;
use Model\Address;
use Model\ContactInformation;
use Model\PersonalInformation;
use Repository\Contracts\Common\Address as AddressContract;
use Repository\Contracts\Common\ContactInformation as ContactInformationContract;
use Repository\Contracts\Common\PersonalInfo as PersonalInfoContract;
use Repository\Contracts\Common\UserRepository as UserRepositoryContract;

class UserProfile {

	protected $personalInfoContract = '';
	protected $addressContract = '';
	protected $contactInfoContract = '';
	protected $userRepositoryContract = '';

	public function __construct(AddressContract $ac, ContactInformationContract $cc, PersonalInfoContract $pc,
		UserRepositoryContract $ur
	) {

		$this->contactInfoContract = $cc;

		$this->addressContract = $ac;

		$this->personalInfoContract = $pc;

		$this->userRepositoryContract = $ur;
	}

	public function getProfileByRelation($relation) {
		$personalInfo = $this->personalInfoContract->getPersonalInfoDetails($relation);
		$address = $this->addressContract->getAddressDetails($relation);
		$contactInfo = $this->contactInfoContract->getContactDetails($relation);

		$dataArray = [];
		$dataArray = array_merge($contactInfo->toArray(), $address->toArray());
		$dataArray = array_merge($dataArray, $personalInfo->toArray());
		return $dataArray;
	}
	public function getPersonalInfoByRelation($relation) {
		return $this->personalInfoContract->getPersonalInfoDetails($relation);

	}
	public function getAddresseByRelation($relation) {

		return $this->addressContract->getAddressDetails($relation);

	}
	public function getContactInfoByRelation($relation) {

		return $this->contactInfoContract->getContactDetails($relation);

	}

	public function updatePersonalInfo($relation, $data_array) {

		return $this->personalInfoContract->addOrUpdatePersonalInfo($relation,
			new PersonalInformation($data_array->all()));

	}
	public function updateAddress($relation, $data_array) {

		return $this->addressContract->addOrUpdateAddressInfo($relation, new Address($data_array->all()));

	}
	public function updateContactInfo($relation, $data_array) {

		return $this->contactInfoContract->addOrUpdateContactInfo($relation, new ContactInformation($data_array->all()));

	}
	public function updateUserProfileStatusToComplete($user) {

		$this->userRepositoryContract->updateUserProfileStatus($user, 1);

	}
	public function updateProfileByRelation($relation, $data_array) {
		$this->personalInfoContract->addOrUpdatePersonalInfo($relation, new PersonalInformation($data_array->all()));
		$this->contactInfoContract->addOrUpdateContactInfo($relation, new ContactInformation($data_array->all()));
		$this->addressContract->addOrUpdateAddressInfo($relation, new Address($data_array->all()));
		return 1;
	}

}
