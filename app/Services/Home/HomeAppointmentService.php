<?php

namespace Service\Home;

use Repository\Contracts\Home\HomeAppointments;

class HomeAppointmentService {

	protected $homeAppointmentsContract = '';

	public function __construct(HomeAppointments $home_appointments) {
		$this->homeAppointmentsContract = $home_appointments;
	}

	public function getAllSpecialtyList() {
		return $this->homeAppointmentsContract->getAllSpecialtyList();
	}
	public function showAppointmentsForProviderView($search_query, $speciality_id, $selectGender, $zip_list, $page, $doctor_id) {
		return $this->homeAppointmentsContract->showAppointmentsForProviderView($search_query, $speciality_id, $selectGender, $zip_list, $page, $doctor_id);
	}
	public function showAppointmentsForFacilityView($search_query, $zip_list, $specialty_id, $page, $doctor_id, $gender, $facility_id) {
		return $this->homeAppointmentsContract->showAppointmentsForFacilityView($search_query, $zip_list, $specialty_id, $page, $doctor_id, $gender, $facility_id);
	}
	public function showAppointmentsForFacilityViewWithGender($gender, $facility_id) {
		return $this->homeAppointmentsContract->showAppointmentsForFacilityViewWithGender($gender, $facility_id);
	}
	public function checkAppointmentExistanceByAppointmentId($appointment_id) {
		return $this->homeAppointmentsContract->checkAppointmentExistanceByAppointmentId($appointment_id);
	}
	public function bookAnAppointment($appointment_id, $siteUser_id) {
		return $this->homeAppointmentsContract->bookAnAppointment($appointment_id, $siteUser_id);
	}
}
