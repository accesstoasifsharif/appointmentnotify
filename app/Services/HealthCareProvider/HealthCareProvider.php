<?php

namespace Service\HealthCareProvider;
use Repository\Contracts\HealthCareProvider\Department as DepartmentContract;
use Repository\Contracts\HealthCareProvider\HealthCareProvider as HealthCareProviderContract;
use Repository\Contracts\HealthCareProvider\Hospital as HospitalContract;
use Repository\Contracts\HealthCareProvider\MedicalClinic as MedicalClinicContract;
use Repository\Contracts\HealthCareProvider\SinglePractice as SinglePracticeContract;
use Repository\Contracts\HealthCareProvider\Staff as StaffContract;

class HealthCareProvider {

	protected $healthCareProviderContract = '';

	protected $hospitalContract = '';
	protected $singlePracticeContract = '';
	protected $medicalClinicContract = '';
	protected $departmentContract = '';
	protected $staffContract = '';

	public function __construct(
		HealthCareProviderContract $hcp,
		HospitalContract $hc,
		SinglePracticeContract $spc,
		MedicalClinicContract $mcc,
		DepartmentContract $dc,
		StaffContract $sc

	) {

		$this->healthCareProviderContract = $hcp;
		$this->hospitalContract = $hc;
		$this->singlePracticeContract = $spc;
		$this->medicalClinicContract = $mcc;
		$this->departmentContract = $dc;
		$this->staffContract = $sc;
	}

	public function getSinglePracticeByUser($user, $doctor) {
		if (is_null($healthCareProvider = $this->healthCareProviderContract->getHealthCareProviderByUser($user))) {
			$healthCareProvider = $this->healthCareProviderContract->createHealthCareProvider($user, singlePracticeTypeId());
		}

		if (is_null($singlePractice = $this->singlePracticeContract->getSinglePracticeByHCP($healthCareProvider))) {

			$singlePractice = $this->singlePracticeContract->createSinglePractice($healthCareProvider, $doctor);

			$admin_level = 1;
			$this->addHCPPerspectiveLevel($user->id, $healthCareProvider->id, $admin_level);
		}
		return $singlePractice;
	}
	public function getHospitalByUser($user) {

		if (is_null($healthCareProvider = $this->healthCareProviderContract->getHealthCareProviderByUser($user))) {
			$healthCareProvider = $this->healthCareProviderContract->createHealthCareProvider($user, medicalClinicTypeId());
		}

		if (is_null($hospital = $this->hospitalContract->getHospitalByHCP($healthCareProvider))) {
			$hospital = $this->hospitalContract->createHospital($healthCareProvider);
			$admin_level = 1;
			$this->addHCPPerspectiveLevel($user->id, $healthCareProvider->id, $admin_level);
		}
		return $hospital;
	}
	public function getMedicalClinicByUser($user) {

		if (is_null($healthCareProvider = $this->healthCareProviderContract->getHealthCareProviderByUser($user))) {
			$healthCareProvider = $this->healthCareProviderContract->createHealthCareProvider($user, medicalClinicTypeId());
		}

		if (is_null($medicalClinic = $this->medicalClinicContract->getMedicalClinicByHCP($healthCareProvider))) {
			$medicalClinic = $this->medicalClinicContract->createMedicalClinic($healthCareProvider);

			$admin_level = 1;
			$this->addHCPPerspectiveLevel($user->id, $healthCareProvider->id, $admin_level);
		}
		return $medicalClinic;
	}
	public function getDepartmentByUser($user) {

		if (is_null($healthCareProvider = $this->healthCareProviderContract->getHealthCareProviderByUser($user))) {
			$healthCareProvider = $this->healthCareProviderContract->createHealthCareProvider($user, departmentTypeId());
		}

		if (is_null($department = $this->departmentContract->getDepartmentByHCP($healthCareProvider))) {
			$department = $this->departmentContract->createDepartment($healthCareProvider);
			$admin_level = 1;
			$this->addHCPPerspectiveLevel($user->id, $healthCareProvider->id, $admin_level);
		}
		return $department;
	}
	public function getStaffByUser($user) {
		if (is_null($healthCareProvider = $this->healthCareProviderContract->getHealthCareProviderByUser($user))) {
			$healthCareProvider = $this->healthCareProviderContract->createHealthCareProvider($user, staffTypeId());
		}

		if (is_null($staff = $this->staffContract->getStaffByHCP($healthCareProvider))) {
			$staff = $this->staffContract->createStaff($healthCareProvider);
		}
		return $staff;
	}
	public function getHealthCareProviderByUser($user) {

		return $this->healthCareProviderContract->getHealthCareProviderByUser($user);
	}
	public function getUserByHealthCareProvider($hcp) {

		return $this->healthCareProviderContract->getUserByHealthCareProvider($hcp);
	}
	public function getDoctorByEmail($email) {
		if (is_null($healthCareProvider = $this->healthCareProviderContract->getHealthCareProviderByUser($user))) {
			$healthCareProvider = $this->healthCareProviderContract->createHealthCareProvider($user, singlePracticeTypeId());
		}

		if (is_null($singlePractice = $this->singlePracticeContract->getSinglePracticeByHCP($healthCareProvider))) {
			$singlePractice = $this->singlePracticeContract->createSinglePractice($healthCareProvider);
		}
		return $singlePractice;
	}

	public function addHCPPerspectiveLevel($user_id, $hcp_id, $level_id) {
		return $this->healthCareProviderContract->addHCPPerspectiveLevel($user_id, $hcp_id, $level_id);
	}
}
