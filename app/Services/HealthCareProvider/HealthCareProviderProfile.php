<?php

namespace Service\HealthCareProvider;
use Model\Address;
use Model\ContactInformation;
use Model\ContactPerson;
use Model\EmployerIdentificationNumber;
use Model\NationalProviderIdentification;
use Model\PersonalInformation;
use Model\ProviderOrganizationName;
use Repository\Contracts\Common\Address as AddressContract;
use Repository\Contracts\Common\ContactInformation as ContactInformationContract;
use Repository\Contracts\Common\ContactPerson as ContactPersonContract;
use Repository\Contracts\Common\PersonalInfo as PersonalInfoContract;
use Repository\Contracts\Common\UserRepository as UserRepositoryContract;
use Repository\Contracts\HealthCareProvider\EmployerIdentificationNumber as EmployerIdentificationNumberContact;
use Repository\Contracts\HealthCareProvider\NationalProviderIdentification as NationalProviderIdentificationContact;
use Repository\Contracts\HealthCareProvider\ProviderOrganizationName as ProviderOrganizationNameContact;

class HealthCareProviderProfile {

	protected $einContract = '';
	protected $poNameContract = '';
	protected $contactPersonContract = '';

	protected $personalInfoContract = '';
	protected $addressContract = '';
	protected $contactInfoContract = '';
	protected $userRepositoryContract = '';
	protected $npiContract = '';

	public function __construct(
		AddressContract $ac,
		ContactInformationContract $cc,
		PersonalInfoContract $pc,
		UserRepositoryContract $ur,
		NationalProviderIdentificationContact $npi,
		ContactPersonContract $cpc,
		EmployerIdentificationNumberContact $einc,
		ProviderOrganizationNameContact $ponc
		/*  to include doctor service here....*/
	) {

		$this->contactInfoContract = $cc;
		$this->addressContract = $ac;
		$this->personalInfoContract = $pc;
		$this->userRepositoryContract = $ur;
		$this->npiContract = $npi;
		$this->contactPersonContract = $cpc;
		$this->einContract = $einc;
		$this->poNameContract = $ponc;
	}

	public function getProfileByRelation($relation) {
		$personalInfo = $this->personalInfoContract->getPersonalInfoDetails($relation);
		$address = $this->addressContract->getAddressDetails($relation);
		$contactInfo = $this->contactInfoContract->getContactDetails($relation);

		$dataArray = [];
		$dataArray = array_merge($contactInfo->toArray(), $address->toArray());
		$dataArray = array_merge($dataArray, $personalInfo->toArray());
		return $dataArray;
	}
	public function getPersonalInfoByRelation($relation) {
		return $this->personalInfoContract->getPersonalInfoDetails($relation);

	}

	public function updatePersonalInfo($relation, $data_array) {

		return $this->personalInfoContract->addOrUpdatePersonalInfo($relation,
			new PersonalInformation($data_array));

	}

	public function updateUserProfileStatusToComplete($user) {

		$this->userRepositoryContract->updateUserProfileStatus($user, 1);

	}
	public function saveOrUpdateSinglePracticeProfileByRelation($singlePractice, $data_array) {
		/*  add as much as contracts or info you like here...*/
		$this->npiContract->addOrUpdateNPI($singlePractice, new NationalProviderIdentification
			($data_array));
		return $singlePractice;
	}
	public function saveOrUpdateMedicalClinicProfileByRelation($medicalClinic, $data_array) {
		/*  add as much as contracts or info you like here...*/
		$this->npiContract->addOrUpdateNPI($medicalClinic, new NationalProviderIdentification
			($data_array));

		$this->addressContract->addOrUpdateAddressInfo($medicalClinic, new Address($data_array));

		$this->contactInfoContract->addOrUpdateContactInfo($medicalClinic, new ContactInformation
			($data_array));

		$this->contactPersonContract->addOrUpdateContactPersonInfo($medicalClinic, new ContactPerson($data_array));

		$this->einContract->addOrUpdateEIN($medicalClinic, new EmployerIdentificationNumber($data_array));

		$this->poNameContract->addOrUpdatePON($medicalClinic, new ProviderOrganizationName($data_array));

		return $medicalClinic;
	}
	public function saveOrUpdateHospitalProfileByRelation($hospital, $data_array) {
		/*  add as much as contracts or info you like here...*/
		$this->npiContract->addOrUpdateNPI($hospital, new NationalProviderIdentification
			($data_array));

		$this->addressContract->addOrUpdateAddressInfo($hospital, new Address($data_array));

		$this->contactInfoContract->addOrUpdateContactInfo($hospital, new ContactInformation
			($data_array));

		$this->contactPersonContract->addOrUpdateContactPersonInfo($hospital, new ContactPerson($data_array));

		$this->einContract->addOrUpdateEIN($hospital, new EmployerIdentificationNumber($data_array));

		$this->poNameContract->addOrUpdatePON($hospital, new ProviderOrganizationName($data_array));

		return $hospital;
	}
	public function saveOrUpdateDepartmentProfileByRelation($department, $data_array) {
		/*  add as much as contracts or info you like here...*/
		$this->npiContract->addOrUpdateNPI($department, new NationalProviderIdentification
			($data_array));

		$this->addressContract->addOrUpdateAddressInfo($department, new Address($data_array));

		$this->contactInfoContract->addOrUpdateContactInfo($department, new ContactInformation
			($data_array));

		$this->contactPersonContract->addOrUpdateContactPersonInfo($department, new ContactPerson($data_array));

		$this->einContract->addOrUpdateEIN($department, new EmployerIdentificationNumber($data_array));

		$this->poNameContract->addOrUpdatePON($department, new ProviderOrganizationName($data_array));

		return $department;
	}
}
