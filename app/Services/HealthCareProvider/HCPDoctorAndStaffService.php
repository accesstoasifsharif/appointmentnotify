<?php

namespace Service\HealthCareProvider;

use Facades\Service\Common\DataTable;
use Facades\Service\Common\UserContext;
use Model\ContactInformation;
use Model\Doctor;
use Model\HealthCareProviderDoctor;
use Model\HealthCareProviderStaff;
use Model\Staff;
use Repository\Contracts\Common\ContactInformation as ContactInformationContract;
use Repository\Contracts\HealthCareProvider\Doctor as DoctorContract;
use Repository\Contracts\HealthCareProvider\HCPDoctor as HCPDoctorContract;
use Repository\Contracts\HealthCareProvider\HCPStaff as HCPStaffContract;
use Repository\Contracts\HealthCareProvider\HealthCareFacility as HealthCareFacilityContract;
use Repository\Contracts\HealthCareProvider\Staff as StaffContract;
use Repository\Contracts\HealthCareProvider\TimeTable as TimeTableContract;

class HCPDoctorAndStaffService {

	protected $Dcontract = '';
	protected $HCPDcontract = '';
	protected $Scontract = '';
	protected $HCPScontract = '';
	protected $TTcontract = '';
	protected $CIcontract = '';
	protected $HCFacilityContract = '';

	public function __construct(DoctorContract $Dcon, ContactInformationContract $CIcon, TimeTableContract $TTcon, HCPDoctorContract $HCPDcon, HCPStaffContract $HCPScon, StaffContract $Scon, HealthCareFacilityContract $hcFC) {

		$this->Dcontract = $Dcon;
		$this->HCPDcontract = $HCPDcon;
		$this->Scontract = $Scon;
		$this->HCPScontract = $HCPScon;
		$this->TTcontract = $TTcon;
		$this->CIcontract = $CIcon;
		$this->HCFacilityContract = $hcFC;
	}

	public function getSelectedDoctorStaffDetail($data_array) {
		$model = UserContext::getModelNameByModelIdentifier($data_array->input('model_identifier'));
		switch ($model) {
		case 'Doctor':{
				return $this->Dcontract->getDoctorInfoByDoctorId($data_array->input('target_id'));
				break;
			}
		case 'Staff':{
				return $this->Scontract->getStaffInfoByStaffId($data_array->input('target_id'));
				break;
			}
		default:{
				return false;
				break;
			}
		}
	}

	public function allDoctorAndStaffByHCPId($request, $hcp_id) {
		$query = $request->input('query');
		$per_page = $request->input('per_page');
		$page = $request->input('page');
		$hcp_facility_id = $request->input('hcp_facility_id');
		$query = ($query != '') ? $query : null;
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;
		$query_array_doctor = $this->getHCPDoctors($query, $hcp_id, $hcp_facility_id);
		$query_array_staff = $this->getHCPStaff($query, $hcp_id, $hcp_facility_id);
		$query_array = array_merge($query_array_doctor, $query_array_staff);
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	protected function getHCPStaff($query, $hcp_id, $hcp_facility_id) {
		if (isset($query)) {
			if ($hcp_facility_id != 0) {
				return $this->HCPScontract->getAllHCPStaffsOfSpecificHCPFacilityOfHCPBySearchQuery($hcp_id, $query, $hcp_facility_id);
			} else {
				return $this->HCPScontract->getAllHCPStaffsOfHCPBySearchQuery($hcp_id, $query);
			}
		} else {
			if ($hcp_facility_id != 0) {
				return $this->HCPScontract->getAllHCPStaffsOfSpecificHCPFacilityOfHCP($hcp_id, $hcp_facility_id);
			} else {
				return $this->HCPScontract->getAllHCPStaffsByHCPId($hcp_id);
			}
		}
	}

	protected function getHCPDoctors($query, $hcp_id, $hcp_facility_id) {
		if (isset($query)) {
			if ($hcp_facility_id != 0) {
				return $this->HCPDcontract->getAllHCPDoctorsOfSpecificHCPFacilityOfHCPBySearchQuery($hcp_id, $query, $hcp_facility_id);
			} else {
				return $this->HCPDcontract->getAllHCPDoctorsOfHCPBySearchQuery($hcp_id, $query);
			}
		} else {
			if ($hcp_facility_id != 0) {
				return $this->HCPDcontract->getAllHCPDoctorsOfSpecificHCPFacilityOfHCP($hcp_id, $hcp_facility_id);
			} else {
				return $this->HCPDcontract->getAllHCPDoctorsByHCPId($hcp_id);
			}
		}
	}

	public function getAllHCPDoctorAndStaff($hcp_id) {
		$query_array_doctor = $this->getHCPDoctors(null, $hcp_id, null);
		$query_array_staff = $this->getHCPStaff(null, $hcp_id, null);
		$query_array = array_merge($query_array_doctor, $query_array_staff);
		return $query_array;
	}

	public function deleteHCPDoctorOrStaff($data_array, $hcp_id) {

		$model = UserContext::getModelNameByModelIdentifier($data_array->input('targetModel'));
		$model_id = $data_array->input('targetId');
		switch ($model) {
		case 'Doctor':{
				$hcp_doctor_list = $this->HCPDcontract->getHCPDoctorByDoctorAndProviderId($model_id, $hcp_id);
				foreach ($hcp_doctor_list as $hcp_doc) {
					$hcp_doctor = $this->HCPDcontract->getHCPDoctorByHCPDoctorId($hcp_doc->hcp_doctor_id);
					$this->HCPDcontract->deleteHCPDoctor($hcp_doctor);
				}
				return 'true';
				break;
			}

		case 'Staff':{
				$hcp_staff_list = $this->HCPScontract->getHCPStaffByStaffAndProviderId($model_id, $hcp_id);
				foreach ($hcp_staff_list as $hcp_staff) {
					$hcp_staff = $this->HCPScontract->getHCPStaffByHCPStaffId($hcp_staff->hcp_staff_id);
					$this->HCPScontract->deleteHCPStaff($hcp_staff);
				}
				return 'true';
				break;
			}

		default:{
				return 0;
				break;
			}
		}
	}

	public function getAllStaffOrDoctorBySearchQuery($data_array) {
		$query = $data_array->input('query');
		if (isset($query)) {
			$query_array_doctor = $this->Dcontract->getDoctorBySearchQuery($query);
			$query_array_staff = $this->Scontract->getStaffBySearchQuery($query);
			$query_array = array_merge($query_array_doctor, $query_array_staff);
			return $query_array;
		}
	}

	public function getTimeTableOfDoctorOrStaffOfHCPFacility($data_array) {
		$model = UserContext::getModelNameByModelIdentifier($data_array->input('targetModel'));
		$model_id = $data_array->input('targetId');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$index = $data_array->input('index');
		switch ($model) {
		case 'Doctor':{
				$hcp_doctor = $this->HCPDcontract->getHCPDoctorByDoctorIdAndHCPFacilityId($model_id, $hcp_facility_id);
				// return $hcp_doctor;
				if ($hcp_doctor) {
					$timeTable = $this->TTcontract->getCompleteTimeTableOfRelation($hcp_doctor);
					if (empty($timeTable)) {
						$timeTable = $this->TTcontract->getCompleteTimeTableFormate();
					}
					$data = ['status' => 1, "index" => $index, 'timeTable' => $timeTable];
				} else {
					$timeTable = $this->TTcontract->getCompleteTimeTableFormate();
					$data = ['status' => 0, "index" => $index, 'timeTable' => $timeTable];
				}
				return $data;
				break;
			}

		case 'Staff':{
				$hcp_staff = $this->HCPScontract->getHCPDoctorStaffByStaffIdAndHCPFacilityId($model_id, $hcp_facility_id);
				if ($hcp_staff) {
					$timeTable = $this->TTcontract->getCompleteTimeTableOfRelation($hcp_staff);
					if (empty($timeTable)) {
						$timeTable = $this->TTcontract->getCompleteTimeTableFormate();
					}
					$data = ['status' => 1, "index" => $index, 'timeTable' => $timeTable];
				} else {
					$timeTable = $this->TTcontract->getCompleteTimeTableFormate();
					$data = ['status' => 0, "index" => $index, 'timeTable' => $timeTable];
				}
				return $data;

				break;
			}

		default:{
				$timeTable = $this->TTcontract->getCompleteTimeTableFormate();
				$data = ['status' => 0, "index" => $index, 'timeTable' => $timeTable];
				return $data;
				break;
			}
		}
	}

	public function addOrUpdateHCPAllStaff($data_array, $hcp_id) {
		if ($data_array->input('submit_event_type') == 'add') {
			return $this->addHCPAllStaff($data_array, $hcp_id);
		} else {
			return $this->updateHCPAllStaff($data_array, $hcp_id);
		}
	}

	protected function updateHCPAllStaff($data_array, $hcp_id) {
		$selectedFacilityList = $this->doctorStaffSelectedFacilityMaker($data_array->input('facilityAttachedTimeTable'));

		$model = UserContext::getModelNameByModelIdentifier($data_array->input('facilityAttachedTimeTable')[0]['doctorOrStaff']['targetModel']);

		switch ($model) {
		case 'Doctor':{
				$hcp_doctor_array = $this->addHealthCareProviderDoctor($data_array->input('facilityAttachedTimeTable')[0]['doctorOrStaff']['targetId'], $selectedFacilityList, $hcp_id, $data_array->input('facilityAttachedTimeTable'));
				$model = 'Model\HealthCareProviderDoctor';
				return $this->saveTimeTable($hcp_doctor_array, $model, $selectedFacilityList);
				break;
			}
		case 'Staff':{
				$hcp_staff_array = $this->addHealthCareProviderStaff($data_array->input('facilityAttachedTimeTable')[0]['doctorOrStaff']['targetId'], $selectedFacilityList, $hcp_id, $data_array->input('facilityAttachedTimeTable'));
				$model = 'Model\HealthCareProviderStaff';
				return $this->saveTimeTable($hcp_staff_array, $model, $selectedFacilityList);
				break;
			}
		default:{
				break;
			}
		}
	}

	protected function addHCPAllStaff($data_array, $hcp_id) {

		$selectedFacilityList = [];
		if ($data_array->input('facilityAttachedTimeTable')) {
			$selectedFacilityList = $this->doctorStaffSelectedFacilityMaker($data_array->input('facilityAttachedTimeTable'));
		} else {
			return "Empty FacilityAttachedTimeTable";
		}

		if ($data_array->input('staffEmailAddress') && $data_array->input('staff_type')) {
			return $this->saveHCPDoctorORStaffViaEmail($data_array, $selectedFacilityList, $hcp_id);
		} else if ($data_array->input('selectedDoctorOrStaff')) {
			return $this->saveHCPDoctorORStaffNotViaEmail($data_array, $selectedFacilityList, $hcp_id);
		} else {
			return "Empty staffEmailAddress AND selectedDoctorOrStaff";
		}
	}

	public function saveHCPDoctorORStaffViaEmail($data_array, $selectedFacilityList, $hcp_id) {
		$email_address = $data_array->input('staffEmailAddress');
		$type = $data_array->input('staff_type');

		switch ($type) {
		case 'Doctor':{
				$doctor = $this->Dcontract->addOrUpdateDoctorInfo(new Doctor());
				$this->CIcontract->addOrUpdateContactInfo($doctor, new ContactInformation(['email' => $email_address]));
// event should be fired header_remove()

				$hcp_doctor_array = $this->addHealthCareProviderDoctor($doctor->id, $selectedFacilityList, $hcp_id, $data_array->input('facilityAttachedTimeTable'));
				$model = 'Model\HealthCareProviderDoctor';
				return $this->saveTimeTable($hcp_doctor_array, $model, $selectedFacilityList);
				break;
			}
		case 'Staff':{
				$staff = $this->Scontract->addOrUpdateStaffInfo(new Staff());
				$this->CIcontract->addOrUpdateContactInfo($staff, new ContactInformation(['email' => $email_address]));
				$hcp_staff_array = $this->addHealthCareProviderStaff($staff->id, $selectedFacilityList, $hcp_id, $data_array->input('facilityAttachedTimeTable'));
				$model = 'Model\HealthCareProviderStaff';
				return $this->saveTimeTable($hcp_staff_array, $model, $selectedFacilityList);
				break;
			}
		default:{
				break;
			}
		}
	}

	public function saveHCPDoctorORStaffNotViaEmail($data_array, $selectedFacilityList, $hcp_id) {
		$selectedDoctorStaff = $data_array->input('selectedDoctorOrStaff');
		$type = UserContext::getModelNameByModelIdentifier($selectedDoctorStaff['model_identifier']);
		switch ($type) {
		case 'Doctor':{
				$hcp_doctor_array = $this->addHealthCareProviderDoctor($selectedDoctorStaff['target_id'], $selectedFacilityList, $hcp_id, $data_array->input('facilityAttachedTimeTable'));
				// return $hcp_doctor_array;
				$model = 'Model\HealthCareProviderDoctor';
				return $this->saveTimeTable($hcp_doctor_array, $model, $selectedFacilityList);
				break;
			}
		case 'Staff':{
				$hcp_staff_array = $this->addHealthCareProviderStaff($selectedDoctorStaff['target_id'], $selectedFacilityList, $hcp_id, $data_array->input('facilityAttachedTimeTable'));
				$model = 'Model\HealthCareProviderStaff';
				return $this->saveTimeTable($hcp_staff_array, $model, $selectedFacilityList);
				break;
			}
		default:{
				break;
			}
		}
	}

	public function doctorStaffSelectedFacilityMaker($facilityAttachedTimeTable) {
		$facilities = [];
		for ($i = 0; $i < count($facilityAttachedTimeTable); $i++) {
			if ($facilityAttachedTimeTable[$i]['selected']) {
				$facility = ['hcp_facility_id' => $facilityAttachedTimeTable[$i]['facility']['hcp_facility_id'], 'index' => $i, 'timeTable' => $facilityAttachedTimeTable[$i]['timeTable']];
				array_push($facilities, $facility);
			}
		}
		return $facilities;
	}

	protected function addHealthCareProviderDoctor($doctor_id, $selectedFacilityList, $hcp_id, $facilityAttachedTimeTable) {

		for ($i = 0; $i < count($facilityAttachedTimeTable); $i++) {
			$hcp_doctor = $this->HCPDcontract->getHCPDoctorByDoctorIdAndHCPFacilityId($doctor_id, $facilityAttachedTimeTable[$i]['facility']['hcp_facility_id']);
			if (isset($hcp_doctor)) {
				$this->HCPDcontract->deleteHCPDoctor($hcp_doctor);
			}

		}

		$hcp_doctor_array = [];
		for ($i = 0; $i < count($selectedFacilityList); $i++) {
			$hcp_doctor = new HealthCareProviderDoctor();
			$hcp_doctor->hcp_facility_id = $selectedFacilityList[$i]['hcp_facility_id'];
			$hcp_doctor->doctor_id = $doctor_id;
			$this->HCPDcontract->addHCPDoctor($hcp_doctor);
			array_push($hcp_doctor_array, ['hcp_target_id' => $hcp_doctor->id]);
		}
		return $hcp_doctor_array;
	}

	protected function addHealthCareProviderStaff($staff_id, $selectedFacilityList, $hcp_id, $facilityAttachedTimeTable) {

		for ($i = 0; $i < count($facilityAttachedTimeTable); $i++) {
			$hcp_staff = $this->HCPDcontract->getHCPStaffByStaffIdAndHCPFacilityId($staff_id, $facilityAttachedTimeTable[$i]['facility']['hcp_facility_id']);
			if (isset($hcp_staff)) {
				$this->HCPDcontract->deleteHCPStaff($hcp_staff);
			}

		}

		$hcp_staff_array = [];
		for ($i = 0; $i < count($selectedFacilityList); $i++) {
			$hcp_staff = new HealthCareProviderStaff();
			$hcp_staff->hcp_facility_id = $selectedFacilityList[$i]['hcp_facility_id'];
			$hcp_staff->staff_id = $staff_id;
			$this->HCPScontract->addHCPStaff($hcp_staff);
			array_push($hcp_staff_array, ['hcp_target_id' => $hcp_staff->id]);
		}
		return $hcp_staff_array;
	}

	protected function saveTimeTable($hcp_doctor_staff_array, $model, $selectedFacilityList) {

		$facilityTimeTable = [];

		for ($i = 0; $i < count($hcp_doctor_staff_array); $i++) {
			$timeTable = $selectedFacilityList[$i]['timeTable'];
			for ($k = 0; $k < count($timeTable); $k++) {
				if (isset($timeTable[$k]['time_to']) && isset($timeTable[$k]['time_from']) && $timeTable[$k]['time_to'] != null && $timeTable[$k]['time_from'] != null) {
					$selectedFacilityList[$i]['selected'][$k] = true;
				} else {
					$selectedFacilityList[$i]['selected'][$k] = false;
				}
			}
			$timeTableData = ['data' => $selectedFacilityList[$i]['timeTable'], 'selected' => $selectedFacilityList[$i]['selected']];
			$facilityTimeTable = $this->TTcontract->timeTableMakerForDB($timeTableData);

			$relation = $model::find($hcp_doctor_staff_array[$i]['hcp_target_id']);
			$this->TTcontract->addOrUpdateTimeTableInfo($relation, $facilityTimeTable);

		}

		return 'true';
	}

	public function getAllHCPDoctorsOfHCPFacility($data_array, $hcp_id) {
		if ($data_array->input('hcp_facility_id') != '0') {
			return $this->HCPDcontract->getAllHCPDoctorsOfSpecificHCPFacilityOfHCP($hcp_id, $data_array->input('hcp_facility_id'));
		} else {
			return $this->HCPDcontract->getAllHCPDoctorsByHCPId($hcp_id);
		}

	}

	public function addOrUpdateHCPDoctor($hcpFacility, $doctor) {
		return $this->HCPDcontract->addOrUpdateHCPDoctor(['hcp_facility_id' => $hcpFacility->id, 'doctor_id' => $doctor->id]);
	}

}
