<?php

namespace Service\HealthCareProvider;
use Model\Address;
use Model\ContactInformation;
use Model\PersonalInformation;
use Repository\Contracts\Common\Address as AddressContract;
use Repository\Contracts\Common\ContactInformation as ContactInformationContract;
use Repository\Contracts\Common\PersonalInfo as PersonalInfoContract;
use Repository\Contracts\Common\UserRepository as UserRepositoryContract;
use Repository\Contracts\HealthCareProvider\Doctor as DoctorContract;

class DoctorService {

	protected $personalInfoContract = '';
	protected $addressContract = '';
	protected $contactInfoContract = '';
	protected $userRepositoryContract = '';
	protected $doctorRepositoryContract = '';
	public function __construct(AddressContract $ac, ContactInformationContract $cc, PersonalInfoContract $pc,
		UserRepositoryContract $ur, DoctorContract $dr
	) {
		$this->contactInfoContract = $cc;
		$this->addressContract = $ac;
		$this->personalInfoContract = $pc;
		$this->userRepositoryContract = $ur;
		$this->doctorRepositoryContract = $dr;
	}
	public function getDoctorByEmails($data_array) {

		if (is_null($contactInfo = $this->contactInfoContract->getDoctorContactInfoByEmails($this->doctorEmailsArray($data_array)))) {

			return null;
		}

		// check if doctor already exists in database or not
		//dd($this->doctorEmailsArray($data_array));
		return $this->doctorRepositoryContract->getDoctorByDoctorId($contactInfo->contactable_id);
	}
	public function addOrUpdateDoctor($doctor, $data_array) {

		$doctor = $this->doctorRepositoryContract->addOrUpdateDoctor($doctor, $data_array);

		$this->contactInfoContract->addOrUpdateContactInfo($doctor, new ContactInformation($data_array));

		$this->personalInfoContract->addOrUpdatePersonalInfo($doctor, new PersonalInformation($data_array));
		return $doctor;
	}
	public function getProfileByRelation($relation) {
		$personalInfo = $this->personalInfoContract->getPersonalInfoDetails($relation);
		$address = $this->addressContract->getAddressDetails($relation);
		$contactInfo = $this->contactInfoContract->getContactDetails($relation);

		$dataArray = [];
		$dataArray = array_merge($contactInfo->toArray(), $address->toArray());
		$dataArray = array_merge($dataArray, $personalInfo->toArray());
		return $dataArray;
	}

	public function getContactInfoByRelation($relation) {

		return $this->contactInfoContract->getContactDetails($relation);

	}

	public function updatePersonalInfo($relation, $data_array) {

		return $this->personalInfoContract->addOrUpdatePersonalInfo($relation,
			new PersonalInformation($data_array->all()));

	}

	public function updateUserProfileStatusToComplete($user) {

		$this->userRepositoryContract->updateUserProfileStatus($user, 1);

	}
	public function updateProfileByRelation($relation, $data_array) {
		$this->personalInfoContract->addOrUpdatePersonalInfo($relation, new PersonalInformation($data_array->all()));
		$this->contactInfoContract->addOrUpdateContactInfo($relation, new ContactInformation($data_array->all()));
		$this->addressContract->addOrUpdateAddressInfo($relation, new Address($data_array->all()));
		return 1;
	}
	public function getStaffByUser($user) {
		if (is_null($healthCareProvider = $this->healthCareProviderContract->getHealthCareProviderByUser($user))) {
			$healthCareProvider = $this->healthCareProviderContract->createHealthCareProvider($user, staffTypeId());
		}

		if (is_null($staff = $this->staffContract->getStaffByHCP($healthCareProvider))) {
			$staff = $this->staffContract->createStaff($healthCareProvider);
		}
		return $staff;
	}
	public function getHealthCareProviderByUser($user) {

		return $this->healthCareProviderContract->getHealthCareProviderByUser($user);
	}

	private function doctorEmailsArray($dataArray) {

		$emails = array();
		array_push($emails, $dataArray['email']);
		array_push($emails, $dataArray['user_email']);

		/* option 1 :: two ways to get unique values
		foreach($something as $value) {
		if( !in_array($value,$liste)) array_push($liste,$value);
		}*/
		/* option 2

		foreach($something as $value) {
		array_push($liste,$value);
		}
		$liste  = array_unique($liste);
		 */

		return array_unique($emails);
	}

}
