<?php

namespace Service\HealthCareProvider;

use App\Events\HealthCareProvider\AppointmentBookedAndApprovedByProvider;
use App\Events\HealthCareProvider\RescheduleAppointmentRequestApproved;
use App\Events\HealthCareProvider\RescheduleAppointmentRequestRejectedByProvider;
use App\Events\HealthCareProvider\RescheduleAppointmentRequestRescheduled;
use App\Events\HealthCareProvider\ReserveAppointmentRequestApproved;
use App\Events\HealthCareProvider\ReserveAppointmentRequestRescheduled;
use App\Events\HealthCareProvider\ShortCallAppointmentPublishByProvider;
use Carbon\Carbon;
use Facades\Service\Common\UserData;
use Facades\Service\HealthCareProvider\PatientShortCallService;
use Model\AnEvent as AnEventModel;
use Model\ShortCallAppointment as ShortCallAppointmentModel;
use Repository\Contracts\Common\AnEvent;
use Repository\Contracts\HealthCareProvider\HCPFacility;
use Repository\Contracts\HealthCareProvider\RescheduleAppointmentRequest;
use Repository\Contracts\HealthCareProvider\ReserveAppointmentRequest;
use Repository\Contracts\HealthCareProvider\ShortCallAppointment;

class AppointmentAndRequestOperationService {

	protected $AEcontract = '';
	protected $ReserveARcontract = '';
	protected $RescheduleARcontract = '';
	protected $HCPFcontract = '';
	protected $ShortCallAcontract = '';

	public function __construct(RescheduleAppointmentRequest $RescheduleARcon, ReserveAppointmentRequest $ReserveARcon, AnEvent $AEcon, HCPFacility $HCPFcon, ShortCallAppointment $ShortCallAcon) {
		$this->AEcontract = $AEcon;
		$this->ReserveARcontract = $ReserveARcon;
		$this->RescheduleARcontract = $RescheduleARcon;
		$this->HCPFcontract = $HCPFcon;
		$this->ShortCallAcontract = $ShortCallAcon;

	}
	/*reschedule the upcoming appointment by provider*/
	public function rescheduleExistingAnEvent($data_array) {
		$an_event = $this->AEcontract->getAnEventByAnEventId($data_array->input('an_event_id'));
		$an_event->date = $data_array->input('date');
		$an_event->start_time = $data_array->input('time');
		return $this->AEcontract->addOrUpdateAnEvent($an_event);
	}

	public function reserveRequestApproval($data_array) {
		$reserve_appointment = $this->ReserveARcontract->getReserveAppointmentByReserveAppointmentId($data_array->input('appointment_reservation_id'));
		$an_event = $this->AEcontract->getAnEventByAnEventId($reserve_appointment->an_event_id);
		$an_event->appointment_status = config('appointmentnotify.appointment_status.Private');
		$an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.Approved');
		$an_event->site_user_id = $reserve_appointment->site_user_id;
		$this->AEcontract->addOrUpdateAnEvent($an_event);
		$this->ReserveARcontract->removeReserveAppointment($reserve_appointment);
		event(new ReserveAppointmentRequestApproved($an_event));
		return 'true';
	}

	public function reserveRequestReschedule($data_array) {
		$reserve_appointment = $this->ReserveARcontract->getReserveAppointmentByReserveAppointmentId($data_array->input('appointment_reservation_id'));
		$an_event = $this->AEcontract->getAnEventByAnEventId($reserve_appointment->an_event_id);
		$an_event->date = $data_array->input('date');
		$an_event->start_time = $data_array->input('time');
		$an_event->appointment_status = config('appointmentnotify.appointment_status.Private');
		$an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.Approved');
		$this->AEcontract->addOrUpdateAnEvent($an_event);
		$this->ReserveARcontract->removeReserveAppointment($reserve_appointment);
		event(new ReserveAppointmentRequestRescheduled($an_event));
		return 'true';
	}

	public function rescheduleRequestApproval($data_array) {
		$reschedule_appointment = $this->RescheduleARcontract->getRescheduleAppointmentByRescheduleAppointmentId($data_array->input('appointment_reschedule_id'));
		$old_an_event = $this->AEcontract->getAnEventByAnEventId($reschedule_appointment->an_event_id);

		$new_an_event = new AnEventModel();
		$new_an_event->date = $data_array->input('approved_date');
		$new_an_event->start_time = $data_array->input('approved_time');
		$new_an_event->end_time = $old_an_event->end_time;
		$new_an_event->doctor_id = $old_an_event->doctor_id;
		$new_an_event->site_user_id = $old_an_event->site_user_id;
		$new_an_event->calendar_type = $old_an_event->calendar_type;
		$new_an_event->appointment_reason_id = $old_an_event->appointment_reason_id;
		$new_an_event->health_care_provider_facility_id = $old_an_event->health_care_provider_facility_id;
		$new_an_event->health_care_facility_id = $old_an_event->health_care_facility_id;
		$new_an_event->appointment_status = config('appointmentnotify.appointment_status.Private');
		$new_an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.Approved');

		$this->AEcontract->addOrUpdateAnEvent($new_an_event);
		$old_an_event->site_user_id = null;
		$old_an_event->appointment_reason_id = null;
		$old_an_event->appointment_status = config('appointmentnotify.appointment_status.Public');
		$old_an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.Publish');

		$this->AEcontract->addOrUpdateAnEvent($old_an_event);
		$this->RescheduleARcontract->removeRescheduleAppointment($reschedule_appointment);
		event(new RescheduleAppointmentRequestApproved($new_an_event));
		return 'true';
	}

	public function rescheduleRequestReschedule($data_array) {
		$reschedule_appointment = $this->RescheduleARcontract->getRescheduleAppointmentByRescheduleAppointmentId($data_array->input('appointment_reschedule_id'));
		$old_an_event = $this->AEcontract->getAnEventByAnEventId($reschedule_appointment->an_event_id);

		$new_an_event = new AnEventModel();
		$new_an_event->date = $data_array->input('date');
		$new_an_event->start_time = $data_array->input('time');
		$new_an_event->end_time = $old_an_event->end_time;
		$new_an_event->doctor_id = $old_an_event->doctor_id;
		$new_an_event->site_user_id = $old_an_event->site_user_id;
		$new_an_event->calendar_type = $old_an_event->calendar_type;
		$new_an_event->appointment_reason_id = $old_an_event->appointment_reason_id;
		$new_an_event->health_care_provider_facility_id = $old_an_event->health_care_provider_facility_id;
		$new_an_event->health_care_facility_id = $old_an_event->health_care_facility_id;
		$new_an_event->appointment_status = config('appointmentnotify.appointment_status.Private');
		$new_an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.Approved');

		$this->AEcontract->addOrUpdateAnEvent($new_an_event);
		$old_an_event->site_user_id = null;
		$old_an_event->appointment_reason_id = null;
		$old_an_event->appointment_status = config('appointmentnotify.appointment_status.Public');
		$old_an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.Publish');

		$this->AEcontract->addOrUpdateAnEvent($old_an_event);
		$this->RescheduleARcontract->removeRescheduleAppointment($reschedule_appointment);
		event(new RescheduleAppointmentRequestRescheduled($new_an_event));
		return 'true';
	}

	public function publishNewAppointment($data_array) {
		$start_time = $data_array->input('start_time');
		$end_time = $data_array->input('end_time');

		if ($end_time == '') {
			$end_time = Carbon::parse($start_time)->addMinutes(config('appointmentnotify.appointment_default_end_time_duration'))->format('H:i:s');
		}

		$an_event = [
			'date' => $data_array->input('date'),
			'start_time' => $data_array->input('start_time'),
			'end_time' => $end_time,
			'doctor_id' => $data_array->input('doctor_id'),
			'health_care_provider_facility_id' => $data_array->input('hcp_facility_id'),
			'health_care_facility_id' => $this->HCPFcontract->getHCPFacilityByHCPFacilityId($data_array->input('hcp_facility_id'))->health_care_facility_id,
			'appointment_status' => config('appointmentnotify.appointment_status.Public'),
			'appointment_current_state' => config('appointmentnotify.appointment_current_state.Publish'),
		];
		$this->AEcontract->addOrUpdateAnEvent(new AnEventModel($an_event));
		return 'true';
	}

	public function publishedAppointmentReschedule($data_array) {
		$an_event = $this->AEcontract->getAnEventByAnEventId($data_array->input('an_event_id'));
		$an_event->date = $data_array->input('date');
		$an_event->start_time = $data_array->input('time');
		$this->AEcontract->addOrUpdateAnEvent($an_event);
		return 'true';
	}

	public function publishedAppointmentCancel($data_array) {
		$an_event = $this->AEcontract->getAnEventByAnEventId($data_array->input('an_event_id'));
		return $this->AEcontract->removeAnEvent($an_event);
	}
	public function publishedAppointmentPatientAssign($data_array) {
		$siteUser = UserData::getSiteUserByUserEmailAddress($data_array->input('patient_email'));
		$an_event = $this->AEcontract->getAnEventByAnEventId($data_array->input('an_event_id'));
		$an_event->appointment_status = config('appointmentnotify.appointment_status.Private');
		$an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.Approved');
		$an_event->site_user_id = $siteUser->id;
		$this->AEcontract->addOrUpdateAnEvent($an_event);
		event(new AppointmentBookedAndApprovedByProvider($an_event));
		return 'true';
	}
	public function publishedAppointmentToShortCallList($data_array) {
		$an_event = $this->AEcontract->getAnEventByAnEventId($data_array->input('an_event_id'));
		$an_event->appointment_status = config('appointmentnotify.appointment_status.Public');
		$an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.ShortCall');
		$this->AEcontract->addOrUpdateAnEvent($an_event);
		$short_call_appointment = [
			'an_event_id' => $an_event->id,
			'expiry_date_time' => Carbon::now()->addMinutes(config('appointmentnotify.short_call_appointment_expiry_duration')),
		];

		$this->ShortCallAcontract->addOrUpdateShortCallAppointment(new ShortCallAppointmentModel($short_call_appointment));
		$short_call_patient_list = PatientShortCallService::getAllShortCallListPatientsOfDoctor($an_event->doctor_id, $an_event->health_care_provider_facility_id);
		event(new ShortCallAppointmentPublishByProvider($short_call_patient_list, $an_event, $short_call_appointment));
		return 'true';
	}

	public function rescheduleRequestCancel($data_array) {
		$reschedule_appointment = $this->RescheduleARcontract->getRescheduleAppointmentByRescheduleAppointmentId($data_array->input('appointment_reschedule_id'));
		$an_event = $this->AEcontract->getAnEventByAnEventId($reschedule_appointment->an_event_id);
		$an_event->appointment_status = config('appointmentnotify.appointment_status.Private');
		$an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.Approved');
		$this->AEcontract->addOrUpdateAnEvent($an_event);
		$this->RescheduleARcontract->removeRescheduleAppointment($reschedule_appointment);
		event(new RescheduleAppointmentRequestRejectedByProvider($an_event));
		return 'true';
	}

	public function dumpCanceledAppointment($data_array) {
		$an_event = $this->AEcontract->getAnEventByAnEventId($data_array->input('an_event_id'));
		return $this->AEcontract->removeAnEvent($an_event);
	}

	public function publishCanceledAppointment($data_array) {
		$an_event = $this->AEcontract->getAnEventByAnEventId($data_array->input('an_event_id'));
		$an_event->site_user_id = null;
		$an_event->appointment_reason_id = null;
		$an_event->appointment_status = config('appointmentnotify.appointment_status.Public');
		$an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.Publish');
		return $this->AEcontract->removeAnEvent($an_event);
	}

	public function putCanceledAppointmentToShortCallPool($data_array) {
		$an_event = $this->AEcontract->getAnEventByAnEventId($data_array->input('an_event_id'));

		$an_event->appointment_status = config('appointmentnotify.appointment_status.Public');
		$an_event->appointment_current_state = config('appointmentnotify.appointment_current_state.ShortCall');
		$this->AEcontract->addOrUpdateAnEvent($an_event);
		$short_call_appointment = [
			'an_event_id' => $an_event->id,
			'expiry_date_time' => Carbon::now()->addMinutes(config('appointmentnotify.short_call_appointment_expiry_duration')),
		];

		$this->ShortCallAcontract->addOrUpdateShortCallAppointment(new ShortCallAppointmentModel($short_call_appointment));
		$short_call_patient_list = PatientShortCallService::getAllShortCallListPatientsOfDoctor($an_event->doctor_id, $an_event->health_care_provider_facility_id);
		event(new ShortCallAppointmentPublishByProvider($short_call_patient_list, $an_event, $short_call_appointment));
		return 'true';
	}

}
