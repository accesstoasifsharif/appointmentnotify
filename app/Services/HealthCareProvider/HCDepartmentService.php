<?php

namespace Service\HealthCareProvider;

use Facades\Service\Common\DataTable;
use Repository\Contracts\HealthCareProvider\HealthCareDepartment;

class HCDepartmentService {

	protected $HCDcontract = '';

	public function __construct(HealthCareDepartment $HCDcon) {

		$this->HCDcontract = $HCDcon;

	}

	public function getAllHCDepartmentsByHCPId($request, $hcp_id) {
		return $hcp_id;
		$query = $request->input('query');
		$per_page = $request->input('per_page');
		$page = $request->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;
		if ($query != '') {
			$query_array = $this->HCPDcontract->getAllHCDepartmentsBySearchQuery($query, $hcp_id);
		} else {
			$query_array = $this->HCPDcontract->getAllHCDepartments($hcp_id);
		}

		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

}
