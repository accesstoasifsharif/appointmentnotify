<?php

namespace Service\HealthCareProvider;

use Facades\Service\Common\MessageService;
use Facades\Service\Common\UserData;
use Facades\Service\HealthCareProvider\HCPPatientService;
use Repository\Contracts\HealthCareProvider\HCPDoctor as HCPDoctorContract;
use Repository\Contracts\HealthCareProvider\HCPStaff as HCPStaffContract;

class HCPConversationService {

	protected $HCPScontract = '';
	protected $HCPDcontract = '';

	public function __construct(HCPStaffContract $HCPScon, HCPDoctorContract $HCPDcon) {
		$this->HCPScontract = $HCPScon;
		$this->HCPDcontract = $HCPDcon;
	}

	public function getHCPAllStaffConversationPreview($data_array, $hcp_id, $auth_user_id) {

		$hcp_facility_id = $data_array->input('hcp_facility_id');
		if ($hcp_facility_id != 0) {
			$raw_array_doctor = $this->HCPScontract->getAllHCPStaffsOfSpecificHCPFacilityOfHCP($hcp_id, $hcp_facility_id);
			$raw_array_staff = $this->HCPDcontract->getAllHCPDoctorsOfSpecificHCPFacilityOfHCP($hcp_id, $hcp_facility_id);
		} else {
			$raw_array_doctor = $this->HCPScontract->getAllHCPStaffsByHCPId($hcp_id);
			$raw_array_staff = $this->HCPDcontract->getAllHCPDoctorsByHCPId($hcp_id);
		}
		$data = array_merge($raw_array_doctor, $raw_array_staff);
		return $this->getConversationPreviewOfStaff($data, $auth_user_id);
	}

	protected function getConversationPreviewOfStaff($data, $auth_user_id) {
		$auth_user_identifier = config("appointmentnotify.model_identifiers.User");
		for ($i = 0; $i < count($data); $i++) {
			$user_2 = UserData::getUserByModelIdAndModelIdentifier($data[$i]->target_id, $data[$i]->model_identifier);
			if ($user_2->id == $auth_user_id) {
				unset($data[$i]);
			}
		}
		$data = array_values($data);
		for ($j = 0; $j < count($data); $j++) {
			$user_2 = UserData::getUserByModelIdAndModelIdentifier($data[$j]->target_id, $data[$j]->model_identifier);
			$conversation = MessageService::getSpecificUserConverstionPreview($auth_user_id, $user_2->id);
			if ($conversation != '0' && count($conversation->messages) > 0) {
				$data[$j]->auth_user_id = $auth_user_id;
				$data[$j]->auth_user_identifier = $auth_user_identifier;
				$data[$j]->message_preview = $conversation->messages[0];
			} else {
				$data[$j]->auth_user_id = $auth_user_id;
				$data[$j]->auth_user_identifier = $auth_user_identifier;
				$data[$j]->message_preview = '';
			}
		}
		return $data;
	}

	public function getPatientsConversationPreview($data_array, $hcp_id, $auth_user_id) {

		$patients = HCPPatientService::getAllHCPDoctorPatients($hcp_id, $data_array->input('hcp_doctor_id'));

		return $this->getConversationPreviewOfPatient($patients, $auth_user_id);
	}

	protected function getConversationPreviewOfPatient($data, $auth_user_id) {

		for ($i = 0; $i < count($data); $i++) {
			$user_1 = UserData::getUserByModelIdAndModelIdentifier($data[$i]->target_id, $data[$i]->model_identifier);
			$user_2 = UserData::getUserByModelIdAndModelIdentifier($data[$i]->doctor_id, $data[$i]->doctor_identifier);
			$conversation = MessageService::getSpecificUserConverstionPreview($user_1->id, $user_2->id);

			if ($conversation != '0' && count($conversation->messages) > 0) {
				$data[$i]->message_preview = $conversation->messages[0];
			} else {
				$data[$i]->message_preview = '';
			}

		}

		return $data;
	}

}
