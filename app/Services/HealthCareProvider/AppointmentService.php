<?php

namespace Service\HealthCareProvider;

use Facades\Service\Common\DataTable;
use Repository\Contracts\HealthCareProvider\PastAppointment;
use Repository\Contracts\HealthCareProvider\PublishedAppointment;
use Repository\Contracts\HealthCareProvider\UpcomingAppointment;

class AppointmentService {

	protected $UCAcontract = '';
	protected $PAcontract = '';
	protected $PublishedAcontract = '';

	public function __construct(UpcomingAppointment $UCAcon, PastAppointment $PAcon, PublishedAppointment $PublishedAcon) {

		$this->UCAcontract = $UCAcon;
		$this->PAcontract = $PAcon;
		$this->PublishedAcontract = $PublishedAcon;

	}

	public function getAllUpcomingAppointments($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;
		if ($doctor_id == 0) {
			if ($hcp_facility_id == 0) {
				$query_array = $this->UCAcontract->getAllDoctorAllHCPFacilityAppointments($hcp_id);
			} else {
				$query_array = $this->UCAcontract->getAllDoctorSpecificHCPFacilityAppointments($hcp_facility_id, $hcp_id);
			}
		} else {
			if ($hcp_facility_id == 0) {
				$query_array = $this->UCAcontract->getSpecificDoctorAllHCPFacilityAppointments($doctor_id, $hcp_id);
			} else {
				$query_array = $this->UCAcontract->getSpecificDoctorSpecificHCPFacilityAppointments($doctor_id, $hcp_facility_id, $hcp_id);
			}
		}
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllUpcomingAppointmentsDetail($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;

		$query_array = $this->UCAcontract->getSpecificDoctorSpecificHCPFacilityAppointmentsDetail($doctor_id, $hcp_facility_id, $hcp_id);
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllPublishedAppointments($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;
		if ($doctor_id == 0) {
			if ($hcp_facility_id == 0) {
				$query_array = $this->PublishedAcontract->getAllDoctorAllHCPFacilityAppointments($hcp_id);
			} else {
				$query_array = $this->PublishedAcontract->getAllDoctorSpecificHCPFacilityAppointments($hcp_facility_id, $hcp_id);
			}
		} else {
			if ($hcp_facility_id == 0) {
				$query_array = $this->PublishedAcontract->getSpecificDoctorAllHCPFacilityAppointments($doctor_id, $hcp_id);
			} else {
				$query_array = $this->PublishedAcontract->getSpecificDoctorSpecificHCPFacilityAppointments($doctor_id, $hcp_facility_id, $hcp_id);
			}
		}
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllPublishedAppointmentsDetail($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;

		$query_array = $this->PublishedAcontract->getSpecificDoctorSpecificHCPFacilityAppointmentsDetail($doctor_id, $hcp_facility_id, $hcp_id);
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllPastAppointments($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;
		if ($doctor_id == 0) {
			if ($hcp_facility_id == 0) {
				$query_array = $this->PAcontract->getAllDoctorAllHCPFacilityAppointments($hcp_id);
			} else {
				$query_array = $this->PAcontract->getAllDoctorSpecificHCPFacilityAppointments($hcp_facility_id, $hcp_id);
			}
		} else {
			if ($hcp_facility_id == 0) {
				$query_array = $this->PAcontract->getSpecificDoctorAllHCPFacilityAppointments($doctor_id, $hcp_id);
			} else {
				$query_array = $this->PAcontract->getSpecificDoctorSpecificHCPFacilityAppointments($doctor_id, $hcp_facility_id, $hcp_id);
			}
		}
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllPastAppointmentsDetail($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;

		$query_array = $this->PAcontract->getSpecificDoctorSpecificHCPFacilityAppointmentsDetail($doctor_id, $hcp_facility_id, $hcp_id);
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

}
