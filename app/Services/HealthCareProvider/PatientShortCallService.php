<?php

namespace Service\HealthCareProvider;

use Facades\Service\Common\UserData;
use Illuminate\Support\Facades\DB;
use Model\PatientShortCall as PatientShortCallModel;
use Repository\Contracts\Common\PatientShortCall;

class PatientShortCallService {

	protected $PSCcontract = '';

	public function __construct(PatientShortCall $PSCcon) {

		$this->PSCcontract = $PSCcon;

	}

	public function addPatientToShortCall($data_array) {

		$siteUser = UserData::getSiteUserByUserEmailAddress($data_array->input('patient_email'));
		$site_user_id = null;
		if (isset($siteUser)) {
			$site_user_id = $siteUser->id;
		}
		$short_call = [
			"doctor_id" => $data_array->input('doctor_id'),
			"health_care_provider_facility_id" => $data_array->input('hcp_facility_id'),
			"site_user_id" => $site_user_id,
			"site_user_email" => $data_array->input('patient_email'),
			"time_span" => $data_array->input('duration'),
			"expiry_date_time" => $data_array->input('duration'),
		];

		if (!$this->PSCcontract->checkExistance($data_array->input('doctor_id'), $data_array->input('hcp_facility_id'), $site_user_id)) {
			$this->PSCcontract->addOrUpdatePatientShortCall(new PatientShortCallModel($short_call));
			return 'true';
		}
		return 'false';
	}

	public function getAllShortCallListPatientsOfDoctor($doctor_id, $hcp_facility_id) {
		return DB::select("
			select *
			from patient_short_calls
			join site_users on site_users.id = patient_short_calls.site_user_id
			where patient_short_calls.doctor_id = " . $doctor_id . "
			AND patient_short_calls.health_care_provider_facility_id = " . $hcp_facility_id . "
			");
	}
}
