<?php

namespace Service\HealthCareProvider;

use Repository\Contracts\HealthCareProvider\HCPPatient;

class HCPPatientService {

	protected $HCPPcontract = '';

	public function __construct(HCPPatient $HCPPcon) {
		$this->HCPPcontract = $HCPPcon;
	}

	public function getAllHCPDoctorPatients($hcp_id, $hcp_doctor_id) {
		return $this->HCPPcontract->getAllHCPDoctorPatients($hcp_id, $hcp_doctor_id);
	}

	public function getPatientDetailByPatientId($data_array) {
		return $this->HCPPcontract->getPatientDetail($data_array->input('target_id'));
	}

}