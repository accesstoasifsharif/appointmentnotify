<?php

namespace Service\HealthCareProvider;

use Facades\Service\Common\DataTable;
use Repository\Contracts\HealthCareProvider\CancelAppointmentRequest;
use Repository\Contracts\HealthCareProvider\RescheduleAppointmentRequest;
use Repository\Contracts\HealthCareProvider\ReserveAppointmentRequest;

class AppointmentRequestService {

	protected $ReserveARcontract = '';
	protected $RescheduleARcontract = '';
	protected $CancelARcontract = '';

	public function __construct(ReserveAppointmentRequest $ReserveARcon, RescheduleAppointmentRequest $RescheduleARcon, CancelAppointmentRequest $CancelARcon) {

		$this->ReserveARcontract = $ReserveARcon;
		$this->RescheduleARcontract = $RescheduleARcon;
		$this->CancelARcontract = $CancelARcon;

	}

	public function getAllReserveAppointmentRequests($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;
		if ($doctor_id == 0) {
			if ($hcp_facility_id == 0) {
				$query_array = $this->ReserveARcontract->getAllDoctorAllHCPFacilityAppointmentRequests($hcp_id);
			} else {
				$query_array = $this->ReserveARcontract->getAllDoctorSpecificHCPFacilityAppointmentRequests($hcp_facility_id, $hcp_id);
			}
		} else {
			if ($hcp_facility_id == 0) {
				$query_array = $this->ReserveARcontract->getSpecificDoctorAllHCPFacilityAppointmentRequests($doctor_id, $hcp_id);
			} else {
				$query_array = $this->ReserveARcontract->getSpecificDoctorSpecificHCPFacilityAppointmentRequests($doctor_id, $hcp_facility_id, $hcp_id);
			}
		}
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllReserveAppointmentRequestsDetail($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;

		$query_array = $this->ReserveARcontract->getSpecificDoctorSpecificHCPFacilityAppointmentRequestsDetail($doctor_id, $hcp_facility_id, $hcp_id);
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllRescheduleAppointmentRequests($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;
		if ($doctor_id == 0) {
			if ($hcp_facility_id == 0) {
				$query_array = $this->RescheduleARcontract->getAllDoctorAllHCPFacilityAppointmentRequests($hcp_id);
			} else {
				$query_array = $this->RescheduleARcontract->getAllDoctorSpecificHCPFacilityAppointmentRequests($hcp_facility_id, $hcp_id);
			}
		} else {
			if ($hcp_facility_id == 0) {
				$query_array = $this->RescheduleARcontract->getSpecificDoctorAllHCPFacilityAppointmentRequests($doctor_id, $hcp_id);
			} else {
				$query_array = $this->RescheduleARcontract->getSpecificDoctorSpecificHCPFacilityAppointmentRequests($doctor_id, $hcp_facility_id, $hcp_id);
			}
		}
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllRescheduleAppointmentRequestsDetail($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;

		$query_array = $this->RescheduleARcontract->getSpecificDoctorSpecificHCPFacilityAppointmentRequestsDetail($doctor_id, $hcp_facility_id, $hcp_id);
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllCancelAppointmentRequests($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;
		if ($doctor_id == 0) {
			if ($hcp_facility_id == 0) {
				$query_array = $this->CancelARcontract->getAllDoctorAllHCPFacilityAppointmentRequests($hcp_id);
			} else {
				$query_array = $this->CancelARcontract->getAllDoctorSpecificHCPFacilityAppointmentRequests($hcp_facility_id, $hcp_id);
			}
		} else {
			if ($hcp_facility_id == 0) {
				$query_array = $this->CancelARcontract->getSpecificDoctorAllHCPFacilityAppointmentRequests($doctor_id, $hcp_id);
			} else {
				$query_array = $this->CancelARcontract->getSpecificDoctorSpecificHCPFacilityAppointmentRequests($doctor_id, $hcp_facility_id, $hcp_id);
			}
		}
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getAllCancelAppointmentRequestsDetail($data_array, $hcp_id) {
		$doctor_id = $data_array->input('doctor_id');
		$hcp_facility_id = $data_array->input('hcp_facility_id');
		$per_page = $data_array->input('per_page');
		$page = $data_array->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;

		$query_array = $this->CancelARcontract->getSpecificDoctorSpecificHCPFacilityAppointmentRequestsDetail($doctor_id, $hcp_facility_id, $hcp_id);
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

}
