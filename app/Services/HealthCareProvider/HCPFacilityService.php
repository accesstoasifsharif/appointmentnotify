<?php

namespace Service\HealthCareProvider;

use Auth;
use Facades\Service\Common\DataTable;
use Facades\Service\HealthCareProvider\HealthCareProvider as HCPService;
use Model\Address;
use Model\ContactInformation;
use Model\HealthCareFacility;
use Model\HealthCareProvider;
use Model\HealthCareProviderFacility;
use Model\NationalProviderIdentification;
use Repository\Contracts\Common\Address as AddressContract;
use Repository\Contracts\Common\ContactInformation as ContactInformationContract;
use Repository\Contracts\HealthCareProvider\HCPFacility as HCPFacilityContract;
use Repository\Contracts\HealthCareProvider\HealthCareFacility as HealthCareFacilityContract;
use Repository\Contracts\HealthCareProvider\NationalProviderIdentification as NationalProviderIdentificationContract;
use Repository\Contracts\HealthCareProvider\TimeTable as TimeTableContract;

class HCPFacilityService {

	protected $HCFcontract = '';
	protected $HCPFcontract = '';
	protected $Acontract = '';
	protected $TTcontract = '';
	protected $CIcontract = '';
	protected $NPIcontract = '';

	public function __construct(HealthCareFacilityContract $HCFcon, AddressContract $Acon, ContactInformationContract $CIcon, TimeTableContract $TTcon, HCPFacilityContract $HCPFcon, NationalProviderIdentificationContract $NPIcon) {

		$this->HCFcontract = $HCFcon;
		$this->HCPFcontract = $HCPFcon;
		$this->Acontract = $Acon;
		$this->TTcontract = $TTcon;
		$this->CIcontract = $CIcon;
		$this->NPIcontract = $NPIcon;

	}

	public function allFacilitiesByHCPId($request, $hcp_id) {
		$query = $request->input('query');
		$per_page = $request->input('per_page');
		$page = $request->input('page');
		$per_page = ($per_page == 'undefined') ? DEFAULT_PAGE_PAGINATION : $per_page;
		if ($query != '') {
			$query_array = $this->HCPFcontract->getAllHCPFacilitiesBySearchQuery($query, $hcp_id);
		} else {
			$query_array = $this->HCPFcontract->getAllHCPFacilities($hcp_id);
		}
		$data = DataTable::getPaginate($query_array, $per_page, $page);
		return $data;
	}

	public function getFacilityDetailByFacilityId($facility_id) {
		$data = $this->HCFcontract->getFacilityDetailByFacilityId($facility_id);
		return $data;
	}

	public function storeHCPFacility($data_array, $hcp) {

		$facility = new HealthCareFacility();
		if ($data_array->input('facility_id')) {
			$facility = $this->HCFcontract->getHealthCareFacilityById($data_array->input('facility_id'));
			$facility->facility_name = $data_array->input('facility_name');
		} else {
			$facility->facility_name = $data_array->input('facility_name');
			$facility->creator_id = Auth::User()->id;
			$facility->honor_id = $hcp->user->id;
		}
		$this->HCFcontract->addOrUpdateHealthCareFacility($facility);
		$address = [
			'address_line_1' => $data_array->input('address_line_1'),
			'address_line_2' => $data_array->input('address_line_2'),
			'city_name'      => $data_array->input('city_name'),
			'zip_code'       => $data_array->input('zip_code'),
			'state_id'       => $data_array->input('state_id'),
			'country_id'     => config('appointmentnotify.country_id.USA'),
		];
		$this->Acontract->addOrUpdateAddressInfo($facility, new Address($address));
		$contact = [
			'phone_number' => $data_array->input('phone_number'),
			'fax_number'   => $data_array->input('fax_number'),
			'email'        => $data_array->input('email'),
			'url'          => $data_array->input('url'),
		];
		$this->CIcontract->addOrUpdateContactInfo($facility, new ContactInformation($contact));
		$this->NPIcontract->addOrUpdateNPI($facility, new NationalProviderIdentification(['npi' => $data_array->input('facility_npi')]));

		$timeTable = $this->TTcontract->timeTableMakerForDB($data_array->input('timeTable'));
		$this->TTcontract->addOrUpdateTimeTableInfo($facility, $timeTable);

		$check_facility = $this->HCPFcontract->getHCPFacilityByFacilityAndProviderId($facility->id, $hcp->id);

		if (!isset($check_facility)) {
			$hcp_facility = [
				'health_care_facility_id' => $facility->id,
				'health_care_provider_id' => $hcp->id,
			];
			$this->HCPFcontract->addHCPFacility(new HealthCareProviderFacility($hcp_facility));
		}
		return 1;
	}

	public function getHCPFacilityTimeTableDetail($facility_id) {
		if (isset($facility_id)) {
			$facility = $this->HCFcontract->getHealthCareFacilityById($facility_id);
			$timeTable = $this->TTcontract->getCompleteTimeTableOfRelation($facility);
			if (!empty($timeTable)) {
				return $timeTable;
			} else {
				return $this->facilityDefaultTimeTableMaker($this->TTcontract->getCompleteTimeTableFormate());
			}
		}
		return $this->facilityDefaultTimeTableMaker($this->TTcontract->getCompleteTimeTableFormate());
	}

	protected function facilityDefaultTimeTableMaker($time_table_date) {
		for ($i = 0; $i < count($time_table_date); $i++) {
			if ($time_table_date[$i]['day'] != 'sunday') {
				$time_table_date[$i]['time_from'] = "9:00 AM";
				$time_table_date[$i]['time_to'] = "5:00 PM";
			}
		}
		return $time_table_date;
	}

	public function deleteHCPFacility($facility_id, $hcp_id) {
		$hcp_facility = $this->HCPFcontract->getHCPFacilityByFacilityAndProviderId($facility_id, $hcp_id);
		return $this->HCPFcontract->deleteHCPFacility($hcp_facility);
	}

	public function searchExistingFacility($data_array) {
		$query = $data_array->input('query');
		if (!isset($query)) {
			return 0;
		}
		return $this->HCPFcontract->getAllHCPFacilitiesBySearchQuery($query);
	}

	public function getAllHCPFacilities($hcp_id) {
		return $this->HCPFcontract->getAllHCPFacilities($hcp_id);
	}
	public function addOrUpdateHealthCareProviderFacility(HealthCareProvider $hcp, $data_array) {
		$user = HCPService::getUserByHealthCareProvider($hcp);
		/* check here to see if duplicate facility exists or not! */
		$facilityDataArray = array_merge(['facility_name' => $data_array['facility_name']], ['creator_id' => $user->id, 'honor_id' => $user->id]);
		$facility = $this->HCFcontract->addOrUpdateHealthCareFacility(new HealthCareFacility($facilityDataArray));

		$this->Acontract->addOrUpdateAddressInfo($facility, new Address($data_array));
		return $this->HCPFcontract->addOrUpdateHCPFacility(['health_care_facility_id' => $facility->id, 'health_care_provider_id' => $hcp->id]);
	}
	/* this method is called to check if duplicate facility exist or not */
	public function isFacilityExist($data_array) {
		$address = new Address($data_array);
		$facility = new HealthCareFacility($aarr);

	}
}
