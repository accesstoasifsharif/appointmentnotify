<?php

namespace Service\HealthCareProvider;

use Repository\Contracts\HealthCareProvider\CalendarAppointment;

class CalendarService {

	protected $CAcontract = '';

	public function __construct(CalendarAppointment $UCAcon) {
		$this->CAcontract = $UCAcon;
	}

	public function getAllAppointmentsForCalendar($data_array, $hcp_id) {
		$hcp_facility_id = $data_array->hcp_facility_id;
		$doctor_id = $data_array->doctor_id;
		if ($doctor_id == 0) {
			if ($hcp_facility_id == 0) {

				$upcoming_month = $this->CAcontract->getAllDoctorAllHCPFacilityUpcomingAppointmentsMonthView($hcp_id);

				$past_month = $this->CAcontract->getAllDoctorAllHCPFacilityPastAppointmentsMonthView($hcp_id);
				$reschedule_month = $this->CAcontract->getAllDoctorAllHCPFacilityRescheduleAppointmentsMonthView($hcp_id);
				$upcoming_day = $this->CAcontract->getAllDoctorAllHCPFacilityUpcomingAppointmentsDayView($hcp_id);
				$past_day = $this->CAcontract->getAllDoctorAllHCPFacilityPastAppointmentsDayView($hcp_id);
				$reschedule_day = $this->CAcontract->getAllDoctorAllHCPFacilityRescheduleAppointmentsDayView($hcp_id);
			} else {
				$upcoming_month = $this->CAcontract->getAllDoctorSpecificHCPFacilityUpcomingAppointmentsMonthView($hcp_facility_id, $hcp_id);
				$past_month = $this->CAcontract->getAllDoctorSpecificHCPFacilityPastAppointmentsMonthView($hcp_facility_id, $hcp_id);
				$reschedule_month = $this->CAcontract->getAllDoctorSpecificHCPFacilityRescheduleAppointmentsMonthView($hcp_facility_id, $hcp_id);

				$upcoming_day = $this->CAcontract->getAllDoctorSpecificHCPFacilityUpcomingAppointmentsDayView($hcp_facility_id, $hcp_id);
				$past_day = $this->CAcontract->getAllDoctorSpecificHCPFacilityPastAppointmentsDayView($hcp_facility_id, $hcp_id);
				$reschedule_day = $this->CAcontract->getAllDoctorSpecificHCPFacilityRescheduleAppointmentsDayView($hcp_facility_id, $hcp_id);

			}
		} else {
			if ($hcp_facility_id == 0) {
				$upcoming_month = $this->CAcontract->getSpecificDoctorAllHCPFacilityUpcomingAppointmentsMonthView($doctor_id, $hcp_id);
				$past_month = $this->CAcontract->getSpecificDoctorAllHCPFacilityPastAppointmentsMonthView($doctor_id, $hcp_id);
				$reschedule_month = $this->CAcontract->getSpecificDoctorAllHCPFacilityRescheduleAppointmentsMonthView($doctor_id, $hcp_id);

				$upcoming_day = $this->CAcontract->getSpecificDoctorAllHCPFacilityUpcomingAppointmentsDayView($doctor_id, $hcp_id);
				$past_day = $this->CAcontract->getSpecificDoctorAllHCPFacilityPastAppointmentsDayView($doctor_id, $hcp_id);
				$reschedule_day = $this->CAcontract->getSpecificDoctorAllHCPFacilityRescheduleAppointmentsDayView($doctor_id, $hcp_id);

			} else {
				$upcoming_month = $this->CAcontract->getSpecificDoctorSpecificHCPFacilityUpcomingAppointmentsMonthView($doctor_id, $hcp_facility_id, $hcp_id);
				$past_month = $this->CAcontract->getSpecificDoctorSpecificHCPFacilityPastAppointmentsMonthView($doctor_id, $hcp_facility_id, $hcp_id);
				$reschedule_month = $this->CAcontract->getSpecificDoctorSpecificHCPFacilityRescheduleAppointmentsMonthView($doctor_id, $hcp_facility_id, $hcp_id);

				$upcoming_day = $this->CAcontract->getSpecificDoctorSpecificHCPFacilityUpcomingAppointmentsDayView($doctor_id, $hcp_facility_id, $hcp_id);
				$past_day = $this->CAcontract->getSpecificDoctorSpecificHCPFacilityPastAppointmentsDayView($doctor_id, $hcp_facility_id, $hcp_id);
				$reschedule_day = $this->CAcontract->getSpecificDoctorSpecificHCPFacilityRescheduleAppointmentsDayView($doctor_id, $hcp_facility_id, $hcp_id);

			}
		}

		
		$month = $this->calendarAppointmentsMaker($upcoming_month, $past_month, $reschedule_month);
		$day = $this->calendarAppointmentsMaker($upcoming_day, $past_day, $reschedule_day);
		$data = ['month_view' => $month, 'day_view' => $day];
		return $data;
	}

	protected function calendarAppointmentsMaker($upcoming_array, $past_array, $reschedule_array) {
		$data_array = array_merge($upcoming_array, $past_array, $reschedule_array);
		return $data_array;
	}

	public function getDetailPreviewOfMonthViewAppointments($data_array, $hcp_id) {
		$an_event_type = $data_array->input('an_event_type');

		switch ($an_event_type) {
			case config('appointmentnotify.an_event_type.upcoming.event_type'):
			{
				$upcoming_data = $this->CAcontract->getDoctorHCPFacilityDateUpcomingAppointmentsDetailPreview($data_array->input('doctor_id'), $data_array->input('date'));
				return $upcoming_data;
				break;
			}

			case config('appointmentnotify.an_event_type.past.event_type'):
			{
				$past_data = $this->CAcontract->getDoctorHCPFacilityDatePastAppointmentsDetailPreview($data_array->input('doctor_id'), $data_array->input('date'));
				return $past_data;
				break;
			}

			case config('appointmentnotify.an_event_type.reschedule.event_type'):
			{
				$reschedule_data = $this->CAcontract->getDoctorHCPFacilityDateRescheduleAppointmentsDetailPreview($data_array->input('doctor_id'), $data_array->input('date'));
				return $reschedule_data;
				break;
			}

			default:
			return "empty";
			break;
		}
		return $data_array->all();

	}

	public function getDetailPreviewOfDayViewAppointment($data_array) {
		$an_event_type = $data_array->input('an_event_type');

		switch ($an_event_type) {
			case config('appointmentnotify.an_event_type.upcoming.event_type'):
			{
				$upcoming_data = $this->CAcontract->getDoctorHCPFacilityUpcomingAppointmentDetailPreview($data_array->input('an_event_id'));
				return $upcoming_data;
				break;
			}

			case config('appointmentnotify.an_event_type.past.event_type'):
			{
				$past_data = $this->CAcontract->getDoctorHCPFacilityPastAppointmentDetailPreview($data_array->input('an_event_id'));
				return $past_data;
				break;
			}

			case config('appointmentnotify.an_event_type.reschedule.event_type'):
			{
				$reschedule_data = $this->CAcontract->getDoctorHCPFacilityRescheduleAppointmentDetailPreview($data_array->input('an_event_id'));
				return $reschedule_data;
				break;
			}

			default:
			return "empty";
			break;
		}
		return $data_array->all();
	}

}
