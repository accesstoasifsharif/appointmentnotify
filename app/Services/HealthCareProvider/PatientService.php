<?php

namespace Service\HealthCareProvider;
use Repository\Contracts\HealthCareProvider\Patient;

class PatientService {
	protected $allPatientscontract = '';

	public function __construct(Patient $p) {
		$this->allPatientscontract = $p;
	}
	public function getAllPatientsSpecificPerspective($hcp_id, $request) {
		return $this->allPatientscontract->getAllPatientsSpecificPerspective($hcp_id, $request);

	}
	public function allPatientsByDoctorId($doctorId, $hcp_id, $request) {
		return $this->allPatientscontract->allPatientsByDoctorId($doctorId, $hcp_id, $request);
	}
}