<?php

namespace Service\SiteUser;
use Repository\Contracts\SiteUser\AppointmentTimeLine;
use Repository\Contracts\SiteUser\SiteUser;

class AppointmentTimeLineService {

	protected $ATLContract = '';
	public function __construct(AppointmentTimeLine $ATLcon) {
		$this->ATLContract = $ATLcon;
	}

	public function getYearListOfAppointments($site_user) {
		return $this->ATLContract->getYearListOfAppointments($site_user);
	}

	public function getYearMonthListOfAppointments($year, $site_user) {
		return $this->ATLContract->getYearMonthListOfAppointments($year, $site_user);
	}

	public function getAppointmentListOfMonthOfYear($year, $month, $site_user) {
		return $this->ATLContract->getAppointmentListOfMonthOfYear($year, $month, $site_user);
	}

}
