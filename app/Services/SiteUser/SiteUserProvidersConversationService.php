<?php

namespace Service\SiteUser;

use Facades\Service\Common\MessageService;
use Facades\Service\Common\UserData;
use Repository\Contracts\SiteUser\SiteUserCircledProvider;

class SiteUserProvidersConversationService {

	protected $SUCPcontract = '';

	public function __construct(SiteUserCircledProvider $SUCPcon) {
		$this->SUCPcontract = $SUCPcon;
	}

	public function getSiteUserProvidersConverstion($site_user_id) {
		$providers = $this->SUCPcontract->getAllVerifiedSiteUserCircledProvider($site_user_id);
		for ($i = 0; $i < count($providers); $i++) {
			$user_1 = UserData::getUserByModelIdAndModelIdentifier($providers[$i]->site_user_id, $providers[$i]->site_user_identifier);
			$user_2 = UserData::getUserByModelIdAndModelIdentifier($providers[$i]->doctor_id, $providers[$i]->doctor_identifier);
			$conversation = MessageService::getSpecificUserConverstionPreview($user_1->id, $user_2->id);
			if ($conversation != '0' && count($conversation->messages) > 0) {
				$providers[$i]->message_preview = $conversation->messages[0];
			} else {
				$providers[$i]->message_preview = '';
			}
		}
		return $providers;
	}

}
