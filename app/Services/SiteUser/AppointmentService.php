<?php

namespace Service\SiteUser;

use App\Events\SiteUser\AppointmentRescheduleRequestBySiteUser;
use App\Events\SiteUser\AppointmentReservationRequestBySiteUser;
use Repository\Contracts\SiteUser\AppointmentsReasons;
use Repository\Contracts\SiteUser\CancelAppointment;
use Repository\Contracts\SiteUser\PastAppointment;
use Repository\Contracts\SiteUser\RescheduleAppointment;
use Repository\Contracts\SiteUser\ReservedAppointments;
use Repository\Contracts\SiteUser\SaveAppointment;
use Repository\Contracts\SiteUser\SaveNotes;
use Repository\Contracts\SiteUser\ShortCallAppointment;
use Repository\Contracts\SiteUser\SiteUser;
use Repository\Contracts\SiteUser\UpcomingAppointment;

class AppointmentService {

	protected $pastAppointmentContract = '';
	protected $siteUserContract = '';
	protected $saveNoteContract = '';
	protected $appointmentsReasonsContract = '';
	protected $cancelAppointmentContract = '';
	protected $rescheduleAppointmentContract = '';
	protected $upcomingAppointmentContract = '';
	protected $saveAppointmentContract = '';
	protected $ReservedAppointmentsContract = "";
	protected $ShortCallAppointmentContract = "";

	public function __construct(PastAppointment $past_appointment, SiteUser $sc, SaveNotes $save_note, AppointmentsReasons $appointment_reason, CancelAppointment $cancel_appointment, RescheduleAppointment $reschedule_appointment, UpcomingAppointment $upcoming_appointment, SaveAppointment $save_appointment, ReservedAppointments $reserved_appointment, ShortCallAppointment $shortcall_appointment) {

		$this->pastAppointmentContract = $past_appointment;
		$this->saveAppointmentContract = $save_appointment;
		$this->siteUserContract = $sc;
		$this->appointmentsReasonsContract = $appointment_reason;
		$this->saveNoteContract = $save_note;
		$this->cancelAppointmentContract = $cancel_appointment;
		$this->rescheduleAppointmentContract = $reschedule_appointment;
		$this->upcomingAppointmentContract = $upcoming_appointment;
		$this->reservedAppointmentsContract = $reserved_appointment;
		$this->shortCallAppointmentContract = $shortcall_appointment;
	}

	public function getDetailOfPastAppointmentByDoctorId($doctorId, $user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->pastAppointmentContract->getPastAppointmentDetailByDoctorId($doctorId, $siteUser->id);
	}
	public function getAllPastAppointemntsBySiteUserId($user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->pastAppointmentContract->getAllPastAppointemntsBySiteUserId($siteUser->id);
	}
	public function getAppointmentReasons() {

		return $this->appointmentsReasonsContract->getAppointmentReasons();
	}
	public function getNoteByAppointmentId($appointment_id) {
		return $this->saveNoteContract->getNoteByAppointmentId($appointment_id);
	}

	public function saveNote($data_array) {
		return $this->saveNoteContract->saveNote($data_array);
	}
	public function cancelUpComingAppointemnt($appointment_id) {
		return $this->cancelAppointmentContract->cancelUpComingAppointemnt($appointment_id);
	}
	public function saveRescheduleTime($data_array) {

		$an_event = $this->rescheduleAppointmentContract->saveRescheduleTime($data_array);
		return $an_event;
		event(new AppointmentRescheduleRequestBySiteUser($an_event));
		return 'true';
	}

	public function getAllUpComingAppointemntsBySiteUserId($user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->upcomingAppointmentContract->getAllUpComingAppointemntsBySiteUserId($siteUser->id);
	}
	/*Asif Sharif*/
	public function allDistinctUpcomingAppointmentsBySiteUserId($user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->upcomingAppointmentContract->allDistinctUpcomingAppointmentsBySiteUserId($siteUser->id);
	}
	public function allDistinctPastAppointmentsBySiteUserId($user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->pastAppointmentContract->allDistinctPastAppointmentsBySiteUserId($siteUser->id);
	}

	/*Asif Sharif*/
	public function getUpcomingAppointmentDetailByDoctorId($doctorId, $user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->upcomingAppointmentContract->getUpcomingAppointmentDetailByDoctorId($doctorId, $siteUser->id);
	}
	public function saveAppointment($data_array, $user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		$an_event = $this->saveAppointmentContract->saveAppointment($data_array, $siteUser->id);
		event(new AppointmentReservationRequestBySiteUser($an_event));
		return "Entry saved in appointment table , AppointmentReservation and an_event table!";

	}
	public function getAggregatedRescheduledAppointmentBySiteUserId($user) {

		$siteUser = $this->siteUserContract->getSiteUser($user);

		return $this->rescheduleAppointmentContract->getAggregatedRescheduledAppointmentBySiteUserId($siteUser->id);

	}
	public function getAllCancelledAppointmentBySiteUserId($user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->cancelAppointmentContract->getAllCancelledAppointmentBySiteUserId($siteUser->id);
	}
	public function getRescheduledAppointmentDetailByDoctorId($doctorId, $user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->rescheduleAppointmentContract->getRescheduledAppointmentDetailByDoctorId($doctorId, $siteUser->id);
	}
	public function getAllCancelledAppointmentsDetailByDoctorId($doctorId, $user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->cancelAppointmentContract->getAllCancelledAppointmentsDetailByDoctorId($doctorId, $siteUser->id);
	}
	public function revokeCancelledAppointment($appointment_id) {
		return $this->cancelAppointmentContract->revokeCancelledAppointment($appointment_id);
	}
	public function getAggregatedReservedAppointmentsBySiteUserId($user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->reservedAppointmentsContract->getAggregatedReservedAppointmentsBySiteUserId($siteUser->id);
	}
	public function allDistinctReservedAppointmentsBySiteUserId($user) {

		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->reservedAppointmentsContract->allDistinctReservedAppointmentsBySiteUserId($siteUser->id);
	}
	public function getAllReservedAppointmentsDetailByDoctorId($doctorId, $user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->reservedAppointmentsContract->getAllReservedAppointmentsDetailByDoctorId($doctorId, $siteUser->id);
	}
	public function getAggregatedShortCallAppointmentsBySiteUserId($user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->shortCallAppointmentContract->getAggregatedShortCallAppointmentsBySiteUserId($siteUser->id);
	}
	public function getShortCallAppointmentDetailByDoctorId($doctorId, $user) {
		$siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->shortCallAppointmentContract->getShortCallAppointmentDetailByDoctorId($doctorId, $siteUser->id);
	}

}
