<?php

namespace Service\SiteUser;
use Repository\Contracts\SiteUser\SiteUser as SiteUserContract;

class SiteUser {

	protected $siteUserContract = '';

	public function __construct(SiteUserContract $su) {

		$this->siteUserContract = $su;

	}

	public function getSiteUserByUser($user) {

		return $this->siteUserContract->getSiteUser($user);
	}

}
