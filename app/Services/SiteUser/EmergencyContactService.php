<?php
namespace Service\SiteUser;
use Repository\Contracts\SiteUser\EmergencyContact;
use Repository\Contracts\SiteUser\SiteUser;

class EmergencyContactService {
	protected $addUdateEmergencyContactContract = '';
	protected $siteUserContract = '';
	// protected $siteUserContract = '';
	public function __construct(EmergencyContact $ec, SiteUser $sc) {

		$this->addUdateEmergencyContactContract = $ec;
		$this->siteUserContract = $sc;
	}
	public function addOrUpdateEmergencyContact($data_array, $user) {
		// $siteUser = $this->siteUserContract->getSiteUser($user);
		return $this->addUdateEmergencyContactContract->addOrUpdateEmergencyContact($data_array, $user->id);
	}
}