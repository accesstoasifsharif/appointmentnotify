<?php

namespace App\Events\HealthCareProvider;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ShortCallAppointmentPublishByProvider {
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $short_call_patient_list;
	public $an_event;
	public $short_call_appointment;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($short_call_patient_list, $an_event, $short_call_appointment) {
		$this->short_call_patient_list = $short_call_patient_list;
		$this->an_event = $an_event;
		$this->short_call_appointment = $short_call_appointment;
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return Channel|array
	 */
	public function broadcastOn() {
		return new PrivateChannel('channel-name');
	}
}
