<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessageConversation {
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $sender = '';
	public $reciever = '';
	public $message = '';
	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($sender, $reciever, $message) {
		$this->sender = $sender;
		$this->reciever = $reciever;
		$this->message = $message;
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return Channel|array
	 */
	public function broadcastOn() {
		return new PrivateChannel('channel-name');
	}
}
