<?php

namespace App;

use App\Role;
use Illuminate\Database\Eloquent\Model;

class BusinessType extends Model
{
	protected $fillable = [
	'business_name', 'config_id'
	];


	public function roles()
	{
		return $this->hasMany(Role::class);
	}


}
