<?php

use Facades\Service\Common\UserContext;

if (!function_exists('healthCareProviderWizardUrl')) {
	function healthCareProviderWizardUrl() {
		return UserContext::getWizardRedirectUrlByUserTypeId(

			config(Config_File_Name . '.' . HealthCareProvider . '.user_type_id')
		);
	}
}

if (!function_exists('healthCareProviderRedirectUrl')) {
	/**
	 * Add method purpose here..
	 *
	 * @param  array   $array
	 * @return array
	 */
	function healthCareProviderRedirectUrl() {

		return config(Config_File_Name . '.' . HealthCareProvider . '.redirectUrl');

	}
}
if (!function_exists('healthCareProviderNamespace')) {
	/**
	 * Add method purpose here..
	 *
	 * @param  array   $array
	 * @return array
	 */
	function healthCareProviderNamespace() {
		return config(Config_File_Name . '.' . HealthCareProvider . '.namespace');

	}
}
if (!function_exists('healthCareProviderPrefix')) {
	/**
	 * Add method purpose here..
	 *
	 * @param  array   $array
	 * @return array
	 */
	function healthCareProviderPrefix() {

		return config(Config_File_Name . '.' . HealthCareProvider . '.prefix');

	}

}
if (!function_exists('hospitalTypeId')) {
	function hospitalTypeId() {
		return config(Config_File_Name . '.' . HealthCareProvider . '.hcp_type.' . Hospital . '.id');
	}
}

if (!function_exists('medicalClinicTypeId')) {
	function medicalClinicTypeId() {
		return config(Config_File_Name . '.' . HealthCareProvider . '.hcp_type.' . MedicalClinic . '.id');
	}
}
if (!function_exists('departmentTypeId')) {
	function departmentTypeId() {
		return config(Config_File_Name . '.' . HealthCareProvider . '.hcp_type.' . Department . '.id');
	}
}
if (!function_exists('singlePracticeTypeId')) {
	function singlePracticeTypeId() {
		return config(Config_File_Name . '.' . HealthCareProvider . '.hcp_type.' . SinglePractice . '.id');
	}
}
if (!function_exists('staffTypeId')) {
	function staffTypeId() {
		return config(Config_File_Name . '.' . HealthCareProvider . '.hcp_type.' . Staff . '.id');
	}
}
if (!function_exists('getProviderTypeByHCPTypeId')) {
	function getProviderTypeByHCPTypeId($id) {
		$hcp_Types_array = config(Config_File_Name . '.' . HealthCareProvider . '.hcp_type');
		foreach ($hcp_Types_array as $key => $value) {
			if ($value['id'] == $id) {

				return $value['name'];
			}
		}
		return '-1';

	}
}