<?php

use Facades\Service\Common\UserContext;

if (!function_exists('siteUserWizardUrl')) {
	function siteUserWizardUrl() {
		return UserContext::getWizardRedirectUrlByUserTypeId(

			config(Config_File_Name . '.' . SiteUser . '.user_type_id')
		);
	}
}

if (!function_exists('siteUserRedirectUrl')) {
	/**
	 * Add method purpose here..
	 *
	 * @param  array   $array
	 * @return array
	 */
	function siteUserRedirectUrl() {

		return config(Config_File_Name . '.' . SiteUser . '.redirectUrl');
	}
}
if (!function_exists('siteUserNamespace')) {
	/**
	 * Add method purpose here..
	 *
	 * @param  array   $array
	 * @return array
	 */
	function siteUserNamespace() {
		return config(Config_File_Name . '.' . SiteUser . '.namespace');
	}
}
if (!function_exists('siteUserPrefix')) {
	/**
	 * Add method purpose here..
	 *
	 * @param  array   $array
	 * @return array
	 */
	function siteUserPrefix() {

		return config(Config_File_Name . '.' . SiteUser . '.prefix');

	}

}
