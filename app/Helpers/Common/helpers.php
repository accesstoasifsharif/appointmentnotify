<?php

use Carbon\Carbon;
use Facades\Service\Common\TokenGenerator;
use Illuminate\Support\Facades\Config;

if (!function_exists('fooBar')) {
	function fooBar() {
		return 'hello foobar';
	}
}
if (!function_exists('shortCallListTime')) {

	function shortCallListTime() {
		return Config::get(Config_File_Name . '.APP_SHORT_CALL_LIST_TIME_SPAN');
	}
}

if (!function_exists('generateToken')) {

	function generateToken() {
		return TokenGenerator::generateToken();
	}
}
if (!function_exists('getHashedToken')) {

	function getHashedToken($token) {
		return TokenGenerator::getHashedToken($token);
	}
}
if (!function_exists('verifyTokenHash')) {

	function verifyTokenHash($token, $hashedToken) {
		return TokenGenerator::verifyTokenHash($token, $hashedToken);
	}
}
if (!function_exists('isTokenExpired')) {

	function isTokenExpired($createdAt) {
		$expires = 60;
		return Carbon::parse($createdAt)->addSeconds($expires)->isPast();
	}
}
if (!function_exists('inviteUrl')) {

	function inviteUrl($token, $routeName) {

		return url(config('app.url') . route($routeName, $token, false));
	}
}

if (!function_exists('unique_multidim_array')) {

	function uniqueMultidimArray($array, $key) {
		$temp_array = array();
		$i = 0;
		$key_array = array();

		foreach ($array as $val) {
			if (!in_array($val[$key], $key_array)) {
				$key_array[$i] = $val[$key];
				$temp_array[$i] = $val;
			}
			$i++;
		}
		return $temp_array;
	}
}