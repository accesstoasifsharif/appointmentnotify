<?php

if (!function_exists('adminRedirectUrl')) {
	/**
	 * Add method purpose here..
	 *
	 * @param  array   $array
	 * @return array
	 */
	function adminRedirectUrl() {
		return strtolower(Admin);
	}
}
if (!function_exists('adminNamespace')) {
	/**
	 * Add method purpose here..
	 *
	 * @param  array   $array
	 * @return array
	 */
	function adminNamespace() {
		return Admin;
	}
}
if (!function_exists('adminPrefix')) {
	/**
	 * Add method purpose here..
	 *
	 * @param  array   $array
	 * @return array
	 */
	function adminPrefix() {
		return strtolower(Admin);
	}

}
