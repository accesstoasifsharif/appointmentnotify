<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ANRefresh extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'an:refresh';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This commnad rebuild all the essentials components of Appointment Notify';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {

		$this->info(APP_NAME . " refreshing database migration and seeds !");
		$this->info("Please wait.........");

		$exitCode = Artisan::call('migrate:refresh');
		$this->info("Refreshing dabase migration done with exit code: " . $exitCode);

		$this->info("Running composer dump autload ");
		echo exec('composer dump-autoload -o');

		$exitCode = Artisan::call('db:seed');
		$this->info("Seeding dabase  done with exit code: " . $exitCode);

		$exitCode = Artisan::call('passport:install');
		$this->info("Installing Laravel Passport done with exit code: " . $exitCode);

		$this->info(APP_NAME . " Refreshing done......... Have a nice day ahead :) ");
	}
}
