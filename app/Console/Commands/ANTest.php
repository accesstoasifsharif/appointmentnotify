<?php

namespace App\Console\Commands;

use App\Events\SiteUser\OrderShipped;
use Illuminate\Console\Command;

class ANTest extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'test';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'testing for Appoitnment Notify';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->info("Running the test commnad ..");
		$order = ['id' => 'adnan_moghal'];
		event(new OrderShipped($order));

		//	Log::debug('Showing user profile for user: ');
		//  ['id' => 'user->id']
		$this->info("test commnad ends ..");
	}
}
