<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;

class ShortCallAppointmentPublishByProviderNotification extends Notification implements ShouldBroadcast {
	use Queueable;

	public $sender_user;
	public $reciever_user;
	public $event;
/**
 * Create a new notification instance.
 *
 * @return void
 */
	public function __construct($Sender_User, $Reciever_User, $Event) {
		$this->sender_user = $Sender_User;
		$this->reciever_user = $Reciever_User;
		$this->event = $Event;
	}

/**
 * Get the notification's delivery channels.
 *
 * @param  mixed  $notifiable
 * @return array
 */
	public function via($notifiable) {
		return ['database', 'broadcast'];
	}

	public function toDatabase($notifiable) {
		return $this->toDefault($notifiable);
	}

	public function toBroadcast($notifiable) {
		return $this->toDefault($notifiable);
	}

/**
 * Get the array representation of the notification.
 *
 * @param  mixed  $notifiable
 * @return array
 */
	public function toArray($notifiable) {
		return $this->toDefault($notifiable);
	}

	protected function toDefault($notifiable) {
		$notification_text = "there is an appointment slot available for you ... Hurry Up! Booked it..";
		return [
			'sender' => $this->sender_user->id,
			'reciever' => $this->reciever_user->id,
			'data' => [
				'notification_message' => $notification_text,
				'url' => '',
				'detail' => '',
			],

		];
	}

/**
 * Get the channels the event should broadcast on.
 *
 * @return array
 */
	public function broadcastOn() {
		return ['provider-to-patient'];
	}

}
