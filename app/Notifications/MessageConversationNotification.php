<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class MessageConversationNotification extends Notification implements ShouldBroadcast {
	use Queueable;

	public $sender_user;
	public $reciever_user;
	public $data;
/**
 * Create a new notification instance.
 *
 * @return void
 */
	public function __construct($Sender_User, $Reciever_User, $Data) {
		$this->sender_user = $Sender_User;
		$this->reciever_user = $Reciever_User;
		$this->data = $Data;
	}

/**
 * Get the notification's delivery channels.
 *
 * @param  mixed  $notifiable
 * @return array
 */
	public function via($notifiable) {
		return ['database', 'broadcast'];
	}

	public function toDatabase($notifiable) {
		return $this->toDefault($notifiable);
	}

	public function toBroadcast($notifiable) {
		return $this->toDefault($notifiable);
	}

/**
 * Get the array representation of the notification.
 *
 * @param  mixed  $notifiable
 * @return array
 */
	public function toArray($notifiable) {
		return $this->toDefault($notifiable);
	}

	protected function toDefault($notifiable) {
		$notification_text = $this->sender_user->name . " send you a message.";
		Log::debug("Message Conversation broadcasted on message-conversation ");
		return [
			'sender' => $this->sender_user->id,
			'reciever' => $this->reciever_user->id,
			'data' => [
				'notification_message' => $notification_text,
				'url' => '',
				'detail' => '',
			],
		];
	}

/**
 * Get the channels the event should broadcast on.
 *
 * @return array
 */
	public function broadcastOn() {

		return ['message-conversation'];
	}

}
