<?php

namespace App;

use App\RoleLevel;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Model\HealthCareProvider;

class PerspectiveLevel extends Model
{
	protected $fillable = [
	'user_id',
	'health_care_provider_id',
	'role_level_id'
	];


	public function healthCareProvider()
	{
		return $this->belongsTo(HealthCareProvider::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function roleLevel()
	{
		return $this->belongsTo(RoleLevel::class);
	}
}
