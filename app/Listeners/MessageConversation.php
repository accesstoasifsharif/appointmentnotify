<?php

namespace App\Listeners;

use App\Events\MessageConversation as MessageConversationEvent;
use App\Notifications\MessageConversationNotification;
use Illuminate\Support\Facades\Log;

class MessageConversation {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  MessageConversationEvent  $event
	 * @return void
	 */
	public function handle(MessageConversationEvent $event) {
		$reciever_user = $event->reciever;
		$sender_user = $event->sender;
		if (isset($reciever_user) && isset($sender_user)) {
			Log::debug("Message Conversation  Event Listener");
			$reciever_user->notify(new MessageConversationNotification($sender_user, $reciever_user, $event->message));
		} else {
			Log::debug("Message Conversation  error not found");
		}

	}
}
