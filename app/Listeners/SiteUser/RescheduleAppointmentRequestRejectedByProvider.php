<?php

namespace App\Listeners\SiteUser;

use App\Events\HealthCareProvider\RescheduleAppointmentRequestRejectedByProvider as RescheduleAppointmentRequestRejectedByProviderEvent;
use App\Notifications\RescheduleAppointmentRequestRejectedByProviderNotification;
use Facades\Service\Common\UserData;

class RescheduleAppointmentRequestRejectedByProvider {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  RescheduleAppointmentRequestRejectedByProviderEvent  $event
	 * @return void
	 */
	public function handle(RescheduleAppointmentRequestRejectedByProviderEvent $event) {
		$sender_user = UserData::getUserByDoctorId($event->data->doctor_id);
		$reciever_user = UserData::getUserBySiteUserId($event->data->site_user_id);

		if (isset($reciever_user) && isset($sender_user)) {
			$reciever_user->notify(new RescheduleAppointmentRequestRejectedByProviderNotification($sender_user, $reciever_user, $event->data));
		} else {
			Log::debug("RescheduleAppointmentRequestRejectedByProvider reciever_user not found");
		}
	}
}
