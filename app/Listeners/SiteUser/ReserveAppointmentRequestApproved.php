<?php

namespace App\Listeners\SiteUser;

use App\Events\HealthCareProvider\ReserveAppointmentRequestApproved as ReserveAppointmentRequestApprovedEvent;
use App\Notifications\ReserveAppointmentRequestApprovalNotification;
use Facades\Service\Common\UserData;
use Illuminate\Support\Facades\Log;

class ReserveAppointmentRequestApproved {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  ReserveAppointmentRequestApproved  $event
	 * @return void
	 */
	public function handle(ReserveAppointmentRequestApprovedEvent $event) {

		$sender_user = UserData::getUserByDoctorId($event->data->doctor_id);
		$reciever_user = UserData::getUserBySiteUserId($event->data->site_user_id);

		if (isset($reciever_user)) {
			$reciever_user->notify(new ReserveAppointmentRequestApprovalNotification($sender_user, $reciever_user, $event->data));
		} else {
			Log::debug("ReserveAppointmentRequestApproved reciever_user not found");
		}

	}
}
