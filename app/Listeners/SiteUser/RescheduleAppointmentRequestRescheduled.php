<?php

namespace App\Listeners\SiteUser;

use App\Events\HealthCareProvider\RescheduleAppointmentRequestRescheduled as RescheduleAppointmentRequestRescheduledEvent;
use App\Notifications\RescheduleAppointmentRequestRescheduleNotification;
use Facades\Service\Common\UserData;
use Illuminate\Support\Facades\Log;

class RescheduleAppointmentRequestRescheduled {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  RescheduleAppointmentRequestRescheduled  $event
	 * @return void
	 */
	public function handle(RescheduleAppointmentRequestRescheduledEvent $event) {
		$sender_user = UserData::getUserByDoctorId($event->data->doctor_id);
		$reciever_user = UserData::getUserBySiteUserId($event->data->site_user_id);

		if (isset($reciever_user)) {
			$reciever_user->notify(new RescheduleAppointmentRequestRescheduleNotification($sender_user, $reciever_user, $event->data));
		} else {
			Log::debug("RescheduleAppointmentRequestApproved reciever_user not found");
		}
	}
}
