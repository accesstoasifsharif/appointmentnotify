<?php

namespace App\Listeners\SiteUser;

use App\Events\HealthCareProvider\ShortCallAppointmentPublishByProvider as ShortCallAppointmentPublishByProviderEvent;
use App\Notifications\ShortCallAppointmentPublishByProviderNotification;
use Facades\Service\Common\UserData;

class ShortCallAppointmentPublishByProvider {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  ShortCallAppointmentPublishByProviderEvent  $event
	 * @return void
	 */
	public function handle(ShortCallAppointmentPublishByProviderEvent $event) {

		$short_call_listee = $event->short_call_patient_list;
		$sender_user = UserData::getUserByDoctorId($event->an_event->doctor_id);
		for ($i = 0; $i < count($short_call_listee); $i++) {
			$reciever_user = UserData::getUserBySiteUserId($short_call_listee[$i]->site_user_id);
			if (isset($reciever_user) && isset($sender_user)) {
				$reciever_user->notify(new ShortCallAppointmentPublishByProviderNotification($sender_user, $reciever_user, $event));
			} else {
				Log::debug("ShortCallAppointmentPublishByProvider reciever_user not found");
			}
		}

	}
}
