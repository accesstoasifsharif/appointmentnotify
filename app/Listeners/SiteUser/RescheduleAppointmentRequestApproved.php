<?php

namespace App\Listeners\SiteUser;

use App\Events\HealthCareProvider\RescheduleAppointmentRequestApproved as RescheduleAppointmentRequestApprovedEvent;
use App\Notifications\RescheduleAppointmentRequestApprovalNotification;
use Facades\Service\Common\UserData;
use Illuminate\Support\Facades\Log;

class RescheduleAppointmentRequestApproved {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  RescheduleAppointmentRequestApprovedEvent  $event
	 * @return void
	 */
	public function handle(RescheduleAppointmentRequestApprovedEvent $event) {
		$sender_user = UserData::getUserByDoctorId($event->data->doctor_id);
		$reciever_user = UserData::getUserBySiteUserId($event->data->site_user_id);

		if (isset($reciever_user) && isset($sender_user)) {
			$reciever_user->notify(new RescheduleAppointmentRequestApprovalNotification($sender_user, $reciever_user, $event->data));
		} else {
			Log::debug("ReserveAppointmentRequestApproved reciever_user not found");
		}
	}
}
