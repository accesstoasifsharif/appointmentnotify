<?php

namespace App\Listeners\SiteUser;

use App\Events\HealthCareProvider\AppointmentBookedAndApprovedByProvider as AppointmentBookedAndApprovedByProviderEvent;
use App\Notifications\AppointmentBookedAndApprovedByProviderNotification;
use Facades\Service\Common\UserData;
use Illuminate\Support\Facades\Log;

class AppointmentBookedAndApprovedByProvider {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  AppointmentBookedAndApprovedByProviderEvent  $event
	 * @return void
	 */
	public function handle(AppointmentBookedAndApprovedByProviderEvent $event) {
		$sender_user = UserData::getUserByDoctorId($event->data->doctor_id);
		$reciever_user = UserData::getUserBySiteUserId($event->data->site_user_id);

		if (isset($reciever_user) && isset($sender_user)) {
			$reciever_user->notify(new AppointmentBookedAndApprovedByProviderNotification($sender_user, $reciever_user, $event->data));
		} else {
			Log::debug("AppointmentBookedAndApprovedByProvider reciever_user not found");
		}
	}
}
