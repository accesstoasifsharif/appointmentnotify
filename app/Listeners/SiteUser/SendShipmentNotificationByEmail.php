<?php

namespace App\Listeners\SiteUser;

use App\Events\SiteUser\OrderShipped;
use Illuminate\Support\Facades\Log;

class SendShipmentNotificationByEmail {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderShipped  $event
	 * @return void
	 */
	public function handle(OrderShipped $event) {
		Log::debug('order shipped to user via SendShipmentNotificationByEmail: ' . $event->order['id']);
	}
}
