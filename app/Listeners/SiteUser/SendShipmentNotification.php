<?php

namespace App\Listeners\SiteUser;

use App\Events\SiteUser\OrderShipped;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendShipmentNotification implements ShouldQueue {
	/**
	 * The name of the connection the job should be sent to.
	 *
	 * @var string|null
	 */
	//public $connection = 'async';

	/**
	 * The name of the queue the job should be sent to.
	 *
	 * @var string|null
	 */
	//public $queue = 'default';

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderShipped  $event
	 * @return void
	 */
	public function handle(OrderShipped $event) {
		Log::debug('order shipped to user via SendShipmentNotification: ' . $event->order['id']);
	}
}
