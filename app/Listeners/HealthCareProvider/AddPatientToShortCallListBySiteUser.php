<?php

namespace App\Listeners\HealthCareProvider;

use App\Events\SiteUser\AddPatientToShortCallListBySiteUser as AddPatientToShortCallListBySiteUserEvent;
use App\Notifications\AddPatientToShortCallListBySiteUserNotification;
use Facades\Service\Common\UserData;
use Illuminate\Support\Facades\Log;

class AddPatientToShortCallListBySiteUser {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  AddPatientToShortCallListBySiteUserEvent  $event
	 * @return void
	 */
	public function handle(AddPatientToShortCallListBySiteUserEvent $event) {
		$reciever_user = UserData::getUserByDoctorId($event->data->doctor_id);
		$sender_user = UserData::getUserBySiteUserId($event->data->site_user_id);

		if (isset($reciever_user) && isset($sender_user)) {
			$reciever_user->notify(new AddPatientToShortCallListBySiteUserNotification($sender_user, $reciever_user, $event->data));
		} else {
			Log::debug("Add to ShortCall list  error not found");
		}
	}
}
