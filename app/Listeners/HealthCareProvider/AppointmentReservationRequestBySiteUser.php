<?php

namespace App\Listeners\HealthCareProvider;

use App\Events\SiteUser\AppointmentReservationRequestBySiteUser as AppointmentReservationRequestBySiteUserEvent;
use App\Notifications\AppointmentReservationRequestBySiteUserNotification;
use Facades\Service\Common\UserData;

class AppointmentReservationRequestBySiteUser {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  AppointmentReservationRequestBySiteUserEvent  $event
	 * @return void
	 */
	public function handle(AppointmentReservationRequestBySiteUserEvent $event) {
		$reciever_user = UserData::getUserByDoctorId($event->data->doctor_id);
		$sender_user = UserData::getUserBySiteUserId($event->data->site_user_id);

		if (isset($reciever_user) && isset($sender_user)) {
			$reciever_user->notify(new AppointmentReservationRequestBySiteUserNotification($sender_user, $reciever_user, $event->data));
		} else {
			Log::debug("AppointmentReservationRequestBySiteUserNotification  error not found");
		}
	}
}
