<?php

namespace App\Listeners\HealthCareProvider;

use App\Events\SiteUser\AppointmentRescheduleRequestBySiteUser as AppointmentRescheduleRequestBySiteUserEvent;
use App\Notifications\AppointmentRescheduleRequestBySiteUserNotification;
use Facades\Service\Common\UserData;

class AppointmentRescheduleRequestBySiteUser {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  AppointmentRescheduleRequestBySiteUserEvent  $event
	 * @return void
	 */
	public function handle(AppointmentRescheduleRequestBySiteUserEvent $event) {
		$reciever_user = UserData::getUserByDoctorId($event->data->doctor_id);
		$sender_user = UserData::getUserBySiteUserId($event->data->site_user_id);

		if (isset($reciever_user) && isset($sender_user)) {
			$reciever_user->notify(new AppointmentRescheduleRequestBySiteUserNotification($sender_user, $reciever_user, $event->data));
		} else {
			Log::debug("AppointmentRescheduleRequestBySiteUserNotification  error not found");
		}
	}
}
