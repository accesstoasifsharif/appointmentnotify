<?php

namespace App\Listeners;

use App\Events\CityZipCodeSeedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Model\City;
use Model\State;

class PostDeploymentEventListener implements ShouldQueue {
	// use InteractsWithQueue; //, Queueable , SerializesModels;

	public function onCityZipCodeSeed(CityZipCodeSeedEvent $event) {
		$file_n = storage_path(ASSET_FILES . ZIP_CODE_FILE);
		$file = fopen($file_n, "r");
		while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {
			if (!in_array(null, $data)) {
				$city = new City();
				$city->zip = $data[0];
				$city->city = $data[1];
				$city->state = $data[2];
				$city->latitude = $data[3];
				$city->longitude = $data[4];
				$city->timezone = $data[5];
				$city->dst = $data[6];
				$city->save();
			}
		}
		fclose($file);
	}

	/*public function onStateSeed(StateSeedEvent $event)
		    {
		        $file_n = storage_path(ASSET_FILES . US_State_FILE);
		        $file = fopen($file_n, "r");
		        while ( ($data = fgetcsv($file, 1000, ",")) !==FALSE ){
		            if (!in_array(null,$data)) {
		                $state = new State();
		                $state->state_name = $data[0];
		                $state->postal_code = $data[1];
		                $state->country_id = 1;
		                $state->save();
		            }
		        }
		        fclose($file);
	*/

	public function subscribe($events) {
		$events->listen(
			'App\Events\CityZipCodeSeedEvent',
			'App\Listeners\PostDeploymentEventListener@onCityZipCodeSeed'
		);

		/*	$events->listen(
			'App\Events\StateSeedEvent',
			'App\Listeners\PostDeploymentEventListener@onStateSeed'
          );*/
	}

}
