<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Support\Facades\Log;

class EventListener {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  Event  $event
	 * @return void
	 */
	public function handle($event) {
		Log::debug('order shipped to user via EventLister@handle: ' . $event->order['id']);
	}
	public function sendEmail($event) {
		Log::debug('order shipped to user via EventLister@sendEmail: ' . $event->order['id']);
	}

}
