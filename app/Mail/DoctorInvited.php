<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DoctorInvited extends Mailable {
	use Queueable, SerializesModels;
	public $inviteUrl;
	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct($urlLink) {
		$this->inviteUrl = $urlLink;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {

		//dd($this->inviteUrl);
		return $this->markdown('emails.doctor.signup_invited');
	}
}
