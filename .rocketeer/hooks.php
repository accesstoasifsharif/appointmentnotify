<?php

return [

	// Tasks
	//
	// Here you can define in the `before` and `after` array, Tasks to execute
	// before or after the core Rocketeer Tasks. You can either put a simple command,
	// a closure which receives a $task object, or the name of a class extending
	// the Rocketeer\Abstracts\AbstractTask class
	//
	// In the `custom` array you can list custom Tasks classes to be added
	// to Rocketeer. Those will then be available in the command line
	// with all the other tasks
	//////////////////////////////////////////////////////////////////////

	// Tasks to execute before the core Rocketeer Tasks
	'before' => [
		'setup' => [],
		'deploy' => [],
		'cleanup' => [],
	],

	// Tasks to execute after the core Rocketeer Tasks
	'after' => [
		'setup' => [],
		'deploy' => [
			'cp .env.staging .env',
			'php artisan optimize',

			'sudo chmod -R 777 /var/www/appointmentnotify',

			'php artisan cache:clear',
			'php artisan config:clear',
			'php artisan route:clear',

			'php artisan config:cache',
			'php artisan route:cache',

			'php artisan migrate:refresh',
			//'php artisan migrate',
			'php artisan db:seed',
			'sudo rm /var/www/appointmentnotify/current/public/storage',
			'php artisan storage:link',
			'php artisan passport:install',

			'sudo chmod -R 777 /var/www/appointmentnotify',

			'sudo chown ubuntu storage/oauth-*.key',
			'sudo chown www-data:www-data storage/oauth-*.key',
			'sudo chmod 600 storage/oauth-*.key',

			//'npm install',
			//	'node socket.js',
		],
		'cleanup' => [],
	],

	// Custom Tasks to register with Rocketeer
	'custom' => [],

];
