Vue.component('message', {
	props:['title','footer'],
	template: `
	<article class="message is-primary">
	<div class="message-header">
	{{title}}
	<button class="delete"></button>
	</div>
	<div class="message-body">

	<slot></slot>
	<br>
	<div class="message-footer btn">{{footer}}</div>
	</div>
	
	</article>
	`
});


new Vue({
	el: '#taskroot'

});