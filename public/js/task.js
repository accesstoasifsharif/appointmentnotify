class Errors {
	constructor() {
		this.errors = { };
	}

	get(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		}
	}

	record(errors) {
		this.errors = errors;
	}

	has(field) {
		return this.errors.hasOwnProperty(field);
	}

	any() {
		return Object.keys(this.errors).length > 0;
	}

	clear(field) {
		if (field) 
		{
			delete this.errors[field]
		};
		return;
	}
}

class Form {
	constructor(data) {
		this.originalData = data;
		for (let field in data) {
			this[field] = data[field];
		}
		this.errors= new Errors();
	}

	reset() {
		for (let field in this.originalData) {
			this[field] = '';
		}
	}
	data() {
		let data = Object.assign({},this);
		delete data.originalData;
		delete data.errors;
		return data;
	}

	submit(requestType, url) {
		axios[requestType](url,this.data())
		.then(this.onSuccess.bind(this))
		.catch(this.onFail.bind(this));
	}

	onSuccess(response) {
		alert('Task Created Successfully');
		this.errors.clear();
		this.reset();
	}

	onFail(error) {
		this.errors.record(error.response.data);
	}
}

new Vue({
	el: "#root",
	data: {
		form: new Form({
			title: '',
			description: ''
		})
	},

	methods: {
		onSubmit() {
			this.form.submit('post','/test/store');
		}
	}
});