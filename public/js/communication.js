window.Event= new class{
	constructor()
	{
		this.vue= new Vue();
	}
	fire(event,data=null){
		this.vue.$emit(event,data);
	}
	listen(event,callback){
		this.vue.$on(event,callback);
	}
}
Vue.component('coupan',{
	template:`
	<input type="text" placeholder="Enter Coupan" name="" @blur="onCoupanApplied">
	`
	,
	methods:{
		onCoupanApplied(){
			Event.fire('applied');
		}
	}

});




Vue.component('modal',{
	template:`
	<div id="modal1" class="modal">
	<div class="modal-content">
	<slot name="header"></slot>
	<slot></slot>
	</div>
	<div class="modal-footer">
	<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
	</div>
	</div>
	`
})
new Vue({
	el:'#modalexample'
});



new Vue({

	el:'#root',
	data:{
		oncoupanapplied:false
	},
	created() {
		Event.listen('applied',()=> alert('Handlig'));
	}
	
});