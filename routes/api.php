<?php
use Illuminate\Http\Request;
//Route::middleware('auth:api')->post('/validateusercredentials', 'TestController@ValidateUserCredentials');

Route::middleware('auth:api')->get('/user', function (Request $request) {
	/*$user = auth()->authenticate();
	return $user;*/
	return $request->user();

});
// Route::middleware('auth:api')->get('/getSession', 'HomeController@getSession');

// Route::group(array('middleware' => 'auth:api'), function () {
// });
Route::get('/getallspecialtylist', 'HomeAppointmentsController@getAllSpecialtyList');
Route::get('/showappointmentsforproviderview', 'HomeAppointmentsController@showAppointmentsForProviderView');

Route::get('/checkappointmentexistancebyappointmentid/{appointment_id}', 'HomeAppointmentsController@checkAppointmentExistanceByAppointmentId');

Route::get('/bookanappointment/{appointment_id}', 'HomeAppointmentsController@bookAnAppointment');

Route::get('/showappointmentsforfacilityview', 'HomeAppointmentsController@showAppointmentsForFacilityView');
Route::get('/showappointmentsforfacilityviewwithgender', 'HomeAppointmentsController@showAppointmentsForFacilityViewWithGender');

Route::get('/getsingledoctorfacilitiesbydoctorid/{doctor_id}', 'HomeAppointmentsController@getSingleDoctorFacilitiesByDoctorId');
