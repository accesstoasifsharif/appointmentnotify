<?php

Route::group(array('middleware' => 'auth:api'), function () {

	/*paginated list route*/
	Route::get('/getAllHCPFacilities', 'HealthCareProviderFacilityController@getHCPFacilities');
	/* without paginated list route*/
	Route::get('/allHCPFacilities', 'HealthCareProviderFacilityController@getAllHCPFacilities');

	Route::get('/getHCPFacilityDetail/{facility_id}', 'HealthCareProviderFacilityController@getHCPFacilityDetail');

	Route::get('/getHCPFacilityTimeTableDetail/{facility_id?}', 'HealthCareProviderFacilityController@getHCPFacilityTimeTableDetail');

	Route::post('/storeHCPFacility', 'HealthCareProviderFacilityController@storeHCPFacility');
	Route::get('/deleteHCPFacility/{facility_id}', 'HealthCareProviderFacilityController@deleteHCPFacility');

	Route::get('/searchExistingFacility', 'HealthCareProviderFacilityController@searchExistingFacility');

	/*paginated list route Staff */
	Route::get('/getAllHCPDoctorAndStaff', 'HealthCareProviderDoctorAndStaffController@getHCPDoctorAndStaff');

	Route::get('/allHCPDoctorAndStaff', 'HealthCareProviderDoctorAndStaffController@getAllHCPDoctorAndStaff');

	Route::get('/deleteHCPDoctorOrStaff', 'HealthCareProviderDoctorAndStaffController@deleteHCPDoctorOrStaff');

	Route::get('/searchAllDoctorOrStaff', 'HealthCareProviderDoctorAndStaffController@searchAllDoctorOrStaff');

	Route::get('/getTimeTableOfDoctorOrStaffOfHCPFacility', 'HealthCareProviderDoctorAndStaffController@getTimeTableOfDoctorOrStaffOfHCPFacility');

	Route::post('/addOrUpdateHCPAllStaff', 'HealthCareProviderDoctorAndStaffController@addOrUpdateHCPAllStaff');

	Route::get('/getSelectedDoctorStaffDetail', 'HealthCareProviderDoctorAndStaffController@getSelectedDoctorStaffDetail');

	Route::get('/allHCPDoctorsOfHCPFacility', 'HealthCareProviderDoctorAndStaffController@getAllHCPDoctorsOfHCPFacility');

	/*paginated list route*/
	Route::get('/getAllHCDepartments', 'HealthCareDepartmentController@getAllHCDepartments');

	/*Route For Appointments Upcoming.....*/
	Route::get('/getAllUpcomingAppointments', 'AppointmentController@getAllUpcomingAppointments');
	/*Route For Appointments Upcoming Detail.....*/
	Route::get('/getAllUpcomingAppointmentsDetail', 'AppointmentController@getAllUpcomingAppointmentsDetail');

	/*Route For Appointments Past.....*/
	Route::get('/getAllPastAppointments', 'AppointmentController@getAllPastAppointments');
	/*Route For Appointments Past Detail.....*/
	Route::get('/getAllPastAppointmentsDetail', 'AppointmentController@getAllPastAppointmentsDetail');

	/*Route For Reserve Appointments Request.....*/
	Route::get('/getAllReserveAppointmentRequests', 'AppointmentRequestController@getAllReserveAppointmentRequests');
	/*Route For Reserve Appointments Detail.....*/
	Route::get('/getAllReserveAppointmentRequestsDetail', 'AppointmentRequestController@getAllReserveAppointmentRequestsDetail');

	/*Route For Reschedule Appointments Request.....*/
	Route::get('/getAllRescheduleAppointmentRequests', 'AppointmentRequestController@getAllRescheduleAppointmentRequests');
	/*Route For Reschedule Appointments Detail.....*/
	Route::get('/getAllRescheduleAppointmentRequestsDetail', 'AppointmentRequestController@getAllRescheduleAppointmentRequestsDetail');

	/*Route For Cancel Appointments Request.....*/
	Route::get('/getAllCancelAppointmentRequests', 'AppointmentRequestController@getAllCancelAppointmentRequests');
	/*Route For Cancel Appointments Detail.....*/
	Route::get('/getAllCancelAppointmentRequestsDetail', 'AppointmentRequestController@getAllCancelAppointmentRequestsDetail');

	/* Appointments & Requests Operations Route list */

	Route::post('/rescheduleExistingAnEvent', 'AppointmentAndRequestOperationController@rescheduleExistingAnEvent');
	Route::get('/reserveRequestApproval', 'AppointmentAndRequestOperationController@reserveRequestApproval');

	Route::post('/reserveRequestReschedule', 'AppointmentAndRequestOperationController@reserveRequestReschedule');

	Route::get('/rescheduleRequestApproval', 'AppointmentAndRequestOperationController@rescheduleRequestApproval');
	Route::post('/rescheduleRequestReschedule', 'AppointmentAndRequestOperationController@rescheduleRequestReschedule');

	/* Publish Appointment Route */
	Route::post('/publishNewAppointment', 'AppointmentAndRequestOperationController@publishNewAppointment');

	/* Patient Short Call */
	Route::post('/addPatientToShortCall', 'HealthCareProviderPatientShortCallController@addPatientToShortCall');

	/* Profile setting routes for Providers */
	Route::post('/saveOrUpdateSPProfile', 'SinglePracticeProfileController@saveOrUpdateSPProfile');

	Route::post('/saveOrUpdateMCProfile', 'MedicalClinicProfileController@saveOrUpdateMCProfile');
	Route::post('/saveOrUpdateHospitalProfile', 'HospitalProfileController@saveOrUpdateHospitalProfile');
	Route::post('/saveOrUpdateDepartmentProfile', 'DepartmentProfileController@saveOrUpdateDepartmentProfile');
	/*Provider site Messages Tab Route*/

	Route::get('/getHCPAllStaffConversationPreview', 'HealthCareProviderMessageController@getHCPAllStaffConversationPreview');

	Route::get('/getHCPPatient', 'HealthCareProviderPatientController@getHCPPatient');

	Route::get('/getPatientsConversationPreview', 'HealthCareProviderMessageController@getPatientsConversationPreview');

	/*Provider site Calendar Tab Route*/
	Route::get('/getAllAppointments', 'HealthCareProviderCalendarController@getAllAppointments');

	Route::get('/getDetailPreviewOfMonthViewAppointments', 'HealthCareProviderCalendarController@getDetailPreviewOfMonthViewAppointments');
	Route::get('/getDetailPreviewOfDayViewAppointment', 'HealthCareProviderCalendarController@getDetailPreviewOfDayViewAppointment');

	/*For get all patients of specific perspective*/
	Route::get('/getallpatientsspecificperspective', 'PatientController@getAllPatientsSpecificPerspective');
	/*All Patient of specific doctor*/
	Route::get('/allpatientsbydoctorid', 'PatientController@allPatientsByDoctorId');
	/*Route For Appointments Published.....*/
	Route::get('/getAllPublishedAppointments', 'AppointmentController@getAllPublishedAppointments');
	/*Route For Appointments Published Detail.....*/
	Route::get('/getAllPublishedAppointmentsDetail', 'AppointmentController@getAllPublishedAppointmentsDetail');

	/* Route For Published Appointment Operations*/

	Route::post('/publishedAppointmentReschedule', 'AppointmentAndRequestOperationController@publishedAppointmentReschedule');

	Route::get('/publishedAppointmentCancel', 'AppointmentAndRequestOperationController@publishedAppointmentCancel');

	Route::post('/publishedAppointmentPatientAssign', 'AppointmentAndRequestOperationController@publishedAppointmentPatientAssign');

	Route::get('/publishedAppointmentToShortCallList', 'AppointmentAndRequestOperationController@publishedAppointmentToShortCallList');

	Route::get('/rescheduleRequestCancel', 'AppointmentAndRequestOperationController@rescheduleRequestCancel');

	Route::get('/dumpCanceledAppointment', 'AppointmentAndRequestOperationController@dumpCanceledAppointment');

	Route::get('/publishCanceledAppointment', 'AppointmentAndRequestOperationController@publishCanceledAppointment');

	Route::get('/putCanceledAppointmentToShortCallPool', 'AppointmentAndRequestOperationController@putCanceledAppointmentToShortCallPool');

});
