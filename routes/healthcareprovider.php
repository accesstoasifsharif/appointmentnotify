<?php

Route::get('/{id?}', 'HealthCareProviderController@index')->name(healthCareProviderRedirectUrl());

Route::get('/{id?}/registrationwizard', 'HealthCareProviderController@wizard')->name(healthCareProviderWizardUrl());

Route::get('/testing', 'HealthCareProviderController@testing');

// Route::get('/{id?}/dashboard', 'HealthCareProviderController@healthCareProviderDashboard');

Route::get('/{id?}/dashboard', 'HealthCareProviderController@healthCareProviderDashboard')->name('dashboard');
Route::get('/{id?}/facility', 'HealthCareProviderController@healthCareProviderFacility')->name('facility');
Route::get('/{id?}/hospital', 'HealthCareProviderController@healthCareProviderHospital')->name('hospital');
Route::get('/{id?}/department', 'HealthCareProviderController@healthCareProviderDepartment')->name('department');
Route::get('/{id?}/staff', 'HealthCareProviderController@healthCareProviderStaff')->name('staff');
Route::get('/{id}/appointments', 'HealthCareProviderController@healthCareProviderAppointment')->name('appointments');
Route::get('/{id}/calendar', 'HealthCareProviderController@healthCareProviderCalendar')->name('calendar');
Route::get('/{id}/messages', 'HealthCareProviderController@healthCareProviderMessage')->name('messages');

Route::get('/{id}/patients', 'HealthCareProviderController@healthCareProviderPatient')->name('patients');

Route::get('allusers', 'HealthCareProviderController@allUsers');

Route::get('/timeConvert', 'HealthCareProviderController@timeConvert');

Route::get('/{id}/accountsettings', 'HealthCareProviderController@healthCareProviderAccountSettings')->name('accountsettings');
