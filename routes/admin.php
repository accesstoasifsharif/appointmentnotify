<?php

//Route::group(array('prefix' => 'admin'), function() {

Route::get('/', 'AdminController@dashboard')->name(adminRedirectUrl());
Route::get('dashboard', 'AdminController@dashboard');
Route::get('test', 'AdminController@test');
//});
Route::get('/getZipCodeByLatLong5', '\Admin\ApiControllers\CommonApiController@getZipCodeByLatLong');