<?php

Route::get('/getAvatar', 'CommonApiController@getAvatarByModelAndId');

Route::group(array('middleware' => 'auth:api'), function () {
	Route::get('/getAllCountries', 'CommonApiController@allCountries');
	Route::get('/getAllStates', 'CommonApiController@allStates');
	Route::get('/allSpecialties', 'CommonApiController@allSpecialties');
	Route::get('/shortCallDuration', 'CommonApiController@shortCallDuration');

	// Route for notifications
	Route::get('/allNotifications', 'NotificationApiController@allNotifications');
	Route::get('/markNotificationAsRead/{notification_id}', 'NotificationApiController@markNotificationAsRead');

	Route::get('/markNotificationAsUnRead/{notification_id}', 'NotificationApiController@markNotificationAsUnRead');

	// Route for Messages

	Route::get('/conversationWithSpecificUser', 'MessageApiController@SpecificUserConverstion');
	Route::post('/saveSendMessage', 'MessageApiController@storeMessage');
	Route::POST('/updatepassword', 'UserController@resetPassword');
	Route::POST('/updatehippaconsent', 'UserController@upDateHippaConsent');
	Route::get('/getlicenseagreement', 'UserController@getLicenseAgreement');
	Route::get('/getagreement', 'UserController@getAgreement');
	// Route::get('/getConversationPreview', 'MessageApiController@getConversationPreview');

});
