<?php

use Facades\Service\Admin\Admin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
// 	return view('welcome');
// });
Route::get('/', 'HomeController@homePage');

Auth::routes();
Admin::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('token', function () {
	return view('gettoken');
});

Route::get('/test/index', 'TestController@index');
Route::get('webpack', 'WebpackController@index');
Route::get('dummyUserCreate', 'WebpackController@dummyUserCreate');
Route::get('partial', 'TestController@partial');
Route::get('secondpartial', 'TestController@secondPartial');
Route::get('vuepractice', 'TestController@vuePractice');

Route::get('communicatonapp', 'TestController@communicatonapp');

// Vue Practice by asif
Route::get('vuecomponentone', 'WebpackController@vueComponentOne');
Route::get('vuecomponenttwo', 'WebpackController@vueComponentTwo');
Route::get('vuecomponentthree', 'WebpackController@vueComponentThree');
Route::get('vuecomponentfour', 'WebpackController@vueComponentFour');
Route::get('vuecomponentfive', 'WebpackController@vueComponentFive');

Route::get('/work', [
	'as'   => 'work.test',
	'uses' => 'TestController@namedRoute',

]);
Route::get('nameRoute', 'TestController@namedRoute');

// Route::post('nameRoute','TestController@namedRoute');
// Route::post('nameRoute','TestController@namedRoute');

Route::group(array('prefix' => 'test'), function () {
	Route::get('adnanTest', 'TestController@adnanTest');
	Route::get('/projects', 'ProjectsController@index');
	Route::post('/storeproject', 'ProjectsController@store');

	Route::get('task', 'TaskController@create');
	Route::post('store', 'TaskController@store');
	Route::get('/helperTest', function () {

	});
	Route::get('/hashcheck', 'TestController@hashCheck');
	Route::get('/check', 'TestController@red')->name('test.red');
	Route::get('/invite', '\App\Http\Controllers\Auth\InviteController@test');
});
Route::group(array('prefix' => 'invite', 'namespace' => 'Auth'), function () {

	Route::get('/doctorInvite/{token}', 'InviteController@doctorInvite')->name('doctor.invite');
	Route::post('/inviteset', 'InviteController@setPassword')->name('invite.set');

});
