<?php

Route::get('/{id?}', 'SiteUserController@index')->name(siteUserRedirectUrl());

Route::get('/{id?}/dashboard', 'SiteUserController@dashboard')->name('dashboard');

Route::get('/{id?}/registrationwizard', 'SiteUserController@wizard')->name(siteUserWizardUrl());

Route::get('getdummyavatars', 'SiteUserController@dummyAvatars');
Route::post('storeprofile', 'SiteUserController@storeProfile');

/* Routes for Provider tab (Asif Sharif)*/
Route::get('/{id?}/providers', 'SiteUserController@Providers')->name('providers');
Route::get('/{id?}/calendar', 'SiteUserController@Calendar')->name('calendar');
Route::get('/{id?}/accountsetting', 'SiteUserController@accountSetting');

// Rote for Site User Appointments Tab (Raza)

Route::get('/{id?}/messages', 'SiteUserController@siteUserMessaging')->name('messages');

Route::get('/{id?}/appointments', 'SiteUserController@appointments')->name('appointments');
