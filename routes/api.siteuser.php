<?php

Route::group(array('middleware' => 'auth:api'), function () {
	/*Routes for short call lsit*/
	Route::POST('/saveshortcalllist', 'ShortCallListController@saveShortCallList');

	Route::get("/shortcalllistdoctor", "ShortCallListController@getAllDoctorsOfAuthenticatedUser");

	Route::get("/getfacilitybydoctorid/{doctor_id}", "ShortCallListController@getFacilityByDoctorId");

	Route::get('/shortcalltime', 'ShortCallListController@index');

	/*Routes for appointments*/
	Route::get('/getappointmentreasons', 'AppointmentController@getAppointmentReasons');

	Route::POST('/saveappointment', 'AppointmentController@saveAppointment');

	Route::get('/allupcomingappointmentsbysiteuserid', 'AppointmentController@getAllUpComingAppointemntsBySiteUserId');

	/*Asif Sharif*/
	Route::get('/alldistinctupcomingappointmentsbysiteuserid', 'AppointmentController@allDistinctUpcomingAppointmentsBySiteUserId');
	Route::get('/alldistinctpastappointmentsbysiteuserid', 'AppointmentController@allDistinctPastAppointmentsBySiteUserId');

	/*Asif Sharif*/
	Route::get('/getdetailofupcomingappointmentbydoctorid/{id}', 'AppointmentController@getUpcomingAppointmentDetailByDoctorId');

	Route::get('/allpastappointmentsbysiteuserid', 'AppointmentController@getAllPastAppointemntsBySiteUserId');
	Route::get('/getdetailofpastgappointmentbyioctorid/{id}', 'AppointmentController@getDetailOfPastAppointmentByDoctorId');

	Route::get('/getnotebyappointmentid/{id}', 'AppointmentController@getNoteByAppointmentId');
	Route::post('/savenoteonpastappointment', 'AppointmentController@saveNote');

	Route::post('/rescheduletimeforupcomingappointments', 'AppointmentController@saveRescheduleTime');
	Route::get('/cancelappointment/{id}', 'AppointmentController@cancelUpComingAppointemnt');

	Route::get('/getaggregatedrescheduledappointmentbysiteuserid', 'AppointmentController@getAggregatedRescheduledAppointmentBySiteUserId');
	Route::get('getallcancelledappointmentsbysiteuserid', 'AppointmentController@getAllCancelledAppointmentBySiteUserId');
	Route::get('/getallcancelledappointmentsdetailbydoctorid/{doctor_id}', 'AppointmentController@getAllCancelledAppointmentsDetailByDoctorId');
	Route::get('/revokecancelledappointment/{appointment_id}', 'AppointmentController@revokeCancelledAppointment');
	Route::get('/getrescheduledappointmentdetailbydoctorid/{doctor_id}', 'AppointmentController@getRescheduledAppointmentDetailByDoctorId');
	Route::get('/getalldistinctreservedappointmentsbysiteuserid', 'AppointmentController@allDistinctReservedAppointmentsBySiteUserId');
	Route::get('/getaggregatedreservedappointmentsbysiteuserid', 'AppointmentController@getAggregatedReservedAppointmentsBySiteUserId');
	Route::get('/getreservedappointmentsdetailbydoctorid/{doctor_id}', 'AppointmentController@getAllReservedAppointmentsDetailByDoctorId');
	Route::get('/getaggregatedshortcallappointmentsbysiteuserid', 'AppointmentController@getAggregatedShortCallAppointmentsBySiteUserId');
	Route::get('/getshortcallappointmentdetailbydoctorid/{doctor_id}', 'AppointmentController@getShortCallAppointmentDetailByDoctorId');
	/* Routes for provider tabs*/
	Route::get("providerdetail", "ProviderController@providerDetail");
	Route::get("getuser", "ProviderController@getUser");
	Route::get('getdoctorbyid/{doctor_id}', 'ProviderController@getDoctorById');
	Route::get('deletedoctorbyid/{doctor_id}', 'ProviderController@deleteDoctorById');
	Route::get('getsingledoctorinfobyid/{doctor_id}', 'ProviderController@getSingleDoctorInfoById');
	Route::get('getfacilitytimetable/{facility_id}', 'ProviderController@getFacilityTimeTable');
	Route::get('getallsiteuserprovidersbysiteuserid', 'ProviderController@getAllSiteUserProvidersBySiteUserId');
	Route::get('allsiteuserproviders', 'ProviderController@allSiteUserProviders');

	/* Routes for calendar tab*/
	Route::get('getallappointmentsbysiteuserid', 'CalendarController@getAllAppointmentsBySiteUserId');
	/* Routes for account setting*/
	Route::get('getsiteuserinfo', 'SiteUserAccountSettingController@getSiteUserInfo');
	Route::post('updatesiteuserinfo', 'SiteUserAccountSettingController@updateSiteUserInfo');

	Route::post('saverecoveryoption', 'SiteUserAccountSettingController@saveRecoveryOptions');
	Route::post('updaterecoveryoption', 'SiteUserAccountSettingController@updateRecoveryOptions');
	Route::get('getallrecoveryotionsbyuserid', 'SiteUserAccountSettingController@getAllRecoveryOtionsByUserId');
	/* Site User Profile */
	Route::get('getsiteuserProfile', 'SiteUserAccountSettingController@getSiteUserProfile');
	Route::get('getsiteuserAddress', 'SiteUserAccountSettingController@getSiteUserAddress');
	Route::get('getsiteuserContactInfo', 'SiteUserAccountSettingController@getSiteUserContactInfo');
	Route::get('getsiteuserPersonalInfo', 'SiteUserAccountSettingController@getSiteUserPersonalInfo');
	Route::post('updateSiteUserPersonalInfo', 'SiteUserAccountSettingController@updateSiteUserPersonalInfo');
	Route::post('updateSiteUserAddress', 'SiteUserAccountSettingController@updateSiteUserAddress');
	Route::post('updateSiteUserContactInfo', 'SiteUserAccountSettingController@updateSiteUserContactInfo');

	Route::post('saveSiteUserProfile', 'SiteUserAccountSettingController@saveSiteUserProfile');

	Route::get('getSiteUserProvidersConverstion', 'MessagesController@getSiteUserProvidersConverstion');

// Emergency Contact Route

	Route::get('/getEmergencyContactData', 'SiteUserEmergencyContactController@emergencyContactData');

	Route::post('/addOrUpDateEmergencyContactData', 'SiteUserEmergencyContactController@addOrUpDateEmergencyContact');

	Route::get('/yearListOfAppointments', 'DashboardController@yearListOfAppointments');
	Route::get('/yearMonthListOfAppointments/{year}', 'DashboardController@yearMonthListOfAppointments');
	Route::get('/appointmentListOfMonthOfYear/{year}/{month}', 'DashboardController@appointmentListOfMonthOfYear');

});
