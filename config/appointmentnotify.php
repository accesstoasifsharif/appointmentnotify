<?php
return [

	'APP_NAME'                                => env('APP_NAME', 'Default App'),

	'trail_days'                              => '60',

	// in minutes
	'appointment_default_end_time_duration'   => '30',
	'short_call_appointment_expiry_duration'  => '180',

	// in days
	'token_validity'                          => '60',

	'profile_status'                          => [
		'complete'   => '1',
		'incomplete' => '0',
	],

	'user_type_name'                          => [SiteUser, HealthCareProvider, Admin],
	Admin                                     =>
	[
		'user_type_id' => '9',
		'redirectUrl'  => strtolower(Admin),
		'namespace'    => Admin,
		'prefix'       => strtolower(Admin),
	]
	,

	SiteUser                                  =>
	[
		'user_type_id'     => '0',
		'redirectUrl'      => strtolower(SiteUser),
		'profileWizardUrl' => strtolower(SiteUser) . '.wizard',
		'namespace'        => SiteUser,
		'prefix'           => strtolower(SiteUser),
	],

	'site_user_features'                      => ['dashboard', 'providers', 'appointments', 'calendar', 'messages'],

	HealthCareProvider                        =>
	[
		'user_type_id'     => '1',
		'redirectUrl'      => strtolower(HealthCareProvider),
		'profileWizardUrl' => strtolower(HealthCareProvider) . '.wizard',
		'namespace'        => HealthCareProvider,
		'prefix'           => strtolower(HealthCareProvider),

		'type_name'        => ['0' => 'Hospital', '1' => 'Medical Clinic', '2' => 'Department', '3' => 'Single Practice', '4' => 'Staff'],
		'type'             => [snake_case('Hospital') => '0', snake_case('Medical Clinic') => '1', snake_case('Department') => '2', snake_case('Single Practice') => '3', snake_case('Staff') => '4'],

		'hcp_type'         => [
			Hospital       => ['id' => '0', 'name' => snake_case(Hospital), 'description' => Hospital],
			MedicalClinic  => ['id' => '1', 'name' => snake_case(MedicalClinic), 'description' => MedicalClinic],
			Department     => ['id' => '2', 'name' => snake_case(Department), 'description' => Department],
			SinglePractice => ['id' => '3', 'name' => snake_case(SinglePractice), 'description' => SinglePractice],
			Staff          => ['id' => '4', 'name' => snake_case(Staff), 'description' => Staff],
		],

	],

	'an_event_type'                           => ['upcoming' => ['event_type' => '0', 'color' => 'green'], 'past' => ['event_type' => '1', 'color' => 'gray'], 'reschedule' => ['event_type' => '2', 'color' => 'blue']],

	'address_type'                            => ['home_address' => '0', 'office_address' => '1', 'mail_address' => '2'],

	'alert_type'                              => ['reminder' => '0', 'notification' => '1'],

	'alert_name'                              => ['email' => '0', 'sms' => '1', 'platform' => '2'],

	'insurance_category'                      => ['your_current_insurance' => '0', 'all_insurances' => '1'],

	'insurance_type'                          => ['medical_insurance' => '0', 'dental_insurance' => '1'],

	'contact_type'                            => ['home' => '0', 'work' => '1', 'fax' => '2'],

	'calendar_type'                           => ['platform_cal' => '0', 'google_cal' => '1', 'i_cal' => '2', 'outlook_cal' => '3'],

	'appointment_status'                      =>
	[
		'Private'    => '0',
		'Public'     => '1',
		'Reschedule' => '2',
		'Suspend'    => '3',
		'Cancel'     => '4',
	],
	'appointment_current_state'               =>
	[
		'Pending'   => '0',
		'Approved'  => '1',

		'Publish'   => '2',
		'Suspend'   => '3',
		'Cancel'    => '4',
		'ShortCall' => '5',

	],
	'APP_APPOINTMENT_CANCELATION_REVOKE_TIME' => ['RevokeTimeLimit' => '30'],

	'APP_SHORT_CALL_LIST_TIME_SPAN'           => ['1 Week', '2 Week', '3 Week', '4 Week'],

	'features'                                => ['dashboard', 'hospital', 'department', 'facility', 'staff', 'appointments', 'calendar', 'messages', 'patients'],

	// 'billing', 'subscription'

	'role_levels'                             => ['admin', 'non_admin'],

	'model_identifiers'                       => ['SiteUser' => '0', 'Doctor' => '1', 'Staff' => '2', 'HealthCareFacility' => '3', 'User' => '4'],

	'country_id'                              => ['USA' => '1'],

	'roles'                                   => [],

	'business_type_hospital_admin'            => [
		[
			'name'             => 'hospital_dashboard_admin',
			'business_type_id' => '1',
			'role_level_id'    => '1',
			'feature_id'       => '1',
		],
		[
			'name'             => 'hospital_hospital_admin',
			'business_type_id' => '1',
			'role_level_id'    => '1',
			'feature_id'       => '2',
		],
		[
			'name'             => 'hospital_department_admin',
			'business_type_id' => '1',
			'role_level_id'    => '1',
			'feature_id'       => '3',
		],
		[
			'name'             => 'hospital_facility_admin',
			'business_type_id' => '1',
			'role_level_id'    => '1',
			'feature_id'       => '4',
		],
		[
			'name'             => 'hospital_staff_admin',
			'business_type_id' => '1',
			'role_level_id'    => '1',
			'feature_id'       => '5',
		],
		[
			'name'             => 'hospital_appointment_admin',
			'business_type_id' => '1',
			'role_level_id'    => '1',
			'feature_id'       => '6',
		],
		[
			'name'             => 'hospital_calendar_admin',
			'business_type_id' => '1',
			'role_level_id'    => '1',
			'feature_id'       => '7',
		],
		[
			'name'             => 'hospital_message_admin',
			'business_type_id' => '1',
			'role_level_id'    => '1',
			'feature_id'       => '8',
		],
		[
			'name'             => 'hospital_patient_admin',
			'business_type_id' => '1',
			'role_level_id'    => '1',
			'feature_id'       => '9',
		],
	],

	'business_type_hospital_non_admin'        => [
		[
			'name'             => 'hospital_dashboard_non_admin',
			'business_type_id' => '1',
			'role_level_id'    => '2',
			'feature_id'       => '1',
		],
		[
			'name'             => 'hospital_facility_non_admin',
			'business_type_id' => '1',
			'role_level_id'    => '2',
			'feature_id'       => '4',
		],
		[
			'name'             => 'hospital_staff_non_admin',
			'business_type_id' => '1',
			'role_level_id'    => '2',
			'feature_id'       => '5',
		],
		[
			'name'             => 'hospital_appointment_non_admin',
			'business_type_id' => '1',
			'role_level_id'    => '2',
			'feature_id'       => '6',
		],
		[
			'name'             => 'hospital_calendar_non_admin',
			'business_type_id' => '1',
			'role_level_id'    => '2',
			'feature_id'       => '7',
		],
		[
			'name'             => 'hospital_message_non_admin',
			'business_type_id' => '1',
			'role_level_id'    => '2',
			'feature_id'       => '8',
		],
		[
			'name'             => 'hospital_patient_non_admin',
			'business_type_id' => '1',
			'role_level_id'    => '2',
			'feature_id'       => '9',
		],
	],

	'business_type_medical_clinic_admin'      => [
		[
			'name'             => 'medical_clinic_dashboard_admin',
			'business_type_id' => '2',
			'role_level_id'    => '1',
			'feature_id'       => '1',
		],
		[
			'name'             => 'medical_clinic_department_admin',
			'business_type_id' => '2',
			'role_level_id'    => '1',
			'feature_id'       => '3',
		],
		[
			'name'             => 'medical_clinic_facility_admin',
			'business_type_id' => '2',
			'role_level_id'    => '1',
			'feature_id'       => '4',
		],
		[
			'name'             => 'medical_clinic_staff_admin',
			'business_type_id' => '2',
			'role_level_id'    => '1',
			'feature_id'       => '5',
		],
		[
			'name'             => 'medical_clinic_appointment_admin',
			'business_type_id' => '2',
			'role_level_id'    => '1',
			'feature_id'       => '6',
		],
		[
			'name'             => 'medical_clinic_calendar_admin',
			'business_type_id' => '2',
			'role_level_id'    => '1',
			'feature_id'       => '7',
		],
		[
			'name'             => 'medical_clinic_message_admin',
			'business_type_id' => '2',
			'role_level_id'    => '1',
			'feature_id'       => '8',
		],
		[
			'name'             => 'medical_clinic_patient_admin',
			'business_type_id' => '2',
			'role_level_id'    => '1',
			'feature_id'       => '9',
		],

	],

	'business_type_medical_clinic_non_admin'  => [
		[
			'name'             => 'medical_clinic_dashboard_non_admin',
			'business_type_id' => '2',
			'role_level_id'    => '2',
			'feature_id'       => '1',
		],

		[
			'name'             => 'medical_clinic_facility_non_admin',
			'business_type_id' => '2',
			'role_level_id'    => '2',
			'feature_id'       => '4',
		],

		[
			'name'             => 'medical_clinic_staff_non_admin',
			'business_type_id' => '2',
			'role_level_id'    => '2',
			'feature_id'       => '5',
		],
		[
			'name'             => 'medical_clinic_appointment_non_admin',
			'business_type_id' => '2',
			'role_level_id'    => '2',
			'feature_id'       => '6',
		],
		[
			'name'             => 'medical_clinic_calendar_non_admin',
			'business_type_id' => '2',
			'role_level_id'    => '2',
			'feature_id'       => '7',
		],
		[
			'name'             => 'medical_clinic_message_non_admin',
			'business_type_id' => '2',
			'role_level_id'    => '2',
			'feature_id'       => '8',
		],
		[
			'name'             => 'medical_clinic_patient_non_admin',
			'business_type_id' => '2',
			'role_level_id'    => '2',
			'feature_id'       => '9',
		],
	],

	'business_type_department_admin'          => [
		[
			'name'             => 'department_dashboard_admin',
			'business_type_id' => '3',
			'role_level_id'    => '1',
			'feature_id'       => '1',
		],

		[
			'name'             => 'department_department_admin',
			'business_type_id' => '3',
			'role_level_id'    => '1',
			'feature_id'       => '3',
		],

		[
			'name'             => 'department_facility_admin',
			'business_type_id' => '3',
			'role_level_id'    => '1',
			'feature_id'       => '4',
		],

		[
			'name'             => 'department_staff_admin',
			'business_type_id' => '3',
			'role_level_id'    => '1',
			'feature_id'       => '5',
		],

		[
			'name'             => 'department_appointment_admin',
			'business_type_id' => '3',
			'role_level_id'    => '1',
			'feature_id'       => '6',
		],
		[
			'name'             => 'department_calendar_admin',
			'business_type_id' => '3',
			'role_level_id'    => '1',
			'feature_id'       => '7',
		],
		[
			'name'             => 'department_message_admin',
			'business_type_id' => '3',
			'role_level_id'    => '1',
			'feature_id'       => '8',
		],
		[
			'name'             => 'department_patient_admin',
			'business_type_id' => '3',
			'role_level_id'    => '1',
			'feature_id'       => '9',
		],

	],
	'business_type_department_non_admin'      => [
		[
			'name'             => 'department_dashboard_non_admin',
			'business_type_id' => '3',
			'role_level_id'    => '2',
			'feature_id'       => '1',
		],

		[
			'name'             => 'department_facility_non_admin',
			'business_type_id' => '3',
			'role_level_id'    => '2',
			'feature_id'       => '4',
		],

		[
			'name'             => 'department_staff_non_admin',
			'business_type_id' => '3',
			'role_level_id'    => '2',
			'feature_id'       => '5',
		],
		[
			'name'             => 'department_appointment_non_admin',
			'business_type_id' => '3',
			'role_level_id'    => '2',
			'feature_id'       => '6',
		],
		[
			'name'             => 'department_calendar_non_admin',
			'business_type_id' => '3',
			'role_level_id'    => '2',
			'feature_id'       => '7',
		],
		[
			'name'             => 'department_message_non_admin',
			'business_type_id' => '3',
			'role_level_id'    => '2',
			'feature_id'       => '8',
		],
		[
			'name'             => 'department_patient_non_admin',
			'business_type_id' => '3',
			'role_level_id'    => '2',
			'feature_id'       => '9',
		],

	],

	'business_type_single_practice_admin'     => [
		[
			'name'             => 'single_practice_dashboard_admin',
			'business_type_id' => '4',
			'role_level_id'    => '1',
			'feature_id'       => '1',
		],

		[
			'name'             => 'single_practice_facility_admin',
			'business_type_id' => '4',
			'role_level_id'    => '1',
			'feature_id'       => '4',
		],

		[
			'name'             => 'single_practice_staff_admin',
			'business_type_id' => '4',
			'role_level_id'    => '1',
			'feature_id'       => '5',
		],

		[
			'name'             => 'single_practice_appointment_admin',
			'business_type_id' => '4',
			'role_level_id'    => '1',
			'feature_id'       => '6',
		],
		[
			'name'             => 'single_practice_calendar_admin',
			'business_type_id' => '4',
			'role_level_id'    => '1',
			'feature_id'       => '7',
		],
		[
			'name'             => 'single_practice_message_admin',
			'business_type_id' => '4',
			'role_level_id'    => '1',
			'feature_id'       => '8',
		],
		[
			'name'             => 'single_practice_patient_admin',
			'business_type_id' => '4',
			'role_level_id'    => '1',
			'feature_id'       => '9',
		],

	],

	'business_type_single_practice_non_admin' => [
		[
			'name'             => 'single_practice_dashboard_non_admin',
			'business_type_id' => '4',
			'role_level_id'    => '2',
			'feature_id'       => '1',
		],

		[
			'name'             => 'single_practice_facility_non_admin',
			'business_type_id' => '4',
			'role_level_id'    => '2',
			'feature_id'       => '4',
		],

		[
			'name'             => 'single_practice_staff_non_admin',
			'business_type_id' => '4',
			'role_level_id'    => '2',
			'feature_id'       => '5',
		],

		[
			'name'             => 'single_practice_appointment_non_admin',
			'business_type_id' => '4',
			'role_level_id'    => '2',
			'feature_id'       => '6',
		],
		[
			'name'             => 'single_practice_calendar_non_admin',
			'business_type_id' => '4',
			'role_level_id'    => '2',
			'feature_id'       => '7',
		],
		[
			'name'             => 'single_practice_message_non_admin',
			'business_type_id' => '4',
			'role_level_id'    => '2',
			'feature_id'       => '8',
		],
		[
			'name'             => 'single_practice_patient_non_admin',
			'business_type_id' => '4',
			'role_level_id'    => '2',
			'feature_id'       => '9',
		],
	],

];