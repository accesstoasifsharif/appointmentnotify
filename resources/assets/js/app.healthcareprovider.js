
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');
 
 import router from './routes/routes.healthcareprovider.platform';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */



 // Vue.component('datatable', require('./components/common/DataTable.vue'));
 // Vue.component('tabular-view', require('./components/common/TabularView.vue'));
 // Vue.component('table-buttons', require('./components/common/Button.vue'));	
 // Vue.component('input-field', require('./components/common/InputField.vue'));
 // Vue.component('auto-complete', require('./components/common/AutoComplete/AutoComplete.vue'));


 // import Autocomplete from './Autocomplete.vue';

 // Vue.component('autocomplete',require('./components/common/Autocomplete.vue'));

 Vue.component('special-actions', require('./components/healthcareprovider/SpecialActions.vue'));


 Vue.component('hcp-dashboard',require('./views/healthcareprovider/HCP_Dashboard.vue'));

 Vue.component('facility-view', require('./components/healthcareprovider/Facility.vue'));
 Vue.component('staff-view', require('./components/healthcareprovider/Staff.vue'));
 Vue.component('appointment-request-view', require('./components/healthcareprovider/Appointment.vue'));
 Vue.component('department-view', require('./components/healthcareprovider/Department.vue')); 
 Vue.component('messages-view',require('./components/healthcareprovider/Messages.vue'));
 /*Calender Component*/
 Vue.component('calender-view', require('./components/healthcareprovider/Calender.vue'));

 Vue.component('medical-practice-wizard',require('./components/healthcareprovider/MedicalPracticeWizard.vue'));
 Vue.component('single-practice-wizard',require('./components/healthcareprovider/SinglePracticeWizard.vue'));

 Vue.component('hospital-wizard',require('./components/healthcareprovider/HospitalWizard.vue'));
 Vue.component('department-wizard',require('./components/healthcareprovider/DepartmentWizard.vue'));
 Vue.component('book-appointment',require('./components/healthcareprovider/BookAppointment.vue'));
 Vue.component('patient-list',require('./components/healthcareprovider/PatientList.vue'));
 Vue.component('materialize-modal', require('./components/common/MaterializeModal.vue'));
 Vue.component('materialize-modal-header', require('./components/common/MaterializeModalHeader.vue'));
 Vue.component('time-table', require('./components/common/TimeTable.vue'));
 Vue.component('time-picker', require('./components/common/TimePicker.vue'));
 Vue.component('circular-view', require('./components/common/CircularView.vue'));
 Vue.component('image-view', require('./components/common/Image.vue'));

 Vue.component('ul-tabs',require('./components/common/Ultabs.vue'));
 Vue.component('tab-component',require('./components/common/Tabs.vue'));
 Vue.component('time-picker', require('./components/common/TimePicker.vue'));
 Vue.component('date-picker', require('./components/common/DatePicker.vue'));


 Vue.component('date-picker', require('./components/common/DatePicker.vue'));
 Vue.component('header-component', require('./components/common/Header.vue'));
 Vue.component('notification-bell', require('./components/common/NotificationBell.vue'));


 Vue.component('single-practice-account-setting',require('./views/healthcareprovider/single-practice-account-setting.vue'));
 Vue.component('healthcareprovider-reset-password',require('./views/healthcareprovider/healthcareprovider-reset-password.vue'));
 Vue.component('healthcareprovider-hippa-consent',require('./views/healthcareprovider/healthcareprovider-hippa-consent.vue'));
 
 Vue.use(VueCookie);

 Vue.component('feature-list', require('./components/common/FeatureList.vue') )





 

 new Vue({

 	el: '#app_healthcareprovider',
 	router: router,
 	methods: {
 		testing: function(data) {
 			alert(data);
 		},
 	},
 	data:{
 		features :[],
 	},

 	methods: { 
 		extractFeatures  (data) {

 			var jsonData = JSON.parse(JSON.stringify(data)  );
 			for (var i = 0; i < jsonData.length; i++) {
 				var url = '/'+ window.AppointmentNotify.urlPrefix + '/'+ window.AppointmentNotify.selected_perspective_slug + '/'+jsonData[i].feature.feature_name
 				this.features.push(
 				{ 
 					url : url ,
 					name:jsonData[i].feature.feature_name  
 				}
 				) ; 
 			} 
 		},

 	},
 	mounted()  {

 		this.extractFeatures(window.AppointmentNotify.allFeatures); 

 		Socket.on('patient-to-provider',function(data){
 			Event.$emit('onNotification');
 		});


 	},
 	created(){
 		Event.$on('clear_cookies', eventData => {
 			window.location.href = window.AppointmentNotify.redirectToLogin;
 		});
 	}

 });
 


 $('.chat[data-chat=person2]').addClass('active-chat');
 $('.person[data-chat=person2]').addClass('active');

 $('.left_pane .person').mousedown(function(){

 	if ($(this).hasClass('.active')) {

 		return false;
 	} else {
 		
 		var findChat = $(this).attr('data-chat');
 		var personName = $(this).find('.name').text();
 		$('.right_pane .top .name').html(personName);
 		$('.chat').removeClass('active-chat');
 		$('.left_pane .person').removeClass('active');
 		$(this).addClass('active');
 		$('.chat[data-chat = '+findChat+']').addClass('active-chat');
 	}
 });

 









 
 

