require('./bootstrap');

import router from './routes/routes.healthcareprovider.signup_wizard';

Vue.component('login-form', require('./views/common/login.vue'));
Vue.component('register-form', require('./views/common/Register.vue'));
Vue.component('profile-wizard', require('./views/siteuser/ProfileWizard.vue'));

Vue.component('modal', require('./components/common/Modal.vue'));
Vue.component('dummy-avatar', require('./components/siteuser/DummyAvatar.vue'));

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
    );

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
    );

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
    );


new Vue({

    el: '#app',
    router: router,
    data: {
        showModal: false,
    },
    method:{
        validateLoginCredentials() {
            alert('validateLoginCredentials')
            axios.post('/api/validateusercredentials', {
                email: 'amoghal@gmail.com',
                password: 'bus'
            })
            .then(response => alert('Success'))
            .catch(error => this.errors.record(error.response.data))
        },
        checkSession() {

            axios.get('/api/getSession')
            .then(response => {
                alert('session received..')
            })
            .catch(error => this.errors.record(  error.response.data   ))

        },
        makeToast() {

            let option = { 
                icon : 'check',
                theme: "primary", 
                position: "top-right", 
                duration : 5000,
                action : {
                    text : 'Dismiss',
                    onClick : (e, toastObject) => {

                     toastObject.goAway(0);
                 }
             },
         };
      /*  let myToast = this.$toasted.success("Holla !!");
      myToast.text("Changing the text !!!").goAway(1500);*/


      Vue.toasted.show("hello toast!",option);
            /*var $toastContent = $('<span>I am toast content</span>').add($('<button class="btn-flat toast-action">Undo</button>'));
            Materialize.toast($toastContent, 10000); */  

        },

    }
});
