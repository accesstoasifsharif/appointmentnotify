/* ------------------------------------ Click on login and Sign Up to  changue and view the effect
---------------------------------------
*/

function cambiar_login() {
  document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_login";  
  document.querySelector('.cont_form_login').style.display = "block";
  document.querySelector('.cont_form_sign_up').style.opacity = "0";               

  setTimeout(function(){  document.querySelector('.cont_form_login').style.opacity = "1"; },400);  
  
  setTimeout(function(){    
    document.querySelector('.cont_form_sign_up').style.display = "none";
  },200);  
}

function cambiar_sign_up(at) {
  document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_sign_up";
  document.querySelector('.cont_form_sign_up').style.display = "block";
  document.querySelector('.cont_form_login').style.opacity = "0";
  
  setTimeout(function(){  document.querySelector('.cont_form_sign_up').style.opacity = "1";
},100);  

  setTimeout(function(){   document.querySelector('.cont_form_login').style.display = "none";
},400);  


}    



function ocultar_login_sign_up() {

  document.querySelector('.cont_forms').className = "cont_forms";  
  document.querySelector('.cont_form_sign_up').style.opacity = "0";               
  document.querySelector('.cont_form_login').style.opacity = "0"; 

  setTimeout(function(){
    document.querySelector('.cont_form_sign_up').style.display = "none";
    document.querySelector('.cont_form_login').style.display = "none";
  },500);  
  
}

$('.dropdown-button').dropdown({
  inDuration: 300,
  outDuration: 300,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'right', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    }
    );




// $('.nav-pills').pills();
$(document).ready(function(){
  $('ul.tabs').tabs();
  $('.nav.nav-pills').tabs();
  $('select').material_select();
  $(".button-collapse").sideNav();
  // $("#closeSideNav").click(function(){
  //   $(".button-collapse").sideNav('hide');
  // });
  $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
      });
  $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Clear', // text for clear-button
        canceltext: 'Cancel', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
      });

  $('.materialize_modal').modal();
  $('.tooltipped').tooltip({delay: 40});

  $('.read-more-content').addClass('hide');
  $('.read-more-show, .read-more-hide').removeClass('hide');

  $('.read-more-show').on('click', function(e) {
    $(this).next('.read-more-content').removeClass('hide');
    $(this).addClass('hide');
    e.preventDefault();
  });
  $('.read-more-hide').on('click', function(e) {
    $(this).parent('.read-more-content').addClass('hide');
    var moreid=$(this).attr("more-id");
    $('.read-more-show#'+moreid).removeClass('hide');
    e.preventDefault();
  });
});
