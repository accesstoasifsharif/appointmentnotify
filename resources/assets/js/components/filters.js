Vue.filter('an_date', function(date) {
	return moment(date).format('MMMM D, YYYY');
});

Vue.filter('an_date_day', function(date) {
	return moment(date).format('dddd, MMMM Do YYYY');
});

Vue.filter('an_time', function(time) {
	return moment(time,'HH:mm').format('hh:mma');
});

Vue.filter('reverse', function (value) {
	return value.split('').reverse().join('')
})

Vue.filter('serial', function (value) {

	return parseInt(value)+1;
})

Vue.filter('time_ago', function (date) {
	var temp = moment.utc(date).local();
	return moment(temp).fromNow();
})

Vue.filter('chat_date', function (date) {
	var current = new Date();
	var temp = moment.utc(date).local();
	return moment(temp).calendar(moment(current));
})










