export default {
    minLen: 10,
    wait: 500,
    timeout: null,

    isUpdateItems (text) {
        if (text.length > this.minLen) {
            return true
        }

    },

    callUpdateItems (text, cb) {
        clearTimeout(this.timeout)
        if (this.isUpdateItems(text)) {
            this.timeout = setTimeout(cb, this.wait)
        }

    },

    findItem (items, text, autoSelectOneResult) {
        if (!text) {
            return
        }
        if (autoSelectOneResult && items.length == 1) {
            return items[0]
        }
    },


    updateItemsList(text,items) {
        var matches = [];
        var flag=true;
        for(var i=0; i<items.length; i++) {
            flag=true;
            for(var key in items[i]) {
                if (flag) 
                {
                    var val = items[i][key];     
                    if(typeof val === 'string' || val instanceof String)
                    {
                        if( val.toLowerCase().indexOf(text.toLowerCase()) >= 0){
                            matches.push(items[i]);  
                            flag=false;
                        }
                    }              
                }
            }
        }
        return matches;
    }

}