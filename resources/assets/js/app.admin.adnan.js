require('./bootstrap');

window.Event = new Vue();
Vue.component('coupon',{
	props: ['value'],

	template:
	`
	<input placeholder="Enter your coupan here" :value="value" @input="updateCode($event.target.value)" ref="input"/>
	` ,
	
	methods:{

		updateCode(value){  
		//	alert('input entered' + code);
		this.$emit('input',value);
	} ,
		onCouponApplied(){   // @blur="onCouponApplied"
		//	this.$emit('applied');
		Event.$emit('applied-global');
	} 
} 
})

Vue.component('modal-card',{

	template:
	`
	<div class="modal"  :class="{ 'is-active': isActive }" >
	<div class="modal-background"></div>
	<div class="modal-card">
	<header class="modal-card-head">
	<p class="modal-card-title">
	<slot name="header"></slot>
	</p>
	<button class="delete" @click="closeCardModal"></button>
	</header>
	<section class="modal-card-body">
	<slot name="body"></slot>
	</section>
	<footer class="modal-card-foot">
	<slot name="footer"><button>Okey</button></slot>
	</footer>
	</div>
	</div>

	` ,
	data(){ 
		return { 
			isActive: false

		};
	} ,
	methods:{
		closeCardModal(){
			this.isActive= false;
		}

	} ,
	created(){

		Event.$on('applyModalActive', () =>{ 
			//alert('applied here and received')
			this.isActive = true;
		});
	},
})

Vue.component('modal-bulma',{
	props :['title','body'],
	data(){ 
		return { 

		};
	},
	template:
	`
	<div class="modal is-active">
	<div class="modal-background"></div>
	<div class="modal-content">
	<div class="box">
	{{title}}
	<slot></slot>
	</div>
	
	</div>
	<button class="modal-close is-large" @click="$emit('close')"></button>
	</div> 
	` ,
	methods:{
		hideModal(){ 
			this.isVisible = 'is-active';
		} 
	} 
})

Vue.component('message-bulma',{
	props :['title','body'],
	data(){

		return {

			isVisible: true
		};
	},
	template:
	`

	<article class="message is-primary" v-show="isVisible">
	<div class="message-header">
	{{title}}
	<button class="delete" @click="hideModel"></button>
	</div>
	<div class="message-body">
	{{body}}
	</div>
	</article>

	` ,
	methods:{
		hideModel(){

			this.isVisible = false;
		}

	}

	
})
Vue.component('task-list',{
	template:'<div><task v-for="task in tasks">{{task.description}}</task></div>',
	data() {
		return {
			tasks:[

			{ description: 'got to market', completed:true },
			{ description: 'make custom components', completed:false},
			{ description: 'make nested components', completed:false},
			{ description: 'make form facase', completed:true}
			]

		}; 
	},
	
})

Vue.component('task',{
	template:'<li><slot></slot></li>',
	
})

new Vue({
	el: '#app_adnan',
	data: {
		coupon:'Freebee',
		showModal:false,
		isLoading: false,
		title:'this is the  title',
		newName: '',
		foo: 'bar',
		names:['Joe','Marry','Jane'],
		tasks:[

		{ description: 'got to market', completed:true },
		{ description: 'make custom components', completed:false},
		{ description: 'make nested components', completed:false},
		{ description: 'make form facase', completed:true}
		]
	},
	computed: {

		inCompleteTasks(){
			return this.tasks.filter(task=> !task.completed);

		}

	},
	methods: {

		cardModalClicked(){
			Event.$emit('applyModalActive' )  

		},
		myFunction(code){

			alert('got it here: ' + code);
		},
		onCouponApplied(){
			alert('coupon was applied');

		},
		toggleClass(){
			alert('hello');
			this.isLoading= true;
		},
		addName(){

			this.names.push(this.newName);
			this.newName = '';

		}

	},
	created(){

		Event.$on('applied-global', () => alert('applied globaly'));
	},
	mounted: function () {

	}
});