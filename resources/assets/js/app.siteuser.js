require('./bootstrap');

Vue.component('siteuser-dashboard', require('./views/siteuser/Siteuser_Dashboard.vue'));
Vue.component('providers', require('./components/siteuser/Providers.vue'));
Vue.component('appointments', require('./components/siteuser/Appointments.vue'));

Vue.component('calendar', require('./components/siteuser/Calendar.vue'));
Vue.component('past-appointments', require('./components/siteuser/PastAppointment.vue'));
Vue.component('siteuser-account-setting', require('./components/siteuser/SiteUserAccountSetting.vue'));

Vue.component('cancel-appointments', require('./components/siteuser/CancelAppointments.vue'));
Vue.component('reserve-appointments', require('./components/siteuser/ReserveAppointments.vue'));
Vue.component('short-call-list',require('./components/siteuser/ShortCallList.vue'));
Vue.component('save-appointment-note',require('./components/common/SaveAppointmentNote.vue'));
Vue.component('messages',require('./components/siteuser/Messages.vue'));



Vue.component('help-text',require('./components/common/help_text.vue'));
Vue.component('materialize-modal', require('./components/common/MaterializeModal.vue'));
Vue.component('materialize-modal-header', require('./components/common/MaterializeModalHeader.vue'));

Vue.component('tabular-view', require('./components/common/TabularView.vue'));
Vue.component('table-buttons', require('./components/common/Button.vue'));	
Vue.component('save-short-call-list', require('./components/siteuser/SaveShortCallList.vue'));

Vue.component('table-component',require('./components/common/Table.vue'));
Vue.component('ul-tabs',require('./components/common/Ultabs.vue'));
Vue.component('tab-component',require('./components/common/Tabs.vue'));
Vue.component('special-actions', require('./components/siteuser/SpecialActions.vue'));
Vue.component('time-table', require('./components/common/TimeTable.vue'));
Vue.component('time-picker', require('./components/common/TimePicker.vue'));
Vue.component('circular-view', require('./components/common/CircularView.vue'));
Vue.component('image-view', require('./components/common/Image.vue'));
Vue.component('dummy-avatar', require('./components/siteuser/DummyAvatar.vue'));
Vue.component('reschedule-appointments',require('./components/siteuser/RescheduleAppointments.vue'));
Vue.component('date-picker', require('./components/common/DatePicker.vue'));


Vue.component('header-component', require('./components/common/Header.vue'));
Vue.component('notification-bell', require('./components/common/NotificationBell.vue'));
Vue.component('auto-complete', require('./components/common/AutoComplete/AutoComplete.vue'));
Vue.component('reset-password',require('./components/common/ResetPassword.vue'));
Vue.component('hippa-consent',require('./components/common/HippaConsent.vue'));
Vue.component('emergency-contact',require('./components/siteuser/EmergencyContact.vue'));

Vue.component('appointment-time-line',require('./components/siteuser/AppointmentTimeLine.vue'));



Vue.component('feature-list', require('./components/common/FeatureList.vue') )

var myVue = new Vue({
	el: '#app_siteuser',
	data:{
		linkActiveClass: 'active',
		features :[],
		

	},
	methods:{
		extractFeatures  (data) {
		//	alert(JSON.parse(JSON.stringify(data)  ));
		var jsonData = JSON.parse(JSON.stringify(data)  );
		for (var i = 0; i < jsonData.length; i++) {
			var url = '/'+ window.AppointmentNotify.urlPrefix + '/'+ window.AppointmentNotify.selected_perspective_slug + '/'+jsonData[i]
			this.features.push(
			{ 
				url : url ,
				name:jsonData[i]  
			}
			) ; 
		} 

		

	},
	checkPassport(){

		axios.get('/api/user')
		.then(response => 
		{
			alert('Success')
		} 
		)
		.catch(error =>  this.errors.record( 

			error.response.data
			))

	},
},
mounted()  {

	this.extractFeatures(window.AppointmentNotify.allFeatures); 	
	Socket.on('provider-to-patient',function(data){
		Event.$emit('onNotification');
			// alert("Hello");
		});
},
});






/*$('.left_pane .person').mousedown(function(){
	if ($(this).hasClass('.active')) {

		return false;
	} else {
		alert("hello")
		var findChat = $(this).attr('data-chat');
		var personName = $(this).find('.name').text();
		$('.right_pane .top .name').html(personName);
		$('.chat').removeClass('active-chat');
		$('.left_pane .person').removeClass('active');
		$(this).addClass('active');
		$('.chat[data-chat = '+findChat+']').addClass('active-chat');
	}
});
*/