import Toasted from 'vue-toasted';
import Form from './core/Form';

import MaskedInput from 'vue-text-mask';
import emailMask from 'text-mask-addons/dist/emailMask';
import VueRouter from 'vue-router';
import Title from './components/common/sweetalert/sweetalert_title.html';
// import ClipLoader from 'vue-spinner/src/ClipLoader.vue';





window._ = require('lodash');

window.axios = require('axios');

window.Vue = require('vue');
window.VueCookie = require('vue-cookie');


window.ImagePath = '/storage/';


window.Event = new Vue();
window.Form = Form;

window.moment = require('moment');

window.axios.defaults.headers.common = {
	'X-CSRF-TOKEN': window.AppointmentNotify.csrfToken,
	'X-Requested-With': 'XMLHttpRequest',
};




window.Perspective_Slug = window.AppointmentNotify.selected_perspective_slug; 
window.DomainForSocket =  window.location.hostname + ':3002';//'http://appointmentnotify.dev:3002';
window.Socket = io(DomainForSocket);


window.sweet_alert_title = Title;
window.alertButtonColor = '#00bd86';



Vue.use(Toasted,{ 
	theme: "primary", 
	position: "top-right", 
	duration : 5000
});



require('sweetalert'); 
require('./components/filters.js');

Vue.use(require('vue-full-calendar'));
Vue.component('masked-input', MaskedInput);
Vue.component('context-help', require('./components/common/ContextSensitiveHelp.vue'));


Vue.use(VueCookie);
Vue.use(emailMask);/*not working yet*/ 
Vue.use(VueRouter);



// let token = document.head.querySelector('meta[name="csrf-token"]');

// if (token) {
	// 	window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
	// } else {
		// 	console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
		// }

