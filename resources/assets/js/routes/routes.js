import VueRouter form 'vue-router';

let routes = [
{
	path: '/',
	component: require('./views/siteuser/signup_wizard')
}
];

export default new VueRouter({
	routes
});