import VueRouter from 'vue-router';

var routes = [
{
	path: '/',
	component: require('../views/healthcareprovider/signup_wizard_index')
},
{
	path: '/hospital',
	component: require('../views/healthcareprovider/signup_wizard_hospital')
},
{
	path: '/singlepractice',
	component: require('../views/healthcareprovider/signup_wizard_singlePractice')
},
{
	path: '/medicalpractice',
	component: require('../views/healthcareprovider/signup_wizard_medicalPractice')
},
{
	path: '/department',
	component: require('../views/healthcareprovider/signup_wizard_department')
},


{ path: '*', redirect:  window.AppointmentNotify.redirectUrl }
];

export default new VueRouter({
	routes,
	linkActiveClass: 'active'
});