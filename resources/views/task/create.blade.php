<!DOCTYPE html>
<html>
<head>
	<title>Task</title>

	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
	<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
</head>
<style type="text/css">
	body
	{
		padding-top: 20px;
	}
</style>
<body>
	<div class="container">
		<ol>
			@foreach($tasks as $task)
			<li>{{ $task->title }}</li>
			@endforeach
		</ol>		
	</div>
	<div class="container" id="root">
		<div class="row">
			<form class="col s12" method="POST" action="/test/store" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
				{{csrf_field()}}
				<div class="row">
					<div class="input-field col s6">
						<input id="title" type="text" class="validate" name="title" v-model="form.title">
						<label for="title">title</label>
						<span class="text-success" v-if="form.errors.has('title')" v-text="form.errors.get('title')"></span>
					</div>
					<div class="input-field col s6">
						<input id="description" type="text" class="validate" name="description" v-model="form.description">
						<label for="description">description</label>
						<span class="text-success" v-if="form.errors.has('description')" v-text="form.errors.get('description')"></span>
					</div>
				</div>
				<button type="submit" class="btn waves-effect waves-light" :disabled="form.errors.any()">Save</button>
			</form>
		</div>
	</div>



	<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/task.js')}}"></script>

</body>
</html>








