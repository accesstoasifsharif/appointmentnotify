<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Styles -->
	<!--  Styles For Vue.js Practice -->
	
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">


	<title>Form Practice</title>

	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/siteuser.css')}}">
</head>
<body>
	<div class="container">
		<ol>
			@foreach($projects as $project)

			<li>{{$project->name}}</li>

			@endforeach
		</ol>


		<div class="row" id="projectApp">
			<form class="col s12" action="/test/storeproject" method="post" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
				{{ csrf_field() }}
				<div class="row">
					<div class="input-field col s6">
						<input id="name" name="name" type="text" v-model="form.name" >
						<label for="name">Project Name</label>
						<p v-if="form.errors.has('name')" v-text="form.errors.get('name')"></p>
					</div>
					<div class="input-field col s6">
						<input id="description" name="description" type="text" v-model="form.description" >
						<label for="description">Description</label>
						<p  v-if="form.errors.has('description')" v-text="form.errors.get('description')"></p>
					</div>
				</div>
				
				
				
				<button type="submit" class="btn waves-effect waves-light" :disabled='form.errors.any()'>Submit<i class="material-icons right">send</i></button>
			</form>
		</div>
	</div>
	<div class="container">
		<my-component inline-template>
			<div>
				<p>These are compiled as the component's own template.</p>
				<p>Not parent's transclusion content.</p>
			</div>
		</my-component>
	</div>



	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.2/axios.js"></script>
	<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>

	<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/project.js')}}"></script>
	

</body>
</html>








