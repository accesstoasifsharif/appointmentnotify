@extends('siteuser.layouts.master')

@section('content')
<calendar inline-template>
	<div class="row">
		<div class="col s1"></div>
		<div class="col s10" style="margin-bottom: 2%;margin-top:;">
			<context-help>

				@component('common/partials.help_text')
				@slot('visible_text')
				The Calendar Dashboard is a calendar view of all your appointments: upcoming and past. The Dashboard offers viewing your appointments


				@endslot
				@slot('hidden_text')
				in a given month, week or day of the week. Like the Appointment Dashboard, the Calendar Dashboard let's you manage appointments by allowing you to add new appointments, edit existing appointments and also view all your past appointments with your care Providers.
				@endslot
				@endcomponent

			</context-help>
		</div>
		<div class="col s2">
			<ul class="collection" style="margin-top: 60px;">
				<li class="collection-item avatar">
					<i class="material-icons circle calendar_appointment_btn red" style="background-color: #229954;" @click="revertFunc">done_all</i>
					<p>All<br>
						Appointments
					</p>
				</li>

				<li class="collection-item avatar">
					<i class="material-icons circle calendar_appointment_btn" style="background-color: #229954;" @click="getOnlyUpcomingAppointments">chevron_right</i>
					<p>Upcoming<br>
						Appointments
					</p>
				</li>
				<li class="collection-item avatar">
					<i class="material-icons circle calendar_appointment_btn" style="background-color: #29b6f6;" @click="getOnlyPastAppointments">chevron_right</i>
					<p>Past <br>
						Appointments
					</p>
				</li>

			</ul>
		</div>
		<div class="col s10">
			<full-calendar ref="calendar" :events="events" @event-selected="eventSelected" @event-created="eventCreated" @event-drop="eventDrop" @event-resize="eventResize" @day-click="dayClick"></full-calendar>
		</div>
		<materialize-modal :id="'calendarAppointmentDetail'">
			<template slot="modal-header">
				<materialize-modal-header>
					<template slot="header-image">
						<image-view :target-id="selected.doctor_id" :target-model="selected.doctor_img" :img-class="'circle responsive-img'"></image-view>
					</template>
					<template slot="header-title">
						@{{selected.first_name}}
						@{{selected.middle_name}}
						@{{selected.last_name}}
					</template>
					<template slot="header-speciality-name">
						@{{selected.specialty_name}}
					</template>
				</materialize-modal-header>
			</template>
			<template slot="modal-body">
				<div class="row">
					<div class="offset-s2 col s8 offset-s2">
						<div class="card horizontal">
							<div class="card-stacked">
								<div class="card-action" style="border-bottom: 1px solid rgba(160,160,160,0.2);">
									<h5 class="center" v-if="getAppointmentTitle==0">
										Upcoming Appointment Detail
										<br/>
										<br/>
										<a href="#" class="btn waves-effect waves-light" @click="cancelUpcomingAppointment">Cancel</a>
										<a href="#appointmentRescheduleModal" class="btn waves-effect waves-light" @click="getAppointemtnId(selected.appointment_id,selected.doctor_id,selected.an_event_id)">Reschedule</a>
									</h5>
									<h5 class="center" v-else-if="getAppointmentTitle==1">
										Reserve Appointment Detail
									</h5>
									<h5 class="center" v-else>
										Past Appointment Detail
									</h5>
								</div>
								<div class="card-content doc_info_card_content">
									<div class="row">
										<div class="col s4">
											<label> <b> Facility Name: </b> </label>
										</div>
										<div class="col s8">
											@{{selected.facility_name}}
										</div>
									</div>
									<div class="row">
										<div class="col s4">
											<label> <b> Date: </b> </label>
										</div>
										<div class="col s8">
											@{{selected.date}}
										</div>
									</div>
									<div class="row">
										<div class="col s4">
											<label> <b> Start Time: </b> </label>
										</div>
										<div class="col s8">
											<span>@{{selected.start_time}}</span>
										</div>
									</div>
									<div class="row">
										<div class="col s4">
											<label> <b> End Time: </b> </label>
										</div>
										<div class="col s8">
											<span>@{{selected.end_time}}</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row" v-if="getAppointmentNote==1">
					<div class="offset-s2 col s8 offset-s2">
						<div class="card horizontal">
							<div class="card-stacked">
								<div class="card-action" style="border-bottom: 1px solid rgba(160,160,160,0.2);">
									<h5 class="center">
										Appointment Notes
									</h5>
								</div>
								<div class="card-content doc_info_card_content">
									<div class="row">
										<div class="col s10">
											@{{ description }}
										</div>
										<div class="col s2">
											<a href="#calendar_upcoming_appointments_note" class="btn-floating"><i class="material-icons">edit</i> </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</template>
		</materialize-modal>

		<materialize-modal :id="'calendar_upcoming_appointments_note'" :class="'small_modal'" >
			<template slot="modal-header">
				<materialize-modal-header >
					<template slot="header-title">
						Note
					</template>

				</materialize-modal-header>
			</template>
			<template slot="modal-body">
				<div class="col s12" >
					<div class="row">
						<div class="input-field col s12">
							<textarea id="textarea1" v-model="description" class="materialize-textarea" autofocus="true"></textarea>
							<label for="textarea1">Textarea</label>
						</div>

					</div>
				</div>
			</template>
			<template slot="modal-footer">
				<button class="btn waves-effect waves-light " @click="saveNote(selected.appointment_id)">Save</button>
			</template>
		</materialize-modal>

		<materialize-modal :id="'appointmentRescheduleModal'">
			<template slot="modal-header" style="border: solid;">
				<materialize-modal-header >
					<template slot="header-title">
						Reschedule Modal
					</template>
				</materialize-modal-header>
			</template>
			<template slot="modal-body">
				<div class="col s12">
					<h5 class="center">Write Down Any Two Prefer Time</h5>
				</div>
				<div class="col s12" >
					<div class="row">
						<div class="col s6" style="border-right:  1px solid ">
							<h5 class="center-align" >Prefer Time 1</h5>
							<div class="input-field col s12">
								<input type="text" ref="rDateOne" id="r_date_1" class="datepicker">
								<label for="r_date_1">Select Date 1</label>
							</div>
							<div class="input-field col s12">
								<input type="text" ref="rTimeOne" id="r_time_1" class="timepicker">
								<label for="r_time_1">Select Time 1</label>
							</div>
						</div>
						<div class="col s6">
							<h5 class="center-align">Prefer Time 2</h5>
							<div class="input-field col s12">
								<input type="text" ref="rDateTwo" id="r_date_2" class="datepicker">
								<label for="r_date_2">Select Date 2</label>
							</div>
							<div class="input-field col s12">
								<input type="text" ref="rTimeTwo" id="r_time_2" class="timepicker">
								<label for="r_time_2">Select Time 2</label>
							</div>
						</div>
					</div>
				</div>
			</template>
			<template slot="modal-footer">
				<button class="btn waves-effect waves-light" @click="saveRescheduleTime" v-bind:disabled="!isValidRescheduleTime">Save</button>
			</template>
		</materialize-modal>
	</div>
</calendar>

@endsection

@section('calendar_scrtipts')
<script type="text/javascript" src="/js/moment.js"></script>
<script src='/js/fullcalendar.js'></script>

@endsection


