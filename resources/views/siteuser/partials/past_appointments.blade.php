@component('common.table')



@slot('table_head')
<th>#</th>
<th></th>
<th>Name</th>
<th>Specialty</th>
<th>Appointments</th>
<th>Actions</th>
@endslot
@slot('table_data')


<tr v-for="(p_appointment , index) in past_appointments" >
	<td  >@{{index | serial}}</td>
	<td  > <image-view :target-id="p_appointment.doctor_id" :target-model="p_appointment.doctor_img" :img-class="'circle table_image'"
		></image-view> </td>
		<td  >
			@{{p_appointment.first_name}}
			@{{p_appointment.middle_name}}
			@{{p_appointment.last_name}}
		</td>
		<td v-text="p_appointment.specialty_name" ></td>
		<td v-text="p_appointment.total" ></td>
		<td>
			<a href="#past_appointments_detail_modal" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here for detail" @click="getDetailOfPastAppointmentByDoctorId(p_appointment.doctor_id)">Detail</a>

		</td>
	</tr>
	@endslot

	@endcomponent



	<!--past_appointments_detail_modal modal -->
	<materialize-modal :id="'past_appointments_detail_modal'" :class="'large_modal'" >
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-image">
					<image-view :target-id="doctor_detail_of_past.doctor_id" :target-model="doctor_detail_of_past.doctor_img" :img-class="'circle table_image'"></image-view>
				</template>
				<template slot="header-title">
					@{{doctor_detail_of_past.first_name}}
					@{{doctor_detail_of_past.middle_name}}
					@{{doctor_detail_of_past.last_name}}
				</template>
				<template slot="header-speciality-name">
					@{{doctor_detail_of_past.specialty_name}}
				</template>

			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<div class="col s12 without-pagination" >
				@component('common.table')



				@slot('table_head')
				<th>#</th>
				<th>Facility</th>
				<th>Date</th>
				<th>Time</th>
				<th>Address</th>
				<th>Action</th>
				@endslot
				@slot('table_data')
				<tr v-for="(p_detail_appointment , index) in detailed_appointment_of_past" >
					<td  >@{{index |serial}}</td>
					<td v-text="p_detail_appointment.facility_name" ></td>
					<td >@{{p_detail_appointment.date | an_date}}</td>
					<td  >@{{p_detail_appointment.start_time | an_time}} </td>
					<td>@{{p_detail_appointment.address_line_1}} @{{p_detail_appointment.address_line_2}} @{{p_detail_appointment.city_name}}</td>
					<td>
						<a href="#past_appointments_note" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here to add note." @click="getNoteByAppointmentId(p_detail_appointment.appointment_id)" >Notes</a>


					</td>
				</tr>
				@endslot
				@endcomponent

			</div>



		</template>
		<template slot="modal-footer">
		</template>
	</materialize-modal>







	<!-- up past_appointments_note modal -->
	<materialize-modal :id="'past_appointments_note'" :class="'small_modal'" >
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-title">
					Notes
				</template>

			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<div class="col s12" >
				<div class="row">
					<div class="input-field col s12">
						<textarea id="textarea1" v-model="description" class="materialize-textarea" autofocus="true"></textarea>
						<label for="textarea1">Textarea</label>
					</div>
				</div>
			</div>
		</template>
		<template slot="modal-footer">
			<button class="btn waves-effect waves-light " @click="saveNote(appointment_id)">Save</button>
		</template>
	</materialize-modal>