<form action="" method="get" @submit.prevent="updateContactInfo" @keydown="contactInfoForm.errors.clear($event.target.name)">
    <div class="row">
        <div class="input-field col s6">
            <i class="material-icons prefix">stay_primary_portrait</i>
            <label for="user_mobile">Mobile
                <span class="help" v-if="contactInfoForm.errors.has('phone_number')">
                    (@{{contactInfoForm.errors.get('phone_number')}})
                </span>
            </label>
            <!--  <input id="user_mobile" type="text" class="validate" name="phone_number" v-model = 'contactInfoForm.phone_number' required="" aria-required="true"> -->
            <masked-input
            id="user_mobile"
            type="tel"
            name="phone_number"
            class="validate"
            v-model="contactInfoForm.phone_number"
            :mask="['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]"
            :guide="false"
            placeholderChar="#">
        </masked-input>
    </div>
    <div class="input-field col s6">
        <i class="material-icons prefix">stay_primary_portrait</i>
        <input
        id="icon_email"
        type="text"
        class="validate"
        name="email"

        >

        <label for="icon_email">Home Phone
            <span class="help" >

            </span>
        </label>
    </div>

</div>
<div class="row">
  <div class="input-field col s6">
    <i class="material-icons prefix">phone</i>
    <!--  <input id="icon_telephone" type="tel" class="validate" name="fax_number" v-model = 'contactInfoForm.fax_number'> -->
    <masked-input
    id="icon_telephone"
    type="tel"
    name="fax_number"
    class="validate"
    v-model="contactInfoForm.fax_number"
    :mask="['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]"
    :guide="true"
    placeholderChar="#">
</masked-input>
<label for="icon_telephone">Fax
    <span class="help" v-if="contactInfoForm.errors.has('fax_number')">
        (@{{contactInfoForm.errors.get('fax_number')}})
    </span>
</label>
</div>
<div class="input-field col s6">
    <i class="material-icons prefix">email</i>
    <input
    id="icon_email"
    type="text"
    class="validate"
    name="email"
    v-model = 'contactInfoForm.email'
    >

    <label for="icon_email">Email
        <span class="help" v-if="contactInfoForm.errors.has('email')">
            (@{{contactInfoForm.errors.get('email')}})
        </span>
    </label>
</div>
</div>
<div class="col s12">
    <button type="submit" class="btn right waves-effect waves-light">Update</button>
</div>
</form>
