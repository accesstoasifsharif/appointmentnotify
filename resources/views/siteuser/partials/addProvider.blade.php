<div class="row">
	<div class="col s4">
		<div class="input-field">
			<input id="search_field" type="search" class="validate" v-model="searchDoctor" @keyup="getDoctorName">
			<label for="search_field">Search</label>
		</div>
	</div>
	<div class="col s4">
		<div class="input-field">
			<input id="zipcode" type="search" class="validate" v-model="zipcode" @keyup="getZipCode">
			<label for="zipcode">Zipcode</label>
		</div>
	</div>
	<div class="col s4">
		<div class="input-field">
			<select class="browser-default" v-model="distance" style="color: #000;" @change="getDistance">
				<option v-for="mile in miles" :value="mile.value" style="color: #000;">@{{mile.text}}</option>
			</select>
		</div>
	</div>
</div>





@component('common.table')

@slot('table_head')
<th>Id</th>
<th>Avatar</th>
<th>Name</th>
<th>Speciality Name</th>
<th>Actions</th>
@endslot
@slot('table_data')
@{{finalProviderlist}}
<!-- @{{siteUserNearestDoctors}} -->
<tr v-for="(doctor, index) in siteUserNearestDoctors">
	<td style="color:#000;">@{{index|serial}}</td>
	<td style="color:#000;">
		<image-view :target-id="doctor.unique_doctor_id" :target-model="doctor.doctor_identifier" :img-class="'circle table_image'"></image-view>
	</td>
	<td v-text="doctor.doctor_name" style="color:#000;"></td>
	<td v-text="doctor.specialty_name" style="color:#000;"></td>
	<td>
		<button class="btn-floating waves-effect waves-light" @click="addProvider(doctor.unique_doctor_id,index)" id="addProviderBtn" :disabled="isDisabled" v-if="finalProviderlist.status==true">
			add
		</button>
	</td>
</tr>
@endslot
@slot('table_pagination')
<ul class="pagination">
	<li v-if="add_provider_pagination.current_page > 1">
		<a href="#" aria-label="Previous" @click.prevent="addProviderTablePagiantion(add_provider_pagination.current_page - 1)">
			<span aria-hidden="true">&laquo;</span>
		</a>
	</li>
	<li v-for="page in addProviderpagesNumber" v-bind:class="[ page == addProviderisActived ? 'active' : '']">
		<a href="#" @click.prevent="addProviderTablePagiantion(page)">@{{ page }}</a>
	</li>
	<li v-if="add_provider_pagination.current_page < add_provider_pagination.last_page">
		<a href="#" aria-label="Next" @click.prevent="addProviderTablePagiantion(add_provider_pagination.current_page + 1)">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>
@endslot
@endcomponent
