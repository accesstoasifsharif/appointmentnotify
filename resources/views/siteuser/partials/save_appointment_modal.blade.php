<materialize-modal :id="'s_save_appointment_modal'" :class="'modal_size'" >
	<template slot="modal-header">
		<materialize-modal-header >
			<template slot="header-title" >
				Save Appointment
			</template>

		</materialize-modal-header>
	</template>
	<template slot="modal-body">
		<div class="row">
			<div class="row">
				<form class="col s12" method="POST" action="">

					<div class="input-field col s12 m12">
						<select  class="browser-default" v-model="selected"  @change="onTableLength" >
							<option value=""  selected>Choose Doctor</option>
							<option v-for="(doctor, index) in doctors" :value="doctor.doctor_id">@{{ doctor.first_name }}</option>
						</select>

					</div>
					<div class="input-field col s12 m12">
						<select class="browser-default" v-model="selected_facility" @change="onFacilityChange" >
							<option value="" >Choose Facility</option>
							<option  v-for="(facility, index) in facilities" :value="facility.id" >@{{ facility.facility_name }}</option>
						</select>

					</div>
					<div class="input-field col s12 m12">
						<select class="browser-default" v-model="selected_reason" >
							<option value=""  selected>Choose Reason</option>
							<option  v-for="(reason, index) in reasons" :value="reason.id">@{{ reason.reason_name }}</option>

						</select>

					</div>
					<div class="input-field col s12">
						<input id="appointment_date" type="text" class="datepicker"  ref="myTestField">
						<label for="appointment_date" data-error="wrong" data-success="right">Appointment Date</label>

					</div>
					<div class="input-field col s12">
						<time-picker :id=" '_day_time_picker_from'" :text="'From'" @timepicked="setTimeFieldData" :model= "time_from" ></time-picker>
					</div>
					<div class="input-field col s12">
						<time-picker :id=" '_day_time_picker_to'" :text="'To'" @timepicked="setTimeFieldData" :model= "time_to" ></time-picker>

					</div>








				</form>
			</div>
		</div>
	</template>
	<template slot="modal-footer">

		<button type="button" class="btn waves-effect waves-light" @click="saveAppointment" v-bind:disabled="!isValidAppointment">Save</button>
	</template>
</materialize-modal>