<div class="offset-s1 col s10 offset-s1">
	<div class="card horizontal">
		<div class="card-image">
			<image-view :target-id="singleDoctorInfoById.doctor_id" :target-model="singleDoctorInfoById.model_identifier" :img-class="'circle modal_card_img'"></image-view>
		</div>
		<div class="card-stacked">
			<div class="card-action" style="border-bottom: 1px solid rgba(160,160,160,0.2);">
				<h5>
					@{{singleDoctorInfoById.first_name}}
					@{{singleDoctorInfoById.middle_name}}
					@{{singleDoctorInfoById.last_name}}
				</h5>
			</div>
			<div class="card-content doc_info_card_content">
				<div class="row">
					<div class="col s4">
						<label> <b> Speciality Name: </b> </label>
					</div>
					<div class="col s8">
						@{{singleDoctorInfoById.specialty_name}}
					</div>
				</div>
				<div class="row">
					<div class="col s4">
						<label> <b> Gender: </b> </label>
					</div>
					<div class="col s8">
						@{{singleDoctorInfoById.gender}}
					</div>
				</div>
				<div class="row">
					<div class="col s4">
						<label> <b> Phone #: </b> </label>
					</div>
					<div class="col s8">
						@{{singleDoctorInfoById.phone_number}}
					</div>
				</div>
				<div class="row">
					<div class="col s4">
						<label> <b> Fax #: </b> </label>
					</div>
					<div class="col s8">
						@{{singleDoctorInfoById.fax_number}}
					</div>
				</div>
				<div class="row">
					<div class="col s4">
						<label> <b> E-mail: </b> </label>
					</div>
					<div class="col s8">
						@{{singleDoctorInfoById.email}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- @{{singleDoctorInfoById}} -->



