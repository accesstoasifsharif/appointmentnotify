<div>
	@component('common.table')



	@slot('table_head')
	<th>#</th>
	<th></th>
	<th>Name</th>
	<th>Speciality</th>
	<th>Appointments</th>
	<th>Action</th>
	@endslot
	@slot('table_data')


	<tr v-for="(reschedule_appointment , index) in aggregated_reschedule_appointments" >
		<td  >@{{index | serial}}</td>
		<td  >
			<image-view :target-id="reschedule_appointment.doctor_id" :target-model="reschedule_appointment.doctor_img" :img-class="'circle table_image'"
			></image-view>
		</td>
		<td  >
			@{{reschedule_appointment.first_name}}
			@{{reschedule_appointment.middle_name}}
			@{{reschedule_appointment.last_name}}
		</td>
		<td v-text="reschedule_appointment.specialty_name" ></td>
		<td v-text="reschedule_appointment.total" ></td>
		<td>
			<a href="#reschedule_appointments_detail" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here for detail." @click="getRescheduledAppointmentDetailByDoctorId(reschedule_appointment.doctor_id)">Detail</a>

		</td>
	</tr>
	@endslot


	@endcomponent
	<!-- reschedule_appointments_detailmodal -->
	<materialize-modal :id="'reschedule_appointments_detail'" :class="'small_modal'" >
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-image">
					<image-view :target-id="detailed_reschedule_appointment_doctor_info.doctor_id" :target-model="detailed_reschedule_appointment_doctor_info.doctor_img" :img-class="'circle table_image'"></image-view>
				</template>
				<template slot="header-title">
					@{{detailed_reschedule_appointment_doctor_info.first_name}}
					@{{detailed_reschedule_appointment_doctor_info.middle_name}}
					@{{detailed_reschedule_appointment_doctor_info.last_name}}
				</template>
				<template slot="header-speciality-name">
					@{{detailed_reschedule_appointment_doctor_info.specialty_name}}
				</template>

			</materialize-modal-header>
		</template>
		<template slot="modal-body">

			<div class="col s12 without-pagination" >
				<div class="col s12  m6 l4" v-for="(reschedule_appointment_detail , index) in detailed_reschedule_appointment">
					<div class="card ">
						<div class="card-content ">
							<span class="card-title center-align">@{{reschedule_appointment_detail.facility_name}}
							</span>
							<p class="col s12">
								<p class="center-align">Prefered Data And Time</p>
								<p class="center-align"> <b>Date : 1 </b> @{{reschedule_appointment_detail.prefer_date_1 | an_date}}</p>


								<p class="center-align"><b>Time : 1 </b> @{{reschedule_appointment_detail.prefer_time_1 | an_time}}</p>



								<hr>
								<p class="center-align"> <b>Date 2 : </b> @{{reschedule_appointment_detail.prefer_date_2 | an_date }}</p>


								<p class="center-align"><b>Time : 2 </b> @{{reschedule_appointment_detail.prefer_time_2 | an_time}}</p>

							</p>
						</div>
					</div>


				</div>
			</div>
		</template>
		<template slot="modal-footer">

		</template>
	</materialize-modal>

</div>