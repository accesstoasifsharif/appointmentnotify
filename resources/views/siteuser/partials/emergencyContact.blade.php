<div class="col l4 m4 s12">
	<div class="input-field col s12">
		<input type="text" id="full_name" v-model="emergencyContactForm.full_name">
		<label for="full_name">Full Name</label>
	</div>
	<div class="input-field col s12">
		<input type="text" id="phone_number" v-model="emergencyContactForm.phone_number">
		<label for="phone_number">Phone Number</label>
	</div>
	<div class="input-field col s12">
		<input type="text" id="email" v-model="emergencyContactForm.email">
		<label for="email">E-mail</label>
	</div>
	<div class="input-field col s12">
		<input type="text" id="relation" v-model="emergencyContactForm.relation">
		<label for="relation">Relation</label>
	</div>
	<div class="col s12">
		<button class="btn waves-effect waves-light" @click= "addOrUpDateEmergencyContactData"> Save</button>
	</div>
</div>