 @component('common.table')
 @slot('table_head')
 <th>#</th>
 <th></th>
 <th>Name</th>
 <th>Specialty</th>
 <th>Appointments</th>
 <th>Actions</th>
 @endslot
 @slot('table_data')
 <tr v-for="(reserve_appointment , index) in aggregated_reserve_appointments" >
 	<td  >@{{index| serial}}</td>
 	<td  > <image-view :target-id="reserve_appointment.doctor_id" :target-model="reserve_appointment.doctor_img" :img-class="'circle table_image'"
 		></image-view> </td>
 		<td  >
 			@{{reserve_appointment.first_name}}
 			@{{reserve_appointment.middle_name}}
 			@{{reserve_appointment.last_name}}
 		</td>
 		<td v-text="reserve_appointment.specialty_name" ></td>
 		<td v-text="reserve_appointment.total" ></td>
 		<td>
 			<a href="#reserved_appointments_detail_modal" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here for detail." @click="getAllReservedAppointmentsDetailByDoctorId(reserve_appointment.doctor_id)">Detail</a>

 		</td>
 	</tr>
 	@endslot

 	@endcomponent



 	<!-- reserved appointments detail modal -->
 	<materialize-modal :id="'reserved_appointments_detail_modal'" :class="'large_modal'" >
 		<template slot="modal-header">
 			<materialize-modal-header >
 				<template slot="header-image">
 					<image-view :target-id="reserved_doctor_info.doctor_id" :target-model="reserved_doctor_info.doctor_img" :img-class="'circle table_image'"></image-view>
 				</template>
 				<template slot="header-title">
 					@{{reserved_doctor_info.first_name}}
 					@{{reserved_doctor_info.middle_name}}
 					@{{reserved_doctor_info.last_name}}
 				</template>
 				<template slot="header-speciality-name">
 					@{{reserved_doctor_info.specialty_name}}
 				</template>

 			</materialize-modal-header>
 		</template>
 		<template slot="modal-body">
 			<div class="col s12 without-pagination" >
 				@component('common.table')



 				@slot('table_head')
 				<th>#</th>
 				<th>Facility</th>
 				<th>Date</th>
 				<th>Time</th>
 				<th>Address</th>

 				@endslot
 				@slot('table_data')
 				<tr v-for="(detail_appointment , index) in reserved_appointments_detail" >
 					<td  >@{{index| serial}}</td>
 					<td v-text="detail_appointment.facility_name" ></td>
 					<td>@{{detail_appointment.date| an_date}}</td>
 					<td>@{{detail_appointment.start_time | an_time}}</td>
 					<td>@{{detail_appointment.address_line_1}} @{{detail_appointment.address_line_2}} @{{detail_appointment.city_name}}

 					</td>


 				</tr>
 				@endslot

 				@endcomponent

 			</div>
 		</template>
 		<template slot="modal-footer">
 		</template>
 	</materialize-modal>
