<form action="" method="get" @submit.prevent="updatePersonalInfo" @keydown="personalInfoForm.errors.clear($event.target.name)">
    <div class="row">
        <div class="col s6">
            <div class="input-field col s12">
                <i class="material-icons prefix">face</i>
                <input id="icon_prefix" id="icon_perfix1" name="first_name" type="text" class="validate" v-model='personalInfoForm.first_name'>
                <label for="icon_perfix1">First Name
                    <span class="help" v-if="personalInfoForm.errors.has('first_name')">(@{{personalInfoForm.errors.get('first_name')}})</span>
                </label>
            </div>
            <div class="input-field col s12">
                <i class="material-icons prefix">perm_identity</i>
                <input id="icon_prefix2" type="text" class="validate" name="middle_name" v-model = 'personalInfoForm.middle_name'>
                <label for="icon_prefix2">Middle Name
                    <span class="help" v-if="personalInfoForm.errors.has('middle_name')">(@{{personalInfoForm.errors.get('middle_name')}})</span>
                </label>
            </div>
            <div class="input-field col s12">
                <i class="material-icons prefix">account_circle</i>
                <input id="icon_prefix3" type="text" class="validate" name="last_name" v-model = 'personalInfoForm.last_name'>
                <label for="icon_prefix3">Last Name
                    <span class="help" v-if="personalInfoForm.errors.has('last_name')">(@{{personalInfoForm.errors.get('last_name')}})</span>
                </label>
            </div>
        </div>
        <div class="col s6">
            <div class="input-field col s12">
                <i class="material-icons prefix">assignment_ind</i>
                <input id="icon_telephone" type="tel" class="validate" name="psuedo_name" v-model = 'personalInfoForm.psuedo_name'>
                <label for="icon_telephone">User Name
                    <span class="help" v-if="personalInfoForm.errors.has('psuedo_name')">(@{{personalInfoForm.errors.get('psuedo_name')}})</span>
                </label>
            </div>
            <div class="input-field col s12">
                <i class="material-icons prefix">today</i>
                <input type="text" id="icon_telephone2" class="datepicker" name="date_of_birth" ref="date_of_birth" v-model="personalInfoForm.date_of_birth">
                <label for="icon_telephone2">Date of Birth
                    <span class="help" v-if="personalInfoForm.errors.has('date_of_birth')" >(@{{personalInfoForm.errors.get('date_of_birth')}})</span>
                </label>
            </div>
            <div class="input-field col s12">
                <label class="left" >Gender</label>
                <div class="  right" style="padding-right: 8%;height: auto;">
                    <p class="left">
                     <input class="with-gap right" type="radio" id="test5" v-model="personalInfoForm.gender" name="gender" :value="'male'">
                     <label for="test5">Male</label>
                 </p>
                 <p class="left">
                    <input class="with-gap right" type="radio" id="test8" v-model="personalInfoForm.gender" name="gender" :value="'female'">
                    <label for="test8">Female</label>
                </p>
                <p class="left">
                    <input class="with-gap right" type="radio" id="test10" v-model="personalInfoForm.gender" name="gender" :value="'other'">
                    <label for="test10">Other</label>
                </p>
            </div>
            <span class="help" v-if="personalInfoForm.errors.has('gender')" v-text="personalInfoForm.errors.get('gender')"></span>
        </div>
    </div>
</div>
<div class="col s12">
    <button type="submit" class="btn right waves-effect waves-light"  >Update</button>
</div>
</form>