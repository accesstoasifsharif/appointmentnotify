<emergency-contact inline-template>
	<div class="row center-align">
		<h4 class=" center-align" style="border-bottom: 1px solid rgba(160,160,160,0.2); padding-bottom: 5px;">
			Emergency Contact
		</h4>
		@include('siteuser.partials.emergencyContact')
		<!-- @include('siteuser.partials.emergencyContact')
		@include('siteuser.partials.emergencyContact') -->
	</div>
</emergency-contact>