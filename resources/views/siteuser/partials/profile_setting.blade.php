<!-- <div class="row">
  <form class="col s12">
    <div class="row">
      <div class="input-field col s12">
        <input id="email2" type="email" class="validate" required="" aria-required="true">
        <label for="email2">Email</label>
      </div>
      <div class="input-field col s12">
        <input id="example" name="example" type="text" class="validate" required="" aria-required="true">
        <label for="example">Field</label>
      </div>
      <div class="input-field col s12">
        <select required="" aria-required="true">
<option value="" disabled selected>Choose your option</option>
<option value="1">Option 1</option>
<option value="2">Option 2</option>
<option value="3">Option 3</option>
</select>
        <label>Materialize Select</label>
      </div>
      <div class="input-field col s12">
        <button class="btn waves-effect waves-light" type="submit" name="action">Submit</button>
      </div>
    </div>
  </form>
</div> -->
<div class="row">
	<div class="col s12 m12 l12 xl12">
<!-- 		<div class="row">
			<div class="col s12 m12 l12 xl12">
				<div class="card-panel blue lighten-1">
					<span class="white-text">
						Your Profile section contains basic information about you including your Platform account nickname (username), gender, date of birth, address and contact information.
					</span>
		        </div>
		    </div>
		</div> -->
		@component('common/partials.help_text')
		@slot('visible_text')
		Your Profile section contains basic information about you including your Platform account nickname
		@endslot
		@slot('hidden_text')
		(username), gender, date of birth, address and contact information.
		@endslot
		@endcomponent
	</div>
	<div class="col s12 appointments_tab">
		<ul-tabs style="background-color: transparent;">
			<tab-component :href="'#personal_information'" style="margin-left: 11%;">
				Personal Info
			</tab-component>
			<tab-component :href="'#contact_information'">
				Contact
			</tab-component>
			<tab-component :href="'#address_information'">
				Address
			</tab-component>
		</ul-tabs>
	</div>

	<div class="col s12" id="personal_information" style="padding-top: 15px;">
		@include('siteuser.partials.personal_information')
	</div>
	<div class="col s12" id="contact_information" style="padding-top: 15px;">
		@include('siteuser.partials.contact_information')
	</div>
	<div class="col s12" id="address_information" style="padding-top: 15px;">
		@include('siteuser.partials.address_information')
	</div>
		<!-- <div class="col s12">
			<button type="submit" class="btn right waves-effect waves-light" onclick="Materialize.toast('Your Information saved successfully', 4000)" @click.prevent="updateSiteUserInfo(siteUserInfo.site_user_id)">Save</button>
		</div> -->

	</div>
