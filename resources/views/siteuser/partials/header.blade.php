
<nav class="nav-extended">
	<div class="nav-wrapper">
		<div class="row nav_row">
			<div class="col s3">
				@include('common.partials.logo')
			</div>
			<div class="col s9 offset-s3">
				@include('siteuser.partials.nav')
			</div>
			<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
		</div>
		@include('common.partials.headertabsmobileview')
	</div>
</nav>
