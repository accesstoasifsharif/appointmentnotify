@component('common.table')
	@slot('table_head')
		<th>Name</th>
		<th>Address</th>
		<th>Phone #</th>
		<th>Email</th>
		<th>Zip Code</th>
		<th>Time</th>
	@endslot
@slot('table_data')
<tr v-for="facility in singleDoctorFacilityInfoById">
	<td v-text="facility.facility_name" style="color:#000;"></td>
	<td style="color:#000;">@{{facility.address_line_1}} @{{facility.address_line_2}} @{{facility.city_name}}</td>
	<td v-text="facility.phone_number" style="color:#000;"></td>
	<td v-text="facility.email" style="color:#000;"></td>
	<td v-text="facility.zip_code" style="color:#000;"></td>
	<td>
		<i class="tooltipped material-icons" data-position="left" data-delay="10" data-tooltip="click here to add provider" @click="getFacilityTimeTable(facility.health_care_facility_id)" >access_time</i>
	</td>
</tr>
@endslot
@endcomponent





