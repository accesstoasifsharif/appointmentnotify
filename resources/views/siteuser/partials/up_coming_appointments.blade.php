@component('common.table')

@slot('table_head')
<th>#</th>
<th></th>
<th>Name</th>
<th>Specialty</th>
<th>Appointments</th>
<th>Actions</th>
@endslot
@slot('table_data')
<tr v-for="(u_appointment , index) in upcoming_appointments" >
	<td  >@{{index  |serial}}</td>
	<td  > <image-view :target-id="u_appointment.doctor_id" :target-model="u_appointment.doctor_img" :img-class="'circle table_image'"
		></image-view> </td>
		<td  class="truncate">

			@{{u_appointment.first_name}}
			@{{u_appointment.middle_name}}
			@{{u_appointment.last_name}}

		</td>
		<td v-text="u_appointment.specialty_name" ></td>
		<td v-text="u_appointment.total" ></td>
		<td>
			<a href="#upcoming_appointments_detail_modal" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here for detail." @click="getDetailOfAppointmentByDoctorId(u_appointment.doctor_id)">Detail</a>

		</td>
	</tr>
	@endslot

	@endcomponent



	<!-- up coming appointments detail modal -->
	<materialize-modal :id="'upcoming_appointments_detail_modal'" :class="'large_modal'" >
		<template slot="modal-header">
			<materialize-modal-header >

				<template slot="header-image">
					<image-view :target-id="doctor_detail.doctor_id" :target-model="doctor_detail.doctor_img" :img-class="'circle '"></image-view>
				</template>
				<template slot="header-title">
					@{{doctor_detail.first_name}}
					@{{doctor_detail.middle_name}}
					@{{doctor_detail.last_name}}
				</template>
				<template slot="header-speciality-name">
					@{{doctor_detail.specialty_name}}
				</template>

			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<div class="col s12 without-pagination" >
				@component('common.table')



				@slot('table_head')
				<th>#</th>
				<th>Facility</th>
				<th>Date</th>
				<th>Time</th>
				<th>Address</th>
				<th>Action</th>
				@endslot
				@slot('table_data')

				<tr v-for="(detail_appointment , index) in detailed_appointment" >
					<td >@{{index | serial }}</td>
					<td >@{{detail_appointment.facility_name}}</td>
					<td  >@{{detail_appointment.date | an_date }}</td>
					<td  >@{{detail_appointment.start_time | an_time}} </td>
					<td>@{{detail_appointment.address_line_1}} @{{detail_appointment.address_line_2}} @{{detail_appointment.city_name}}

					</td>

					<td>

						<a href="#" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here to cancel appointment." @click="cancelUpComingAppointemnt(detail_appointment.appointment_id)" >Cancel</a>
						<a href="#upcoming_appointments_reschedule" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here to reschedule appointment" @click="getAppointemtnId(detail_appointment.appointment_id,detail_appointment.doctor_id,detail_appointment.an_event_id)">Reschedule</a>
						<a href="#upcoming_appointments_note" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here to add note." @click="getNoteByAppointmentId(detail_appointment.appointment_id)" >Notes</a>

					</td>
				</tr>
				@endslot

				@endcomponent

			</div>

		</template>
		<template slot="modal-footer">
		</template>
	</materialize-modal>












	<!-- upcoming_appointments_note -->

	<!-- up upcoming_appointments_note modal -->
	<materialize-modal :id="'upcoming_appointments_note'" :class="'small_modal'" >
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-title">
					Notes
				</template>

			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<div class="col s12" >
				<div class="row">
					<div class="input-field col s12">
						<textarea id="textarea1" v-model="description" class="materialize-textarea" autofocus="true"></textarea>
						<label for="textarea1">Textarea</label>
					</div>

				</div>
			</div>
		</template>
		<template slot="modal-footer">
			<button class="btn waves-effect waves-light " @click="saveNote(appointment_id)">Save</button>
		</template>
	</materialize-modal>




	<!-- upcoming_appointments_reschedule -->

	<!-- up upcoming_appointments_reschedule modal -->
	<materialize-modal :id="'upcoming_appointments_reschedule'" :class="'small_modal'" >
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-title">
					Reschedule
				</template>

			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<div class="col s12" >
				<div class="row">

					<div class="col s6" style="border-right:  1px solid ">

						<h5 class="center-align" >Prefer Time 1</h5>
						<date-picker :id="'upcoming_appointment_reschedule_date_1'" :text="'Date_1'"  :model= "save_reschedule_time.date_1" @datepicked="onRescheduleDatePick"></date-picker>
						<div class="input-field col s12">
							<time-picker :id=" '_day_time_picker_from_reschedule_1'" :text="'From'" @timepicked="setTimeFieldData" :model= "reschedule_time_1" ></time-picker>
						</div>
						<!-- <div class="input-field col s12">
							<input type="text" ref="rTimeOne" id="r_time_1" class="timepicker">
							<label for="r_time_1">Select Time 1</label>
						</div> -->

					</div>
					<div class="col s6">
						<h5 class="center-align">Prefer Time 2</h5>
						<div class="input-field col s12">
							<date-picker :id="'upcoming_appointment_reschedule_date_2'" :text="'Date'"  :model= "save_reschedule_time.date_2" @datepicked="onRescheduleDatePick"></date-picker>
						</div>
						<!-- <div class="input-field col s12">
							<input type="text" ref="rDateTwo" id="r_date_2" class="datepicker">
							<label for="r_date_2">Select Date 2</label>
						</div> -->
						<div class="input-field col s12">
							<time-picker :id=" '_day_time_picker_from_reschedule_2'" :text="'To'" @timepicked="setTimeFieldData" :model= "reschedule_time_2" ></time-picker>
						</div>
					</div>
				</div>
			</div>

		</template>
		<template slot="modal-footer">
			<button class="btn waves-effect waves-light" v-bind:disabled="!isValidRescheduleTime" @click="saveRescheduleTime" >Save</button>
		</template>
	</materialize-modal>





