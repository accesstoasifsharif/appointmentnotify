<materialize-modal :id="'short_call_list_modal'" :class="'modal_size'" >
	<template slot="modal-header">
		<materialize-modal-header >
			<template slot="header-title" >
				Short Call List
			</template>

		</materialize-modal-header>
	</template>
	<template slot="modal-body">
		<div class="row">
			<form class="col s12" method="POST" action="">
				<div class="input-field col s12 m12">
					<select  class="browser-default" v-model="selected"  @change="onTableLength" >
						<option value=""  selected>Choose Doctor</option>
						<option v-for="(doctor, index) in doctors" :value="doctor.doctor_id">@{{ doctor.first_name }}</option>
					</select>

				</div>
				<div class="input-field col s12 m12">
					<select class="browser-default" v-model="selected_facility" @change="onFacilityChange" required="">
						<option value="" >Choose Facility</option>
						<option  v-for="(facility, index) in facilities" :value="facility.id" >@{{ facility.facility_name }}</option>
					</select>

				</div>
				<div class="input-field col s12 m12">
					<select class="browser-default" v-model="shorTime" @change="timeChange" >
						<option value="" disabled selected>Choose Duration</option>
						<option v-for="(time, index) in shortcalltime" :value="time" >@{{time}}</option>
					</select>

				</div>

			</form>
		</div>
	</template>
	<template slot="modal-footer">

		<button type="button" class="btn waves-effect waves-light"
		v-on:click="SaveShortCall" v-bind:disabled="!isValidShortCall">Save</button>
	</template>
</materialize-modal>

