
@component('common.table')



@slot('table_head')
<th>#</th>
<th></th>
<th>Name</th>
<th>Specialty</th>
<th>Appointments</th>
<th>Actions</th>
@endslot
@slot('table_data')
<tr v-for="(cancelled_appointment , index) in aggregated_cancelled_appointmetns" >
	<td >@{{index | serial}}</td>
	<td  > <image-view :target-id="cancelled_appointment.doctor_id" :target-model="cancelled_appointment.doctor_img" :img-class="'circle  table_image'"
		></image-view> </td>
		<td  >
			@{{cancelled_appointment.first_name}}
			@{{cancelled_appointment.middle_name}}
			@{{cancelled_appointment.last_name}}
		</td>
		<td v-text="cancelled_appointment.specialty_name" ></td>
		<td v-text="cancelled_appointment.total" ></td>
		<td>
			<a href="#cancelled_appointments_detail_modal" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here for detail." @click="getAllCancelledAppointmentsDetailByDoctorId(cancelled_appointment.doctor_id)">Detail</a>

		</td>
	</tr>
	@endslot

	@endcomponent




	<!-- up cancelled appointments detail modal -->
	<materialize-modal :id="'cancelled_appointments_detail_modal'" :class="'large_modal'" >
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-image">
					<image-view :target-id="cancelled_appointment_doctor_info.doctor_id" :target-model="cancelled_appointment_doctor_info.doctor_img" :img-class="'circle table_image'"></image-view>
				</template>
				<template slot="header-title">
					@{{cancelled_appointment_doctor_info.first_name}}
					@{{cancelled_appointment_doctor_info.middle_name}}
					@{{cancelled_appointment_doctor_info.last_name}}
				</template>
				<template slot="header-speciality-name">
					@{{cancelled_appointment_doctor_info.specialty_name}}
				</template>

			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<div class="col s12 without-pagination" >
				@component('common.table')



				@slot('table_head')
				<th>#</th>
				<th>Facility</th>
				<th>Date</th>
				<th>Time</th>
				<th>Address</th>
				<th>Action</th>
				@endslot
				@slot('table_data')
				<tr v-for="(detail_appointment , index) in cancelled_appointment_detail" >
					<td >@{{index | serial}}</td>
					<td v-text="detail_appointment.facility_name" ></td>
					<td> @{{detail_appointment.date | an_date}}</td>
					<td >@{{detail_appointment.start_time | an_time}} </td>
					<td>@{{detail_appointment.address_line_1}} @{{detail_appointment.address_line_2}} @{{detail_appointment.city_name}}

					</td>

					<td>

						<a href="#" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here to revoke appointment." @click="revokeCancelledAppointment(detail_appointment.appointment_id)" >Revoke</a>


					</td>
				</tr>
				@endslot

				@endcomponent

			</div>


		</template>
		<template slot="modal-footer">
		</template>
	</materialize-modal>


