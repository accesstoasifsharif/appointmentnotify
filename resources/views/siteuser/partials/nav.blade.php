<ul class="hide-on-med-and-down">
	<li class="col s7" >


		<!-- <ul>
			<li class="col s12">
				@include('siteuser.partials.siteuser_search')
			</li>
		</ul> -->

	</li>
	@if(Auth::guest())
	<li class="right">
		<a class="waves-effect waves-light btn" href="{{ route('login') }}">Login</a>
	</li>
	<li class="right">
		<a class="waves-effect waves-light btn" href="{{ route('register') }}">Register</a>
	</li>
	@else
	<li class="col s5">
		<div class="col s1" >
			<notification-bell></notification-bell>
		</div>
		<div class="col s11" >
			<div class="right chip dropdown-button btn nav_chip" data-activates='main_user_dropdown'>
				<img src="/images/common/person-flat.png" alt="Contact Person">
				<span class="truncate tooltipped" data-position="bottom" data-delay="50" data-tooltip="{{Auth::User()->fullName}}">{{Auth::User()->fullName}}
				</span>
			</div>
		</div>

	</li>
	<li>
		<ul id='main_user_dropdown' class='dropdown-content'>
			<li><a href="/siteuser/{{$selected_perspective_slug}}/accountsetting"><i class="material-icons">settings</i> Your Setting</a></li>
			<li class="divider"></li>
			<li><a href="#"><i class="material-icons">email</i>Email Us</a></li>
			<li> <a href="{{ route('logout') }}"
				onclick="event.preventDefault();
				document.getElementById('logout-form').submit();">
				<i class="material-icons">power_settings_new</i>Logout
			</a>

			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
			</form></li>
		</ul>
	</li>
	@endif
</ul>