<short-call-list inline-template>
	<div>

		@component('common.table')
		@slot('table_head')
		<th>#</th>
		<th></th>
		<th>Name</th>
		<th>Specialty</th>
		<th>Appointments</th>
		<th>Actions</th>
		@endslot
		@slot('table_data')
		<tr v-for="(short_calls , index) in aggregated_short_calls" >
			<td  >@{{index| serial}}</td>
			<td  > <image-view :target-id="short_calls.doctor_id" :target-model="short_calls.doctor_img" :img-class="'circle table_image'"
				></image-view> </td>
				<td  >
					@{{short_calls.first_name}}
					@{{short_calls.middle_name}}
					@{{short_calls.last_name}}
				</td>
				<td v-text="short_calls.specialty_name" ></td>
				<td v-text="short_calls.total" ></td>
				<td>
					<a href="#short_call_appointments_detail_modal" class="btn waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here for detail." @click="getShortCallAppointmentDetailByDoctorId(short_calls.doctor_id)">Detail</a>

				</td>
			</tr>
			@endslot

			@endcomponent


			<!--  short call appointments detail modal -->
			<materialize-modal :id="'short_call_appointments_detail_modal'" :class="'large_modal'" >
				<template slot="modal-header">
					<materialize-modal-header >
						<template slot="header-image">
							<image-view :target-id="short_call_doctor_info.doctor_id" :target-model="short_call_doctor_info.doctor_img" :img-class="'circle table_image'"></image-view>
						</template>
						<template slot="header-title">
							@{{short_call_doctor_info.first_name}}
							@{{short_call_doctor_info.middle_name}}
							@{{short_call_doctor_info.last_name}}
						</template>
						<template slot="header-speciality-name">
							@{{short_call_doctor_info.specialty_name}}
						</template>

					</materialize-modal-header>
				</template>
				<template slot="modal-body">
					<div class="col s12 without-pagination" >
						@component('common.table')



						@slot('table_head')
						<th>#</th>
						<th>Facility</th>
						<th>Address</th>
						<th>Request Duration</th>
						<th>Expired After</th>
						@endslot
						@slot('table_data')
						<tr v-for="(detail_appointment , index) in detail_short_call" >
							<td>@{{index |serial}}</td>
							<td v-text="detail_appointment.facility_name" ></td>
							<td>@{{detail_appointment.address_line_1}} @{{detail_appointment.address_line_2}} @{{detail_appointment.city_name}}
							</td>
							<td>@{{detail_appointment.time_span}}</td>
							<td  >@{{detail_appointment.expiry_date_time| an_date}}</td>
					<!-- <td  >@{{detail_appointment.start_time}} - @{{detail_appointment.start_time}}</td>
				-->


			</tr>

			@endslot

			@endcomponent

		</div>

	</template>
	<template slot="modal-footer">
	</template>
</materialize-modal>

</div>
</short-call-list>
