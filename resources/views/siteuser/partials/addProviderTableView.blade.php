@component('common.table')

@slot('table_search')
<div class="input-field col s12">
	<input id="icon_prefix" type="search" class="validate" v-model="searchText" placeholder="Search by Doctor Name or Specialty Name"  @keyup.enter="searchFacilityTableQuery" @keyup="searchFacilityTableQuery">
	<label for="icon_prefix">Search</label>
</div>
@endslot
@slot('table_page_select')
<span>Rows per page:</span>
<select class="browser-default"  @change="onFacilityTableLength">
	<option v-for="option in perPageOptions" :value="option" :selected="option == pagination.per_page">
		@{{option}}
	</option>
</select>
@endslot

@slot('table_head')
<th>Id</th>
<th>Avatar</th>
<th>Name</th>
<th>Speciality Name</th>
<th>Actions</th>
@endslot
@slot('table_data')
<tr v-for="(doctor, index) in allDoctorBySiteUserId">
	<td style="color:#000;">@{{index|serial}}</td>
	<td style="color:#000;">
		<image-view :target-id="doctor.id" :target-model="doctor.model_identifier" :img-class="'circle table_image'"></image-view>
	</td>
	<td v-text="doctor.doctor_name" style="color:#000;"></td>
	<td v-text="doctor.specialty_name" style="color:#000;"></td>
	<td>
		<a href="#doctor_info" class="btn-floating waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here to add provider" @click="singleDoctorInfo(doctor.id)"><i class="material-icons">info</i></a>
		<a href="#" class="btn-floating waves-effect waves-light tooltipped"  data-position="top" data-delay="10" data-tooltip="click here to delete provider"><i class="material-icons" @click="deleteDoctor(doctor.id)">delete</i></a>
	</td>
</tr>
@endslot
@slot('table_pagination')
<ul class="pagination">
	<li v-if="pagination.current_page > 1">
		<a href="#" aria-label="Previous" @click.prevent="onFacilityTablePage(pagination.current_page - 1)">
			<span aria-hidden="true">&laquo;</span>
		</a>
	</li>
	<li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
		<a href="#" @click.prevent="onFacilityTablePage(page)">@{{ page }}</a>
	</li>
	<li v-if="pagination.current_page < pagination.last_page">
		<a href="#" aria-label="Next" @click.prevent="onFacilityTablePage(pagination.current_page + 1)">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>
@endslot
@endcomponent
