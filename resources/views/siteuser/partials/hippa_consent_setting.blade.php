
<hippa-consent inline-template>
	<div>
		<div class="row">
			<div class="col s12 m12 l12 xl12">
				<context-help>
					Choosing not to agree to Appointmentnotify’s HIPAA Authorization may result in not having access to all of Appointmentnotify’s services.
				</context-help>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<h4>
					User Complete Name - <span>Not Authorized</span>
				</h4>
				<p>
					<input type="checkbox" id="hippa_consent" v-model="HIPPA.value" />
					<label for="hippa_consent"> I have read and accept <a href="#hippa_consent_modal" @click= "getHipaaAgreement('HIPAA',0)">HIPAA Authorization</a> </label>

				</p>
				<button class="btn waves-effect waves-light" @click="upDateHippaConsent('HIPAA')">Update</button>

			</div>
		</div>




		<materialize-modal :id="'hippa_consent_modal'">
			<template slot="modal-header">
				<materialize-modal-header>
					<template slot="header-title">
						@{{agreement.name}}
					</template>
				</materialize-modal-header>
			</template>
			<template slot="modal-body">
				<p>
					@{{agreement.description}}
				</p>
			</template>
		</materialize-modal>
	</div>
</hippa-consent>
