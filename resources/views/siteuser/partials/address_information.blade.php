<form action="" method="get" @submit.prevent="updateAddress" @keydown="addressForm.errors.clear($event.target.name)">
	<div class="row">
		<div class="input-field col s12">
			<i class="material-icons prefix">trending_up</i>
			<input id="address_line_1" type="text" class="validate" name="address_line_1" v-model = 'addressForm.address_line_1'>
			<label for="address_line_1">Address Line 1
				<span class="help" v-if="addressForm.errors.has('address_line_1')">
					(@{{addressForm.errors.get('address_line_1')}})
				</span>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="input-field col s12">
			<i class="material-icons prefix">trending_up</i>
			<input id="address_line_2" type="text" class="validate" name="address_line_2" v-model = 'addressForm.address_line_2'>
			<label for="address_line_2">Address Line 2
				<span class="help" v-if="addressForm.errors.has('address_line_2')">
					(@{{addressForm.errors.get('address_line_2')}})
				</span>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="input-field col s4">
			<i class="material-icons prefix">phone</i>
			<input id="city_name" type="text" class="validate" name="city_name" v-model = 'addressForm.city_name'>
			<label for="city_name">City</label>
			<span class="help" v-if="addressForm.errors.has('city_name')">
				(@{{addressForm.errors.get('city_name')}})
			</span>
		</div>
<!--   	<div class="input-field col s4">
        <i class="material-icons prefix">textsms</i>
        <input type="text" id="state_id" class="autocomplete" name="state_id" v-model = 'form.state_name'>
        <label for="state_id">State</label>
        <span class="help" v-if="form.errors.has('state_id')" v-text="form.errors.get('state_id')"></span>
    </div> -->

    <div class="input-field col s4">
    	<select id="state_postal" class="browser-default" v-model="addressForm.state_id">
    		<option v-for="state in states" :value="state.id" v-text = "state.postal_code">
    		</option>
    	</select>

    </div>

    <div class="input-field col s4">
    	<i class="material-icons prefix">vpn_key</i>
    	<!--  <input id="zip_code" type="text" class="validate" name="zip_code" v-model = 'addressForm.zip_code'> -->
    	<masked-input
    	id="zip_code"
    	type="text"
    	name="zip_code"
    	class="validate"
    	v-model="addressForm.zip_code"
    	:mask="[/\d/, /\d/, /\d/, /\d/, /\d/]"
    	:guide="false"
    	></masked-input>
    	<label for="zip_code">Zip Code
    		<span class="help" v-if="addressForm.errors.has('zip_code')">
    			(@{{addressForm.errors.get('zip_code')}})
    		</span>
    	</label>
    </div>
</div>
<div class="col s12">
	<button type="submit" class="btn right waves-effect waves-light">Update</button>
</div>
</form>
