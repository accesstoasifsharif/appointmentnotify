<div class="row">
	<div class="col s4">
		<h6><b>Medical Insurance</b></h6>
		<select class="browser-default">
			<option>I Am Paying For Myself</option>
			<optgroup label="Your Current Insurance">
				<option>Aetna</option>
				<option>Blue Cross Blue Shield</option>
				<option>Gigna</option>
				<option>GHI</option>
				<option>HIP</option>
				<option>UnitedHealthcare</option>
				<option>UnitedHealthcare Oxford</option>
			</optgroup>
			<optgroup label="All Insurances">
				<option>1199 SEIU</option>
				<option>20/20 Eyecare Plan</option>
				<option>AARP</option>
				<option>Absolute Total Care</option>
				<option>Access Medicare (NY)</option>
				<option>Accountable Health Plan of Ohio</option>
				<option>ACE</option>
				<option>Advantage Health</option>
				<option>AdvantageMD</option>
				<option>Advantica</option>
				<option>Adventist Health</option>
				<option>Advocate Health Care</option>
				<option>Aetna</option>
				<option>Aetna Better Health</option>
				<option>Affinity Health Plan</option>
				<option>AgeWell New York</option>
				<option>Agile Health Insurance</option>
				<option>AIG</option>
				<option>Alameda Alliance for Health</option>
				<option>All Savers Insurance</option>
				<option>Allegian Health Plans</option>
				<option>Allegiance Life and Health</option>
				<option>Alliant Health Plans</option>
				<option>Allianz Worldwide Care</option>
				<option>AlohaCare</option>
				<option>AlphaCare</option>
				<option>AltaMed Senior BuenaCare (PACE)</option>
				<option>Altius (Coventry Health Care)</option>
				<option>AlwaysCare</option>
				<option>Ambetter</option>
				<option>American Behavioral</option>
				<option>American Eldercare</option>
				<option>American Healthcare Alliance</option>
				<option>American Republic Insurance Company</option>
				<option>America 1st Choice</option>
				<option>AmeriGroup</option>
				<option>AmeriHealth</option>
				<option>AmeriHealth Caritas</option>
				<option>Amida Care</option>
				<option>Amish Church Fund</option>
				<option>Amplifon Hearing Health Care</option>
				<option>Anthem Blue Cross</option>
				<option>Anthem Blue Cross Blue Shield</option>
				<option>APWU</option>
				<option>Arise Health Plan</option>
				<option>Arizona Foundation for Medical Care</option>
				<option>Arkansas Blue Cross Blue Shield</option>
				<option>Ascension Health</option>
				<option>Assurant Employee Benefits</option>
				<option>Assurant Health</option>
				<option>Asuris Northwest Health</option>
				<option>ATRIO Health Plans</option>
				<option>Aultcare</option>
				<option>Avera Health Plans</option>
				<option>Averde Health</option>
				<option>Avesis</option>
				<option>AvMed</option>
				<option>Banker Life</option>
				<option>Banner Health</option>
				<option>Baptist Health Plan</option>
				<option>Beaumont Employee Health Plan</option>
				<option>Beech Street</option>
				<option>Best Choice Plus</option>
				<option>Best Life And Health</option>
				<option>Better Health (Florida Medicaid)</option>
				<option>Block Vision</option>
				<option>Blue Choice Health Plan</option>
				<option>Blue Cross Blue Shield Federal Employee Program</option>
				<option>Blue Cross Blue Shield of Alabama</option>
				<option>Blue Cross Blue Shield of Arizona</option>
				<option>Blue Cross Blue Shield of Florida (Florida Blue)</option>
				<option>Blue Cross Blue Shield of Georgia</option>
				<option>Blue Cross Blue Shield of Illinois</option>
				<option>Blue Cross Blue Shield of Kansas</option>
				<option>Blue Cross Blue Shield of Kansas City</option>
				<option>Blue Cross Blue Shield of Louisiana</option>
				<option>Blue Cross Blue Shield of Massachusetts</option>
				<option>Blue Cross Blue Shield of Michigan</option>
				<option>Blue Cross Blue Shield of Minnesota</option>
				<option>Blue Cross Blue Shield of Mississippi</option>
				<option>Blue Cross Blue Shield of Montana</option>
				<option>Blue Cross Blue Shield of Nebraska</option>
				<option>Blue Cross Blue Shield of New Mexico</option>
				<option>Blue Cross Blue Shield of North Carolina</option>
				<option>Blue Cross Blue Shield of North Dakota</option>
				<option>Blue Cross Blue Shield of Oklahoma</option>
				<option>Blue Cross Blue Shield of Rhode Island</option>
				<option>Blue Cross Blue Shield of South Carolina</option>
				<option>Blue Cross Blue Shield of Tennessee</option>
				<option>Blue Cross Blue Shield of Texas</option>
				<option>Blue Cross Blue Shield of Vermont</option>
				<option>Blue Cross Blue Shield of Western New York</option>
				<option>Blue Cross Blue Shield of Wyoming</option>
				<option>Blue Cross of Idaho</option>
				<option>Blue Cross of Northeastern Pennsylvania</option>
				<option>Blue Shield of California</option>
				<option>Blue Shield of Northeastern New York</option>
				<option>Bluegrass Family Health Plan</option>
				<option>BMC HealthNet Plan</option>
				<option>Brand New Day</option>
				<option>BridgeSpan</option>
				<option>Bridgeway Health Solutions</option>
				<option>Buckeye Health Plan</option>
				<option>California Health and Wellness</option>
				<option>CalOptima</option>
				<option>CalPERS</option>
				<option>CalViva Health</option>
				<option>Capital Blue Cross</option>
				<option>Capital Health Plan</option>
				<option>Care Access Health Plan</option>
				<option>Care Improvement Plus</option>
				<option>Care N Care</option>
				<option>Care1st</option>
				<option>CareConnect</option>
				<option>CareFirst Blue Cross Blue Shield</option>
				<option>CareMore</option>
				<option>CareOregon</option>
				<option>CarePlus Health Plans (Florida Medicare)</option>
				<option>CareSource</option>
				<option>Cascade Health Alliance</option>
				<option>Caterpillar</option>
				<option>CBA Blue</option>
				<option>CDPHP</option>
				<option>CeltiCare Health Plan</option>
				<option>CenCal Health</option>
				<option>Centennial Care</option>
				<option>CenterLight Healthcare</option>
				<option>Central California Alliance for Health</option>
				<option>Central Health Plan of California</option>
				<option>Century Healthcare - CHC</option>
				<option>CHAMPVA</option>
				<option>Children Community Health Plan</option>
				<option>Children Medical Services (CMS)</option>
				<option>Chinese Community Health Plan</option>
				<option>Choice Care Network</option>
				<option>CHP Group</option>
				<option>Christian Healthcare Ministries</option>
				<option>CHRISTUS Health Plan</option>
				<option>Cigna</option>
				<option>Cigna-HealthSpring</option>
				<option>Citizens Choice Health Plan</option>
				<option>Clark County Self-Funded Health</option>
				<option>Clements Worldwide</option>
				<option>Cleveland Clinic Employee Health Plan</option>
				<option>Clover Health</option>
				<option>Cofinity</option>
				<option>Colorado Access</option>
				<option>Colorado Choice</option>
				<option>Columbia United Providers</option>
				<option>Common Ground Healthcare Cooperative</option>
				<option>Commonwealth Care</option>
				<option>Commonwealth Care Alliance</option>
				<option>Community Behavioral Health</option>
				<option>Community Care Behavioral Health Organization</option>
				<option>Community Care of North Carolina</option>
				<option>Community Eye Care</option>
				<option>Community First Health Plans</option>
				<option>Community Health Alliance</option>
				<option>Community Health Choice</option>
				<option>Community Health Group</option>
				<option>Community Health Options</option>
				<option>Community Health Partners</option>
				<option>Community Health Plan of Washington</option>
				<option>CommunityCare of Oklahoma</option>
				<option>Companion Life</option>
				<option>CompBenefits</option>
				<option>Comprehensive Health Insurance Plan (CHIP) of Illinois</option>
				<option>Comprehensive Medical and Dental Program (CMDP)</option>
				<option>ComPsych</option>
				<option>Connect Care</option>
				<option>ConnectiCare</option>
				<option>Consolidated Health Plans</option>
				<option>Consumer Health Network</option>
				<option>Consumers Choice Health Plan</option>
				<option>Consumer Mutual Insurance of Michigan</option>
				<option>Contra Costa Health Plan</option>
				<option>Cook Children Health Plan</option>
				<option>Coordinated Care Health</option>
				<option>Corvel</option>
				<option>CountyCare (Cook County)</option>
				<option>Coventry Health Care</option>
				<option>Cox HealthPlans</option>
				<option>Crystal Run Healthcare</option>
				<option>Culinary Health Fund</option>
				<option>DAKOTACARE</option>
				<option>Davis Vision</option>
				<option>DC Medicaid</option>
				<option>Dean Health Plan</option>
				<option>Denver Health Medical Plan</option>
				<option>Department of Medical Assistance Services</option>
				<option>Deseret Mutual</option>
				<option>Devon Health Services</option>
				<option>DMC Care</option>
				<option>DOCS (Doctors of the Oregon South Coast)</option>
				<option>Driscoll Health Plan</option>
				<option>Easy Choice Health Plan (California)</option>
				<option>Easy Choice Health Plan of New York</option>
				<option>El Paso First Health Plans</option>
				<option>Elderplan</option>
				<option>EmblemHealth</option>
				<option>EMI Health</option>
				<option>Empire Blue Cross Blue Shield</option>
				<option>Empire BlueCross BlueShield HealthPlus</option>
				<option>Empire Plan</option>
				<option>Encore Health Network</option>
				<option>Epic Hearing Health Care</option>
				<option>ESSENCE Healthcare</option>
				<option>Evergreen Health Cooperative</option>
				<option>Evolutions Healthcare Systems</option>
				<option>Excellus Blue Cross Blue Shield</option>
				<option>EyeMed</option>
				<option>Eyetopia Vision Care</option>
				<option>Fallon Community Health Plan (FCHP)</option>
				<option>Family Health Network</option>
				<option>FamilyCare Health Plans</option>
				<option>Fidelis Care (NY)</option>
				<option>First Choice Health</option>
				<option>First Choice Health Plan of Mississippi</option>
				<option>First Health (Coventry Health Care)</option>
				<option>FirstCare Health Plans</option>
				<option>FirstCarolinaCare</option>
				<option>Florida Blue: Blue Cross Blue Shield of Florida</option>
				<option>Florida Health Care Plans</option>
				<option>Florida Health Partners</option>
				<option>Florida Hospital Healthcare System (FHHS)</option>
				<option>Florida KidCare</option>
				<option>Florida Memorial Health Network</option>
				<option>Fort Bend County Indigent Health Care</option>
				<option>Fortified Provider Network</option>
				<option>Freedom Health</option>
				<option>Galaxy Health</option>
				<option>Gateway Health</option>
				<option>GEHA</option>
				<option>Geisinger Health Plan</option>
				<option>GEMCare Health Plan</option>
				<option>General Vision Services (GVS)</option>
				<option>GeoBlue</option>
				<option>GHI</option>
				<option>Gilsbar 360 Alliance</option>
				<option>Global Health</option>
				<option>Gold Coast Health Plan</option>
				<option>Golden State Medicare Health Plan</option>
				<option>Group Health Cooperative</option>
				<option>Group Health Cooperative of Eau Claire</option>
				<option>Group Health Cooperative of South Central Wisconsin</option>
				<option>Guardian</option>
				<option>Gundersen Health Plan</option>
				<option>GWH-Cigna (formerly Great West Healthcare)</option>
				<option>HAP (Health Alliance Plan)</option>
				<option>HAP Midwest Health Plan</option>
				<option>Harbor Health Plan</option>
				<option>Harken Health</option>
				<option>Harmony Health Plan</option>
				<option>Harvard Pilgrim Health Care</option>
				<option>Hawaii Medical Assurance Association (HMAA)</option>
				<option>Hawaii Medical Service Association (HMSA)</option>
				<option>Health Alliance</option>
				<option>Health Care District of Palm Beach County</option>
				<option>Health Choice</option>
				<option>Health Choice Arizona</option>
				<option>Health Choice Utah</option>
				<option>Health First (FL)</option>
				<option>Health First Colorado</option>
				<option>Health First Health Plans (Florida)</option>
				<option>Health Net</option>
				<option>Health New England</option>
				<option>Health Partners Plans</option>
				<option>Health Plan of Nevada</option>
				<option>Health Plan of San Joaquin</option>
				<option>Health Plan of San Mateo</option>
				<option>Health Plus</option>
				<option>Health Republic of New Jersey</option>
				<option>Health Republic of Oregon</option>
				<option>Health Sun</option>
				<option>HealthChoice Oklahoma</option>
				<option>HealthFirst (NY)</option>
				<option>Healthlink</option>
				<option>HealthNow</option>
				<option>HealthPartners</option>
				<option>HealthPlus of Michigan</option>
				<option>HealthScope Benefits</option>
				<option>HealthSmart</option>
				<option>HealthSpan</option>
				<option>HealthyCT</option>
				<option>Hear In America</option>
				<option>Heritage Vision Plans</option>
				<option>HFN</option>
				<option>HFS Medical Benefits</option>
				<option>HighMark Blue Cross Blue Shield</option>
				<option>Highmark Blue Cross Blue Shield of Delaware</option>
				<option>HighMark Blue Shield</option>
				<option>Highmark BlueCross BlueShield of West Virginia</option>
				<option>Hillsborough Health Care Plan</option>
				<option>HIP</option>
				<option>Home State Health Plan</option>
				<option>Hometown Health</option>
				<option>Horizon Blue Cross Blue Shield of New Jersey</option>
				<option>Horizon Blue Cross Blue Shield of New Jersey For Barbanas Health</option>
				<option>Horizon Blue Cross Blue Shield of New Jersey For Novartis</option>
				<option>Horizon NJ Health</option>
				<option>Hudson Health Plan</option>
				<option>Humana</option>
				<option>HUSKY Health</option>
				<option>IHC Health Solutions</option>
				<option>Illinicare Health</option>
				<option>Imagine Health</option>
				<option>IMO (Independent Medical Systems)</option>
				<option>Independence Blue Cross</option>
				<option>Independence Care System</option>
				<option>Independent Health</option>
				<option>Indiana University Health</option>
				<option>Ingham Health Plan</option>
				<option>InHealth</option>
				<option>Inland Empire Health Plan</option>
				<option>Innovation Health</option>
				<option>Integra</option>
				<option>Inter Valley Health Plan</option>
				<option>INTotal Health</option>
				<option>Iowa MediPASS</option>
				<option>Itasca Medical Care</option>
				<option>Johns Hopkins Employer Health Programs</option>
				<option>Kaiser Permanente</option>
				<option>Kansas HealthWave</option>
				<option>KelseyCare Advantage</option>
				<option>Kentucky Health Cooperative</option>
				<option>Keystone First</option>
				<option>KPS Health Plans</option>
				<option>L.A. Care Health Plan</option>
				<option>Land of Lincoln Health</option>
				<option>Landmark Healthplan</option>
				<option>Legacy Health</option>
				<option>Liberty Health Advantage</option>
				<option>Liberty Mutual</option>
				<option>LifeSynch</option>
				<option>LifeWise</option>
				<option>Lighthouse Guild</option>
				<option>Lincoln Financial Group</option>
				<option>Louisiana Healthcare Connections</option>
				<option>Lutheran Preferred</option>
				<option>Magellan Health</option>
				<option>MagnaCare</option>
				<option>Magnolia Health Plan</option>
				<option>Mail Handlers Benefit Plan</option>
				<option>MaineCare</option>
				<option>Managed Health Network (MHN)</option>
				<option>Managed Health Services (Indiana)</option>
				<option>Managed Health Services (Wisconsin)</option>
				<option>March Vision Care</option>
				<option>Maricopa Health Plan</option>
				<option>Martin Point HealthCare</option>
				<option>Maryland Medical Assistance (Medicaid)</option>
				<option>Maryland Physicians Care</option>
				<option>MassHealth</option>
				<option>Mayo Medical Plan</option>
				<option>McLaren Health Plan</option>
				<option>MCM Maxcare</option>
				<option>MDwise</option>
				<option>Medica</option>
				<option>Medica HealthCare Plans (Florida)</option>
				<option>Medicaid</option>
				<option>Medi-Cal</option>
				<option>Medical Eye Services (MES Vision)</option>
				<option>Medical Mutual</option>
				<option>Medicare</option>
				<option>MediGold</option>
				<option>MediPass</option>
				<option>MedStar Family Choice</option>
				<option>MedStar Medicare Choice</option>
				<option>MedStar Select</option>
				<option>Memorial Hermann</option>
				<option>Mercy Care</option>
				<option>Meridian Health Plan</option>
				<option>MetLife</option>
				<option>MetroPlus Health Plan</option>
				<option>Metropolitan Health Plan</option>
				<option>MHNet Behavioral Health</option>
				<option>Minuteman Health</option>
				<option>Missouri Care</option>
				<option>MO HealthNet</option>
				<option>Moda Health</option>
				<option>Molina Healthcare</option>
				<option>Montana Health Cooperative</option>
				<option>Mountain Health Co-Op</option>
				<option>Multiplan PHCS</option>
				<option>MVP Health Care</option>
				<option>National Vision Administrators</option>
				<option>Nationwide</option>
				<option>Navajo Nation</option>
				<option>Neighborhood Health Plan (Massachusetts)</option>
				<option>Neighborhood Health Plan of Rhode Island</option>
				<option>Network Health Plan</option>
				<option>Nevada Health CO-OP</option>
				<option>Nevada Preferred</option>
				<option>New Hampshire Healthy Families</option>
				<option>New Mexico Health Connections</option>
				<option>New York Hotel Trades Council</option>
				<option>North Carolina Health Choice (NCHC) for Children</option>
				<option>North Shore LIJ CareConnect</option>
				<option>NovaNet</option>
				<option>NovaSys Health</option>
				<option>NY State No-Fault</option>
				<option>Ohio Health Choice</option>
				<option>OHMS (Oregon Health Management Services)</option>
				<option>OneNet PPO</option>
				<option>OptiCare Managed Vision</option>
				<option>Opticare of Utah</option>
				<option>OptimaHealth</option>
				<option>Optimum HealthCare</option>
				<option>Oregon Health Co-Op</option>
				<option>Oscar Health Insurance Co.</option>
				<option>OSU Health Plan</option>
				<option>Oxford (UnitedHealthcare)</option>
				<option>Pacific Health Alliance</option>
				<option>PacificSource Health Plans</option>
				<option>Palmetto GBA</option>
				<option>Pan-American Life Insurance Group</option>
				<option>Paramount Healthcare</option>
				<option>Parkland Community Health Plan</option>
				<option>Parkview Total Health</option>
				<option>Partnership HealthPlan of California</option>
				<option>Passport Health Plan (Kentucky)</option>
				<option>Passport To Health (Montana Medicaid)</option>
				<option>Patient 1st (Alabama Medicaid)</option>
				<option>PBA (Patrolmen Benefit Association)</option>
				<option>Peach State Health Plan</option>
				<option>PeachCare for Kids</option>
				<option>PennCare</option>
				<option>Peoples Health</option>
				<option>Phoenix Health Plan</option>
				<option>Physician Assured Access System</option>
				<option>Physicians Health Plan</option>
				<option>Physicians Health Plan of Northern Indiana, Inc.</option>
				<option>Physicians Plus Insurance Corporation</option>
				<option>PhysiciansCare</option>
				<option>Piedmont Community Health Plan</option>
				<option>Piedmont WellStar Health Plans</option>
				<option>POMCO</option>
				<option>Positive Health Care</option>
				<option>Preferential Care Network</option>
				<option>Preferred Care Partners</option>
				<option>Preferred Health Systems</option>
				<option>Preferred Medical Plan</option>
				<option>Preferred Network Access (PNA)</option>
				<option>PreferredOne</option>
				<option>Premera Blue Cross</option>
				<option>Premier Health Plan</option>
				<option>Presbyterian Health Plan/Presbyterian Insurance Company</option>
				<option>Prestige Health Choice</option>
				<option>Primary Care Case Management (North Dakota Medicaid)</option>
				<option>Prime Health Services, Inc</option>
				<option>PrimeWest Health</option>
				<option>Principal Financial Group</option>
				<option>Priority Health</option>
				<option>Priority Partners</option>
				<option>Prominence Health Plan</option>
				<option>Providence Health Plans</option>
				<option>ProviDRs Care (WPPA)</option>
				<option>Public Aid (Illinois Medicaid)</option>
				<option>Public Employees Health Program (PEHP)</option>
				<option>QualCare</option>
				<option>QualChoice Arkansas</option>
				<option>Quality Health Plans of New York</option>
				<option>Regence Blue Cross Blue Shield</option>
				<option>RiverLink Health</option>
				<option>Riverside Health</option>
				<option>Rocky Mountain Health Plans</option>
				<option>Sagamore Health Network</option>
				<option>Samaritan Health Plan Operations</option>
				<option>San Francisco Health Plan</option>
				<option>Sanford Health Plan</option>
				<option>Santa Clara Family Health Plan</option>
				<option>SCAN Health Plan</option>
				<option>Scott & White Health Plan</option>
				<option>Security Health Plan of Wisconsin, Inc.</option>
				<option>Select Care</option>
				<option>Select Health Network</option>
				<option>SelectHealth</option>
				<option>Sendero Health Plans</option>
				<option>Senior Whole Health</option>
				<option>Seton Health Plan</option>
				<option>Sharp Health Plan</option>
				<option>Sierra Health and Life</option>
				<option>SightCare</option>
				<option>SIHO Insurance Services</option>
				<option>SIMNSA Health Plan</option>
				<option>Simply Healthcare</option>
				<option>Solstice</option>
				<option>SoonerCare (Oklahoma Medicaid)</option>
				<option>Soundpath Health</option>
				<option>South Country Health Alliance</option>
				<option>South Florida Community Care Network</option>
				<option>Spectera</option>
				<option>SSM Health Care</option>
				<option>Standard Life and Accident Insurance Company</option>
				<option>Stratose</option>
				<option>SummaCare</option>
				<option>Sunflower Health Plan</option>
				<option>Sunshine Health</option>
				<option>Superior HealthPlan</option>
				<option>Superior Vision</option>
				<option>SutterSelect</option>
				<option>Teachers Health Trust</option>
				<option>TexanPlus</option>
				<option>Texas Children Health Plan</option>
				<option>Texas Kids First</option>
				<option>Texas Women Health Program</option>
				<option>The Hartford</option>
				<option>The Health Plan of the Upper Ohio Valley, Inc.</option>
				<option>The HSC Health Care System</option>
				<option>Total Health Care</option>
				<option>Touchstone</option>
				<option>Travelers</option>
				<option>Tricare</option>
				<option>Trillium Community Health Plan</option>
				<option>Trilogy Health Insurance</option>
				<option>Triple-S Salud: Blue Cross Blue Shield of Puerto Rico</option>
				<option>Trusted Health Plan</option>
				<option>Tuality Health Alliance</option>
				<option>Tufts Health Freedom Plan</option>
				<option>Tufts Health Plan</option>
				<option>Tufts Health Plan-Network Health</option>
				<option>UCare</option>
				<option>UHA Health Insurance</option>
				<option>UniCare</option>
				<option>Uniform Medical Plan</option>
				<option>Union Health Services, Inc</option>
				<option>Union Plans</option>
				<option>United Behavioral Health</option>
				<option>UnitedHealthcare</option>
				<option>UnitedHealthcare Community Plan</option>
				<option>UnitedHealthcare Oxford</option>
				<option>UnitedHealthOne</option>
				<option>Unity Health Insurance</option>
				<option>Univera Healthcare</option>
				<option>Universal American</option>
				<option>University Hospitals (Health Design Plus)</option>
				<option>University of Arizona Health Plans</option>
				<option>University of Chicago Health Plan</option>
				<option>University of Utah Health Plans</option>
				<option>University Physician Network (UPN)</option>
				<option>UPMC Health Plan</option>
				<option>US Family Health Plan</option>
				<option>US Health Group</option>
				<option>USA Managed Care Organization</option>
				<option>USAble Mutual Insurance Company</option>
				<option>Valley Health Plan</option>
				<option>ValueOptions</option>
				<option>Vantage Health Plan, Inc.</option>
				<option>Ventura County Health Care Plan</option>
				<option>VillageCareMax</option>
				<option>Virginia Coordinated Care (VCC)</option>
				<option>Virginia Health Network</option>
				<option>Virginia Premier Health Plan</option>
				<option>Vision Benefits of America</option>
				<option>Vision Care Direct</option>
				<option>Vision Plan of America</option>
				<option>Viva Health Plan</option>
				<option>VNS Choice Health Plans</option>
				<option>Volusia Health Network</option>
				<option>VSP</option>
				<option>Vytra</option>
				<option>WEA Trust</option>
				<option>Wellcare</option>
				<option>Wellmark Blue Cross Blue Shield</option>
				<option>Western Health Advantage</option>
				<option>Workers Compensation</option>
				<option>WPS Health Plan</option>
				<option>YourCare Health Plan</option>
				<option>Zenith</option>
			</optgroup>
		</select>
	</div>
	<div class="col s4">
		<h6><b>Dental Insurance</b></h6>
		<select class="browser-default">
			<option>I Am Paying For Myself</option>
			<optgroup label="Your Current Insurance">
				<option>Aetna</option>
				<option>Cigna</option>
				<option>Delta Dental</option>
				<option>Guardian</option>
				<option>Medicaid</option>
				<option>MetLife</option>
				<option>UnitedHealthcare</option>
			</optgroup>
			<optgroup label="All Insurances">
				<option>1199SEIU</option>
				<option>Access Dental</option>
				<option>ADN Administrators</option>
				<option>Advantica</option>
				<option>Aetna</option>
				<option>Affinity Health Plan</option>
				<option>Aflac</option>
				<option>AIG</option>
				<option>Alameda Alliance for Health</option>
				<option>Alpha Dental</option>
				<option>Altus Dental</option>
				<option>AlwaysCare</option>
				<option>AmeriGroup</option>
				<option>AmeriPlan</option>
				<option>Ameritas</option>
				<option>Anthem Blue Cross</option>
				<option>Anthem Blue Cross Blue Shield</option>
				<option>APWU</option>
				<option>Argus Dental</option>
				<option>Arkansas Blue Cross Blue Shield</option>
				<option>Assurant Employee Benefits</option>
				<option>Asuris Northwest Health</option>
				<option>Avesis</option>
				<option>Basix Dental Discount</option>
				<option>Beam</option>
				<option>Benefit Management Inc</option>
				<option>Benefit Source</option>
				<option>Best Life And Health</option>
				<option>Blue Choice Health Plan</option>
				<option>Blue Cross Blue Shield</option>
				<option>Blue Cross Blue Shield Federal Employee Program</option>
				<option>Blue Cross Blue Shield of Alabama</option>
				<option>Blue Cross Blue Shield of Arizona</option>
				<option>Blue Cross Blue Shield of Florida (Florida Blue)</option>
				<option>Blue Cross Blue Shield of Georgia</option>
				<option>Blue Cross Blue Shield of Illinois</option>
				<option>Blue Cross Blue Shield of Kansas</option>
				<option>Blue Cross Blue Shield of Kansas City</option>
				<option>Blue Cross Blue Shield of Louisiana</option>
				<option>Blue Cross Blue Shield of Massachusetts</option>
				<option>Blue Cross Blue Shield of Michigan</option>
				<option>Blue Cross Blue Shield of Nebraska</option>
				<option>Blue Cross Blue Shield of New Mexico</option>
				<option>Blue Cross Blue Shield of North Carolina</option>
				<option>Blue Cross Blue Shield of North Dakota</option>
				<option>Blue Cross Blue Shield of Oklahoma</option>
				<option>Blue Cross Blue Shield of Rhode Island</option>
				<option>Blue Cross Blue Shield of South Carolina</option>
				<option>Blue Cross Blue Shield of Tennessee</option>
				<option>Blue Cross Blue Shield of Texas</option>
				<option>Blue Cross Blue Shield of Western New York</option>
				<option>Blue Cross Blue Shield of Wyoming</option>
				<option>Blue Cross of Idaho</option>
				<option>Blue Shield of California</option>
				<option>Blue Shield of Northeastern New York</option>
				<option>Boston University</option>
				<option>BridgeSpan</option>
				<option>Bridgeway Health Solutions</option>
				<option>Buckeye Health Plan</option>
				<option>California Dental</option>
				<option>Capital Blue Cross</option>
				<option>CareFirst Blue Cross Blue Shield</option>
				<option>Careington</option>
				<option>CareOregon</option>
				<option>CareSource</option>
				<option>CBA Blue</option>
				<option>Cigna</option>
				<option>Citizens Choice Health Plan</option>
				<option>Community First Health Plans</option>
				<option>CompBenefits</option>
				<option>Comprehensive Medical and Dental Program (CMDP)</option>
				<option>Connect Care</option>
				<option>Connecticut Dental Health Partnership</option>
				<option>Connection Dental</option>
				<option>Coventry Dental</option>
				<option>Culinary Health Fund</option>
				<option>DAKOTACARE</option>
				<option>DDS Inc.</option>
				<option>Dearborn National</option>
				<option>DeCare Dental</option>
				<option>Delta Dental</option>
				<option>DenCap</option>
				<option>Denex Dental</option>
				<option>Dental Benefit Providers</option>
				<option>Dental Care Plus</option>
				<option>Dental Health & Wellness</option>
				<option>Dental Health Alliance</option>
				<option>Dental Health Options</option>
				<option>Dental Health Services</option>
				<option>Dental Network of America</option>
				<option>Dental Select</option>
				<option>Dental Source</option>
				<option>DentalWise</option>
				<option>DentaQuest</option>
				<option>Dentegra</option>
				<option>Dentemax</option>
				<option>Denti-Cal</option>
				<option>Deseret Mutual</option>
				<option>Devon Health Services</option>
				<option>Dominion National</option>
				<option>Elderplan</option>
				<option>EmblemHealth</option>
				<option>EMI Health</option>
				<option>Empire Blue Cross Blue Shield</option>
				<option>Encore Dental</option>
				<option>Excellus Blue Cross Blue Shield</option>
				<option>Fallon Community Health Plan (FCHP)</option>
				<option>FCL Dental (First Continental Life)</option>
				<option>FCL Dental (StarDent)</option>
				<option>Fidelio Dental</option>
				<option>Fidelis Care (NY)</option>
				<option>First Choice Health</option>
				<option>First Dental Health</option>
				<option>Florida Blue: Blue Cross Blue Shield of Florida</option>
				<option>GEHA</option>
				<option>GHI</option>
				<option>Golden Dental Plans</option>
				<option>Golden West Dental</option>
				<option>Group Health Cooperative of Eau Claire</option>
				<option>Guardian</option>
				<option>GWH-Cigna (formerly Great West Healthcare)</option>
				<option>Hawaii Dental Service (Delta Dental)</option>
				<option>Hawaii Medical Assurance Association (HMAA)</option>
				<option>Hawaii Medical Service Association (HMSA)</option>
				<option>Health Choice</option>
				<option>Health Net</option>
				<option>Health Plan of Nevada</option>
				<option>Health Plan of San Mateo</option>
				<option>Health Plus</option>
				<option>HealthFirst (NY)</option>
				<option>HealthPartners</option>
				<option>Healthplex</option>
				<option>HealthSmart</option>
				<option>Highmark Blue Cross Blue Shield</option>
				<option>Highmark Blue Shield</option>
				<option>HIP</option>
				<option>Horizon Blue Cross Blue Shield of New Jersey</option>
				<option>Horizon NJ Health</option>
				<option>Humana</option>
				<option>Illinicare Health</option>
				<option>Independence Blue Cross</option>
				<option>Itasca Medical Care</option>
				<option>J.J. Stanis and Company, Inc.</option>
				<option>Kaiser Permanente</option>
				<option>Keystone First</option>
				<option>Legacy Health</option>
				<option>Liberty Dental</option>
				<option>LifeMap</option>
				<option>LifeWise</option>
				<option>Lincoln Financial Group</option>
				<option>MaineCare</option>
				<option>Maricopa Health Plan</option>
				<option>MassHealth</option>
				<option>Maverest Dental Network</option>
				<option>MCNA Dental</option>
				<option>Medica</option>
				<option>Medicaid</option>
				<option>Medical Mutual</option>
				<option>MediPass</option>
				<option>Mercy Care</option>
				<option>MetLife</option>
				<option>MetroPlus Health Plan</option>
				<option>Minuteman Health</option>
				<option>Moda Health</option>
				<option>Molina Healthcare</option>
				<option>Momentum Insurance</option>
				<option>Mutual of Omaha</option>
				<option>MVP Health Care</option>
				<option>Nationwide</option>
				<option>Nevada Dental Benefits</option>
				<option>Nevada Health CO-OP</option>
				<option>North Carolina Health Choice (NCHC) for Children</option>
				<option>OneNet</option>
				<option>OneNet PPO</option>
				<option>Option One</option>
				<option>Oscar Health Insurance Co.</option>
				<option>Oxford (UnitedHealthcare)</option>
				<option>PacificSource Health Plans</option>
				<option>Passport Health Plan (Kentucky)</option>
				<option>Patriot Health</option>
				<option>PBA (Patrolmen Benefit Association)</option>
				<option>Peach State Health Plan</option>
				<option>PeachCare for Kids</option>
				<option>Phoenix Health Plan</option>
				<option>Physicians Health Plan of Northern Indiana, Inc.</option>
				<option>Piedmont Community Health Plan</option>
				<option>Premera Blue Cross</option>
				<option>Premier Access</option>
				<option>Premier Dental Group</option>
				<option>Primary Care Case Management (North Dakota Medicaid)</option>
				<option>PrimeWest Health</option>
				<option>Principal Financial Group</option>
				<option>Priority Health</option>
				<option>Prominence Health Plan</option>
				<option>Public Employees Health Program (PEHP)</option>
				<option>Quality Plan Administrators</option>
				<option>Quapaw Tribe of Oklahoma</option>
				<option>Regence Blue Cross Blue Shield</option>
				<option>Reliance Standard</option>
				<option>Renaissance Dental</option>
				<option>Scion Dental, Inc</option>
				<option>SecureCare Dental</option>
				<option>Security Life</option>
				<option>SelectHealth</option>
				<option>Sele-Dent</option>
				<option>Sierra Health and Life</option>
				<option>SIMNSA Health Plan</option>
				<option>Solstice</option>
				<option>SoonerCare (Oklahoma Medicaid)</option>
				<option>Southland Benefit Solutions</option>
				<option>STLDentalPlan, LLC</option>
				<option>Stratose</option>
				<option>Sun Life Financial</option>
				<option>Superior Dental Care</option>
				<option>Surency Life & Health</option>
				<option>TDA (Total Dental Administrators)</option>
				<option>Teachers Health Trust</option>
				<option>Texas Children Health Plan</option>
				<option>The Arc of Florida, Inc</option>
				<option>The Standard</option>
				<option>Tricare Dental Program</option>
				<option>Trillium Community Health Plan</option>
				<option>Triple-S Salud: Blue Cross Blue Shield of Puerto Rico</option>
				<option>TruAssure</option>
				<option>Tuality Health Alliance</option>
				<option>UniCare</option>
				<option>Union Plans</option>
				<option>United Concordia</option>
				<option>United Federation of Teachers</option>
				<option>UnitedHealthcare</option>
				<option>UnitedHealthcare Community Plan</option>
				<option>UnitedHealthcare Oxford</option>
				<option>UPMC Health Plan</option>
				<option>USAble Mutual Insurance Company</option>
				<option>Vantage One</option>
				<option>Vision Plan of America</option>
				<option>WEA Trust</option>
				<option>Wellcare</option>
				<option>Wellmark Blue Cross Blue Shield</option>
				<option>Western Dental & Orthodontics</option>
				<option>Willamette Dental Group</option>
			</optgroup>
		</select>
	</div>
	<div class="col s4">
		<h6><b>Vision Insurance</b></h6>
		<select class="browser-default">
			<option>I am paying for myself</option>
		</select>
	</div>
</div>
<div class="row">
	<div class="col s12">
		<button class="btn waves-effect waves-light">
			Save
		</button>
	</div>
</div>
