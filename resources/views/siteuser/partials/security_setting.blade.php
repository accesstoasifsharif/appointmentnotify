

<div class="row">
	<div class="col s12 appointments_tab ">
		<ul-tabs style="background-color: transparent;">
			<tab-component :href="'#reset_password'" class="col s6" >
				Reset Password
			</tab-component>
			<tab-component :href="'#recovery_setting'" class="col s6">
				Recovery Setting
			</tab-component>
		</ul-tabs>
	</div>
	<div class="col s12" id="reset_password" style="padding-top: 15px;">
		<div class="col s3"></div>
		<div class="col s6">
			<reset-password inline-template>
				<div>
					<div class="input-field col s12">
						<i class="material-icons prefix">vpn_key</i>
						<input type="password" id="current_password" v-model="resetPassword.current_password">
						<label for="current_password">Current Password</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">vpn_key</i>
						<input type="password" id="new_password" v-model="resetPassword.new_password" >
						<label for="new_password">New Password</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">repeat</i>
						<input type="password" id="retype_new_password" v-model="resetPassword.retype_new_password">
						<label for="retype_new_password">Re-type New Password</label>
					</div>
					<div class="col s12">

						<button class="btn waves-effect waves-light" @click="upDatePassword()">Update</button>
					</div>
				</div>
			</reset-password>

		</div>
	</div>
	<div class="col s12" id="recovery_setting" style="padding-top: 15px;">
		<div class="col s3"></div>
		<div class="col s6">
			<form action="">
				<div class="input-field col s12">
					<i class="material-icons prefix">contact_mail</i>
					<input type="text" id="recovery_email" v-model="recovery_email">
					<label for="recovery_email">Recovery E-mail</label>
				</div>
				<div class="input-field col s12">
					<i class="material-icons prefix">contact_phone</i>
					<input type="text" id="recovery_phone" v-model="recovery_phone">
					<label for="recovery_phone">Recovery Phone</label>
				</div>
				<div class="input-field col s12">
					<i class="material-icons prefix">description</i>
					<input type="text" id="recovery_text" v-model="recovery_text">
					<label for="recovery_text">Recovery Text</label>
				</div>
				<div class="col s12">
					<button class="btn waves-effect waves-light" @click.prevent="saveRecoveryOption">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
