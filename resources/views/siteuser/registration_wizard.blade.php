@extends('layouts.app')

@section('content')


<profile-wizard inline-template>
  <div>
    <div class="container">
      <form action="post" method="" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
        <div class="row" >
          <div class="col s12 center-align">
            <h5 >
              Setup Your Profile
            </h5>
          </div>

          {{ csrf_field() }}
          <div class="col s9">
            <div class="col s12">
              <div class="card z-depth-2">

                <div class="card-content">
                  @include('auth.partials.siteuser_profile')
                </div>

              </div>
            </div>
            <div class="col s12">
              <div class="card z-depth-2">

                <div class="card-content">

                 @include('auth.partials.siteuser_address')

               </div>

             </div>
           </div>

         </div>

         <div class="col s3">
          <div class="col s12">
            <div class="card z-depth-2">
              <div class="row " style="padding: 2%;">

                <div class="col s4 ">
                  <img :src= "getAvatar()" class="picture-src" id="wizardPicturePreview" title="" style="height: 60px; width: 60px; border: solid 2px; border-radius: 50%;  "/>
                </div>
                <div class="col s8 " style="padding: 0.5rem !important;margin-top: 2%;">
                  <a href="#" class="btn waves-effect waves-light " style="" href="#dummy_avatar_modal">
                    <i class="material-icons" style="vertical-align: middle;">cloud_upload</i>
                    Select
                  </a>
                </div>

              </div>

            </div>
          </div>
          <div class="col s12">
            <div class="card z-depth-2">

              <div class="card-content">
                @include('auth.partials.siteuser_contact')
              </div>

            </div>

          </div>
          <div class="col s12">
            <button type="submit" class="btn wave-effect right">Save and Continue</button>
          </div>
        </div>
      </div><!-- end row -->

    </form>
  </div> <!--  big container -->


<!-- <modal :id="'dummy_avatar_modal'">
    <template slot="header">
        Choose Image From Collection
    </template>
    <template slot="body" class="user_img_modal">
        <dummy-avatar :modalid="'dummy_avatar_modal'">
        </dummy-avatar>
    </template>
    <template slot="footer">
       <button type="button" class="btn btn-info">Save</button>
   </template>
 </modal> -->
</div>


</profile-wizard>










<!--

medical practice
______________________________
Ein number
Provider organization name
contact person
phone number
email
save button

Doctor
___________________________
Personal information
*****************
Username
First name
middle name
Last name
select speciality
select gender

Contact info
*************
Contact number
secondary email
save button




first name
middle name
last name
date of birth
Address
 -->