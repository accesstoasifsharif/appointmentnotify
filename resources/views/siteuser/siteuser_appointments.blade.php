@extends('siteuser.layouts.master')

@section('content')




<appointments inline-template>

	<div class="row" >

		<div class="row">
			<div class="col s1"></div>
			<div class="col s10 "  >
				<context-help>
					@component('common/partials.help_text')
					@slot('visible_text')
					The Appointment Dashboard is a simple view of all your appointments: upcoming and past, as well as some actions (called "Requests")


					@endslot
					@slot('hidden_text')
					on appointments. The Dashboard offers an intuitive interface that let's you manage appointments and place requests for appointments with your care Providers.
					@endslot
					@endcomponent
				</context-help>
			</div>
		</div>

		<div class="col s10 appointments_tab" >
			<ul-tabs>
				<tab-component :href="'#s_appointments'">Appointments</tab-component>
				<tab-component :href="'#s_requests'" >Requests</tab-component>
			</ul-tabs>
		</div>
		<div class="col s2" >
			<a class="btn right tooltipped" href="#appointments_read" data-position="left" data-delay="50" data-tooltip="Here's two minutes read to get more familiar with appointments dashboard."><i class="material-icons " style="font-size: 34px;">lightbulb_outline</i></a>
		</div>

		<div id="s_appointments" class="col s12 main-tab_content" >
			<div class="tabs-vertical " >
				<div class="col s4 m3 l2 inner_tab_col"  >
					<ul class="tabs">
						<li class="tab">
							<a class="waves-effect waves-cyan active tooltipped" href="#s_upcoming" data-position="bottom" data-delay="50" data-tooltip="This section lists all the upcoming appointments with your Providers.">Upcoming</a>
						</li>
						<li class="tab">
							<a class="waves-effect waves-cyan tooltipped" href="#s_past" data-position="bottom" data-delay="50" data-tooltip="This section lists all the past appointments with your Providers.">Past </a>
						</li>

					</ul>
				</div>
				<div class="col s8 m9 l10" >
					<div id="s_upcoming" class="tab-content">

						@include('siteuser.partials.up_coming_appointments')
					</div>
					<div id="s_past" class="tab-content">

						@include('siteuser.partials.past_appointments')
					</div>
				</div>
			</div>
		</div>
		<div id="s_requests" class="col s12 main-tab_content" >

			<div class="tabs-vertical " >
				<div class="col s4 m3 l2 inner_tab_col" >
					<ul class="tabs">
						<li class="tab">
							<a class="waves-effect waves-cyan active " href="#s_reschedule"><i class="zmdi zmdi-apps"></i>Reschedule</a>
						</li>
						<li class="tab">
							<a class="waves-effect waves-cyan" href="#s_cancel"><i class="zmdi zmdi-email"></i>Cancel</a>
						</li>
						<li class="tab">
							<a class="waves-effect waves-cyan" href="#s_reserve"><i class="zmdi zmdi-code"></i>Reserve</a>
						</li>
						<li class="tab">
							<a class="waves-effect waves-cyan" href="#s_shortCallList"><i class="zmdi zmdi-code"></i>Short Call List</a>
						</li>
					</ul>
				</div>
				<div class="col s8 m9 l10" >
					<div id="s_reschedule" class="tab-content">
						<reschedule-appointments inline-template>
							<div>
								@include('siteuser.partials.reschedule_appointments')
							</div>
						</reschedule-appointments>
					</div>
					<div id="s_cancel" class="tab-content">
						<cancel-appointments inline-template>
							<div>

								@include('siteuser.partials.cancelled_appointments')


							</div>
						</cancel-appointments>
					</div>
					<div id="s_reserve" class="tab-content">

						<reserve-appointments inline-template>
							<div>
								@include('siteuser.partials.reserve_appointments')
							</div>
						</reserve-appointments>
					</div>
					<div id="s_shortCallList" class="tab-content">

						@include('siteuser.partials.short_call_list')

					</div>
				</div>
			</div>
		</div>
	</div>
</appointments>


<!-- up past_appointments_note modal -->
<materialize-modal :id="'appointments_read'" :class="'small_modal'" >
	<template slot="modal-header">
		<materialize-modal-header >
			<template slot="header-title">
				Context-Sensitive Help
			</template>

		</materialize-modal-header>
	</template>
	<template slot="modal-body">

		<div class="row">
			<div class="col s1"></div>
			<div class="col s10" >
				<div class="card help-text" >
					<div class="card-content">
						<span class="card-title">Appointments</span>
						<p>The Appointments tab lets you manage your appointments. You can add new appointments, edit existing appointments and also view all your past appointments. You can choose to view the appointments by your care Providers or simply as a list of all your appointments across various care Providers.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s1"></div>
			<div class="col s5" >
				<div class="card help-text" >
					<div class="card-content">
						<span class="card-title">Upcoming</span>
						<p>This section lists all the <b>upcoming appointments</b>	 with your Providers.</p>
					</div>
				</div>
			</div>
			<div class="col s5" >
				<div class="card help-text" >
					<div class="card-content">
						<span class="card-title">Past</span>
						<p>This section lists all the <b>past appointments</b> with your Providers.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s1"></div>
			<div class="col s10" >
				<div class="card help-text" >
					<div class="card-content">
						<span class="card-title">Requests</span>
						<p>The Requests tab puts you in-control by letting you take certain actions on appointments. You can initiate Requests with your Providers to: Reschedule existing appointments, Cancel existing appointments, or Reserve new appointments that are available on the Platform. When you submit a Request, that Request is submitted by the Platform to the relevant care Provider on your behalf. Your care Provider then has the ability to confirm or reject your Request. Your Platform account will maintain the status of all your Requests and will notify you when a decision has been made on a Request that was submitted to your care Provider.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s1"></div>
			<div class="col s5" >
				<div class="card help-text" >
					<div class="card-content">
						<span class="card-title">Reschedule</span>
						<p>This section lists all the <b>requests for rescheduling existing appointments</b> with your care Providers.</p>
					</div>
				</div>
			</div>
			<div class="col s5" >
				<div class="card help-text" >
					<div class="card-content">
						<span class="card-title">Cancel</span>
						<p>This section lists all the <b>requests for canceling existing appointments</b> with your care Providers.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s1"></div>
			<div class="col s5" >
				<div class="card help-text" >
					<div class="card-content">
						<span class="card-title">Reserve</span>
						<p>This section lists all the <b>requests for reserving new appointments</b> with your care Providers.

						</p>
					</div>
				</div>
			</div>
			<div class="col s5" >
				<div class="card help-text" >
					<div class="card-content">
						<span class="card-title">Short Call List</span>
						<p>This section lists all the <b>requests for getting added to the Short Call List</b> of your care Providers.</p>
					</div>
				</div>
			</div>
		</div>





	</template>
	<template slot="modal-footer">
		<!-- 	<button class="btn waves-effect waves-light " @click="saveNote(appointment_id)">Save</button> -->
	</template>
</materialize-modal>
@endsection
