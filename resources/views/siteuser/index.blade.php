@extends('siteuser.layouts.master')

@section('content')
<siteuser-dashboard inline-template>
	<div class="row">
		<div class="col s12 m12 l3">
			<div class="col s12 m12 l12">
				<ul class="side-nav" style="transform: translateX(0px); position: relative; margin-top: 35px; padding-bottom: 10px; width: 100%;">
					<li>
						<div class="user-view">
							<div class="background">
								<img src="{{asset ('storage/images/common/img-bg/user-bg.jpg')}}">
							</div>
							<a href="#!user"><img class="circle" src="{{asset ('storage/images/common/dummy_avatar/defaultMale.png')}}"></a>
							<a href="#!name"><span class="white-text name">John Doe</span></a>
							<a href="#!email"><span class="white-text email">jdandturk@gmail.com</span></a>
						</div>
					</li>
					<a href="/siteuser/{{$selected_perspective_slug}}/accountsetting" class="btn-floating btn-move-up waves-effect waves-light darken-2 right tooltipped" data-position="top" data-delay="10" data-tooltip="Edit your profile info" style="position: relative; top: -30px; right: 0px; margin-right: 10px !important; z-index: 1;">
						<i class="material-icons">edit</i>
					</a>
					<li style="padding-left: 30px;">
						<i class="material-icons">perm_phone_msg</i>
						<i style="position: relative; top: -6px;">+1 (612) 222 8989</i>
					</li>
					<li style="padding-left: 30px;">
						<i class="material-icons" style="">event</i>
						<i style="position: relative; top: -6px;">23 sep, 1990</i>
					</li>
					<li><div class="divider"></div></li>
					<li><a class="subheader">Settings</a></li>
					<a href="/siteuser/{{$selected_perspective_slug}}/accountsetting" class="btn-floating btn-move-up waves-effect waves-light darken-2 right tooltipped" data-position="top" data-delay="10" data-tooltip="Edit your settings" style="position: relative; top: -35px; right: 0px; margin-right: 10px !important;">
						<i class="material-icons">settings</i>
					</a>
					<li>
						<a class="waves-effect" href="#!">Manage your platfrom settings.</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="col s12 m12 l9">
			<div class="col s12 m12 l12">
				<div class="col s12 m8 l8">
					<div class="card" style="background: #fff; min-height: 50px; box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2); position: relative; margin-bottom: 30px; border-radius: 2px;">
						<div class="header bg-red" style="color: #fff;padding: 20px; position: relative; background-color: #00bd86 !important;">
							<h2 style="margin: 0; font-size: 18px; font-weight: normal;">
								Appointments
							</h2>
							<a href="/siteuser/{{$selected_perspective_slug}}/appointments" class="dropdown-button tooltipped" data-position="top" data-delay="10" data-tooltip="Go to Appointments Tab" style="position: absolute; top: 15px; right: 15px; list-style: none; margin-right: -5px;">
								<i class="material-icons">open_in_new</i>
							</a>
						</div>
						<div class="body" style="padding: 5px; ">
							The section is a simple view of all your appointments: upcoming and past, as well as some actions (called "Requests") on appointments.
							<div class="card-tabs">
								<ul class="tabs tabs-fixed-width">
									<li class="tab"><a class="active" href="#test4">Appointments</a></li>
									<li class="tab"><a href="#test5">Requests</a></li>
								</ul>
							</div>
							<div class="card-content grey lighten-4">
								<div id="test4" class="row" style="margin-bottom: 0px;">
									<div class="col s6">
										<a href="#">Upcoming Appointments</a>
									</div>
									<div class="col s6">
										<a href="#">Past Appointments</a>
									</div>
								</div>
								<div id="test5" class="row" style="margin-bottom: 0px;">
									<div class="col s3">
										<a href="#">Reschedule</a>
									</div>
									<div class="col s3">
										<a href="#">Resrve</a>
									</div>
									<div class="col s3">
										<a href="#">Cancel</a>
									</div>
									<div class="col s3">
										<a href="#">Shortcall list</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col s12 m4 l4">
					<p style="font-size: 10px; color: grey;">Your last login was on Wed 27 Sep 2017 at 11:30 PM.</p>
					<div class="card" style="background: #fff; min-height: 50px; box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2); position: relative; margin-bottom: 30px; border-radius: 2px;">
						<div class="header bg-red" style="color: #fff;padding: 20px; position: relative; background-color: #00BCD4 !important">
							<h2 style="margin: 0; font-size: 18px; font-weight: normal;">
								Calendar
							</h2>
							<a href="/siteuser/{{$selected_perspective_slug}}/calendar" class="dropdown-button tooltipped" data-position="top" data-delay="10" data-tooltip="Go to Calendar Dashboard" style="position: absolute; top: 15px; right: 15px; list-style: none; margin-right: -5px;">
								<i class="material-icons">open_in_new</i>
							</a>
						</div>
						<div class="body" style="padding: 5px; ">
							The Calendar Dashboard is a calendar view of all your appointments: upcoming and past. The Dashboard offers viewing your appointments in a given month, week or day of the week.
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 m12 l12">
				<div class="col s12 m6 l6">
					<div class="card" style="background: #fff; min-height: 50px; box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2); position: relative; margin-bottom: 30px; border-radius: 2px;">
						<div class="header bg-red" style="color: #fff;padding: 20px; position: relative; background-color: #FF9800 !important;">
							<h2 style="margin: 0; font-size: 18px; font-weight: normal;">
								Messages
							</h2>
							<a href="/siteuser/{{$selected_perspective_slug}}/messages" class="dropdown-button tooltipped" data-position="top" data-delay="10" data-tooltip="Go to Messages Dashboard" style="position: absolute; top: 15px; right: 15px; list-style: none; margin-right: -5px;">
								<i class="material-icons">open_in_new</i>
							</a>
						</div>
						<div class="body" style="padding: 5px; ">
							The Messages section of the Platform puts you in control to securely chat with your Providers in real-time. All your messages stay intact so you can review the chat history if necessary.
						</div>
					</div>
				</div>
				<div class="col s12 m6 l6">
					<div class="card" style="background: #fff; min-height: 50px; box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2); position: relative; margin-bottom: 30px; border-radius: 2px;">
						<div class="header bg-red" style="color: #fff;padding: 20px; position: relative; background-color: #E91E63 !important;">
							<h2 style="margin: 0; font-size: 18px; font-weight: normal;">
								Providers
							</h2>
							<a href="/siteuser/{{$selected_perspective_slug}}/providers" class="dropdown-button tooltipped" data-position="top" data-delay="10" data-tooltip="Go to Providers tab" style="position: absolute; top: 15px; right: 15px; list-style: none; margin-right: -5px;">
								<i class="material-icons">open_in_new</i>
							</a>
						</div>
						<div class="body" style="padding: 5px; ">
							Your Providers section contains information about your frequently visited care Providers (Doctors, Specialists and other care providers etc.). You can also view the Provider Facilities and tag your Preferred Providers and Facilities.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</siteuser-dashboard>
@endsection
<!-- <div class="row">
	<div class="col s4">
		<div class="card">
			<div class="card-content">
				<span class="card-title">Card Title</span>
				<p>I am a very simple card. I am good at containing small bits of information.
					I am convenient because I require little markup to use effectively.
				</p>
			</div>
		</div>
	</div>
	<div class="col s4">
		<div class="card">
			<div class="card-content">
				<span class="card-title">Card Title</span>
				<p>I am a very simple card. I am good at containing small bits of information.
					I am convenient because I require little markup to use effectively.
				</p>
			</div>
		</div>
	</div>
	<div class="col s4">
		<div class="card">
			<div class="card-content">
				<span class="card-title">Appointments Timeline</span>
				<appointment-time-line></appointment-time-line>
			</div>
		</div>
	</div>
</div> -->








