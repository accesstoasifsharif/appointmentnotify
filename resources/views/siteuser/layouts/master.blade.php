<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Styles -->
	<!--  Styles For Vue.js Practice -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">


	<title>{{ APP_NAME }}</title>
	<script>
		window.AppointmentNotify = {!! json_encode([
			'csrfToken' => csrf_token(),
			'auth_user'=> Auth::User()->id,
			'urlPrefix'=> siteUserPrefix(),
			'allFeatures'=> $features,
			'selected_perspective_slug'=>'chuchu'

			]) !!};
		</script>
		<!-- Styles -->
		<link rel="stylesheet" type="text/css" href="{{asset('css/siteuser.css')}}">
		@yield('help_text_style')
	</head>
	<body>
		<div id="app_siteuser" class="content">
			<div class="content">
				<header-component inline-template>
					@include('siteuser.partials.header')
				</header-component>
				@include('common.partials.feature_header')

				<div class="container main_container">
					@yield('content')
				</div>
			</div>
			<special-actions inline-template>
				<div>
					<div class="fixed-action-btn vertical" >
						<a class="btn-floating red">
							<i class="material-icons">add_circle</i>
						</a>
						<ul style="margin-bottom: 20px;">
							<li style="">
								<a class="btn-floating blue tooltipped" href="#s_save_appointment_modal" data-position="left" data-delay="10" data-tooltip="Click Here To Add Appointment">
									<i class="material-icons">playlist_add</i>
								</a>
							</li>
							<li style="">
								<a class="btn-floating green tooltipped" data-position="left" data-delay="10" data-tooltip="Get Added To Short Call List" href="#short_call_list_modal">
									<i class="material-icons" >event</i>
								</a>
							</li>
						</ul>
					</div>
					@include('siteuser.partials.save_appointment_modal')
					@include('siteuser.partials.short_call_list_modal')
				</div>
			</special-actions>
		</div>
		@include('common.partials.footer')
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script> -->


		<script type="text/javascript" src="{{ asset('js/socket.io.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
		@yield('calendar_scrtipts')
		<script type="text/javascript" src="{{ asset('js/siteuser.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>
		@yield('help_text_scripts')

		<script type="text/javascript">


			$('.datepicker').pickadate({
		    selectMonths: true, // Creates a dropdown to control month
		    selectYears: 15, // Creates a dropdown of 15 years to control year,
		    today: 'Today',
		    clear: 'Clear',
		    close: 'Ok',
		    closeOnSelect: false // Close upon selecting a date,
		});
			$('.timepicker').pickatime({
		    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
		    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
		    twelvehour: false, // Use AM/PM or 24-hour format
		    donetext: 'OK', // text for done-button
		    cleartext: 'Clear', // text for clear-button
		    canceltext: 'Cancel', // Text for cancel-button
		    autoclose: false, // automatic close timepicker
		    ampmclickable: true, // make AM PM clickable
		    aftershow: function(){} //Function for after opening timepicker
		});
	</script>
</body>
</html>
