@extends('siteuser.layouts.master')

@section('content')
<providers inline-template>
	<div class="row">
		<div class="col s12 m12 l12">
			<context-help>
				@component('common/partials.help_text')
				@slot('visible_text')
				Your Providers section contains information about your frequently visited care Providers (Doctors, Specialists and other care providers etc.).
				@endslot
				@slot('hidden_text')
				You can also view the Provider Facilities and tag your Preferred Providers and Facilities. This is where you search Providers, add them to your Platform account and edit the list to maintain a customized view of all your care Providers.
				@endslot
				@endcomponent</context-help>
			</div>
			<div class="col s12 m12 l8 xl8">
				@include('siteuser.partials.addProviderTableView')
			</div>
			<div class="col s12 m12 l4 xl4">
				<a class="right btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="click here to add provider" href="#" @click="addProviderToCircle(1)">
					<!-- <a class="right btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="click here to add provider" href="#addProviderModal"> -->
					<i class="material-icons">add</i>
				</a>
				<div class="row" >
					<ul id="list">
						<li class='list-item' v-for = "doctor in allDoctorBySiteUserId">
							<circular-view>
								<template slot="list_data">
									<image-view :target-id="doctor.id" :target-model="doctor.model_identifier" :img-class="'circle responsive-img'" @click="singleDoctorInfo(doctor.id)"></image-view>
								</template>
							</circular-view>
						</li>
					</ul>
				</div>
			</div>

			<materialize-modal :id="'addProviderModal'">
				<template slot="modal-header">
					<materialize-modal-header>
						<template slot="header-title">
							Add Provider
						</template>
					</materialize-modal-header>
				</template>
				<template slot="modal-body">
					@include('siteuser.partials.addProvider')
				<!-- <tabular-view :searchable=true :title="allDoctorTitle" :columns="allDoctorColumns" :api="allDoctorApi" :buttons="allDoctorButtons" @added="addProvider">
			</tabular-view> -->
		</template>
	</materialize-modal>

	<!-- Modal For Single Doctor Info-->
	<materialize-modal :id="'doctor_info'">
		<template slot="modal-header">
			<materialize-modal-header>
				<template slot="header-image">
					<image-view :target-id="singleDoctorInfoById.doctor_id" :target-model="singleDoctorInfoById.model_identifier" :img-class="'circle responsive-img'"></image-view>
				</template>
				<template slot="header-title">
					@{{singleDoctorInfoById.first_name}}
					@{{singleDoctorInfoById.middle_name}}
					@{{singleDoctorInfoById.last_name}}
				</template>
				<template slot="header-speciality-name">
					@{{singleDoctorInfoById.specialty_name}}
				</template>
			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<div class="nav-content feature_header center">
				<ul class="tabs tabs-transparent main_tabs_ul" style="background-color: #FAFAFA !important;">
					<li class="tab main_tabs_li">
						<a  class="active z-depth-2" href="#doctorDetail">Personal Information</a>
					</li>
					<li class="tab main_tabs_li">
						<a class="z-depth-2" href="#doctorFacilityDetail">Facilities Detail</a>
					</li>
				</ul>
			</div>
			<div class="row">
				<div class="col s12" id="doctorDetail">
					@include('siteuser.partials.singleDoctorInfo')
				</div>
				<div class="col s12" id="doctorFacilityDetail">
					@include('siteuser.partials.singleDoctorFacilities')
				</div>
			</div>
		</template>
	</materialize-modal>

</div>
</providers>
<!--
		<materialize-modal :id="'delete_doctor_from_circle'">
			<template slot="modal-header">
				<materialize-modal-header>
					<template slot="header-title">
						Confirmation
					</template>
				</materialize-modal-header>
			</template>
			<template slot="modal-body">
				<h5 class="center">
					Are you sure to delete <code>Dr. Glenda Santa Maria</code> from your circle??
				</h5>
			</template>
			<template slot="modal-footer">
				<button type="button" class="btn waves-effect waves-light">Yes</button>
				<button type="button" class="btn waves-effect waves-light">No</button>
			</template>

		</materialize-modal> -->






		@endsection

		@section('help_text_scripts')
		<script type="text/javascript">

	// var list = $("#list");

	// var updateLayout = function(listItems){
	// 	for(var i = 0; i < listItems.length; i ++){
	// 		var offsetAngle = 360 / listItems.length;
	// 		var rotateAngle = offsetAngle * i;
	// 		$(listItems[i]).css("transform", "rotate(" + rotateAngle + "deg) translate(0, -200px) rotate(-" + rotateAngle + "deg)")
	// 		};
	// };

	// $(document).on("click", "#add-item", function(){
	// 	var listItem = $("<li class='list-item'>Things go here<button class='remove-item'>Remove</button></li>");
	// 	list.append(listItem);
	// 	var listItems = $(".list-item");
	// 	updateLayout(listItems);
	// });

	// $(document).on("click", ".remove-item", function(){
	// 	$(this).parent().remove();
	// 	var listItems = $(".list-item");
	// 	updateLayout(listItems);
	// });
</script>
@endsection


