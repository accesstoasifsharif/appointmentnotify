@extends('siteuser.layouts.master')
@section('content')
<siteuser-account-setting inline-template>
	<div class="row">
		<div class="col s3" style="background-color: none;">
			<div class="row" style="border-bottom: 1px solid rgba(160,160,160,0.2); margin-bottom: 20px; padding-bottom: 10px;">
				<div class="col s12 center">
					<img :src= "getAvatar()" class="picture-src" id="wizardPicturePreview" title="" style="height: 120px; width: 120px; border: solid 2px; border-radius: 50%; padding: 2px; margin-top: 10px;"/>
					<!-- <img src="{{asset('images/common/person-flat.png')}}" style="height: 120px; width: 120px; border: solid 2px; border-radius: 50%; padding: 2px; margin-top: 10px;"> -->
				</div>
				<div class="col s12 center">
					<div class="profile-usertitle-name" style="font-size: 18px;">
						Zubi
					</div>
				</div>
				<div class="col s12 center" style="padding: 0.5rem !important;">
					<a href="#siteuserImageModal" class="btn waves-effect waves-light red" style="">
						<i class="material-icons" style="vertical-align: middle;">person_outline</i>
						Select New Photo
					</a>
				</div>
			</div>
			<div class="tabs-vertical">
				<ul class="tabs" style="background-color: transparent;">
					<li class="tab" style="text-align: left;">
						<a class="waves-effect waves-light active" href="#profile_setting">
							<i class="material-icons" style="vertical-align: middle; color: #000;">person</i> Profile
						</a>
					</li>
					<li class="tab" style="text-align: left;">
						<a class="waves-effect waves-light" href="#security_setting">
							<i class="material-icons" style="vertical-align: middle; color: #000;">lock</i>
							Security
						</a>
					</li>
					<li class="tab" style="text-align: left;">
						<a class="waves-effect waves-light" href="#notification_setting">
							<i class="material-icons" style="vertical-align: middle; color: #000;">notifications</i>
							Notification
						</a>
					</li>
					<li class="tab" style="text-align: left;">
						<a class="waves-effect waves-light" href="#emergency_contact_setting">
							<i class="material-icons" style="vertical-align: middle; color: #000;">contact_phone</i>
							Emergency Contact</a>
						</li>
						<li class="tab" style="text-align: left;">
							<a class="waves-effect waves-light" href="#insurance_setting">
								<i class="material-icons" style="vertical-align: middle; color: #000;">beach_access</i>
								Insurance
							</a>
						</li>
						<li class="tab" style="text-align: left;">
							<a class="waves-effect waves-light" href="#hippa_consent_setting">
								<i class="material-icons" style="vertical-align: middle; color: #000;">lock</i>
								HIPAA Consent
							</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="col s8" style="margin-left: 30px;">
				<div id="profile_setting" class="tab-content">
					@include('siteuser.partials.profile_setting')
				</div>
				<div id="security_setting" class="tab-content">
					@include('siteuser.partials.security_setting')
				</div>
				<div id="notification_setting" class="tab-content">
					@include('siteuser.partials.notification_setting')
				</div>
				<div id="emergency_contact_setting" class="tab-content">
					@include('siteuser.partials.emergency_contact_setting')
				</div>
				<div id="insurance_setting" class="tab-content">
					@include('siteuser.partials.insurance_setting')
				</div>
				<div id="hippa_consent_setting" class="tab-content">
					@include('siteuser.partials.hippa_consent_setting')
				</div>
			</div>



			<materialize-modal :id="'siteuserImageModal'">
				<template slot="modal-header">
					<materialize-modal-header>
						<template slot="header-title">
							Please choose and image from the following
						</template>
					</materialize-modal-header>
				</template>
				<template slot="modal-body">
        <!-- <dummy-avatar :modalid="'dummy_avatar_modal'">
    </dummy-avatar> -->
</template>
</materialize-modal>








</div>
</siteuser-account-setting>
@endsection





