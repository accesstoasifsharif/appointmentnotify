@extends('siteuser.layouts.master')

@section('content')
<messages inline-template>
	<div>
		<!-- loading  icon -->
		<!-- <i  v-show="loading" class="material-icons" style="left : 50%;top : 50%;position : absolute;z-index : 101;">cached</i> -->
		<div class="row">
			<div class="col s1"></div>
			<div class="col s10 " style="margin-top: ;" >
				<context-help>
					@component('common/partials.help_text')
					@slot('visible_text')
					The Messages section of the Platform puts you in control to securely chat with your Providers in
					@endslot
					@slot('hidden_text')
					real-time. All your messages stay intact so you can review the chat history if necessary. This feature is bi-directional; so you could expect seeing messages initiated by your Providers as well!.
					@endslot
					@endcomponent
				</context-help>
			</div>
		</div>
		<div class="clearfix messages-container ">
			<div class="row">

				<div class="col s3" >
					<div class="people-list-header" style="border-bottom: white solid 2px;" >
						<div class="center-align" style="font-size: 16px;font-weight: bold;margin-top: 2%;">Provider's List</div>
						<div class="search">
							<div class="input-field">
								<input id="icon_prefix" type="text" v-model="searchText" >
								<label for="icon_prefix">Search by doctor facility name</label>
							</div>
						</div>
					</div>
					<div class="people-list" id="people-list" style="height: 300px;overflow-y: scroll;">

						<ul class="list">
							<li class="clearfix active" v-for="(doctor,index) in filteredDoctorsList"  data-chat="person2"  @click.prevent="seletedDoctor(doctor.site_user_id,doctor.doctor_id,doctor.site_user_identifier,doctor.doctor_identifier)" :class="{selected_doctor: doctor === activeDoctor}" @click="activeDoctor = doctor">
								<div class="row user-list">
									<div class="col s3 ">
										<image-view :target-id="doctor.doctor_id" :target-model="doctor.doctor_identifier" :img-class="'responsive-img table-image z-depth-2 center-align'"
										></image-view>
									</div>

									<div class="col s9">
										<span class="truncate">@{{doctor.full_name}}</span>
										<div class="status " v-if="doctor.message_preview">
											<span class="truncate chat-time">
												@{{doctor.message_preview.message}}
											</span>
										</div>
										<div class="about status" v-if="doctor.message_preview">
											<span class="chat-time">@{{doctor.message_preview.created_at | chat_date}}</span>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col s9" >

					<div class="chat ">

						<div class="chat-header" >
							<div class="row " >
								<div class="col s2"></div>
								<div class="col s8 center-align">
									<div class="col s3" style="margin-top:2%;">
										<image-view :target-id="doctorInfo.doctorDetail[0].doctor_id" :target-model="doctorInfo.doctorDetail[0].model_identifier" :img-class="'responsive-img table-image'">
										</image-view>
									</div>
									<div class="col s8" style="margin-top:1%;">
										<div class="chat-about ">
											<div class="chat-with">@{{doctorInfo.doctorDetail[0].first_name}} @{{doctorInfo.doctorDetail[0].middle_name}} @{{doctorInfo.doctorDetail[0].last_name}}</div>
											<span>@{{doctorInfo.doctorDetail[0].specialty_name}}</span>
										</div>
									</div>
								</div>
								<div class="col s2" style="margin-top:2%;">
									<a class="right" href="#" @click.prevent = "doctorDetailModal">
										<i class="material-icons">more_vert</i>
									</a>
								</div>
							</div>
						</div>

						<div class="chat-history" id=chat-messages-ul style="height: 255px;overflow-y: scroll;">
							<ul >
								<div v-for="message in conversationMessages" >
									<li class="clearfix" v-if="message.sender.id == sender_user_id">
										<div class="message-data align-right" >
											<span class="chat-time message-data-time" >@{{message.created_at | chat_date}}</span> &nbsp; &nbsp;
											<span class="message-data-name">@{{message.sender.name}}</span>
										</div>
										<div class="message other-message float-right z-depth-2">
											@{{message.message}}
										</div>
									</li>
									<li v-if="message.sender.id != sender_user_id">
										<div class="message-data">
											<span class="message-data-name">@{{message.sender.name}}</span>
											<span class="message-data-time">@{{message.created_at | chat_date}}</span>
										</div>
										<div class="message my-message z-depth-2">
											@{{message.message}}
										</div>
									</li>
								</div>
							</ul>
						</div> <!-- end chat-history -->

						<div class="chat-message clearfix">
							<div class="col s10">
								<div class="input-field col s12">
									<input type="text" id="textarea1" class="materialize-textarea" v-model="form.message_body"  @keydown="handleEnter">
									<label for="textarea1">Type Message Here</label>
								</div>
							</div>
							<div class="col s2">
								<button class="btn teal waves-effect waves-light left  " style="margin-top: 12%;"  ><i class="material-icons" @click.prevent = "onMessageSend">send</i></button>
							</div>
						</div> <!-- end chat-message -->
					</div> <!-- end chat -->
				</div>

			</div>
		</div> <!-- end container -->






		<materialize-modal :id="'messaging_doctor_detail'">
			<template slot="modal-header">
				<materialize-modal-header >
					<template slot="header-image">
						<image-view :target-id="doctorInfo.doctorDetail[0].doctor_id" :target-model="doctorInfo.doctorDetail[0].model_identifier" :img-class="'img-responsive'" >
						</image-view>
					</template>
					<template slot="header-title">
						<h5 >@{{doctorInfo.doctorDetail[0].first_name}} @{{doctorInfo.doctorDetail[0].middle_name}} @{{doctorInfo.doctorDetail[0].last_name}}</h5>

					</template>
					<template slot="header-speciality-name">
						<p>@{{doctorInfo.doctorDetail[0].specialty_name}}</p>
					</template>
				</materialize-modal-header>
			</template>
			<template slot="modal-body">
				<div class="col s12" >


					<div class="nav-content feature_header center">
						<ul class="tabs tabs-transparent main_tabs_ul" style="background-color: #FAFAFA !important;">
							<li class="tab main_tabs_li">
								<a  class="active z-depth-2" href="#doctorDetail">Personal Information</a>
							</li>
							<li class="tab main_tabs_li">
								<a class="z-depth-2" href="#doctorFacilityDetail">Facilities Detail</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col s12" id="doctorDetail">
							<div class="row">
								<div class="col s12 m12">
									<div class="card">
										<div class="card-content">
											<div class="row">
												<div class="col s3"></div>
												<div class="col s6">
													<table>
														<tr>
															<td><strong>Gender</strong></td>
															<td>@{{doctorInfo.doctorDetail[0].gender}}</td>
														</tr>
														<tr>
															<td><strong>Phone</strong></td>
															<td>@{{doctorInfo.doctorDetail[0].phone_number}}</td>
														</tr>
														<tr>
															<td><strong>Fax number</strong></td>
															<td>@{{doctorInfo.doctorDetail[0].fax_number}}</td>
														</tr>
														<tr>
															<td><strong>Email</strong></td>
															<td>@{{doctorInfo.doctorDetail[0].email}}</td>
														</tr>
														<tr>
															<td><strong>Url</strong></td>
															<td>@{{doctorInfo.doctorDetail[0].url}}</td>
														</tr>
													</table>
												</div>
												<div class="col s3"></div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="col s12" id="doctorFacilityDetail">
							@component('common.table')
							@slot('table_head')
							<th>Name</th>
							<th>Address</th>
							<th>Phone #</th>
							<th>Email</th>
							<th>Zip Code</th>

							@endslot
							@slot('table_data')
							<!-- @{{doctorInfo}} -->
							<tr v-for="facility in facilityInfo">
								<td v-text="facility.facility_name" style="color:#000;"></td>
								<td style="color:#000;">@{{facility.address_line_1}} @{{facility.address_line_2}} @{{facility.city_name}}</td>
								<td v-text="facility.phone_number" style="color:#000;"></td>
								<td v-text="facility.email" style="color:#000;"></td>
								<td v-text="facility.zip_code" style="color:#000;"></td>

							</tr>
							@endslot
							@endcomponent

						</div>
					</div>

				</div>
			</template>
		</materialize-modal>

	</div>
</messages>
@endsection