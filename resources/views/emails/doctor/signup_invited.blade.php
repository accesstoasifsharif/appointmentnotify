@component('mail::message')
# Dear MD,



@component('mail::panel')
You have been  invited by the <strong>Provider</strong>
Please accept the invitation to sign up with <strong>{{ config('app.name') }}</strong>
@endcomponent

@component('mail::button', ['url' =>  $inviteUrl ])
Accept Invite
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
