@extends('layouts.app')

@section('content')
<login-form inline-template>
    <div >
        <div class="col l4">

        </div>
        <div class="auth_main">
            <div class="col l4 z-depth-2 form-col  main_container"  >

                <div class="main " >
                    <div class="form-top    text-center"  >
                        <div class="col l6 z-depth-2 logo-text" >

                            <h5 ><i class="material-icons">fingerprint</i> Login</h5>
                        </div>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="input-field col s12">
                            <i class="material-icons prefix">account_circle</></i>
                            <input id="icon_prefix"  type="text" name="email" value="{{ old('email') }}" v-model="email" >
                            <label for="icon_prefix">Username</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">vpn_key</i>
                            <input id="icon_prefix_login" type="password" name="password" v-model="password" >
                            <label for="icon_prefix_login">Password</label>
                        </div>
                        <div class=" col s12">
                            <p>
                                <input type="checkbox" id="test5" name="remember" {{ old('remember') ? 'checked' : '' }} />
                                <label for="test5">Remember me</label>
                            </p>
                        </div>
                        <div class="input-field col s12">
                            <button type="submit" class="btn col s12 waves-effect waves-light">     Login
                            </button>
                            <div v-text="message"></div>

                            <!-- v-on:click.stop.prevent-->
                         <!--    <button  type="submit" @click.prevent="validateLoginCredentials">Validate User</button>
                            <span class="help is-danger" v-text="errors.get('email')"></span>
                            <span class="help is-danger" v-text="errors.get('password')"></span> -->


                        </div>
                    </form>
                    <div class="input-field forget-password col s12">
                        <a href="{{ route('password.request') }}">Forget Your Password?</a>
                    </div>
                    <div class="input-field col s12">
                        <div class="divider"></div>
                    </div>
                    <div class="input-field col s12">
                        <h6 class="center-align">Don't have an account yet?</h6>
                    </div>

                    <div class="col s12 center-align">

                        <a class="centre-align waves-effect waves-light btn" href="/register">Sign Up Now</a>

                    </div>
                    <div class="col s4 ">
                    </div>
                    <div class="row">

                    </div>

                </div>


            </div>
        </div>
    </div>
</login-form>

@endsection
<!--  <div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<div class="panel panel-default">
<div class="panel-heading">Login</div>
<div class="panel-body">
<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
{{ csrf_field() }}

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
<label for="email" class="col-md-4 control-label">E-Mail Address</label>

<div class="col-md-6">
<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"  autofocus>

@if ($errors->has('email'))
<span class="help-block">
<strong>{{ $errors->first('email') }}</strong>
</span>
@endif
</div>
</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
<label for="password" class="col-md-4 control-label">Password</label>

<div class="col-md-6">
<input id="password" type="password" class="form-control" name="password" >

@if ($errors->has('password'))
<span class="help-block">
<strong>{{ $errors->first('password') }}</strong>
</span>
@endif
</div>
</div>

<div class="form-group">
<div class="col-md-6 col-md-offset-4">
<div class="checkbox">
<label>
<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
</label>
</div>
</div>
</div>

<div class="form-group">
<div class="col-md-8 col-md-offset-4">
<button type="submit" class="btn btn-primary">
Login
</button>

<a class="btn btn-link" href="{{ route('password.request') }}">
Forgot Your Password?
</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>  -->