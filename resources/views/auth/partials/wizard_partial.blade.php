<div class="col s12 " >
    <h5 class="header center-align row">Setup Your Profile</h5>
    <ul class="tabs wizard-tabs" >
        <li class="tab col s4 tabs-radius"  ><a class="active" href="#test1">Profile</a></li>
        <li class="tab col s4 tabs-radius"><a  href="#test2">Contact</a></li>
        <li class="tab col s4 tabs-radius"><a href="#test3">Billing</a></li>
        <div class="indicator green" style="z-index:2;"></div>
    </ul>
    <div id="test1" class="col s12">
      <h5 class="light">Profile</h5>
      <section>
       
          <div class="row">
            <form class="col s12">
              <div class="row">
                <div class="input-field col s6">
                  <i class="material-icons prefix">face</i>
                  <input id="icon_prefix" id="icon_perfix1" type="text" class="validate">
                  <label for="icon_perfix1">First Name</label>
              </div>
              <div class="input-field col s6">
                  <i class="material-icons prefix">assignment_ind</i>
                  <input id="icon_telephone" type="tel" class="validate">
                  <label for="icon_telephone">Username</label>
              </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <i class="material-icons prefix">perm_identity</i>
              <input id="icon_prefix2" type="text" class="validate">
              <label for="icon_prefix2">Middle Name</label>
          </div>
          <div class="input-field col s6">
              <i class="material-icons prefix">today</i>
              <input type="date" id="icon_telephone2" class="datepicker" >
              <label for="icon_telephone2">Date of Birth</label>
          </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">account_circle</i>
          <input id="icon_prefix3" type="text" class="validate">
          <label for="icon_prefix3">Last Name</label>
      </div>
      <div class="input-field col s6">
        <label class="left" >Gender</label>
        <div class="  right" style="padding-right: 8%;height: auto;">
          <p class="left">
            <input class="with-gap right" name="group" type="radio" id="test5"  />
            <label for="test5">Male</label>
        </p>
        <p class="left">
            <input class="with-gap right" name="group" type="radio" id="test8"  />
            <label for="test8">Female</label>
        </p>
        <p class="left">
            <input class="with-gap right" name="group" type="radio" id="test10"  />
            <label for="test10">Other</label>
        </p>
    </div>
</div>

</div>
</form>
</div>
</section>
</div>
<div id="test2" class="col s12">
    <p class="center-align header">We Are Just One Step Away To Complete The Process.</p>
    <h5 class="light">Account</h5>
    <section>
        <div class="row">
            <form class="col s12">
              <div class="row">
                <div class="input-field col s6">
                  <i class="material-icons prefix">stay_primary_portrait</i>
                  <input id="icon_prefix" type="text" class="validate">
                  <label for="icon_prefix">Mobile Number</label>
              </div>
              <div class="input-field col s6">
                  <i class="material-icons prefix">phone</i>
                  <input id="icon_telephone" type="tel" class="validate">
                  <label for="icon_telephone">Landline Number</label>
              </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <i class="material-icons prefix">phone</i>
              <input id="icon_prefix" type="text" class="validate">
              <label for="icon_prefix">Office Contact Number</label>
          </div>
          <div class="input-field col s6">
              <i class="material-icons prefix">email</i>
              <input id="icon_telephone" type="tel" class="validate">
              <label for="icon_telephone">Email</label>
          </div>
      </div>
  </form>
</div>
</section>
</div>
<div id="test3" class="col s12">
 <h5 class="light">Billing</h5>
 <section>
  <form class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">trending_up</i>
          <input id="line_1" type="text" class="validate">
          <label for="line_1">Line 1</label>
      </div>
      
  </div>
  <div class="row">
    <div class="input-field col s12">
      <i class="material-icons prefix">trending_up</i>
      <input id="line_2" type="text" class="validate">
      <label for="line_2">Line 2</label>
  </div>
</div>
<div class="row">
    <div class="input-field col s4">
      <i class="material-icons prefix">phone</i>
      <input id="city" type="text" class="validate">
      <label for="city">business</label>
  </div>
  <div class="input-field col s4">
      <i class="material-icons prefix">textsms</i>
      <input type="text" id="autocomplete-input" class="autocomplete">
      <label for="autocomplete-input">Autocomplete</label>
  </div>
  <div class="input-field col s4">
      <i class="material-icons prefix">vpn_key</i>
      <input id="zip" type="text" class="validate">
      <label for="zip">Zip</label>
  </div>
</div>
</form>
</section>
</div>
