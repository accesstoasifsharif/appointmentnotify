
<div class="col-sm-10 col-sm-offset-1">
    <div class="row">
        <div class="input-field col s12">
            <i class="material-icons prefix">stay_primary_portrait</i>
            <!--  <input id="icon_prefix" type="text" class="validate" name="phone_number" v-model = 'form.phone_number'> -->
            <masked-input
            id="icon_prefix"
            type="tel"
            name="phone_number"
            class="validate"
            v-model="form.phone_number"
            :mask="['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]"
            :guide="false"
            placeholderChar="#">
        </masked-input>
        <label for="icon_prefix">Mobile</label>
        <span class="help red" v-if="form.errors.has('phone_number')" v-text="form.errors.get('phone_number')"></span>
    </div>
    <div class="input-field col s12">
        <i class="material-icons prefix">phone</i>
        <masked-input
        id="icon_telephone"
        type="tel"
        name="fax_number"
        class="validate"
        v-model="form.fax_number"
        :mask="['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]"
        :guide="true"
        placeholderChar="#">
    </masked-input>
    <!--  <input id="icon_telephone" type="tel" class="validate" name="fax_number" v-model = 'form.fax_number'> -->
    <label for="icon_telephone">Fax</label>
    <span class="help red" v-if="form.errors.has('fax_number')" v-text="form.errors.get('fax_number')"></span>
</div>


<div class="input-field col s12">
    <i class="material-icons prefix">email</i>
    <input id="icon_telephone" type="tel" class="validate" name="email" v-model = 'form.email'>
    <label for="icon_telephone">Email</label>
    <span class="help red" v-if="form.errors.has('email')" v-text="form.errors.get('email')"></span>
</div>
</div>
</div>