<nav class="nav-extended">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">
            <img src="/images/logo/an.png" alt="AppointmentNotify">
        </a>

        <ul id="nav-mobile" class="row hide-on-med-and-down right">
            @if(Auth::guest())
            <li class="right"><a  class="waves-effect waves-light btn" href="{{ route('login') }}">Login</a></li>
            <li class="right"><a  class="waves-effect waves-light btn" href="{{ route('register') }}">Register</a></li>
            @else
            <li class="col s12">
                <div class="right chip dropdown-button btn nav_chip" data-activates='main_user_dropdown'>

                   <img src="/images/common/person-flat.png" alt="Contact Person">
                   User_name
               </div>
           </li>
           <li>
            <ul id='main_user_dropdown' class='dropdown-content'>
                <li> <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="material-icons">power_settings_new</i>Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>
            </ul>
        </li>
        @endif
    </ul>
</div>
</nav>

@include('common.partials.headertabsmobileview')

