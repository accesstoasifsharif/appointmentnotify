

<div class="row">

  <div class="input-field col s6">
    <i class="material-icons prefix">trending_up</i>
    <input id="address_line_1" type="text" class="validate" name="address_line_1" v-model = 'form.address_line_1'>
    <label for="address_line_1">Address Line 1</label>
    <span class="help red" v-if="form.errors.has('address_line_1')" v-text="form.errors.get('address_line_1')"></span>
  </div>



  <div class="input-field col s6">
    <i class="material-icons prefix">trending_up</i>
    <input id="address_line_2" type="text" class="validate" name="address_line_2" v-model = 'form.address_line_2'>
    <label for="address_line_2">Address Line 2</label>
    <span class="help red" v-if="form.errors.has('address_line_2')" v-text="form.errors.get('address_line_2')"></span>
  </div>


  <div class="input-field col s4">
    <i class="material-icons prefix">phone</i>
    <input id="city_name" type="text" class="validate" name="city_name" v-model = 'form.city_name'>
    <label for="city_name">City</label>
    <span class="help red" v-if="form.errors.has('city_name')" v-text="form.errors.get('city_name')"></span>
  </div>
  <div class="input-field col s4">
    <!-- <i class="material-icons prefix">textsms</i> -->
      <select id="state_postal" class="browser-default" v-model="form.state_id">
        <option v-for="state in states" :value="state.id" v-text = "state.postal_code">
            </option>
        </select>
    <!-- <input type="text" id="state_id" class="autocomplete" name="state_id" v-model = 'form.state_id'>
    <label for="state_id">State</label> -->
    <span class="help red" v-if="form.errors.has('state_id')" v-text="form.errors.get('state_id')"></span>
  </div>
  <div class="input-field col s4">
    <i class="material-icons prefix">vpn_key</i>

    <masked-input
    id="zip_code"
    type="text"
    name="zip_code"
    class="validate"
    v-model="form.zip_code"
    :mask="[/\d/, /\d/, /\d/, /\d/, /\d/]"
    :guide="false"
    ></masked-input>
    <!--  <input id="zip_code" type="text" class="validate" name="zip_code" v-model = 'form.zip_code'> -->


    <label for="zip_code">Zip</label>
    <span class="help red" v-if="form.errors.has('zip_code')" v-text="form.errors.get('zip_code')"></span>
  </div>
</div>
