
<div class="row">
    <div class="input-field col s4">
        <i class="material-icons prefix">face</i>
        <input id="icon_perfix1" name="first_name" type="text" class="validate" v-model = 'form.first_name' >
        <label for="icon_perfix1">First Name</label>
        <span class="help red" v-if="form.errors.has('first_name')" v-text="form.errors.get('first_name')"></span>

    </div>
    <div class="input-field col s4">
        <i class="material-icons prefix">perm_identity</i>
        <input id="icon_prefix2" type="text" class="validate" name="middle_name" v-model = 'form.middle_name' >
        <label for="icon_prefix2">Middle Name</label>
        <span class="help red" v-if="form.errors.has('middle_name')" v-text="form.errors.get('middle_name')"></span>
    </div>
    <div class="input-field col s4">
        <i class="material-icons prefix">account_circle</i>
        <input id="icon_prefix3" type="text" class="validate" name="last_name" v-model = 'form.last_name'>
        <label for="icon_prefix3">Last Name</label>
        <span class="help red" v-if="form.errors.has('last_name')" v-text="form.errors.get('last_name')"></span>
    </div>
    <div class="input-field col s4">
        <i class="material-icons prefix">assignment_ind</i>
        <input id="icon_telephone" type="tel" class="validate" name="psuedo_name" v-model = 'form.psuedo_name'>
        <label for="icon_telephone">username</label>
        <span class="help red" v-if="form.errors.has('psuedo_name')" v-text="form.errors.get('psuedo_name')"></span>
    </div>
    <div class="input-field col s4">
        <i class="material-icons prefix">today</i>
        <input type="date" id="icon_telephone2" class="datepicker" name="date_of_birth" v-model = 'form.date_of_birth'>
        <label for="icon_telephone2">Date of Birth</label>
        <span class="help red" v-if="form.errors.has('date_of_birth')" v-text="form.errors.get('date_of_birth')"></span>
    </div>
    <div class="input-field col s4">

        <div class="  right" style="padding-right: 3%;height: auto;">
            <p class="left">
                <!-- <input class="with-gap right" type="radio" id="test5" name="gender" value="male" @change.prevent="setGender('male')"> -->
                <input class="with-gap right" type="radio" id="test5" v-model="form.gender" name="gender" :value="'male'" @change.prevent="setGender('male')">
                <label for="test5">Male</label>
            </p>
            <p class="left">
                <input class="with-gap right" type="radio" id="test8" v-model="form.gender" name="gender" :value="'female'" @change.prevent="setGender('female')">
                <!-- <input class="with-gap right" type="radio" id="test8" name="gender" value="female"  @change.prevent="setGender('female')"> -->
                <label for="test8">Female</label>
            </p>
            <p class="left">
                <input class="with-gap right" type="radio" id="test10" name="gender" value="other" @change.prevent="setGender('other')">
                <label for="test10">Other</label>
            </p>
        </div>
        <span class="help red" v-if="form.errors.has('gender')" v-text="form.errors.get('gender')"></span>
    </div>
</div>





