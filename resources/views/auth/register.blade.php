@extends('layouts.app')

@section('content')

<register-form inline-template>
    <div>

        <div class="col l4"></div>
        <div class="auth_main">
            <div class="col l4 z-depth-2 form-col  main main_container" style="border-radius:4px;">
                <div class="main ">
                    <div class="form-top">
                        <div class="col l8 m8 s10 z-depth-2 logo-text" >
                            <h5 ><i class="material-icons person_add_icon">person_add</i>Register</h5>
                        </div>
                    </div>
                    <form role="form" method="POST" action="{{ route('register') }}" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
                        {{ csrf_field() }}

                        <div class="input-field col s12">
                            <i class="material-icons prefix">email</i>
                            <input id="icon_prefix_register" type="email"  name="email" value="{{ old('email') }}"  v-model="form.email">
                            <label for="icon_prefix_register">Email</label>
                            <span class="help red" v-if="form.errors.has('email')" v-text="form.errors.get('email')"></span>

                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">vpn_key</i>
                            <input id="icon_prefix_register2" type="password" name="password" v-model = 'form.password'>
                            <label for="icon_prefix_register2">Password</label>
                            <span class="help red" v-if="form.errors.has('password')" v-text="form.errors.get('password')"></span>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">repeat</i>
                            <input id="icon_prefix_register3" type="password" name="password_confirmation" v-model = 'form.password_confirmation'>
                            <label for="icon_prefix_register3">Retype Password</label>
                        </div>
                        <div class="col s12 center-align">

                            <input class="with-gap" name="user_type" type="radio" id="r2" value="0" v-model='form.user_type'>
                            <label for="r2">I am patient</label>

                            <input class="with-gap" name="user_type" type="radio" id="r1" value="1" v-model='form.user_type'>
                            <label for="r1">I am provider</label>

                            <span class="help red" v-if="form.errors.has('user_type')" v-text="form.errors.get('user_type')"></span>

                        </div>
                        <div class="input-field col s12">
                            <button type="submit" :disabled="form.errors.any()" class="btn col s12 waves-effect waves-light" >     Sign Up
                            </button>
                        </div>
                    </form>
                    <div class="input-field col s12">
                        <div class="divider"></div>
                    </div>
                    <div class="input-field col s12">
                        <h6 class="center-align">Already have an account?</h6>
                    </div>
                    <div class="col s12 center-align">
                        <a class="centre-align waves-effect waves-light btn" href="/login">
                            Sign In Now
                        </a>
                    </div>
                    <div class="row"></div>
                </div>
            </div>
        </div>
    </div>
</register-form>


@endsection

