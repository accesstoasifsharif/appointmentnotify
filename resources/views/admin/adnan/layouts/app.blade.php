<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link href="" rel="stylesheet"> -->


  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/admin.css')}}">
  @yield('css_libraries')
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name') }}</title>

  <!-- Styles -->

  <script>
    window.AppointmentNotify = {!! json_encode([
      'csrfToken' => csrf_token(),
      ]) !!};

    </script>
    <style>

      .is-loading { background: red;}
    </style>

  </head>


  <body id="auth-background">

   <div class="" id="app_adnan" >



     <div class="col s12" >
      @include("admin.partials.header")
    </div>

    <div class="container">

      @yield('content')
    </div>
  </div>

  <!-- Scripts -->

  <script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
  <!-- <script type="text/javascript" src="{{ asset('js/app.auth.js')}}"></script>   -->
  <script type="text/javascript" src="{{ asset('js/app.admin.adnan.js')}}"></script>
  @yield('js_libraries')
</body>
</html>

