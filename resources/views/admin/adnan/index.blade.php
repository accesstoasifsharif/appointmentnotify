@extends('admin.adnan.layouts.app')
@section('css_libraries')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.4/css/bulma.css">
@endsection

@section('content')
<!-- v-model="coupon"           @input="myFunction" -->

<coupon v-model="coupon"  ></coupon>


<button @click="cardModalClicked" >Show card Modal</button>
<modal-card>

	<template slot="header">This is header</template>
	<template slot="body">This is modal body</template>
	<template slot="footer">
		<a class="button is-success">Save changes</a>
		<a class="button">Cancel</a>
	</template>

</modal-card>
<button @click="showModal = true">Show bulma Modal</button>
<modal-bulma title="modal title"   v-if="showModal" @close="showModal = false"> this is  modal body</modal-bulma>

<message-bulma title="something" body="this is the body"></message-bulma>









<task></task>
<button :class="{ 'is-loading' : isLoading }" @click="toggleClass">Click Me</button>
<p class="well hello">Adnan</p>
<button @click="addName">Add Name</button>
<input type="text" v-model="newName" />
<ul>

	<li v-for="name in names" v-text="name"> </li>
</ul>
<h1>All  tasks</h1>
<ul>
	<!-- <li v-for="task  in tasks"  v-text="task.description"></li> -->
	<task-list></task-list>

</ul>
<h1>Incompleted  tasks</h1>
<ul>
	<li v-for="task  in inCompleteTasks"   v-text="task.description"></li>

</ul>


@endsection
