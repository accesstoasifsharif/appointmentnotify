@extends('admin.layouts.master')

@section('css_libraries')
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
@endsection


@section('content')
<admin-dashboard inline-template>
	<div class="row">
		<p class="well hello">@{{test}}</p>

		<zip-lat-long></zip-lat-long>
	</div>
</admin-dashboard>



@endsection

@section("js_libraries")
<!-- <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
@endsection