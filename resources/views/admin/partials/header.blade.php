

<nav class="nav-extended">
    <div class="nav-wrapper">


        <ul id="nav-mobile" class="row hide-on-med-and-down">
            @if(!Auth::guest())

            <li class="right" >
                <div class="right chip dropdown-button btn nav_chip" data-activates='main_user_dropdown' >
                    <img src="/images/common/person-flat.png" alt="Contact Person" >
                    {{Auth::User()->name}}
                </div>
            </li>
            <li>
                <ul id='main_user_dropdown' class='dropdown-content' >
                    <li> <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="material-icons">power_settings_new</i>Logout
                    </a>

                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form></li>
                </ul>
            </li>
            @endif
        </ul>
    </div>

</nav>



@include('common.partials.headertabsmobileview')
