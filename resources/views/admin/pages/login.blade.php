@extends('layouts.app')

@section('content')
<login-form inline-template>
    <div >
        <div class="col l4">

        </div>
        <div class="auth_main">
            <div class="col l4 z-depth-2 form-col  main_container"  >

                <div class="main " >
                    <div class="form-top    text-center"  >
                        <div class="col l6 z-depth-2 logo-text" >

                            <h5 ><i class="material-icons">fingerprint</i> Login</h5>
                        </div>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="input-field col s12">
                            <i class="material-icons prefix">account_circle</></i>
                            <input id="icon_prefix"  type="text" name="email" value="{{ old('email') }}" v-model="email" >
                            <label for="icon_prefix">Username</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">vpn_key</i>
                            <input id="icon_prefix_login" type="password" name="password" v-model="password" >
                            <label for="icon_prefix_login">Password</label>
                        </div>
                        <div class=" col s12">
                            <p>
                                <input type="checkbox" id="test5" name="remember" {{ old('remember') ? 'checked' : '' }} />
                                <label for="test5">Remember me</label>
                            </p>
                        </div>
                        <div class="input-field col s12">
                            <button type="submit" class="btn col s12 waves-effect waves-light">     Login
                            </button>
                            <div v-text="message"></div>

                            <!-- v-on:click.stop.prevent-->
                            <button  type="submit" @click.prevent="validateLoginCredentials">Validate User</button>
                            <span class="help is-danger" v-text="errors.get('email')"></span>
                            <span class="help is-danger" v-text="errors.get('password')"></span>


                        </div>
                    </form>
                    <div class="input-field forget-password col s12">
                        <a href="" href="{{ route('password.request') }}">Forget Your Password?</a>
                    </div>
                    <div class="input-field col s12">
                        <div class="divider"></div>
                    </div>
                    <div class="input-field col s12">
                        <h6 class="center-align">Don't have an account yet?</h6>
                    </div>

                    <div class="col s12 center-align">

                        <a class="centre-align waves-effect waves-light btn" href="/register">Sign Up Now</a>

                    </div>
                    <div class="col s4 ">
                    </div>
                    <div class="row">

                    </div>

                </div>


            </div>
        </div>
    </div>
</login-form>

@endsection
