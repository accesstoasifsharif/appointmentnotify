<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link href="" rel="stylesheet"> -->

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">


  <title>{{ config('app.name') }}</title>

  <!-- Styles -->

  <script>
    window.AppointmentNotify = {!! json_encode([
      'csrfToken' => csrf_token(),
      'redirectUrl'=>  ( !isset($redirectUrl) ? '/' : $redirectUrl)
      ]) !!};

    </script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">

  </head>
  <body>
    @include("auth.partials.header")
    <div class="container" id="app">

      <div class="row ">
        @yield('content')
      </div>

    </div>
    <!-- Scripts -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
    <script type="text/javascript" src="{{asset('js/common.js')}}"></script>

    @yield('vue-script')
    <!--  <script type="text/javascript" src="{{ asset('js/app.auth.js')}}"></script> -->


  </body>
  </html>

