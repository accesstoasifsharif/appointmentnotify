<!DOCTYPE html>
<html>
<head>
	<title>Access Token view</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}"> -->
	<script>
		window.AppointmentNotify = {!! json_encode([
			'csrfToken' => csrf_token(),
			]) !!};

		</script>

	</head>
	<body>
		<!-- @include("auth.partials.header") -->
		<div class="container " id="app">

			@yield('content')
		</div>

		<script type="text/javascript" src="{{asset('js/common.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/app.auth.js')}}"></script>

	</body>
	</html>