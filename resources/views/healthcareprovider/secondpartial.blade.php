@extends('healthcareprovider.layouts.master')

@section('content')
<ul id="nav-mobile" class="side-nav">
    <li class="logo"><a id="logo-container" href="/" class="brand-logo">
        <object id="front-page-logo" type="image/svg+xml" data="res/materialize.svg">Your browser does not support SVG</object></a></li>
    <li class="search">
      <div class="search-wrapper card">
        <input id="search"><i class="material-icons">search</i>
        <div class="search-results"></div>
      </div>
    </li>
    <li class="bold"><a href="" class="waves-effect waves-teal">About</a></li>
    <li class="bold"><a href="" class="waves-effect waves-teal">Getting Started</a></li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header  waves-effect waves-teal">CSS</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="">Color</a></li>
              <li><a href="">Grid</a></li>
              <li><a href="">Helpers</a></li>
              <li><a href="">Media</a></li>
              <li><a href="">Pulse</a></li>
              <li><a href="">Sass</a></li>
              <li><a href="">Shadow</a></li>
              <li><a href="">Table</a></li>
              <li><a href="">Transitions</a></li>
              <li><a href="">Typography</a></li>
            </ul>
          </div>
        </li>
        <li class="bold"><a class="collapsible-header active waves-effect waves-teal">Components</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="">Badges</a></li>
              <li class="active"><a href="">Buttons</a></li>
              <li><a href="">Breadcrumbs</a></li>
              <li><a href="">Cards</a></li>
              <li><a href="">Chips</a></li>
              <li><a href="">Collections</a></li>
              <li><a href="">Footer</a></li>
              <li><a href="">Forms</a></li>
              <li><a href="">Icons</a></li>
              <li><a href="">Navbar</a></li>
              <li><a href="">Pagination</a></li>
              <li><a href="">Preloader</a></li>
            </ul>
          </div>
        </li>
        <li class="bold"><a class="collapsible-header  waves-effect waves-teal">JavaScript</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="">Carousel</a></li>
              <li><a href="">Collapsible</a></li>
              <li><a href="">Dialogs</a></li>
              <li><a href="">Dropdown</a></li>
              <li><a href="">FeatureDiscovery</a></li>
              <li><a href="">Media</a></li>
              <li><a href="">Modals</a></li>
              <li><a href="">Parallax</a></li>
              <li><a href="">Pushpin</a></li>
              <li><a href="">ScrollFire</a></li>
              <li><a href="">Scrollspy</a></li>
              <li><a href="">SideNav</a></li>
              <li><a href="">Tabs</a></li>
              <li><a href="">Transitions</a></li>
              <li><a href="">Waves</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="bold"><a href="" class="waves-effect waves-teal">Mobile</a></li>
    <li class="bold"><a href="" class="waves-effect waves-teal">Showcase</a></li>
    <li class="bold"><a href="" class="waves-effect waves-teal">Themes<span class="new badge"></span></a></li>
  </ul>
@endsection
