@extends('healthcareprovider.layouts.master')
<style type="text/css">
	.img-size{
		height: 50px !important;
		width: 50px !important;
	}
</style>
@section('content')


<facility-view inline-template>
	<div class="row">
		<div class="col s7">
			@component('common.table')
			@slot('table_search')
			<div class="input-field col s12">
				<input id="icon_prefix" type="search" class="validate" v-model="searchText"  @keyup.enter="searchFacilityTableQuery" @keyup="searchFacilityTableQuery">
				<label for="icon_prefix">Search by facility name</label>
			</div>
			@endslot
			@slot('table_search_btn')
			<div class="input-field ">
				<button class="btn-floating waves-effect waves-light left" @click.prevent="searchFacilityTableQuery" > Go </button>
			</div>
			@endslot
			@slot('table_page_select')
			<div class="input-field col s12">
				<select class="browser-default"  @change="onFacilityTableLength">
					<option value=""  selected>Rows per page</option>
					<option v-for="option in perPageOptions" :value="option" :selected="option == pagination.per_page">
						@{{option}}
					</option>
				</select>
			</div>
			@endslot
			@slot('table_head')
			<th>Sr #</th>
			<th>Facility Name</th>
			<th>Zip Code</th>
			<th>Actions</th>
			@endslot
			@slot('table_data')
			<tr v-for="facility,index in allFacilities">
				<td >@{{index | serial}}</td>
				<td v-text="facility.facility_name"></td>
				<td v-text="facility.zip_code"></td>
				<td>
					<button class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="click here for info/edit." @click.prevent="editFacility(facility.facility_id)" ><i class="material-icons">info</i></button>
					<button class="btn-floating waves-effect waves-light red tooltipped" data-position="top" data-delay="10" data-tooltip="click here  to delete."  @click.prevent="deleteFacility(facility.facility_id)"><i class="material-icons">delete</i></button>
				</td>
			</tr>
			@endslot
			@slot('table_pagination')
			<ul class="pagination">
				<li v-if="pagination.current_page > 1">
					<a href="#" aria-label="Previous" @click.prevent="onFacilityTablePage(pagination.current_page - 1)">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
				<li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
					<a href="#" @click.prevent="onFacilityTablePage(page)">@{{ page }}</a>
				</li>
				<li v-if="pagination.current_page < pagination.last_page">
					<a href="#" aria-label="Next" @click.prevent="onFacilityTablePage(pagination.current_page + 1)">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</ul>
			@endslot
			@endcomponent
		</div>
		<div class="col s5">
			<div class="col s12 center-align">
				<h4>Facilities</h4>
				<button type="button" class="btn-floating right waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="click here to add facilities."  @click.prevent="addNewFacility"><i class="material-icons">add_circle</i></button>

			</div>
			<div class="row" >
				<div class="col s12" style="padding-left: 7%;">
					<ul id="list">
						<li class='list-item' v-for = "facility in allFacilitiesForCircularView">
							<circular-view>
								<template slot="list_data">
									<image-view :target-id="facility.facility_id" :target-model="facility.model_identifier" :img-class="'circle  img-size'"
									></image-view>
								</template>
							</circular-view>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<materialize-modal :id="'hcp_facility_addModal'">
			<template slot="modal-header">
				<materialize-modal-header >
					<template slot="header-title" >
						Facility Information
					</template>
				</materialize-modal-header>
			</template>
			<template slot="modal-body">
				<div class="row">
					<div class="col s12">
						<form >
							<div class="row">
								<div class="col s6 ">
									<div class="nav-content feature_header center">
										<ul class="tabs tabs-transparent main_tabs_ul" style="background-color: #FAFAFA !important;">
											<li class="tab main_tabs_li" @click="searchAdd">
												<a  class="active z-depth-2"  href="#by_search">Add By Search</a>
											</li>
											<li class="tab main_tabs_li" @click="manuallyAdd">
												<a class="z-depth-2"  href="#add_manually">Add Manually</a>
											</li>
										</ul>
									</div>
									<div class="col s12" id="by_search" style="padding-top: 15px;" >
										@include('healthcareprovider.partials.facilitybysearch')
									</div>
									<div class="col s12" id="add_manually" style="padding-top: 15px;">
										@include('healthcareprovider.partials.facility_form')
									</div>
								</div>
								<div class="col s6" v-if="add_by_search_facility">
									<h4 class="center-align">Business Hours</h4>
									<time-table :time-table = "timeTable" >
									</time-table>
								</div>


								<div class="col s6" v-if="!add_by_search_facility">
									@include('healthcareprovider.partials.facility_working_hours')
								</div>



							</form>
						</div>
					</div>

				</template>
				<template slot="modal-footer">
					<button type="button" class="btn waves-effect waves-light" @click.prevent = "onSubmit" v-bind:disabled="!isValidFacility" >Save</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>
				</template>
			</materialize-modal>



			<materialize-modal :id="'hcp_facility_editModal'">
				<template slot="modal-header">
					<materialize-modal-header >
						<template slot="header-title" >
							Facility Information
						</template>
					</materialize-modal-header>
				</template>
				<template slot="modal-body">
					<div class="row">
						<div class="col s12">
							<button v-if = "facilityEditFlag == 'true' && isEditable" class="btn-floating waves-effect waves-light tooltipped right" data-position="top" data-delay="10" data-tooltip="click here to edit info." @click="enableEditMode" ><i class="material-icons">edit</i></button>
							<form >
								<div class="row">
									<div class="col s6 ">
										<div class="col s12" id="add_manually" style="padding-top: 15px;">
											@include('healthcareprovider.partials.facility_form')
										</div>
									</div>

									<div class="col s6" v-if="editable">
										<h4 class="center-align">Working Hours</h4>
										<time-table :time-table = "timeTable" >
										</time-table>
									</div>

									<div class="col s6" v-if=" !editable" >

										@include('healthcareprovider.partials.facility_working_hours')
									</div>



								</form>
							</div>
						</div>

					</template>
					<template slot="modal-footer">
						<button type="button" class="btn waves-effect waves-light" @click.prevent = "onSubmit" v-bind:disabled="!isValidFacility" v-if="editable">Save</button>
						<a href="#!" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>
					</template>
				</materialize-modal>






			</div>
		</facility-view>
		@endsection



