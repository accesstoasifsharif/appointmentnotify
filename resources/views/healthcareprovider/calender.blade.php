@extends('healthcareprovider.layouts.master')

@section('content')
<calender-view inline-template>
	<div class="row">
		<div class="col s12 l12">
			@include('healthcareprovider.partials.appointment_header_dropdowns')
		</div>
		{{-- <calender-upcoming-appointment :hcp-facility-id='selected_hcp_facility_id' :doctor-id = 'selected_doctor_id' inline-template>
		<div class="row">
		</div>
	</calender-upcoming-appointment> --}}

	<div class="col s12 l12">
		<full-calendar ref="calendar" :default-view = "default_view" :header="default_header" :events="events" @event-selected="eventSelected"></full-calendar>
	</div>




	<materialize-modal :id="'upcoming_calendar_appointments_detail_modal'">
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-image">
					<image-view :target-id="doctor_detail.doctor_id" :target-model="doctor_detail.doctor_model_identifier" :img-class="'circle responsive-img'"></image-view>
				</template>
				<template slot="header-title">
					@{{doctor_detail.full_name}}
				</template>
				<template slot="header-speciality-name">
					@{{doctor_detail.specialty_name}}
				</template>
			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			@component('common.table')
			@slot('table_head')
			<th>Sr #</th>
			<th>Facility Name</th>
			<th>Reason</th>
			<th>Date</th>
			<th>Time</th>
			<th>Action</th>
			@endslot
			@slot('table_data')
			<tr v-for="(appointment,index) in aggregatedAppointmentsDetail">
				<td >@{{index | serial}}</td>
				<td v-text="appointment.facility_name"></td>
				<td v-text="appointment.reason_name"></td>
				<td v-text="">@{{appointment.date | an_date}}</td>
				<td>@{{appointment.start_time | an_time}}</td>
				<td>
					<button class="btn waves-effect waves-light tooltipped " data-position="left" data-delay="10" data-tooltip="Click here for reschedule appointment." @click.prevent="onRescheduleUpcomingAppointment(appointment.an_event_id)">Reschedule</button>
				</td>
			</tr>
			@endslot
			@endcomponent
		</template>
		<template slot="modal-footer">
		</template>
	</materialize-modal>





	<materialize-modal :id="'past_calendar_appointments_detail_modal'">
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-image">
					<image-view :target-id="doctor_detail.doctor_id" :target-model="doctor_detail.doctor_model_identifier" :img-class="'circle responsive-img'"></image-view>
				</template>
				<template slot="header-title">
					@{{doctor_detail.full_name}}
				</template>
				<template slot="header-speciality-name">
					@{{doctor_detail.specialty_name}}
				</template>
			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			@component('common.table')
			@slot('table_head')
			<th>Sr #</th>
			<th>Facility Name</th>
			<th>Reason</th>
			<th>Date</th>
			<th>Time</th>

			@endslot
			@slot('table_data')
			<tr v-for="(appointment,index) in aggregatedAppointmentsDetail">
				<td>@{{index | serial}}</td>
				<td v-text="appointment.facility_name"></td>
				<td v-text="appointment.reason_name"></td>
				<td>@{{appointment.date | an_date}}</td>
				<td>@{{appointment.start_time | an_time}}</td>
			</tr>
			@endslot
			@endcomponent
		</template>
		<template slot="modal-footer">
		</template>
	</materialize-modal>






	<materialize-modal :id="'reschedule_calendar_appointments_detail_modal'">
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-image">
					<image-view :target-id="doctor_detail.doctor_id" :target-model="doctor_detail.doctor_model_identifier" :img-class="'circle responsive-img'"></image-view>
				</template>
				<template slot="header-title">
					@{{doctor_detail.full_name}}
				</template>
				<template slot="header-speciality-name">
					@{{doctor_detail.specialty_name}}
				</template>
			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<div class="col s12">
				<div class="card col s4" v-for="(request,index) in aggregatedAppointmentsDetail">
					<div class="card-content ">
						<span class="card-title center-align left">@{{request.facility_name}}
						</span>
						<div class="col s12">
							<p class="center-align">Requested Date & Time</p>

							<div v-if="request.date_1 && request.time_1" >
								<div class="row">
									<p class="center-align"> <b>@{{request.date_1 | an_date}}</b><br> at <br><b>@{{request.time_1 | an_time}} </b></p>
								</div>
								<div class="row">
									<p class="center-align">
										<a  href="#"   class="tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to approve appointment."@click.prevent="onApproveRescheduleRequest(request.appointment_reschedule_id,request.date_1,request.time_1)">Approved
										</a>
									</p>
								</div>
								<hr>
							</div>
							<div v-if="request.date_2 && request.time_2">
								<div class="row">
									<p class="center-align"> <b>@{{request.date_2 | an_date}}</b><br> at <br><b>@{{request.time_2 | an_time}} </b></p>
								</div>
								<div class="row">
									<p class="center-align">
										<a  href="#"   class="tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to approve appointment."@click.prevent="onApproveRescheduleRequest(request.appointment_reschedule_id,request.date_2,request.time_2)">Approved
										</a>
									</p>
								</div>
								<hr>
							</div>

						</div>
						<div class="row" style="margin-top: 2%;">
							<div class="col s6">
								<p>
									<a href="#" class="tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to reschedule appointment."@click.prevent="onRescheduleRescheduleRequest(request.appointment_reschedule_id)">Reshedule
									</a>
								</p>
							</div>
							<div class="col s6">
								<p>
									<a href="#" class="tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to cancel appointment."@click.prevent="onCancel(request.appointment_reschedule_id)">Cancel</a>
								</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</template>
		<template slot="modal-footer">
		</template>
	</materialize-modal>








	<materialize-modal :id="'single_upcoming_calendar_appointment_detail_modal'">
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-image">
					<image-view :target-id="singleAppointmentDetail.doctor_id" :target-model="singleAppointmentDetail.doctor_model_identifier" :img-class="'circle responsive-img'"></image-view>
				</template>
				<template slot="header-title">
					@{{singleAppointmentDetail.full_name}}
				</template>
				<template slot="header-speciality-name">
					@{{singleAppointmentDetail.specialty_name}}
				</template>
			</materialize-modal-header>
		</template>


		<template slot="modal-body">
			<div class="row center-align" >
				<div class="col m3 s12"></div>
				<div class="col s12 m6">
					<div class="card">

						<div class="card-content">
							<div class="row">
								<span class="card-title center-align">@{{singleAppointmentDetail.facility_name}}</span>
							</div>
							<p class="center-align">
								Reason : @{{singleAppointmentDetail.reason_name }}<br>
								Time :	@{{singleAppointmentDetail.start_time | an_time }}<br>
								Date :	@{{singleAppointmentDetail.date | an_date }}<br>
								<br>

								<a class="btn waves-effect waves-light tooltipped " data-position="left" data-delay="10" data-tooltip="Click here for reschedule appointment." @click.prevent="onRescheduleUpcomingAppointment(singleAppointmentDetail.an_event_id,'day')" >Reschedule</a>

							</p>
						</div>
					</div>
				</div>
			</div>
		</template>
		<template slot="modal-footer">
		</template>
	</materialize-modal>




	<materialize-modal :id="'single_past_calendar_appointment_detail_modal'">
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-image">
					<image-view :target-id="singleAppointmentDetail.doctor_id" :target-model="singleAppointmentDetail.doctor_model_identifier" :img-class="'circle responsive-img'"></image-view>
				</template>
				<template slot="header-title">
					@{{singleAppointmentDetail.full_name}}
				</template>
				<template slot="header-speciality-name">
					@{{singleAppointmentDetail.specialty_name}}
				</template>
			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<pre>@{{singleAppointmentDetail}}</pre>
		</template>
		<template slot="modal-footer">
		</template>
	</materialize-modal>

	<materialize-modal :id="'single_reschedule_calendar_appointment_detail_modal'">
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-image">
					<image-view :target-id="singleAppointmentDetail.doctor_id" :target-model="singleAppointmentDetail.doctor_model_identifier" :img-class="'circle responsive-img'"></image-view>
				</template>
				<template slot="header-title">
					@{{singleAppointmentDetail.full_name}}
				</template>
				<template slot="header-speciality-name">
					@{{singleAppointmentDetail.specialty_name}}
				</template>
			</materialize-modal-header>
		</template>
		<template slot="modal-body">

			{{-- <pre>
			@{{singleAppointmentDetail}}
		</pre>
		--}}
		<div class="row">
			<div class="col s12 m3"></div>
			<div class="col s12 m6">
				<div class="card ">
					<div class="card-content ">
						<span class="card-title center-align left">@{{singleAppointmentDetail.facility_name}}
						</span>
						<div class="col s12">
							<p class="center-align">Requested Date & Time</p>

							<div v-if="singleAppointmentDetail.date_1 && singleAppointmentDetail.time_1" >
								<div class="row">
									<p class="center-align"> <b>@{{singleAppointmentDetail.date_1 | an_date}}</b><br> at <br><b>@{{singleAppointmentDetail.time_1 | an_time}} </b></p>
								</div>
								<div class="row">
									<p class="center-align">
										<a  href="#"   class="tooltipped btn" data-position="top" data-delay="10" data-tooltip="Click here to approve appointment."@click.prevent="onApproveRescheduleRequest(singleAppointmentDetail.appointment_reschedule_id,singleAppointmentDetail.date_1,singleAppointmentDetail.time_1)">Approved
										</a>
									</p>
								</div>
								<hr>
							</div>
							<div v-if="singleAppointmentDetail.date_2 && singleAppointmentDetail.time_2">
								<div class="row">
									<p class="center-align"> <b>@{{singleAppointmentDetail.date_2 | an_date}}</b><br> at <br><b>@{{singleAppointmentDetail.time_2 | an_time}} </b></p>
								</div>
								<div class="row">
									<p class="center-align">
										<a  href="#"   class="tooltipped btn" data-position="top" data-delay="10" data-tooltip="Click here to approve appointment."@click.prevent="onApproveRescheduleRequest(singleAppointmentDetail.appointment_reschedule_id,singleAppointmentDetail.date_2,singleAppointmentDetail.time_2)">Approved
										</a>
									</p>
								</div>
								<hr>
							</div>

						</div>
						<div class="row" style="margin-top: 2%;">
							<div class="col s6">
								<p>
									<a href="#" class="tooltipped btn left" data-position="top" data-delay="10" data-tooltip="Click here to reschedule appointment."@click.prevent="onRescheduleRescheduleRequest(singleAppointmentDetail.appointment_reschedule_id)">Reshedule
									</a>
								</p>
							</div>
							<div class="col s6">
								<p>
									<a href="#" class="tooltipped btn right" data-position="top" data-delay="10" data-tooltip="Click here to cancel appointment."@click.prevent="onCancel(singleAppointmentDetail.appointment_reschedule_id)">Cancel</a>
								</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</template>
	<template slot="modal-footer">
	</template>
</materialize-modal>





<materialize-modal :id="'reschedule_calendar_appointments_reschedule_modal'" :class="'small_modal'">
	<template slot="modal-header">
		<materialize-modal-header >
			<template slot="header-title">
				Reschedule
			</template>

		</materialize-modal-header>
	</template>

	<template slot="modal-body">
		@include('healthcareprovider.partials.reschedule_appointment_reschedule')
	</template>
	<template slot="modal-footer">
		<button class="btn waves-effect waves-light" @click.prevent="onRescheduleRescheduleRequestAction" v-bind:disabled="!isValidRescheduleTime">Save</button>
	</template>
</materialize-modal>



<materialize-modal :id="'reschedule_calendar_appointments_upcoming_modal'" :class="'small_modal'">
	<template slot="modal-header">
		<materialize-modal-header >
			<template slot="header-title">
				Reschedule
			</template>

		</materialize-modal-header>
	</template>

	<template slot="modal-body">
		@include('healthcareprovider.partials.upcoming_appointment_reschedule')
	</template>
	<template slot="modal-footer">
		<button class="btn waves-effect waves-light" @click.prevent="onRescheduleUpcomingAppointmentAction" v-bind:disabled="!isValidRescheduleTime">Save</button>
	</template>
</materialize-modal>












</div>
</calender-view>

@endsection
@section('calendar_scrtipts')
<script type="text/javascript" src="/js/moment.js"></script>
<script src='/js/fullcalendar.js'></script>

@endsection
