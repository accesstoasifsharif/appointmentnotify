



@if(isset($my_perspectives))
<a class="btn dropdown-button perspective " href='#' data-activates='dropdown1'>perspective</a>
<ul id='dropdown1' class='dropdown-content' >
	@foreach($my_perspectives as $perspective)
	<li  <?php if ($selected_perspective_slug == $perspective->healthCareProvider->user->slug) {echo 'selected';}?> @change = ><a href="/healthcareprovider/{{$perspective->healthCareProvider->user->slug}}/{{$current_route_name}}">{{$perspective->healthCareProvider->user->name}}</a></li>
	@endforeach

</ul>
@endif
