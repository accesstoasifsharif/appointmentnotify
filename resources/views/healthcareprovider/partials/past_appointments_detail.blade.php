
@component('common.table')

@slot('table_head')
<th>Sr #</th>
<th>Facility Name</th>
<th>Reason</th>
<th>Date</th>
<th>Time</th>


@endslot

@slot('table_data')
<tr v-for="(appointment,index) in pastAppointmentsDetail">
	<td>@{{index | serial}}</td>
	<td v-text="appointment.facility_name"></td>
	<td v-text="appointment.reason_name"></td>
	<td>@{{appointment.date | an_date}}</td>
	<td>@{{appointment.start_time | an_time}}</td>
</tr>
@endslot

@slot('table_pagination')
<ul class="pagination center-align">
	<li v-if="modal_pagination.current_page > 1">
		<a href="#" aria-label="Previous" @click.prevent="onDetailTablePage(modal_pagination.current_page - 1)">
			<span aria-hidden="true">&laquo;</span>
		</a>
	</li>
	<li v-for="page in modal_pagesNumber" v-bind:class="[ page == modal_isActived ? 'active' : '']">
		<a href="#" @click.prevent="onDetailTablePage(page)">@{{ page }}</a>
	</li>
	<li v-if="modal_pagination.current_page < modal_pagination.last_page">
		<a href="#" aria-label="Next" @click.prevent="onDetailTablePage(modal_pagination.current_page + 1)">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>
@endslot
@endcomponent