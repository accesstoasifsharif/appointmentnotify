<div class="row">
	<div class="col s6">
		<div class="input-field">
			<select class="browser-default" @change = "onFacilitySelect">
				<option :value="0" :selected="selected_hcp_facility_id == 0">
					All Facilities
				</option>
				<option v-for="facility in allFacilitiesList" :value="facility.hcp_facility_id" :selected="facility.hcp_facility_id == selected_hcp_facility_id">
					@{{facility.facility_name}}
				</option>
			</select>
		</div>
	</div>
	<div class="col s6">
		<div class="input-field">
			<select class="browser-default" @change = "onDoctorSelect">
				<option :value="0" :selected="selected_doctor_id == 0">
					All Doctors
				</option>
				<option v-for="doctor in allDoctorsList" :value="doctor.target_id" :selected="doctor.target_id == selected_doctor_id">
					@{{doctor.full_name}}
				</option>
			</select>
		</div>
	</div>
</div>
