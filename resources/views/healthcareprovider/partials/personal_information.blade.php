<form action="" method="get" >
    <div class="row">
        <div class="col s6">
            <div class="input-field col s12">
                <i class="material-icons prefix">face</i>
                <input id="icon_prefix" id="icon_perfix1" name="first_name" type="text" class="validate" >
                <label for="icon_perfix1">First Name
                    <span class="help"></span>
                </label>
            </div>
            <div class="input-field col s12">
                <i class="material-icons prefix">perm_identity</i>
                <input id="icon_prefix2" type="text" class="validate" name="middle_name" >
                <label for="icon_prefix2">Middle Name
                    <span class="help"></span>
                </label>
            </div>
            <div class="input-field col s12">
                <i class="material-icons prefix">account_circle</i>
                <input id="icon_prefix3" type="text" class="validate" name="last_name" >
                <label for="icon_prefix3">Last Name
                    <span class="help" ></span>
                </label>
            </div>
        </div>
        <div class="col s6">
            <div class="input-field col s12">
                <i class="material-icons prefix">assignment_ind</i>
                <input id="icon_telephone" type="tel" class="validate" name="psuedo_name" >
                <label for="icon_telephone">User Name
                    <span class="help" ></span>
                </label>
            </div>
            <div class="input-field col s12">
                <i class="material-icons prefix">today</i>
                <input type="text" id="icon_telephone2" class="datepicker" name="date_of_birth"  >
                <label for="icon_telephone2">Date of Birth
                    <span class="help"></span>
                </label>
            </div>
            <div class="input-field col s12">
                <label class="left">Gender</label>
                <div class=" right" style="padding-right: 8%;height: auto;">
                    <p class="left">
                     <input class="with-gap right" type="radio" id="test5"  name="gender" >
                     <label for="test5">Male</label>
                 </p>
                 <p class="left">
                    <input class="with-gap right" type="radio" id="test8"  name="gender">
                    <label for="test8">Female</label>
                </p>
                <p class="left">
                    <input class="with-gap right" type="radio" id="test10"  name="gender" >
                    <label for="test10">Other</label>
                </p>
            </div>
            <span class="help"></span>
        </div>
    </div>
</div>
<div class="col s12">
    <button type="submit" class="btn right waves-effect waves-light"  >Update</button>
</div>
</form>