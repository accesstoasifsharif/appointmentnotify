<div class="col s12">
	<div class="input-field">
		<select class="browser-default" @change = "onDoctorSelect">
			<option :selected = "selected_hcp_doctor_id==0" disabled >
				Choose a doctor
			</option>
			<option v-for="doctor in allDoctorsList" :value="doctor.hcp_doctor_id" :selected="doctor.hcp_doctor_id == selected_hcp_doctor_id">
				@{{doctor.full_name}}
			</option>
		</select>
	</div>
</div>


<ul class="list" style="height: 450px;">
	<li class="clearfix active" v-for="(patient,index) in filteredPatientList"  data-chat="person2" @click.prevent="seletedPatientAndDoctor(patient.target_id,patient.model_identifier,patient.doctor_id,patient.doctor_identifier)" :class="{selected_patient: patient === activePatient}" @click="activePatient = patient">
		<div class="row user-list">
			<div class="col s3 ">
				<image-view :target-id="patient.target_id" :target-model="patient.model_identifier" :img-class="'responsive-img'"
				></image-view>
			</div>
			<div class="col s9 ">

				<span class="truncate" >@{{patient.full_name}}</span>
				<div class="status" v-if="patient.message_preview">
					<span class="truncate chat-time">@{{patient.message_preview.message}}</span>
				</div>
				<div class="about status" v-if="patient.message_preview">
					<span class="chat-time">@{{patient.message_preview.created_at | chat_date}}</span>
				</div>
			</div>
		</div>
	</li>
</ul>