<div class="input-field col s12">
	<label for="search">Search....</label>
	<input type="text" id="search" v-model="facility_name_npi_search" name="facility_name_npi_search" @keyup="searchFacility" >

	<ul v-if="open_facility_match && facility_matches.length" class="overlay_ul z-depth-2">
		<li v-for="suggestion in facility_matches" @click="onFacilitySuggestionClick(suggestion)">
			<a href="#">@{{suggestion.facility_name}}</a>
		</li>
	</ul>
</div>

<div class="row" v-if = "selected_facility_detail != ''">
	<div class="col s12 m12">
		<div class="card">

			<div class="card-content">
				<span class="card-title center-align">@{{selected_facility_detail.facility_name}}</span>
				<div class="row ">
					<p class="center-align">Phone :@{{selected_facility_detail.phone_number}}</p>
					<p class="center-align">Fax: @{{selected_facility_detail.fax_number}}</p>
					<p class="center-align">Email: @{{selected_facility_detail.email}}</p>
					<p class="center-align">Address : @{{selected_facility_detail.address_line_1}} @{{selected_facility_detail.address_line_2}} @{{selected_facility_detail.city_name}} @{{selected_facility_detail.state_id}} @{{selected_facility_detail.zip_code}}</p>

				</div>
			</div>
		</div>
	</div>
</div>
