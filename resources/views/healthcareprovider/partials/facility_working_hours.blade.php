<div v-if="timeTable.data">
	<h4 class="center-align">Business Hours</h4>
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-content">

					<table>
						<thead>
							<tr>
								<th>Day</th>
								<th>Open</th>
								<th>Close</th>
							</tr>
						</thead>

						<tbody>
							<tr v-for="(item, index) in timeTable.data" v-if="item.time_from">

								<td style="text-transform: capitalize;
								">@{{item.day}}</td>
								<td>@{{item.time_from}}</td>
								<td>@{{item.time_to}}</td>

							</tr>


						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>