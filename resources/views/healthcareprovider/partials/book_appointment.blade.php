<book-appointment inline-template>
	<materialize-modal :id="'book_appointment_modal'" :class="'modal_size'">
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-title" >
					Book Appointment
				</template>
			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<div class="row">
				<div class="col s12">
					<div class="input-field">
						<input type="email" name="email" id="book_email">
						<label for="book_email">Search</label>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<div class="input-field">
						<date-picker :id="'book_appointment_date'" :model="form.date" :text="'Date'" @datepicked="onBookDatePick"></date-picker>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<div class="input-field col s6">
						<time-picker :id="'book_appointment_start_time'" :model="form.start_time" :text="'From'" @timepicked="onBookTimePick"></time-picker>
					</div>
					<div class="input-field col s6">
						<time-picker :id="'book_appointment_end_time'" :model="form.end_time" :text="'To'"  @timepicked="onBookTimePick"></time-picker>
					</div>
				</div>
			</div>
		</template>
		<template slot="modal-footer">
			<a class="btn" @click="onBookAppointment">Book</a>
		</template>
	</materialize-modal>
</book-appointment>