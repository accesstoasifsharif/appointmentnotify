<div class="input-field col s12">
	<label for="hcp_facility_npi">@{{form.fieldLabel('facility_npi')}}</label>
	<span class="help red" v-if="form.errors.has('facility_npi')" v-text="form.errors.get('facility_npi')"></span>
	<masked-input
	id="hcp_facility_npi" type="text" v-model="form.facility_npi" name="facility_npi" autofocus
	:mask="[ /[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]"
	:guide="false"
	placeholderChar="#" v-bind:disabled="!editable">
</masked-input>
</div>
<div class="input-field col s12 ">
	<input id="hcp_facility_name" type="text" v-model="form.facility_name" name="facility_name" autofocus v-bind:disabled="!editable">
	<label for="hcp_facility_name">@{{form.fieldLabel('facility_name')}}</label>
	<span class="help red" v-if="form.errors.has('facility_name')" v-text="form.errors.get('facility_name')"></span>
</div>
<div class="input-field col s12">
	<input id="hcp_facility_line_1" type="text" v-model="form.address_line_1" name="address_line_1" autofocus v-bind:disabled="!editable">
	<label for="hcp_facility_line_1">@{{form.fieldLabel('address_line_1')}}</label>
	<span class="help red" v-if="form.errors.has('address_line_1')" v-text="form.errors.get('address_line_1')"></span>
</div>

<div class="input-field col s12">
	<input id="hcp_facility_line_2" type="text" v-model="form.address_line_2" name="address_line_2" autofocus v-bind:disabled="!editable">
	<label for="hcp_facility_line_2">@{{form.fieldLabel('address_line_2')}}</label>
	<span class="help red" v-if="form.errors.has('address_line_2')" v-text="form.errors.get('address_line_2')"></span>
</div>

<div class="input-field col s4">
	<input id="hcp_facility_city" type="text" v-model="form.city_name" name="city_name" autofocus v-bind:disabled="!editable">
	<label for="hcp_facility_city">@{{form.fieldLabel('city_name')}}</label>
	<span class="help red" v-if="form.errors.has('city_name')" v-text="form.errors.get('city_name')"></span>
</div>

<div class="input-field col s4">
	<select class="browser-default" v-model="form.state_id" v-bind:disabled="!editable">
		<option v-for="state in states" :value="state.id" v-text = "state.postal_code" >
		</option>
	</select>
	<span class="help red" v-if="form.errors.has('state_id')" v-text="form.errors.get('state_id')"></span>
</div>

<div class="input-field col s4">
	<label for="hcp_facility_zip">@{{form.fieldLabel('zip_code')}}</label>
	<span class="help red" v-if="form.errors.has('zip_code')" v-text="form.errors.get('zip_code')"></span>
	<masked-input id="hcp_facility_zip" type="text" v-model="form.zip_code" name="zip_code"
	class="validate" autofocus :mask="[/[1-9]/, /\d/, /\d/, /\d/, /\d/]" :guide="false" placeholderChar="#" v-bind:disabled="!editable">
</masked-input>
</div>

<div class="input-field col s12">
	<label for="hcp_facility_phone">@{{form.fieldLabel('phone_number')}}</label>
	<span class="help red" v-if="form.errors.has('phone_number')" v-text="form.errors.get('phone_number')"></span>
	<masked-input
	id="hcp_facility_phone" type="text" v-model="form.phone_number" name="phone_number"
	class="validate" autofocus

	:mask="['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]"
	:guide="false"
	placeholderChar="#" v-bind:disabled="!editable">
</masked-input>
</div>

<div class="input-field col s12">
	<label for="hcp_facility_fax">@{{form.fieldLabel('fax_number')}}</label>
	<span class="help red" v-if="form.errors.has('fax_number')" v-text="form.errors.get('fax_number')"></span>
	<masked-input
	id="hcp_facility_fax" type="text" v-model="form.fax_number" name="fax_number"
	class="validate" autofocus

	:mask="[ /[1-9]/, /\d/, /\d/, /\d/,' ', /\d/, /\d/, ' ', /\d/,/\d/, /\d/, /\d/, /\d/]"
	:guide="false"
	placeholderChar="#" v-bind:disabled="!editable">
</masked-input>
</div>

<div class="input-field col s12">
	<input id="hcp_facility_email" type="text" v-model="form.email" name="email" autofocus v-bind:disabled="!editable">
	<label for="hcp_facility_email">@{{form.fieldLabel('email')}}</label>
	<span class="help red" v-if="form.errors.has('email')" v-text="form.errors.get('email')"></span>
</div>

<div class="input-field col s12">
	<input id="hcp_facility_url" type="text" v-model="form.url" name="url" autofocus v-bind:disabled="!editable">
	<label for="hcp_facility_url">@{{form.fieldLabel('url')}}</label>
	<span class="help red" v-if="form.errors.has('url')" v-text="form.errors.get('url')"></span>
</div>