<div class="row">
	<div class="col s12">
		<div class="card col s4" v-for="(request,index) in rescheduleAppointmentRequestsDetail">
			<div class="card-content ">
				<span class="card-title center-align left">@{{request.facility_name}}
				</span>
				<div class="col s12">
					<p class="center-align">Requested Date & Time</p>

					<div v-if="request.date_1 && request.time_1" >
						<div class="row">
							<p class="center-align"> <b>@{{request.date_1 | an_date}}</b><br> at <br><b>@{{request.time_1 | an_time}} </b></p>
						</div>
						<div class="row">
							<p class="center-align">
								<a  href="#"   class="tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to approve appointment."@click.prevent="onApprove(request.appointment_reschedule_id,request.date_1,request.time_1)">Approved
								</a>
							</p>
						</div>
						<hr>
					</div>
					<div v-if="request.date_2 && request.time_2">
						<div class="row">
							<p class="center-align"> <b>@{{request.date_2 | an_date}}</b><br> at <br><b>@{{request.time_2 | an_time}} </b></p>
						</div>
						<div class="row">
							<p class="center-align">
								<a  href="#"   class="tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to approve appointment."@click.prevent="onApprove(request.appointment_reschedule_id,request.date_2,request.time_2)">Approved
								</a>
							</p>
						</div>
						<hr>
					</div>

				</div>
				<div class="row" style="margin-top: 2%;">
					<div class="col s6">
						<p>
							<a href="#" class="tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to reschedule appointment."@click.prevent="onReschedule(request.appointment_reschedule_id)">Reshedule
							</a>
						</p>
					</div>
					<div class="col s6">
						<p>
							<a href="#" class="tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to cancel appointment."@click.prevent="onCancel(request.appointment_reschedule_id)">Reject</a>
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>

	<ul class="pagination center-align">
		<li v-if="modal_pagination.current_page > 1">
			<a href="#" aria-label="Previous" @click.prevent="onDetailTablePage(modal_pagination.current_page - 1)">
				<span aria-hidden="true">&laquo;</span>
			</a>
		</li>
		<li v-for="page in modal_pagesNumber" v-bind:class="[ page == modal_isActived ? 'active' : '']">
			<a href="#" @click.prevent="onDetailTablePage(page)">@{{ page }}</a>
		</li>
		<li v-if="modal_pagination.current_page < modal_pagination.last_page">
			<a href="#" aria-label="Next" @click.prevent="onDetailTablePage(modal_pagination.current_page + 1)">
				<span aria-hidden="true">&raquo;</span>
			</a>
		</li>
	</ul>

</div>



