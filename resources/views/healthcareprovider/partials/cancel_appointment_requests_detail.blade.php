
@component('common.table')

@slot('table_head')
<th>Sr #</th>
<th>Facility Name</th>
<th>Reason</th>
<th>Date</th>
<th>Time</th>
<th>Action</th>

@endslot

@slot('table_data')
<tr v-for="(request,index) in cancelAppointmentRequestsDetail">
	<td >@{{index | serial}}</td>
	<td v-text="request.facility_name"></td>
	<td v-text="request.reason_name"></td>
	<td v-text="">@{{request.date | an_date}}</td>
	<td>@{{request.start_time | an_time}}</td>
	<td>
		<button class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to dump appointment." @click.prevent="onDelete(request.an_event_id)">Dump</button>
		<button class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to auto allocation of appointment." @click.prevent="onAutomatic(request.an_event_id)">AutoAllocation</button>
		<button class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="Click here to short call appointment." @click.prevent="onShortCall(request.an_event_id)">ToShortCall</button>
	</td>
</tr>
@endslot

@slot('table_pagination')
<ul class="pagination center-align">
	<li v-if="modal_pagination.current_page > 1">
		<a href="#" aria-label="Previous" @click.prevent="onDetailTablePage(modal_pagination.current_page - 1)">
			<span aria-hidden="true">&laquo;</span>
		</a>
	</li>
	<li v-for="page in modal_pagesNumber" v-bind:class="[ page == modal_isActived ? 'active' : '']">
		<a href="#" @click.prevent="onDetailTablePage(page)">@{{ page }}</a>
	</li>
	<li v-if="modal_pagination.current_page < modal_pagination.last_page">
		<a href="#" aria-label="Next" @click.prevent="onDetailTablePage(modal_pagination.current_page + 1)">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>
@endslot
@endcomponent