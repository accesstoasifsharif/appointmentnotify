<div class="row">
	<div class="col s3"></div>
	<div class="input-field col s6 ">
		<input id="hcp_staff_search" type="text" v-model="doctorOrStaffSearchText" @keyup="searchDoctorOrStaff">
		<label for="hcp_staff_search">Search...</label>
		<ul v-if="open_search_match && staff_doctor_matches.length" class="overlay_ul z-depth-2">
			<li v-for="suggestion in staff_doctor_matches" @click="onMatchSuggestionClick(suggestion)" >
				<a href="#">@{{suggestion.full_name}}</a>
			</li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col s12 right" v-show="showDoctorStaffInfo" style="margin-top: 1%;">
		<div class="row">
			<div class="col m2"></div>
			<div class="col s12 m8">
				<div class="card">

					<div class="card-content">

						<div class="row">
							<div class="col s2">
								<image-view :target-id="form.selectedDoctorOrStaff.target_id" :target-model="form.selectedDoctorOrStaff.model_identifier" :img-class="'circle table_image right'"
								></image-view>
							</div>
							<div class="col s8">
								<span class="card-title center-align">
									@{{selectedDoctorStaffDetail.personal_information.first_name}} @{{selectedDoctorStaffDetail.personal_information.middle_name}} @{{selectedDoctorStaffDetail.personal_information.last_name}}
								</span>

							</div>
						</div>

						<div class="row ">
							<p class="center-align">Speciality :@{{selectedDoctorStaffDetail.specialty.specialty_name}}</p>
							<p class="center-align">Gender: @{{selectedDoctorStaffDetail.personal_information.gender}}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

