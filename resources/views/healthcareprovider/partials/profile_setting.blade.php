
<div class="row">
	<div class="col s12 m12 l12 xl12">

		@component('common/partials.help_text')
		@slot('visible_text')
		Your Profile section contains basic information about you including your Platform account nickname
		@endslot
		@slot('hidden_text')
		(username), gender, date of birth, address and contact information.
		@endslot
		@endcomponent
	</div>
	<div class="col s12 appointments_tab">
		<ul-tabs style="background-color: transparent;">
			<tab-component :href="'#personal_information'" style="margin-left: 11%;">
				Personal Info
			</tab-component>
			<tab-component :href="'#contact_information'">
				Contact
			</tab-component>
			<tab-component :href="'#address_information'">
				Address
			</tab-component>
		</ul-tabs>
	</div>

	<div class="col s12" id="personal_information" style="padding-top: 15px;">
		@include('healthcareprovider.partials.personal_information')
	</div>
	<div class="col s12" id="contact_information" style="padding-top: 15px;">
		@include('healthcareprovider.partials.contact_information')
	</div>
	<div class="col s12" id="address_information" style="padding-top: 15px;">
		@include('healthcareprovider.partials.address_information')
	</div>


</div>
