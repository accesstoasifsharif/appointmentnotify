
<cancel-appointment-request :hcp-facility-id='selected_hcp_facility_id' :doctor-id = 'selected_doctor_id' inline-template>
	<div>

		@component('common.table')
		@slot('table_head')
		<th>Sr #</th>
		<th></th>
		<th>Name</th>
		<th>Specialty</th>
		<th>Appointments</th>
		<th>Action</th>

		@endslot

		@slot('table_data')
		<tr v-for="request,index in allCancelAppointmentRequests">
			<td >@{{index | serial}}</td>
			<td>
				<image-view :target-id="request.doctor_id" :target-model="request.doctor_model_identifier" :img-class="'table_image'">
				</image-view>
			</td>
			<td v-text="request.full_name"></td>
			<td v-text="request.specialty_name"></td>
			<td v-text="request.total_appointments"></td>
			<td>
				<button class="btn waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="Click here for detail." @click.prevent="onDetail(request.doctor_id,request.doctor_model_identifier)">Detail</button>
			</td>
		</tr>
		@endslot


		@slot('table_pagination')
		<ul class="pagination center-align">
			<li v-if="pagination.current_page > 1">
				<a href="#" aria-label="Previous" @click.prevent="onTablePage(pagination.current_page - 1)">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
			<li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
				<a href="#" @click.prevent="onTablePage(page)">@{{ page }}</a>
			</li>
			<li v-if="pagination.current_page < pagination.last_page">
				<a href="#" aria-label="Next" @click.prevent="onTablePage(pagination.current_page + 1)">
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		</ul>
		@endslot
		@endcomponent


		<materialize-modal :id="'cancel_appointment_requests_detail_modal'" :class="'large_modal'">
			{{-- <template slot="modal-header">
			<pre>@{{selected_doctor_detail.data}}</pre>
		</template> --}}
		<template slot="modal-header">
			<materialize-modal-header >

				<template slot="header-image">
					<image-view :target-id="selected_doctor_detail.data.id" :target-model="selected_doctor_detail.data.model_identifier" :img-class="'circle responsive-img'"></image-view>
				</template>
				<template slot="header-title">
					@{{selected_doctor_detail.personal_information.first_name}}
					@{{selected_doctor_detail.personal_information.middle_name}}
					@{{selected_doctor_detail.personal_information.last_name}}
				</template>
				<template slot="header-speciality-name">
					@{{selected_doctor_detail.specialty.specialty_name}}
				</template>

			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			@include('healthcareprovider.partials.cancel_appointment_requests_detail')
		</template>
		<template slot="modal-footer">

		</template>
	</materialize-modal>

</div>

</cancel-appointment-request>

