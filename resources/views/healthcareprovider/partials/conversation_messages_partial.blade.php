<div class="row">
	<div class="chat-header clearfix col s12">
		<div class="col s2">

		</div>

		<div v-if="staffInfo.personal_information != '' && patientDetail == ''" class="col s8 center-align" >
			<div class="col s3">
				<image-view :target-id="staffInfo.id" :target-model="staffInfo.model_identifier" :img-class="'responsive-img table-image'">
				</image-view>
			</div>
			<div class="chat-about">
				<div class="chat-with">@{{staffInfo.personal_information.first_name}} @{{staffInfo.personal_information.middle_name}} @{{staffInfo.personal_information.last_name}}</div>
				<div v-if="staffInfo.specialty">@{{staffInfo.specialty.specialty_name}}</div>
			</div>
		</div>


		<div v-if="patientDetail != '' && staffInfo.personal_information == ''" class="col s8 center-align" >
			<div class="col s3">
				<image-view :target-id="patientDetail.target_id" :target-model="patientDetail.model_identifier" :img-class="'responsive-img table-image'">
				</image-view>
			</div>
			<div class="chat-about">
				<div class="chat-with">
					<div class="chat-with">@{{patientDetail.first_name}} @{{patientDetail.middle_name}} @{{patientDetail.last_name}}</div>
				</div>
			</div>
		</div>

		<div v-if="patientDetail != '' || staffInfo.personal_information != ''" class="col s2 ">
			<a href="#messaging_doctor_detail" class="right">
				<i class="material-icons">more_vert</i>
			</a>
		</div>

		<div v-if="patientDetail == '' && staffInfo.personal_information == ''">
			<h5>Patient Detail</h5>
		</div>


	</div>

</div>
<!-- end chat-header -->

<div class="chat-history" id=chat-messages-ul style="height: 315px;overflow-y: scroll;">
	<ul>

		<div v-for="message in conversationMessages" >
			<li class="clearfix" v-if="message.sender.id == selectedConversation.left_user">
				<div class="message-data align-right" >
					<span class="chat-time message-data-time" >@{{message.created_at | chat_date}}</span> &nbsp; &nbsp;
					<span class="message-data-name">@{{message.sender.name}}</span>
				</div>
				<div class="message other-message float-right z-depth-2">
					@{{message.message}}
				</div>
			</li>
			<li v-if="message.sender.id == selectedConversation.right_user">
				<div class="message-data">
					<span class="message-data-name">@{{message.sender.name}}</span>
					<span class="message-data-time">@{{message.created_at | chat_date}}</span>
				</div>
				<div class="message my-message z-depth-2">
					@{{message.message}}
				</div>
			</li>
		</div>

	</ul>

</div> <!-- end chat-history -->

<div class="chat-message clearfix">
	<div class="col s10">
		<div class="input-field col s12">
			<input type="text" id="textarea1" class="materialize-textarea" v-model="form.message_body"  @keydown="handleEnter">
			<label for="textarea1">Type Message Here</label>
		</div>
	</div>
	<div class="col s2">
		<button class="btn teal waves-effect waves-light left  " style="margin-top: 12%;"  ><i class="material-icons" @click.prevent = "onMessageSend">send</i></button>
	</div>
</div> <!-- end chat-message -->
