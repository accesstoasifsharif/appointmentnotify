<add-to-short-call inline-template>

	<materialize-modal :id="'short_call_list_modal'" :class="'modal_size'" >
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-title" >
					Short Call List
				</template>

			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<form>
				<div class="row">
					<div class="col s12">
						<div class="input-field">
							<i class="material-icons prefix">email</i>
							<input id="email" type="email" v-model='form.patient_email'>
							<label class="label-icon" for="email"></label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field">
							<select class="browser-default" :v-model="form.hcp_facility_id" @change = "onFacilitySelect">
								<option :selected ="form.hcp_facility_id == ''"  disabled>Choose Facility</option>
								<option v-for="facility in allFacilitiesList" :value="facility.hcp_facility_id" >
									@{{facility.facility_name}}
								</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field">
							<select class="browser-default" :v-model="form.doctor_id"  @change = "onDoctorSelect">
								<option :selected ="form.doctor_id == ''" disabled>Choose Doctor</option>
								<option v-for="doctor in allDoctorsList" :value="doctor.target_id" >
									@{{doctor.full_name}}
								</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field">
							<select class="browser-default" :v-model="form.duration"  @change = "onDurationSelect">
								<option :selected ="form.duration == ''" disabled>Choose Duration</option>
								<option v-for="duration in allDurationList" :value="duration" >
									@{{duration}}
								</option>
							</select>
						</div>
					</div>
				</div>

			</form>
		</template>
		<template slot="modal-footer">

			<button type="button" class="btn waves-effect waves-light" @click.prevent = "onSave">Save</button>
		</template>
	</materialize-modal>

</add-to-short-call>
