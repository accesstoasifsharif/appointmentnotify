






<table>

	<thead>
		<tr>
			<th>Notification Type</th>
			<th>Enable / Disable</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th>Email</th>
			<td>
				<div class="switch large">
					<label>
						Disable
						<input type="checkbox">
						<span class="lever"></span>
						Enable
					</label>
				</div>
			</td>
		</tr>
		<tr>
			<th>SMS</th>
			<td>
				<div class="switch large">
					<label>
						Disable
						<input type="checkbox">
						<span class="lever"></span>
						Enable
					</label>
				</div>
			</td>
		</tr>
		<tr>
			<th>Platform</th>
			<td>
				<div class="switch large">
					<label>
						Disable
						<input type="checkbox">
						<span class="lever"></span>
						Enable
					</label>
				</div>
			</td>
		</tr>
	</tbody>
</table>







