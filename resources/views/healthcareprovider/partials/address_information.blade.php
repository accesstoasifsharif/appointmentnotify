<form action="" method="get">
	<div class="row">
		<div class="input-field col s12">
			<i class="material-icons prefix">trending_up</i>
			<input id="address_line_1" type="text" class="validate" name="address_line_1" >
			<label for="address_line_1">Address Line 1
				<span class="help" >

				</span>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="input-field col s12">
			<i class="material-icons prefix">trending_up</i>
			<input id="address_line_2" type="text" class="validate" name="address_line_2" >
			<label for="address_line_2">Address Line 2
				<span class="help" >

				</span>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="input-field col s4">
			<i class="material-icons prefix">phone</i>
			<input id="city_name" type="text" class="validate" name="city_name" >
			<label for="city_name">City</label>
			<span class="help" >

			</span>
		</div>


		<div class="input-field col s4">
			<select id="state_postal" class="browser-default" >
				<option >
				</option>
			</select>

		</div>

		<div class="input-field col s4">
			<i class="material-icons prefix">vpn_key</i>

			<masked-input
			id="zip_code"
			type="text"
			name="zip_code"
			class="validate"

			:mask="[/\d/, /\d/, /\d/, /\d/, /\d/]"
			:guide="false"
			></masked-input>
			<label for="zip_code">Zip Code
				<span class="help" >

				</span>
			</label>
		</div>
	</div>
	<div class="col s12">
		<button type="submit" class="btn right waves-effect waves-light">Update</button>
	</div>
</form>
