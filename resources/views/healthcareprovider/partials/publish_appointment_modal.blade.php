
<publish-appointment inline-template>

	<materialize-modal :id="'publish_appointment_modal'" :class="'modal_size'">
		<template slot="modal-header">
			<materialize-modal-header >
				<template slot="header-title" >
					Publish your free slots here!
				</template>
			</materialize-modal-header>
		</template>
		<template slot="modal-body">
			<form>
				<div class="row">
					<div class="col s12">
						<div class="input-field">
							<select class="browser-default" :v-model="form.hcp_facility_id" @change = "onFacilitySelect">
								<option :selected ="form.hcp_facility_id == ''"  disabled>Choose Facility</option>
								<option v-for="facility in allFacilitiesList" :value="facility.hcp_facility_id" >
									@{{facility.facility_name}}
								</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field">
							<select class="browser-default" :v-model="form.doctor_id"  @change = "onDoctorSelect">
								<option :selected ="form.doctor_id == ''" disabled>Choose Doctor</option>
								<option v-for="doctor in allDoctorsList" :value="doctor.target_id" >
									@{{doctor.full_name}}
								</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field">
							<date-picker :id="'publish_appointment_date'" :model="form.date" :text="'Date'" @datepicked="onRescheduleDatePick"></date-picker>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="input-field col s6">
							<time-picker :id="'publish_appointment_start_time'" :model="form.start_time" :text="'From'" @timepicked="onRescheduleTimePick"></time-picker>
						</div>
						<div class="input-field col s6">
							<time-picker :id="'publish_appointment_end_time'" :model="form.end_time" :text="'To'"  @timepicked="onRescheduleTimePick"></time-picker>
						</div>
					</div>
				</div>
			</form>
		</template>
		<template slot="modal-footer">
			<button type="button" class="btn waves-effect waves-light" @click.prevent = "onSave">Save</button>
		</template>
	</materialize-modal>
</publish-appointment>
