<form action="" method="get" >
    <div class="row">
        <div class="input-field col s6">
            <i class="material-icons prefix">stay_primary_portrait</i>
            <label for="user_mobile">Mobile
                <span class="help" >

                </span>
            </label>
            <masked-input
            id="user_mobile"
            type="tel"
            name="phone_number"
            class="validate"

            :mask="['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]"
            :guide="false"
            placeholderChar="#">
        </masked-input>
    </div>
    <div class="input-field col s6">
        <i class="material-icons prefix">phone</i>

        <masked-input
        id="icon_telephone"
        type="tel"
        name="fax_number"
        class="validate"

        :mask="['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]"
        :guide="true"
        placeholderChar="#">
    </masked-input>
    <label for="icon_telephone">Fax
        <span class="help" >

        </span>
    </label>
</div>
</div>
<div class="row">
    <div class="input-field col s6">
        <i class="material-icons prefix">email</i>
        <input
        id="icon_email"
        type="text"
        class="validate"
        name="email"

        >

        <label for="icon_email">Email
            <span class="help" >

            </span>
        </label>
    </div>
</div>
<div class="col s12">
    <button type="submit" class="btn right waves-effect waves-light">Update</button>
</div>
</form>
