<div class="col s12">
	<div class="input-field">
		<select class="browser-default" @change = "onFacilitySelect">
			<option :value="0" :selected="selected_hcp_facility_id == 0">
				All Facilities
			</option>
			<option v-for="facility in allFacilitiesList" :value="facility.hcp_facility_id" :selected="facility.hcp_facility_id == selected_hcp_facility_id">
				@{{facility.facility_name}}
			</option>
		</select>
	</div>
</div>
<ul class="list" style="height: 450px;">
	<li class="clearfix active" v-for="(staff,index) in filteredStaffList"  data-chat="person2" @click.prevent = "seletedStaff(staff.target_id,staff.model_identifier,staff.auth_user_id,staff.auth_user_identifier)"
	:class="{selected_staff: staff === activeStaff}" @click="activeStaff = staff">
	<div class="row user-list">
		<div class="col s3 ">
			<image-view :target-id="staff.target_id" :target-model="staff.model_identifier" :img-class="'responsive-img'"
			></image-view>
		</div>
		<div class="col s9">
			<span class="truncate">
				<span>@{{staff.full_name}}</span>
			</span>
			<div class="status" v-if="staff.message_preview">
				<span class="truncate chat-time">@{{staff.message_preview.message}}</span>
			</div>
			<div class="about status" v-if="staff.message_preview">
				<span class="chat-time" >@{{staff.message_preview.created_at | chat_date}}
				</span>
			</div>
		</div>
	</div>
</li>
</ul>