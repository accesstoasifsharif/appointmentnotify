
@component('common.table')

@slot('table_head')
<th>Sr #</th>
<th>Facility Name</th>
<th>Reason</th>
<th>Date</th>
<th>Time</th>
<th>Action</th>

@endslot

@slot('table_data')
<tr v-for="(appointment,index) in publishedAppointmentsDetail">
	<td >@{{index | serial}}</td>
	<td v-text="appointment.facility_name"></td>
	<td v-text="appointment.reason_name"></td>
	<td v-text="">@{{appointment.date | an_date}}</td>
	<td>@{{appointment.start_time | an_time}}</td>
	<td>


		<span class="new badge left waves-effect waves-light tooltipped " data-position="left" data-delay="10" data-tooltip="Click here for reschedule appointment." @click.prevent="onReschedule(appointment.an_event_id)" data-badge-caption="Reschedule"></span>
		<span class="new badge left waves-effect  waves-light tooltipped " data-position="left" data-delay="10" data-tooltip="Click here for reschedule appointment." @click.prevent="onCancel(appointment.an_event_id)" data-badge-caption="Cancel"></span>

		<span class="new badge left waves-effect  waves-light tooltipped " data-position="left" data-delay="10" data-tooltip="Click here for reschedule appointment." @click.prevent="onPatientAssign(appointment.an_event_id)" data-badge-caption="Assign"></span>
		<span class="new badge left waves-effect waves-light tooltipped " data-position="left" data-delay="10" data-tooltip="Click here for reschedule appointment." @click.prevent="onShortCall(appointment.an_event_id)" data-badge-caption="Shorcall"></span>

	</tr>
	@endslot

	@slot('table_pagination')
	<ul class="pagination">
		<li v-if="modal_pagination.current_page > 1">
			<a href="#" aria-label="Previous" @click.prevent="onDetailTablePage(modal_pagination.current_page - 1)">
				<span aria-hidden="true">&laquo;</span>
			</a>
		</li>
		<li v-for="page in modal_pagesNumber" v-bind:class="[ page == modal_isActived ? 'active' : '']">
			<a href="#" @click.prevent="onDetailTablePage(page)">@{{ page }}</a>
		</li>
		<li v-if="modal_pagination.current_page < modal_pagination.last_page">
			<a href="#" aria-label="Next" @click.prevent="onDetailTablePage(modal_pagination.current_page + 1)">
				<span aria-hidden="true">&raquo;</span>
			</a>
		</li>
	</ul>
	@endslot
	@endcomponent