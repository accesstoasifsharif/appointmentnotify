<div class="col s12" >
	<div class="row">

		<h5 class="center-align" >Prefered Date & Time</h5>

		<div class="input-field col s12">
			<date-picker :id="'upcoming_appointment_reschedule_date'" :text="'Date'"  :model= "reschedule.date" @datepicked="onRescheduleDatePick"></date-picker>
		</div>
		<div class="input-field col s12">
			<time-picker :id="'upcoming_appointment_reschedule_time'" :text="'Time'"  :model= "reschedule.time" @timepicked="onRescheduleTimePick"></time-picker>
		</div>
	</div>
</div>
