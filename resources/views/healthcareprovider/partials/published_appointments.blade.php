<published-appointments :hcp-facility-id='selected_hcp_facility_id' :doctor-id = 'selected_doctor_id' inline-template>
	<div>

		@component('common.table')
		@slot('table_head')
		<th>Sr #</th>
		<th></th>
		<th>Name</th>
		<th>Specialty</th>
		<th>Appointments</th>
		<th>Action</th>

		@endslot

		@slot('table_data')
		<tr v-for="appointment,index in allPublishedAppointments">
			<td >@{{index | serial}}</td>
			<td>
				<image-view :target-id="appointment.doctor_id" :target-model="appointment.doctor_model_identifier" :img-class="'table_image'">
				</image-view>
			</td>
			<td v-text="appointment.full_name"></td>
			<td v-text="appointment.specialty_name"></td>
			<td v-text="appointment.total_appointments"></td>
			<td>
				<button class="btn waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="Click here for detail." @click.prevent="onDetail(appointment.doctor_id,appointment.doctor_model_identifier)">Detail</button>
			</td>
		</tr>
		@endslot


		@slot('table_pagination')
		<ul class="pagination">
			<li v-if="pagination.current_page > 1">
				<a href="#" aria-label="Previous" @click.prevent="onTablePage(pagination.current_page - 1)">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
			<li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
				<a href="#" @click.prevent="onTablePage(page)">@{{ page }}</a>
			</li>
			<li v-if="pagination.current_page < pagination.last_page">
				<a href="#" aria-label="Next" @click.prevent="onTablePage(pagination.current_page + 1)">
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		</ul>
		@endslot
		@endcomponent


		<materialize-modal :id="'published_appointments_detail_modal'" >
			<template slot="modal-header">
				<materialize-modal-header >

					<template slot="header-image">
						<image-view :target-id="selected_doctor_detail.data.id" :target-model="selected_doctor_detail.data.model_identifier" :img-class="'circle responsive-img'"></image-view>
					</template>
					<template slot="header-title">
						@{{selected_doctor_detail.personal_information.first_name}}
						@{{selected_doctor_detail.personal_information.middle_name}}
						@{{selected_doctor_detail.personal_information.last_name}}
					</template>
					<template slot="header-speciality-name">
						@{{selected_doctor_detail.specialty.specialty_name}}
					</template>

				</materialize-modal-header>
			</template>
			<template slot="modal-body">
				@include('healthcareprovider.partials.published_appointments_detail')
			</template>
			<template slot="modal-footer">

			</template>
		</materialize-modal>



		<materialize-modal :id="'published_appointments_reschedule_modal'" :class="'small_modal'">
			<template slot="modal-header">
				<materialize-modal-header >
					<template slot="header-title">
						Reschedule
					</template>

				</materialize-modal-header>
			</template>

			<template slot="modal-body">
				@include('healthcareprovider.partials.published_appointment_reschedule')
			</template>
			<template slot="modal-footer">
				<button class="btn waves-effect waves-light" @click.prevent="onRescheduleAction" v-bind:disabled="!isValidRescheduleTime">Save</button>
			</template>
		</materialize-modal>


		<materialize-modal :id="'published_appointment_assign_to_patient'" :class="'small_modal'">
			<template slot="modal-header">
				<materialize-modal-header >
					<template slot="header-title">
						Assign to Patient
					</template>

				</materialize-modal-header>
			</template>

			<template slot="modal-body">
				<input id="email" type="email" v-model='patient_assign.patient_email'>
				<label class="label-icon" for="email"></label>
			</template>

			<template slot="modal-footer">
				<button class="btn waves-effect waves-light" @click.prevent="onPatientAssignAction" v-bind:disabled="!isValueAssigned">Book</button>
			</template>
		</materialize-modal>


	</div>
</published-appointments>
















