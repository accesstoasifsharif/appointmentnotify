
@extends('healthcareprovider.layouts.master')

@section('content')

<staff-view inline-template>
	<div class="row">
		<div class="col s7">

			<div class="input-field col s12">
				<select class="browser-default"  @change = "onFacilitySelect">
					<option :value="0" :selected="selected_hcp_facility_id == 0">
						All Facilities
					</option>
					<option v-for="facility in allFacilitiesList" :value="facility.hcp_facility_id" :selected="facility.hcp_facility_id == selected_hcp_facility_id">
						@{{facility.facility_name}}
					</option>
				</select>
			</div>


			@component('common.table')

			@slot('table_search')
			<div class="input-field col s12">
				<input id="icon_prefix" type="search" class="validate" v-model="searchText"  @keyup.enter="searchStaffTableQuery" @keyup="searchStaffTableQuery">
				<label for="icon_prefix">Search by doctor or staff name</label>
			</div>
			@endslot
			@slot('table_search_btn')
			<div class="input-field col s12">
				<button class="btn-floating waves-effect waves-light left" @click.prevent="searchStaffTableQuery" > Go </button>
			</div>
			@endslot


			@slot('table_page_select')
			<div class="input-field col s12">

				<select class="browser-default"  @change="onStaffTableLength">
					<option value=""  selected>Rows per page</option>
					<option v-for="option in perPageOptions" :value="option" :selected="option == pagination.per_page">
						@{{option}}
					</option>
				</select>
			</div>
			@endslot



			@slot('table_head')
			<th>Sr #</th>
			<th>Avatar</th>
			<th>Name</th>
			<th>Role</th>
			<th>Action</th>
			@endslot

			@slot('table_data')
			<tr v-for="staff,index in allStaff">
				<td >@{{index | serial}}</td>
				<td>
					<image-view :target-id="staff.target_id" :target-model="staff.model_identifier" :img-class="'table_image'"
					></image-view>
				</td>
				<td v-text="staff.full_name"></td>
				<td v-text="staff.model_identifier"></td>
				<td>
					<button class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="click here to edit."  @click.prevent="editStaff(staff.target_id,staff.model_identifier)"><i class="material-icons">edit</i></button>
					<button class="btn-floating waves-effect waves-light red tooltipped" data-position="top" data-delay="10" data-tooltip="click here to delete."  @click.prevent="deleteStaff(staff.target_id,staff.model_identifier)"><i class="material-icons">delete</i></button>
				</td>
			</tr>


			@endslot


			@slot('table_pagination')
			<ul class="pagination">
				<li v-if="pagination.current_page > 1">
					<a href="#" aria-label="Previous" @click.prevent="onStaffTablePage(pagination.current_page - 1)">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
				<li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
					<a href="#" @click.prevent="onStaffTablePage(page)">@{{ page }}</a>
				</li>
				<li v-if="pagination.current_page < pagination.last_page">
					<a href="#" aria-label="Next" @click.prevent="onStaffTablePage(pagination.current_page + 1)">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</ul>
			@endslot
			@endcomponent
		</div>
		<div class="col s5">
			<div class="col s12 center-align">
				<h4>Staff</h4>
				<button type="button" class="btn-floating right waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="click here to add staff."  @click.prevent="addNewStaff">Add</button>

			</div>
			<div class="col s12"  >

				<div class="row " style="padding-left: 7%;">

					<ul id="list" >
						<li class='list-item' v-for = "staff in allStaffForCircularView" >
							<circular-view>
								<template slot="list_data">
									<image-view :target-id="staff.target_id" :target-model="staff.model_identifier" :img-class="'circle responsive-img'"
									></image-view>

								</template>
							</circular-view>
						</li>
					</ul>

				</div>
			</div>
		</div>

		<materialize-modal :id="'hcp_DoctorOrStaff_add_modal'" >
			<template slot="modal-header">
				<materialize-modal-header >
					<template slot="header-title" >
						Add Staff
					</template>
				</materialize-modal-header>
			</template>

			<template slot="modal-body">
				<div class="row">
					<div class="nav-content feature_header center">
						<ul class="tabs tabs-transparent main_tabs_ul" style="background-color: #FAFAFA !important;">
							<li class="tab main_tabs_li" @click="searchAdd">
								<a  class="active z-depth-2" href="#search_staff">Add By Search</a>
							</li>
							<li class="tab main_tabs_li" @click="manuallyAdd">
								<a class="z-depth-2" href="#add_staff_manually">Add Manually</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col s12" id="search_staff">
							@include('healthcareprovider.partials.addDoctorOrStaffForm')
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12" id="add_staff_manually">
						<div class="col s3"></div>
						<div class="col s7" v-if="showEmailField">
							<div class="input-field col s6 " >
								<input id="hcp_staff_email" type="email" v-model="form.staffEmailAddress" >
								<label for="hcp_staff_email">Staff Email</label>
							</div>

							<div class="input-field col s6">
								<input type="radio" name="staff_type" v-model="form.staff_type" :value="'Doctor'" id="staff_type1">
								<label for="staff_type1">Doctor</label>
								<input type="radio" name="staff_type" v-model="form.staff_type" :value="'Staff'" id="staff_type2">
								<label for="staff_type2">Staff</label>
							</div>
						</div>
					</div>


				</div>
				<div class="row">
					<div class="col s12">
						<staff-facility-time-table :facility-attached = "facilityAttached" >
						</staff-facility-time-table>
					</div>
				</div>
			</template>
			<template slot="modal-footer">

				<button type="button" class="btn waves-effect waves-light" @click.prevent = "addOrUpdateHCPAllStaff('add')" v-bind:disabled="!isValidStaff" >Save</button>
				<a href="#!" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>
			</template>

		</materialize-modal>




		<materialize-modal :id="'hcp_DoctorOrStaff_edit_modal'" >
			<template slot="modal-header">
				<materialize-modal-header >
					<template slot="header-image">
						<image-view :target-id="doctorStaffCompleteInfo.data.id" :target-model="doctorStaffCompleteInfo.data.model_identifier" :img-class="'circle responsive-img'"
						></image-view>
					</template>
					<template slot="header-title">
						@{{doctorStaffCompleteInfo.personal.first_name}} @{{doctorStaffCompleteInfo.personal.middle_name}} @{{doctorStaffCompleteInfo.personal.last_name}}
					</template>
					<template slot="header-speciality-name">
						@{{doctorStaffCompleteInfo.specialty.specialty_name}}
					</template>


				</materialize-modal-header>
			</template>

			<template slot="modal-body">
				<div class="row">

					<div class="col s12">
						@include('healthcareprovider.partials.editDoctorOrStaffForm')
					</div>
				</div>
				<div class="row>">
					<div class="col s12">
						<staff-facility-time-table :facility-attached = "facilityAttached" >
						</staff-facility-time-table>
					</div>
				</div>
			</template>
			<template slot="modal-footer">

				<button type="button" class="btn waves-effect waves-light" @click.prevent = "addOrUpdateHCPAllStaff('edit')">Save</button>
				<a href="#!" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>
			</template>

		</materialize-modal>


	</div>


</staff-view>


@endsection