@extends('healthcareprovider.layouts.master')

@section('content')
<appointment-request-view inline-template>
	<div class="row">
		<div class="col s12">
			@include('healthcareprovider.partials.appointment_header_dropdowns')
		</div>
		<div class="col s12 appointments_tab">
			<ul-tabs>
				<tab-component :href="'#s_appointments'">Appointments</tab-component>
				<tab-component :href="'#s_requests'">Requests</tab-component>
			</ul-tabs>
		</div>
		<div id="s_appointments" class="col s12 main-tab_content" >
			<div class="tabs-vertical " >
				<div class="col s4 m3 l2 inner_tab_col"  >
					<ul class="tabs">
						<li class="tab">
							<a class="waves-effect waves-cyan active" href="#s_upcoming">Upcoming </a>
						</li>
						<li class="tab">
							<a class="waves-effect waves-cyan" href="#s_past">Past </a>
						</li>
						<li class="tab">
							<a class="waves-effect waves-cyan" href="#s_publish">Published</a>
						</li>
					</ul>
				</div>
				<div class="col s8 m9 l10" >
					<div id="s_upcoming" class="tab-content">
						@include('healthcareprovider.partials.upcoming_appointments')
					</div>
					<div id="s_past" class="tab-content">
						@include('healthcareprovider.partials.past_appointments')
					</div>
					<div id="s_publish" class="tab-content">
						@include('healthcareprovider.partials.published_appointments')
					</div>

				</div>
			</div>
		</div>
		<div id="s_requests" class="col s12 main-tab_content" >
			<div class="tabs-vertical " >
				<div class="col s4 m3 l2 inner_tab_col" >
					<ul class="tabs">
						<li class="tab">
							<a class="waves-effect waves-cyan active" href="#s_reserve"><i class="zmdi zmdi-code"></i>Reserve</a>
						</li>
						<li class="tab">
							<a class="waves-effect waves-cyan  " href="#s_reschedule"><i class="zmdi zmdi-apps"></i>Reschedule</a>
						</li>
						<li class="tab">
							<a class="waves-effect waves-cyan" href="#s_cancel"><i class="zmdi zmdi-email"></i>Cancel</a>
						</li>

					</ul>
				</div>
				<div class="col s8 m9 l10" >
					<div id="s_reschedule" class="tab-content">
						@include('healthcareprovider.partials.reschedule_appointment_requests')
					</div>
					<div id="s_cancel" class="tab-content">
						@include('healthcareprovider.partials.cancel_appointment_requests')
					</div>
					<div id="s_reserve" class="tab-content">
						@include('healthcareprovider.partials.reserve_appointment_requests')
					</div>
				</div>
			</div>
		</div>
	</div>

</appointment-request-view>


@endsection