@extends('healthcareprovider.layouts.master')
@section('content')
<patient-list inline-template>

	<div>

		<div class="row">
			<div class="col s6">
				<div class="input-field">
					<select class="browser-default" >
						<option >
							All Facilities
						</option>
						<option>

						</option>
					</select>
				</div>
			</div>
			<div class="col s6">
				<div class="input-field">
					<select class="browser-default" >
						<option >
							All Doctors
						</option>
						<option>

						</option>
					</select>
				</div>
			</div>
		</div>

		@component('common.table')

		@slot('table_head')
		<th>Sr#</th>
		<th></th>
		<th>Name</th>
		<th>Specialty</th>
		<th>Actions</th>
		@endslot
		@slot('table_data')
		<tr v-for="  (doctors , index) in alldoctors" >
			<td>@{{index | serial}}</td>
			<td>
				<image-view :target-id="doctors.doctor_id" :target-model="doctors.doctor_img" :img-class="'circle table_image'"
				></image-view>
			</td>
			<td  >
				@{{doctors.first_name}}
				@{{doctors.middle_name}}
				@{{doctors.last_name}}

			</td>
			<td v-text="doctors.specialty_name" ></td>
			<td><a class="btn" @click="allPatientsByDoctorId(doctors.doctor_id)"  >All Patients</td>

		</tr>
		@endslot

		@slot('table_pagination')
		<ul class="pagination center-align">
			<li v-if="pagination.current_page > 1">
				<a href="#" aria-label="Previous" @click.prevent="onTablePage(pagination.current_page - 1)">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
			<li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
				<a href="#" @click.prevent="onTablePage(page)">@{{ page }}</a>
			</li>
			<li v-if="pagination.current_page < pagination.last_page">
				<a href="#" aria-label="Next" @click.prevent="onTablePage(pagination.current_page + 1)">
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		</ul>
		@endslot
		@endcomponent




		<!-- up cancelled appointments detail modal -->
		<materialize-modal :id="'patients_lsit'" :class="'large_modal'" >
			<template slot="modal-header">
				<materialize-modal-header >
			<!-- <template slot="header-image">
				<image-view :target-id="cancelled_appointment_doctor_info.doctor_id" :target-model="cancelled_appointment_doctor_info.doctor_img" :img-class="'circle table_image'"></image-view>
			</template>
			<template slot="header-title">
				Patients List
			</template>
			<template slot="header-speciality-name">
				@{{cancelled_appointment_doctor_info.specialty_name}}
			</template> -->
			<template slot="header-title">
				Patients List
			</template>
		</materialize-modal-header>
	</template>
	<template slot="modal-body">
		<div class="col s12" >
<!--
	@{{allpatientsspecificdoctor}} -->
	@component('common.table')



	@slot('table_head')
	<th>Sr#</th>
	<th></th>
	<th>Name</th>
	<th>Email</th>
	<th>Contact</th>
	<th>D.O.B</th>
	<th>Gender</th>
	@endslot
	@slot('table_data')
	<tr v-for="(patient , index) in allpatientsspecificdoctor" >
		<td >@{{index | serial}} </td>
		<td><image-view :target-id="patient.target_id" :target-model="patient.model_identifier" :img-class="'circle table_image'"
			></image-view></td>
			<td >
				@{{patient.full_name}}

			</td>
			<td>@{{patient.email}}</td>
			<td>@{{patient.phone_number}}</td>
			<td>@{{patient.date_of_birth}}</td>

			<td>@{{patient.gender}}</td>
		</tr>
		@endslot
		@slot('table_pagination')

		<ul class="pagination center-align">
			<li v-if="pagination_modal.current_page > 1">
				<a href="#" aria-label="Previous" @click.prevent="onDetailTablePage(pagination_modal.current_page - 1)">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
			<li v-for="page in modalPagesNumber" v-bind:class="[ page == modalIsActived ? 'active' : '']">

				<a href="#" @click.prevent="onDetailTablePage(page)">@{{ page }}</a>
			}
		}
	</li>
	<li v-if="pagination_modal.current_page < pagination_modal.last_page">
		<a href="#" aria-label="Next" @click.prevent="onDetailTablePage(pagination_modal.current_page + 1)">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>



@endslot
@endcomponent

</div>


</template>
<template slot="modal-footer">
</template>
</materialize-modal>

</div>
</patient-list>

@endsection
