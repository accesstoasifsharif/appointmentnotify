@extends('healthcareprovider.layouts.master')
<style type="text/css">

</style>
@section('content')

<messages-view inline-template>
	<div >
		<!-- new chat layout -->
		<div class="clearfix messages-container row">
			<div class="people-list col s3" id="people-list" >
				<div class="top center-align" >
					<ul id="tabs-swipe-demo" class="tabs" >
						<li class="tab col s6"><a class="active" href="#msg_staff" @click.prevent="staffConversationMode">Staff</a></li>
						<li class="tab col s6"><a  href="#msg_patient" @click.prevent="patientConversationMode">Patients</a></li>
					</ul>
				</div>
				<div class="search">
					<div class="col s12 center-align left-top" style="">
						<div class="input-field col s12">

							<input id="hcp_serch" type="text" v-model="searchText" >
							<label for="hcp_serch">Search....</label>
						</div>

					</div>
				</div>
				<div id="msg_staff" class="col s12" style="height: 365px;overflow-y: scroll;">
					@include('healthcareprovider.partials.provider_staff_conversation_list')
				</div>
				<div id="msg_patient" class="col s12" style="height: 365px;overflow-y: scroll;">
					@include('healthcareprovider.partials.provider_patient_conversation_list')
				</div>
			</div>
			<div class="chat col s9">
				@include('healthcareprovider.partials.conversation_messages_partial')
			</div> <!-- end chat -->
		</div> <!-- end container -->





		<!-- up coming appointments detail modal -->
		<materialize-modal :id="'messaging_doctor_detail'">
			<template slot="modal-header">
				<div v-if="staffInfo.personal_information != ''" class="col s8 center-align" >
					<materialize-modal-header >

						<template slot="header-image">
							<image-view :target-id="staffInfo.id" :target-model="staffInfo.model_identifier" :img-class="'responsive-img table-image'">
							</image-view>
						</template>
						<template slot="header-title">
							<h5>
								@{{staffInfo.personal_information.first_name}} @{{staffInfo.personal_information.middle_name}} @{{staffInfo.personal_information.last_name}}
							</h5>
						</template>
						<template slot="header-speciality-name">
							<div v-if="staffInfo.specialty">@{{staffInfo.specialty.specialty_name}}
							</div>
						</template>

					</materialize-modal-header>
				</div>
				<div v-if="patientDetail != ''" class="col s8 center-align" >
					<materialize-modal-header >

						<template slot="header-image">
							<image-view :target-id="patientDetail.target_id" :target-model="patientDetail.model_identifier" :img-class="'responsive-img table-image'">
							</image-view>
						</template>
						<template slot="header-title">
							<h5>
								@{{patientDetail.first_name}} @{{patientDetail.middle_name}} @{{patientDetail.last_name}}
							</h5>
						</template>


					</materialize-modal-header>
				</div>
			</template>
			<template slot="modal-body">
				<div class="col s12 m12" >
					<div v-if="staffInfo.personal_information != ''" class="col s12 center-align" >
						<div class="row">
							<div class="col s12 m12">
								<div class="card">
									<div class="card-content">
										<div class="row">
											<div class="col s2"></div>
											<div class="col s8">
												<table>
													<tr>
														<td><strong>Date Of Birth</strong></td>
														<td>@{{staffInfo.personal_information.date_of_birth | an_date_day}}</td>
													</tr>
													<tr>
														<td><strong>Gender</strong></td>
														<td>@{{staffInfo.personal_information.gender}}</td>
													</tr>
													<tr>
														<td><strong>Phone</strong></td>
														<td>@{{staffInfo.contact.phone_number}}</td>
													</tr>
													<tr>
														<td><strong>Fax number</strong></td>
														<td>@{{staffInfo.contact.fax_number}}</td>
													</tr>
													<tr>
														<td><strong>Email</strong></td>
														<td>@{{staffInfo.contact.email}}</td>
													</tr>
												</table>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div v-if="patientDetail != ''" class="col s12 center-align" >
						<div class="row">
							<div class="col s12 m12">
								<div class="card">
									<div class="card-content">
										<div class="row">
											<div class="col s2"></div>
											<div class="col s8">
												<table>
													<tr>
														<td><strong>Date Of Birth</strong></td>
														<td>@{{patientDetail.date_of_birth | an_date_day}}</td>
													</tr>
													<tr>
														<td><strong>Gender</strong></td>
														<td>@{{patientDetail.gender}}</td>
													</tr>
													<tr>
														<td><strong>Phone</strong></td>
														<td>@{{patientDetail.phone_number}}</td>
													</tr>
													<tr>
														<td><strong>Fax number</strong></td>
														<td>@{{patientDetail.fax_number}}</td>
													</tr>
													<tr>
														<td><strong>Email</strong></td>
														<td>@{{patientDetail.email}}</td>
													</tr>
												</table>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</template>
		</materialize-modal>

	</div>

</messages-view>

@endsection
