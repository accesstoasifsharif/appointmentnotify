<!DOCTYPE html>
<html >
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ APP_NAME }}</title>

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/healthcareprovider.css')}}">
	@yield('css_libraries')
	<style type="text/css">
		.perspective{
			position: fixed;
			top: 70px;
			left:5px;
			/*padding-top: 3% !important;*/
			border-radius: 30px;
			height: 38px;
			margin-bottom: 0;
			z-index: 998;
		}
	</style>
	<script>
		window.AppointmentNotify = {!! json_encode([
			'redirectToLogin'=> '/login',
			'redirectConditionalStatusCode'=> 555,
			'urlPrefix'=> healthCareProviderPrefix(),
			'allFeatures'=> $feature_roles,
			'csrfToken' => csrf_token(),
			'selected_perspective_slug' => $selected_perspective_slug,
			'current_route_name' => $current_route_name,
			'auth_user'=> Auth::User()->id,
			'selected_perspective_name'=> $selected_perspective_name,
			]) !!};
		</script>

	</head>
	<body >

		<div id="app_healthcareprovider" class="content">

			<header-component inline-template>
				@include('healthcareprovider.partials.header')
			</header-component>

			@include('healthcareprovider.partials.healthcareprovider_select')
			@include('common.partials.feature_header')

			<div  class="container" style="background-color: #FBFBFB; padding: 10px;">

				@yield('content')
			</div>
			<special-actions inline-template>
				<div>
					<div class="fixed-action-btn vertical" >
						<a class="btn-floating red">
							<i class="large material-icons">add_circle</i>
						</a>
						<ul style="margin-bottom: 20px;">
							<li>
								<a class="btn-floating blue tooltipped" href="#book_appointment_modal" data-position="left" data-delay="10" data-tooltip="Click Here To book new appointment">
									<i class="material-icons">book</i>
								</a>
							</li>
							<li>
								<a class="btn-floating blue tooltipped" href="" data-position="left" data-delay="10" data-tooltip="Click Here To export appointment">
									<i class="material-icons">cloud_download</i>
								</a>
							</li>
							<li>
								<a class="btn-floating blue tooltipped" href="#publish_appointment_modal" data-position="left" data-delay="10" data-tooltip="Click Here To Publish Appointment">
									<i class="material-icons">playlist_add</i>
								</a>
							</li>
							<li>
								<a class="btn-floating green tooltipped" data-position="left" data-delay="10" data-tooltip="Add To Short Call List" href="#short_call_list_modal">
									<i class="material-icons" >event</i>
								</a>
							</li>
						</ul>
					</div>
					@include('healthcareprovider.partials.book_appointment')
					@include('healthcareprovider.partials.publish_appointment_modal')
					@include('healthcareprovider.partials.short_call_list_modal')
				</div>
			</special-actions>

		</div>
		@include('common.partials.footer')

	</body>

	<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script> -->
	<script type="text/javascript" src="{{ asset('js/socket.io.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.materialize-autocomplete.js')}}"></script>
	@yield('calendar_scrtipts')
	<script type="text/javascript" src="{{ asset('js/healthcareprovider.js')}}"></script>
	@yield('js_libraries')
	<script type="text/javascript" src="{{ asset('js/app.healthcareprovider.js')}}"></script>
	</html>
