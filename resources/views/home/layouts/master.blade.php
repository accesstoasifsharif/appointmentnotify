<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Styles -->
	<!--  Styles For Vue.js Practice -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
	<title>{{ APP_NAME }}</title>
	<script>
		window.AppointmentNotify = {!! json_encode([
			'csrfToken' => csrf_token(),
			]) !!};
		</script>
		<style type="text/css">
		</style>
	</head>
	<body>
		<div id="app_home">
			<home inline-template>
				<div class="row">
					@include('home.partials.header')
					<div class="container">
						@yield('home-content')
					</div>
				</div>
			</home>
		</div>


		<script	async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCkXfu1Mwk8GEWxcEI-84Wjb0q0xuZHlg"></script>


		<script type="text/javascript" src="{{ asset('js/socket.io.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/app.home.js')}}"></script>


	</body>
	</html>




