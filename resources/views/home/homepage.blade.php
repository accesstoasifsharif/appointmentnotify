@extends('home/layouts.master')
@section('home-content')
<div class="row">
	<div class="col s8" style="">
		<div class="row" style=" margin-bottom: 0px;">
			<div class="col s5" style="">
				<div class="nav-content feature_header center">
					<ul class="tabs tabs-transparent main_tabs_ul">
						<li class="tab main_tabs_li">
							<a  class="active z-depth-2" href="#providerView"  @click="providerViewAppointment">Provider</a>
						</li>
						<li class="tab main_tabs_li">
							<a class="z-depth-2" href="#facilityView" @click="facilityViewAppointment">Facility</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col s4" style="padding: 11px;" v-if="getSpecialityDropdown==true">
				<select class="browser-default" v-model="selectSpeciality" style="color: #000;" @change="getSpeciality">
					<option value="">All Specialties</option>
					<option v-for="speciality in doctorSpecialtyList" :value="speciality.id" style="color: #000;">@{{speciality.specialty_name}}</option>
				</select>
			</div>
			<div class="col s4" style="padding: 11px;" v-if="getSpecialityDropdown==false">
				<select class="browser-default" v-model="selectFacilityViewSpeciality" style="color: #000;" @change="getFaciltyViewSpeciality">
					<option value="">All Specialties</option>
					<option v-for="speciality in doctorSpecialtyList" :value="speciality.id" style="color: #000;">@{{speciality.specialty_name}}</option>
				</select>
			</div>
			<!-- getSpecialityDropdown -->
			<div class="col s2" style="padding: 11px;" v-show="genderDropdown">
				<select class="browser-default" @change="getGender" v-model="selectGender">
					<option v-for="gender in genderOptions" :value="gender">@{{gender}}</option>
				</select>
			</div>
		</div>
		<clip-loader :loading="loading"></clip-loader>
		<div class="col s12" id="providerView">
			@include('home.partials.homepage_provider_view')
		</div>
		<div class="col s12" id="facilityView">
			@include('home.partials.homepage_facility_view')
		</div>
	</div>
	<div class="col s4">
		<p id="demo"></p>
		<hr>
		<google-map name="home" :map-facilities="finalFacilityList" :map-doctor-id="doctorIdForMap" :facility-for-animation="facilityArrayByDoctorId" :map-facility-id="facilityIdForMap"></google-map>
	</div>
</div>

<materialize-modal :id="'appointmentDetail'">
	<template slot="modal-header">
		<materialize-modal-header>
			<template slot="header-title">
				Appointment Detail
			</template>
		</materialize-modal-header>
	</template>
	<template slot="modal-body">
		@component('common.table')
		@slot('table_head')
		<th>Id</th>
		<th>Facility Name</th>
		<th>Appointment Date</th>
		<th>Appointment Time</th>
		<th>Actions</th>
		@endslot
		@slot('table_data')
		<tr v-for=" (appointmentDetail, index) in providerViewAppointmentDetail">
			<td style="color:#000;">@{{index|serial}}</td>
			<td v-text="appointmentDetail.facility_name" style="color:#000;"></td>
			<td v-text="appointmentDetail.appointment_date" style="color:#000;"></td>
			<td v-text="appointmentDetail.appointment_time" style="color:#000;"></td>
			<td>
				<a href="#" class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="click here to add provider"@click="bookingConfirmation(appointmentDetail.unique_appointment_id)">Book</a>
			</td>
		</tr>
		@endslot
		@endcomponent
	</template>
</materialize-modal>

<materialize-modal :id="'FacilityViewAppointmentDetail'">
	<template slot="modal-header">
		<materialize-modal-header>
			<template slot="header-title">
				All available appointment
			</template>
		</materialize-modal-header>
	</template>
	<template slot="modal-body">
		<div class="col s2" style="padding: 11px;">
			<select class="browser-default" @change="getFacilityViewGender" v-model="selectFacilityViewGender">
				<option v-for="gender in genderOptions" :value="gender">@{{gender}}</option>
			</select>
		</div>
		@component('common.table')
		@slot('table_head')
		<th>Id</th>
		<th>Doctor Name</th>
		<th>Speciality Name</th>
		<th>Total Appointment</th>
		<th>Actions</th>
		@endslot
		@slot('table_data')
		<tr v-for=" (appointment, index) in facilityViewAppointmentDetail">
			<td style="color:#000;">@{{index|serial}}</td>
			<td v-text="appointment.doctor_name" style="color:#000;"></td>
			<td v-text="appointment.specialty_name" style="color:#000;"></td>
			<td v-text="appointment.total_appointments" style="color:#000;"></td>
			<td>
				<a href="#" class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="click here to add provider"@click="facilityViewAppointmentDetailByFacilityIdByDoctorId(appointment.doctor_id)">detail</a>
			</td>
		</tr>
		@endslot
		@endcomponent
	</template>
</materialize-modal>
<materialize-modal :id="'FacilityViewAppointmentDetailIdByDoctorId'">
	<template slot="modal-header">
		<materialize-modal-header>
			<template slot="header-title">
				All available appointment
			</template>
		</materialize-modal-header>
	</template>
	<template slot="modal-body">
		@component('common.table')
		@slot('table_head')
		<th>Id</th>
		<th>Facility Name</th>
		<th>Appointment Date</th>
		<th>Appointment Time</th>
		<th>Actions</th>
		@endslot
		@slot('table_data')
		<tr v-for=" (appointment, index) in FacilityViewAppointmentDetailByDoctorId">
			<td style="color:#000;">@{{index|serial}}</td>
			<td v-text="appointment.facility_name" style="color:#000;"></td>
			<td v-text="appointment.appointment_date" style="color:#000;"></td>
			<td v-text="appointment.appointment_time" style="color:#000;"></td>
			<td>
				<a href="#" class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="10" data-tooltip="click here to book appointment" @click="bookingConfirmation(appointment.unique_appointment_id)">Book</a>
			</td>
		</tr>
		@endslot
		@endcomponent
	</template>
</materialize-modal>



</div>

@endsection



