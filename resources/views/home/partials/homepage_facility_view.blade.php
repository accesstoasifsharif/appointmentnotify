<div class="row" style=" margin-bottom: 0px;">
	<div class="col s12 m6 l4 facility_view_div" v-for="appointment in facilityViewAppointmentQuery" @mouseover="startFacilityMarkerAnimation(appointment.facility_id)" @mouseleave="stopFacilityViewAnimation(appointment.facility_id)" >
<!-- 		<pre>
			@{{appointment}}
		</pre> -->
		<div class="card">
			<div class="teal waves-effect waves-block waves-light" style="height: 50px;">
			</div>
			<div class="card-content" style="">
				<image-view :target-id="appointment.facility_id" :target-model="appointment.health_care_facility_identifier" :img-class="'circle homepage_img'" style="width: 60px !important; height: 60px !important; position: absolute; top: 20px; z-index: 1; cursor: pointer;border-radius: 50%;"></image-view>
				<a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right" style="position: relative; top: -45px; right: -18px; margin-right: 10px !important;">
					<i class="material-icons">arrow_upward</i>
				</a>
				<span class="card-title activator grey-text text-darken-4 truncate" style="font-size: 20px; margin-top: 10px;">@{{appointment.facility_name}}</span>

				<p>
					<b>@{{appointment.total_appointments}}</b> available appointments with <b>@{{appointment.total_doctors}}</b> doctors
				</p>
				<!-- 				<p>Total available Appointments <br/> are <b>@{{appointment.total_appointments}}</b></p> -->

				<a href="#" @click="facilityViewAppointmentDetailByFacilityId(appointment.facility_id)" class="btn-floating btn-move-up waves-effect waves-light darken-2 right" style="position: relative; top: -20px; right: -18px; margin-right: 10px !important;">
					<i class="material-icons">info</i>
				</a>
			</div>
			<div class="card-reveal" style="display: none; transform: translateY(0px);  padding: 10px;">
				<span class="card-title"><h6><i class="material-icons right">close</i></h6></span>
				<span class="grey-text text-darken-4"><h5>@{{appointment.facility_name}}</h5></span>
				<p>
					@{{appointment.address_line_1}}
					@{{appointment.address_line_2}}
					@{{appointment.city_name}}
					@{{appointment.zip_code}}
					@{{appointment.postal_code}}
				</p>
				<a href="#appointmentDetail" class="btn-floating waves-effect waves-light"><i class="material-icons">info</i> </a>
			</div>
		</div>
	</div>
	@component('common.table')
	@slot('table_pagination')
	<ul class="pagination">
		<li v-if="facility_view_pagination.current_page > 1">
			<a href="#" aria-label="Previous" @click.prevent="homePageFacilityViewAppointmentPagination(facility_view_pagination.current_page - 1)">
				<span aria-hidden="true">&laquo;</span>
			</a>
		</li>
		<li v-for="page in FacilityViewPagesNumber" v-bind:class="[ page == FacilityViewIsActived ? 'active' : '']">
			<a href="#" @click.prevent="homePageFacilityViewAppointmentPagination(page)">@{{ page }}</a>
		</li>
		<li v-if="facility_view_pagination.current_page < facility_view_pagination.last_page">
			<a href="#" aria-label="Next" @click.prevent="homePageFacilityViewAppointmentPagination(facility_view_pagination.current_page + 1)">
				<span aria-hidden="true">&raquo;</span>
			</a>
		</li>
	</ul>
	@endslot

	@endcomponent
</div>
