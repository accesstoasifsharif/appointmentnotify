<div class="row" style=" margin-bottom: 0px;">
	<div class="col s12 m6 l4" v-for="appointment in providerViewAppointmentQuery" @mouseover="startMarkerAnimation(appointment.doctor_id)" @mouseleave="stopAnimation(appointment.doctor_id)" id="testDiv">
<!-- 		<pre>
			@{{appointment}}
		</pre> -->
		<div class="card">
			<div class="teal waves-effect waves-block waves-light" style="height: 50px;">
			</div>
			<div class="card-content" style="">
				<image-view :target-id="appointment.doctor_id" :target-model="appointment.doctor_identifier" :img-class="'circle homepage_img'" style="width: 60px !important; height: 60px !important; position: absolute; top: 20px; z-index: 1; cursor: pointer;border-radius: 50%;"></image-view>
				<a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right" style="position: relative; top: -45px; right: -18px; margin-right: 10px !important;">
					<i class="material-icons">arrow_upward</i>
				</a>
				<span class="card-title activator grey-text text-darken-4 truncate" style="font-size: 17px; margin-top: 10px;"><b> @{{appointment.doctor_name}}</b></span>
				<p style="font-size: 14px;"><b> @{{appointment.total_appointments}}</b> available Appointments across <b>@{{appointment.total_facility}}</b> Facilities</p>
				<a href="#" @click="appointmentDetail(appointment.doctor_id)" class="btn-floating btn-move-up waves-effect waves-light darken-2 right" style="position: relative; top: -20px; right: -18px; margin-right: 10px !important;">
					<i class="material-icons">info</i>
				</a>
			</div>
			<div class="card-reveal" style="display: none; transform: translateY(0px); padding: 10px;">
				<span class="card-title"><h6><i class="material-icons right">close</i></h6></span>
				<span class="grey-text text-darken-4"><h5 style="font-size: 20px;">@{{appointment.doctor_name}}</h5></span>
				<p style="font-size: 14px;">
					<i class="material-icons tiny">
						local_hospital
					</i>
					@{{appointment.specialty_name}}
				</p>
				<p>
					<i class="material-icons tiny">
						home
					</i>
					@{{appointment.doctor_address}}
				</p>
				<a href="#" @click="appointmentDetail(appointment.doctor_id)" class="btn-floating waves-effect waves-light"><i class="material-icons">info</i> </a>
			</div>
		</div>
	</div>
	@component('common.table')
	@slot('table_pagination')
	<ul class="pagination">
		<li v-if="pagination.current_page > 1">
			<a href="#" aria-label="Previous" @click.prevent="homePageAppointmentPagination(pagination.current_page - 1)">
				<span aria-hidden="true">&laquo;</span>
			</a>
		</li>
		<li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
			<a href="#" @click.prevent="homePageAppointmentPagination(page)">@{{ page }}</a>
		</li>
		<li v-if="pagination.current_page < pagination.last_page">
			<a href="#" aria-label="Next" @click.prevent="homePageAppointmentPagination(pagination.current_page + 1)">
				<span aria-hidden="true">&raquo;</span>
			</a>
		</li>
	</ul>
	@endslot

	@endcomponent
</div>
