<nav class="nav-extended">
	<div class="nav-wrapper">
		<div class="row nav_row">
			<div class="col s3">
				@include('common.partials.logo')
			</div>
			<div class="col s9">
				<ul id="nav-mobile" class="hide-on-med-and-down">
					<form method="GET">
						<li class="col s9">
							<ul>
								<li class="col s6">
									<div class="input-field">

										<input id="search" type="search" v-model="search_query" placeholder="Search by doctor , facility name" @keyup="getSearchQuery" @keydown="getSearchQuery"  @keyup.enter="getSearchQuery">
										<label class="label-icon" for="search"><i class="material-icons">search</i></label>
										<i class="material-icons">close</i>

									</div>
								</li>
								<li class="col s3">
									<div class="input-field">
										<input id="zip_code" type="search" placeholder="Zip Code" v-model="zip_code" @keyup="getZipCode" @keyup.enter="getZipCode" @keydown="getZipCode" style="padding-left: 0.5rem;">
										<!-- <label for="zip_code">Zip Code</label> -->
										<!-- <i class="material-icons" style="padding: 0px; height: 50px; line-height: 50px; font-size: small;">close</i> -->
									</div>
								</li>
								<li class="col s3">
									<select class="browser-default" v-model="distance" style="color: #000;" @change="getDistanceInMile">
										<option v-for="mile in miles" :value="mile.value" style="color: #000;">@{{mile.text}}</option>
									</select>
								</li>
							</ul>
						</li>
					</form>
					@if(Auth::guest())
					<li class="right">
						<a class="waves-effect waves-light btn" href="{{ route('login') }}">Login</a>
					</li>
					<li class="right">
						<a class="waves-effect waves-light btn" href="{{ route('register') }}">Register</a>
					</li>
					@else
					<li class="col s3">
						<div class="left">
							<a href="#" class="tooltipped" data-position="bottom" data-delay="10" data-tooltip="Goto Dashboard" style="padding: 0px;">
								<i class="material-icons">dashboard</i>
							</a>
						</div>
						<div class="right chip dropdown-button btn nav_chip" data-activates='main_user_dropdown'>
							<img src="/images/common/person-flat.png" alt="Contact Person">
							{{Auth::User()->fullName}}
						</div>
					</li>
					<li>
						<ul id='main_user_dropdown' class='dropdown-content'>
							<li><a href="#"><i class="material-icons">settings</i> Your Setting</a></li>
							<li class="divider"></li>
							<li><a href="#"><i class="material-icons">email</i>Email Us</a></li>
							<li> <a href="{{ route('logout') }}"
								onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">
								<i class="material-icons">power_settings_new</i>Logout
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form></li>
						</ul>
					</li>
					@endif
				</ul>
			</div>
		</div>
		@include('common.partials.headertabsmobileview')
	</div>
</nav>
