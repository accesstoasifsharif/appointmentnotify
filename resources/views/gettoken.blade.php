@extends('layouts.token')

@section('content')
<h1 class="well">Create Your Access Token</h1>
<passport-clients></passport-clients>

<passport-authorized-clients></passport-authorized-clients>

<passport-personal-access-tokens></passport-personal-access-tokens>

<a class="btn btn-primary" href="{{redirect()->back()->getTargetUrl()}}" > Go Back </a>
<a class="btn btn-primary" href="/" > Go Back to main</a>
@endsection

