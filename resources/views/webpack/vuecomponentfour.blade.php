<!DOCTYPE html>
<html>
<head>
	<title>Vue Component</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
	<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
</head>
<style type="text/css">
	body
	{
		padding-top: 20px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
	});
</script>
<body>
	<div class="container" id="root">
		<tabs>
			<tab name="about us" :selected="true">
				<p>
					here is the content of about us tab
				</p>
			</tab>
			<tab name="about our company">
				<p>
					here is the content of about our company tab
				</p>
			</tab>
			<tab name="about our culture">
				<p>
					here is the content of about our culture tab
				</p>
			</tab>
		</tabs>

	</div>

	<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>

	<script type="text/javascript">
		Vue.component('tabs',{
			template: `
				<div class="row">
					<div class="col s12">
						<ul class="tabs">
							<li class="tab col s3"><a href="#test1">Test 1</a></li>
							<li class="tab col s3"><a class="active" href="#test2">Test 2</a></li>
							<li class="tab col s3 disabled"><a href="#test3">Disabled Tab</a></li>
							<li class="tab col s3"><a href="#test4">Test 4</a></li>
						</ul>
					</div>
					<div class="tabs-detail">
						<slot> </slot>
					</div>
				</div>`,
				mounted() {
					console.log(this.$children);
				}
		});
		Vue.component('tab',{
			template: `
				<div>
					<slot>
					</slot>
				</div>`
		});
		new Vue({
			el: "#root"
		});
	</script>

</body>
</html>
