<!DOCTYPE html>
<html>
<head>
	<title>Vue Component</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
	<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
</head>
<style type="text/css">
	body
	{
		padding-top: 20px;
	}
</style>
<body>
	<div class="container" id="root">
		<a class="waves-effect waves-light btn" @click="showModal">Modal</a>

		<modal @close="closeModal" title="My first modal">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			
		</modal>
	</div>

	<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>

	<script type="text/javascript">
		Vue.component('modal',{
			props: ['title'],			
			template: `
			<div id="modal1" class="modal">
				<div class="modal-content">
					<h4 v-text="title"></h4>
					<p><slot></slot></p>
				</div>
				<div class="modal-footer">
					<button class="waves-effect waves-green btn-flat" @click="$emit('close')">Close</button>
				</div>
			</div>`
		});
		new Vue({
			el: "#root",
			data: {
				
			},
			methods: {
				showModal() {
					$('#modal1').modal('open');
				},
				closeModal()
				{
					$('#modal1').modal('close');
				}
			}
		});
	</script>

</body>
</html>
