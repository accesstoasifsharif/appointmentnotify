<!DOCTYPE html>
<html>
<head>
	<title>Vue Practice</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
</head>
<body>
	

	<div class="container" id="root">
		<coupan @applied="onCoupanApplied"></coupan>
		<h1 v-show="oncoupanapplied">Your Coupan Was Applied.</h1>
	</div>
	<div class="container" id="modalexample">
		<!-- Modal Trigger -->
		<a class="waves-effect waves-light btn" href="#modal1">Modal</a>
		<modal>
			<h1 slot='header'>Thsi is modal header</h1>
			<p>Here we can insert any type of text.</p>
		</modal>
		<!-- Modal Structure -->
		
	</div>

	<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/communication.js')}}"></script>

	<script type="text/javascript">
		$('#modal1').modal('open');

	</script>


	
</body>
</html>