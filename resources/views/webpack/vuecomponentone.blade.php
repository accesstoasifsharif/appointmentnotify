<!DOCTYPE html>
<html>
<head>
	<title>Vue Component</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
</head>
<body>
	<div class="container" id="root">
		<task-list></task-list>
	</div>


	<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>

	<script type="text/javascript">
		Vue.component('task-list',{
			template: `
				<div>
					<task v-for="task in tasks" v-text="task.task"></task >
				</div>
			`,
			data() {
				return {
					tasks: [
						{ task: 'First Task', complete: true },
						{ task: 'Second Task', complete: true },
						{ task: 'Third Task', complete: false },
						{ task: 'Fourth Task', complete: true },
						{ task: 'Fifth Task', complete: false }
					]
				};
			}
		});

		Vue.component('task',{
			template: '<li><slot></slot></li>'
		});

		new Vue({
			el: "#root"

		});
	</script>

</body>
</html>