<!DOCTYPE html>
<html>
<head>
	<title>{{ APP_NAME }}</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
</head>
<body>
<div class="container">
<h1>
  <input type="text" id="datepicker">
</h1>
  
</div>
 <nav>
  <div class="nav-wrapper">
    <a href="#!" class="brand-logo">Logo</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li><a href="sass.html">Sass</a></li>
      <li><a href="badges.html">Components</a></li>
      <li><a href="collapsible.html">Javascript</a></li>
      <li><a href="mobile.html">Mobile</a></li>
    </ul>
    <ul class="side-nav" id="mobile-demo">
      <li><a href="sass.html">Sass</a></li>
      <li><a href="badges.html">Components</a></li>
      <li><a href="collapsible.html">Javascript</a></li>
      <li><a href="mobile.html">Mobile</a></li>
    </ul>
  </div>
</nav>

<div class="container">
	<h1 style="padding: 10px;">
		<span>
      <i class="fa fa-user-md"></i>
    </span>
    Webpack Practice
    <span>
     <i class="glyphicon glyphicon-user"></i>
   </span>
 </h1>
 <hr/>
 <div class="row">
  <form class="col s12">
    <div class="row">
      <div class="input-field col s6">
        <input placeholder="Placeholder" id="first_name" type="text" class="validate">
        <label for="first_name">First Name</label>
      </div>
      <div class="input-field col s6">
        <input id="last_name" type="text" class="validate">
        <label for="last_name">Last Name</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12">
        <input disabled value="I am not editable" id="disabled" type="text" class="validate">
        <label for="disabled">Disabled</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12">
        <input id="password" type="password" class="validate">
        <label for="password">Password</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12">
        <input id="email" type="email" class="validate">
        <label for="email">Email</label>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        This is an inline input field:
        <div class="input-field inline">
          <input id="email" type="email" class="validate">
          <label for="email" data-error="wrong" data-success="right">Email</label>
        </div>
      </div>
    </div>
  </form>
</div>
<a class="waves-effect waves-light btn">button</a>
<a class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>button</a>
<a class="waves-effect waves-light btn"><i class="material-icons right">cloud</i>button</a>
<a class="waves-effect waves-light btn"><i class="material-icons right">insert_chart</i>button</a>
<br/>
<br/>
<br/>
<br/>
</div>
<footer class="page-footer">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">Footer Content</h5>
        <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
      </div>
      <div class="col l4 offset-l2 s12">
        <h5 class="white-text">Follow us:</h5>
        <ul>
          <li><a class="grey-text text-lighten-3" href="#!"><i class="fa fa-lg fa-facebook"></i> Facebook</a></li>
          <li><a class="grey-text text-lighten-3" href="#!"><i class="fa fa-lg fa-twitter"></i> Twitter</a></li>
          <li><a class="grey-text text-lighten-3" href="#!"><i class="fa fa-lg fa-google-plus"></i> Google+</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      © 2017 Copyright by AlleyHealth
      <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
    </div>
  </div>
</footer>


<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>
<script >
  $(document).ready(function () {
    $('#datepicker').datepicker();
  });
  $(".button-collapse").sideNav();
</script>
</body>
</html>















