<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/provider.css')}}">
</head>
<body>
  <div class="container">
    <nav class="top-nav">
      <div class="container">
        <div class="nav-wrapper"><a class="page-title">Buttons</a></div>
      </div>
    </nav>
    <div class="container"><a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a></div>
    <p class="caption">
      There are 3 main button types described in material design. The raised button is a standard button that signify actions and seek to give depth to a mostly flat page. The floating circular action button is meant for very important functions. Flat buttons are usually used within elements that already have depth like cards or modals.
    </p>
    <h2 class="header">Raised</h2>
    <a class="waves-effect waves-light btn">button</a>
    <a class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>button</a>
    <a class="waves-effect waves-light btn"><i class="material-icons right">cloud</i>button</a>        
    <div id="floating" class="section scrollspy">
      <h2 class="header">Floating</h2>
      <a class="btn-floating waves-effect waves-light btn-large red"><i class="material-icons">add</i></a><br><br>
      <pre><code class="language-markup col s12">
        &lt;a class="btn-floating btn-large waves-effect waves-light red">&lt;i class="material-icons">add</i>&lt;/i>&lt;/a>
      </code></pre>
      <br>

      <h4 class="light">Fixed Action Button</h4>
      <p>If you want a fixed floating action button, you can add multiple actions that will appear on hover. Our demo is in the bottom righthand corner of the page.</p>
      <pre><code class="language-markup col s12">
        &lt;div class="fixed-action-btn">
        &lt;a class="btn-floating btn-large red">
        &lt;i class="large material-icons">mode_edit&lt;/i>
        &lt;/a>
        &lt;ul>
        &lt;li>&lt;a class="btn-floating red">&lt;i class="material-icons">insert_chart&lt;/i>&lt;/a>&lt;/li>
        &lt;li>&lt;a class="btn-floating yellow darken-1">&lt;i class="material-icons">format_quote&lt;/i>&lt;/a>&lt;/li>
        &lt;li>&lt;a class="btn-floating green">&lt;i class="material-icons">publish&lt;/i>&lt;/a>&lt;/li>
        &lt;li>&lt;a class="btn-floating blue">&lt;i class="material-icons">attach_file&lt;/i>&lt;/a>&lt;/li>
        &lt;/ul>
        &lt;/div>
      </code></pre>

      <p>You can also open and close the Fixed Action Button Menu programatically.</p>
      <pre><code class="language-javascript">
        $('.fixed-action-btn').openFAB();
        $('.fixed-action-btn').closeFAB();
        $('.fixed-action-btn.toolbar').openToolbar();
        $('.fixed-action-btn.toolbar').closeToolbar();
      </code></pre>
      <br>
      <h4 class="light">Horizontal FAB</h4>
      <p>Creating a horizontal FAB is easy! Just add the class <code class="language-markup">horizontal</code> to the FAB.</p>
      <div style="position: relative; height: 70px;">
        <div class="fixed-action-btn horizontal" style="position: absolute; display: inline-block; right: 24px;">
          <a class="btn-floating btn-large red">
            <i class="large material-icons">mode_edit</i>
          </a>
          <ul>
            <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>
            <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
            <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
            <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
          </ul>
        </div>
      </div>
      <pre><code class="language-markup col s12">
        &lt;div class="fixed-action-btn horizontal">
        &lt;a class="btn-floating btn-large red">
        &lt;i class="large material-icons">mode_edit&lt;/i>
        &lt;/a>
        &lt;ul>
        &lt;li>&lt;a class="btn-floating red">&lt;i class="material-icons">insert_chart&lt;/i>&lt;/a>&lt;/li>
        &lt;li>&lt;a class="btn-floating yellow darken-1">&lt;i class="material-icons">format_quote&lt;/i>&lt;/a>&lt;/li>
        &lt;li>&lt;a class="btn-floating green">&lt;i class="material-icons">publish&lt;/i>&lt;/a>&lt;/li>
        &lt;li>&lt;a class="btn-floating blue">&lt;i class="material-icons">attach_file&lt;/i>&lt;/a>&lt;/li>
        &lt;/ul>
        &lt;/div>
      </code></pre>

      <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
        <a class="btn-floating btn-large red">
          <i class="material-icons">mode_edit</i>
        </a>
        <ul>
          <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>
          <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
          <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
          <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
        </ul>
      </div>
      <br>

      <h4 class="light">Click-only FAB</h4>
      <p>If you want to disable the hover behaviour, and instead toggle the FAB menu when the user clicks on the large button (works great on mobile!), just add the <code class="language-markup">click-to-toggle</code> class to the FAB.</p>
      <div style="position: relative; height: 70px;">
        <div class="fixed-action-btn horizontal click-to-toggle" style="position: absolute; right: 24px;">
          <a class="btn-floating btn-large red">
            <i class="material-icons">menu</i>
          </a>
          <ul>
            <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>
            <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
            <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
            <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
          </ul>
        </div>
      </div>
      <pre><code class="language-markup col s12">
        &lt;div class="fixed-action-btn horizontal click-to-toggle">
        &lt;a class="btn-floating btn-large red">
        &lt;i class="material-icons">menu&lt;/i>
        &lt;/a>
        &lt;ul>
        &lt;li>&lt;a class="btn-floating red">&lt;i class="material-icons">insert_chart&lt;/i>&lt;/a>&lt;/li>
        &lt;li>&lt;a class="btn-floating yellow darken-1">&lt;i class="material-icons">format_quote&lt;/i>&lt;/a>&lt;/li>
        &lt;li>&lt;a class="btn-floating green">&lt;i class="material-icons">publish&lt;/i>&lt;/a>&lt;/li>
        &lt;li>&lt;a class="btn-floating blue">&lt;i class="material-icons">attach_file&lt;/i>&lt;/a>&lt;/li>
        &lt;/ul>
        &lt;/div>
      </code></pre>

      <h4 class="light">FAB to Toolbar</h4>
      <p>Instead of displaying individual button options, you can transition your FAB into a toolbar on click. Just add the <code class="language-markup">toolbar</code> class to the FAB.</p>
      <iframe src="fab-toolbar-demo.html" frameborder="0" width="100%" height="100px"></iframe>
      <pre><code class="language-markup col s12">
        &lt;div class="fixed-action-btn toolbar">
        &lt;a class="btn-floating btn-large red">
        &lt;i class="large material-icons">mode_edit&lt;/i>
        &lt;/a>
        &lt;ul>
        &lt;li class="waves-effect waves-light">&lt;a href="#!">&lt;i class="material-icons">insert_chart&lt;/i>&lt;/a>&lt;/li>
        &lt;li class="waves-effect waves-light">&lt;a href="#!">&lt;i class="material-icons">format_quote&lt;/i>&lt;/a>&lt;/li>
        &lt;li class="waves-effect waves-light">&lt;a href="#!">&lt;i class="material-icons">publish&lt;/i>&lt;/a>&lt;/li>
        &lt;li class="waves-effect waves-light">&lt;a href="#!">&lt;i class="material-icons">attach_file&lt;/i>&lt;/a>&lt;/li>
        &lt;/ul>
        &lt;/div>
      </code></pre>

    </div>
  </div>

  <script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
</body>
</html>