<!DOCTYPE html>
<html>
<head>
	<title>Vue Component</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
</head>
<body>
	<div class="container" id="root">
		<card title="Salam World" body="Lorem ipsum dolor sit amet."></card>
		<card title="hello World" body="my name is asif"></card>
	</div>

		<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>

		<script type="text/javascript">
			Vue.component('card',{
				props: ['title','body'],
				data() {
					return {
						isVisible: true
					}
				},
				template: `
					<div class="card blue-grey darken-1" v-show="isVisible">
						<div class="card-content white-text">
							<span class="card-title" v-text="title">
							</span>
							<p v-text="body">
							</p>
							</div>
							<div class="card-action">
								<a href="#" @click="isVisible=false">Hide Me</a>
								<a href="#">This is a link</a>
							</div>
						</div>
					</div>`
			})
			new Vue({
				el: "#root"

			});
		</script>

	</body>
	</html>