<!DOCTYPE html>
<html>
<head>
	<title>Vue Component</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
	<script type="text/javascript" src="{{ asset('js/common.js')}}"></script>
</head>
<style type="text/css">
	body
	{
		padding-top: 20px;
	}
</style>
<body>
	<div class="container" id="root">
		<coupon></coupon>
	</div>

	<script type="text/javascript" src="{{ asset('js/app.siteuser.js')}}"></script>

	<script type="text/javascript">
		window.Event = new class {
			constructor () {
				this.vue = new Vue();
			}

			fire (event, data=null) {
				this.vue.$emit(event,data);
			}
			
			listen (event, callback) {
				this.vue.$on(event,callback);
			}
		}

		Vue.component('coupon',{
			template: `
				<input placeholder="Enter your Coupon Code" @blur="onCouponApplied">`,
			methods: {
				onCouponApplied() {
					Event.fire('applied');
				}
			}
		});
		new Vue({
			el: "#root",
			data: {
				couponApplied : false
			},
			created() {
				Event.listen('applied',() => alert('handling it!'));
			}
		});
	</script>

</body>
</html>

