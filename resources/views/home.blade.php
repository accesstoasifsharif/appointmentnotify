@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
            <button  type="submit" @click.prevent="makeToast">make Toast</button>
            {{$sessionValue}}
            <button  type="submit" @click.prevent="checkSession">check Session</button>
            <button  type="submit" @click.prevent="checkPassport">check Passport</button>

            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <input id="icon_prefix"  type="text" name="email"   >
                <input id="icon_prefix_login" type="password" name="password"   >
                <button  type="submit" @click.prevent="validateLoginCredentials">Validate User</button>

            </form>
        </div>
    </div>
</div>
@endsection
