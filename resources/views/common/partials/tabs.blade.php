<div class="nav-content">
      <ul class="tabs tabs-transparent">
	<li class="tab"><a class="active white-text text-darken-2" href="#">Dashboard</a></li>
	<li class="tab"><a class=" waves-effect waves-light" href="#">Facilities</a></li>
	<li class="tab"><a class=" waves-effect waves-light" href="#">Staff</a></li>
	<li class="tab"><a class=" waves-effect waves-light" href="#">Appointments</a></li>
	<li class="tab"><a class=" waves-effect waves-light" href="#">Calendar Dashboard</a></li>
	<li class="tab"><a class=" waves-effect waves-light" href="#">Messages</a></li>
	<li class="tab"><a class=" waves-effect waves-light" href="#">Short Call List</a></li>
    <div class="indicator white" style="z-index:1"></div>
      </ul>
    </div>