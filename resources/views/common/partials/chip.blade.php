<ul id="nav-mobile" class="right hide-on-med-and-down">
	<li>
		<div class="chip nav_chip dropdown-button btn" data-activates='main_user_dropdown'>
			<img src="../images/common/person-flat.png" alt="Contact Person">
			Dr. Glenda Santa
		</div>
	</li>
	<li>
		<ul id='main_user_dropdown' class='dropdown-content'>
			<li><a href="#"><i class="material-icons">settings</i> Your Setting</a></li>
			<li class="divider"></li>
			<li><a href="#"><i class="material-icons">email</i>Email Us</a></li>
			<li><a href="#"><i class="material-icons">power_settings_new</i> Logout</a></li>
		</ul>
	</li>
</ul>