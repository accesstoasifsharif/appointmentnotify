<ul class="side-nav" id="mobile-demo">
	<li>
		<div class="user-view">
			<a href="#"><img src="{{asset('images/logo/logo.png')}}"></a>
		</div>
	</li>
	<li>
		<div class="chip dropdown-button btn" data-activates='sidebar_user_dropdown'>
			<img src="{{asset('images/common/person-flat.png')}}" alt="Contact Person">
			Dr. Glenda Santa
		</div>
	</li>
	<li>
		<ul id='sidebar_user_dropdown' class='dropdown-content'>
			<li><a href="#"><i class="material-icons">settings</i> Your Setting</a></li>
			<li class="divider"></li>
			<li><a href="#"><i class="material-icons">email</i>Email Us</a></li>
			<li><a href="#"><i class="material-icons">power_settings_new</i> Logout</a></li>
		</ul>
	</li>
	<li><a class="waves-effect" href="#">Dashboard</a></li>
	<li><a class="waves-effect" href="#">Facilities</a></li>
	<li><a class="waves-effect" href="#">Staff</a></li>
	<li><a class="waves-effect" href="#">Appointments</a></li>
	<li><a class="waves-effect" href="#">Calendar Dashboard</a></li>
	<li><a class="waves-effect" href="#">Messages</a></li>

	<li><a class="waves-effect" href="#">Patients</a></li>
	<!-- <a href="#" class="btn waves-effect waves-light" id="closeSideNav">close</a> -->


	<li><a class="waves-effect" href="#">Short Call List</a></li>

</ul>
