<ul class="hide-on-med-and-down">
	<li class="col s8" >
		@if(!Auth::guest())
		@if(Auth::User()->user_type == config('appointmentnotify.'.HealthCareProvider.'.user_type_id'))
		<ul>
			<li class="col s6">
				<h5 class="truncate">{{$selected_perspective_name}}</h5>

			</li>
			<li class="col s6">
				@include('healthcareprovider.partials.healthcareprovider_search')
			</li>
		</ul>
		@endif
		@if(Auth::User()->user_type == config('appointmentnotify.'.SiteUser.'.user_type_id'))
		<ul>
			<li class="col s12">
				@include('siteuser.partials.siteuser_search')
			</li>
		</ul>
		@endif
		@endif
	</li>
	@if(Auth::guest())
	<li class="right">
		<a class="waves-effect waves-light btn" href="{{ route('login') }}">Login</a>
	</li>
	<li class="right">
		<a class="waves-effect waves-light btn" href="{{ route('register') }}">Register</a>
	</li>
	@else
	<li class="col s4">
		<div class="col s5">
			<notification-bell></notification-bell>
		</div>
		<div class="right chip dropdown-button btn nav_chip" data-activates='main_user_dropdown'>
			<img src="/images/common/person-flat.png" alt="Contact Person">
			{{Auth::User()->name}}
		</div>

	</li>
	<li>
		<ul id='main_user_dropdown' class='dropdown-content'>
			<li><a href="/siteuser/{{$selected_perspective_slug}}/accountsetting"><i class="material-icons">settings</i> Your Setting</a></li>
			<li class="divider"></li>
			<li><a href="#"><i class="material-icons">email</i>Email Us</a></li>
			<li> <a href="{{ route('logout') }}"
				onclick="event.preventDefault();
				document.getElementById('logout-form').submit();">
				<i class="material-icons">power_settings_new</i>Logout
			</a>

			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
			</form></li>
		</ul>
	</li>
	@endif
</ul>