<article>
	<p>
		<!-- visible paragraph will be placed here -->
		{{ $visible_text }}
		<a class="read-more-show hide teal-text" href="#" id="help_text_id">Read More</a>
		<span class="read-more-content">
			<!-- hidden paragraph will be placed here -->
			{{ $hidden_text }}
			<a class="read-more-hide hide teal-text" href="#" more-id="help_text_id">Read Less</a>
		</span>
	</p>
</article>
