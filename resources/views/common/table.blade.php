<table class="striped">
	<div class="row">
		@if(isset($table_search))
		<div class="col s6">
			{{ $table_search }}
		</div>
		@endif

		@if(isset($table_search_btn))
		<div class="col s2">
			{{ $table_search_btn }}
		</div>
		@endif

		@if(isset($table_page_select))
		<div class="col s4">
			{{ $table_page_select }}
		</div>
		@endif

	</div>
	<thead>
		@if(isset($table_head))
		<tr>
			{{ $table_head }}
		</tr>
		@endif
	</thead>

	<tbody>
		@if(isset($table_data))
		{{ $table_data }}
		@endif
	</tbody>
</table>

@if(isset($table_pagination))
<div class="row">
	<div class="col s12">
		{{ $table_pagination }}
	</div>
</div>
@endif



