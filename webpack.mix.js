let mix = require('laravel-mix');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



 mix.copy('node_modules/font-awesome/fonts','public/fonts/font-awesome')
 .copy('node_modules/materialize-css/dist/fonts/roboto','public/fonts/materialize-css/roboto/');

 mix.sass('resources/assets/sass/scss/common/common.scss','../resources/assets/sass/css/external_css');

 var node_modules_css = [
 'node_modules/font-awesome/css/font-awesome.css',
 'node_modules/animate.css/animate.css',
 'node_modules/sweetalert/dist/sweetalert.css'
 ];

 mix.combine(node_modules_css,'resources/assets/sass/css/external_css/all_node_modules.css');
 mix.combine('resources/assets/sass/css/external_css/*.css','public/css/common.css');

 mix.sass('resources/assets/sass/scss/healthcareprovider/healthcareprovider.scss','public/css/')
 .sass('resources/assets/sass/scss/siteuser/siteuser.scss','public/css/')
 .sass('resources/assets/sass/scss/admin/admin.scss','public/css/');







 







 mix.js('resources/assets/js/app.auth.SU.signup.wizard.js','public/js/');
 mix.js('resources/assets/js/app.auth.HCP.signup.wizard.js','public/js/');
 mix.js('resources/assets/js/app.siteuser.js','public/js/');
 mix.js('resources/assets/js/app.healthcareprovider.js','public/js/');
 mix.js('resources/assets/js/app.auth.js','public/js/');
 mix.js('resources/assets/js/app.admin.js','public/js/');
 mix.js('resources/assets/js/app.admin.adnan.js','public/js/');
 mix.js('resources/assets/js/app.home.js','public/js/');
 var node_modules_js = [
 'node_modules/jquery/dist/jquery.js',
 'node_modules/jquery-ui-dist/jquery-ui.js',
 'node_modules/materialize-css/dist/js/materialize.js'
 ];
 mix.combine(node_modules_js,'resources/assets/js/external/all_node_modules.js');

 mix.combine([
 	'resources/assets/js/external/*.js',
 	'resources/assets/js/internal/common/*.js'],'public/js/common.js');
 mix.combine([
 	'resources/assets/js/internal/siteuser/*.js'],'public/js/siteuser.js');
 mix.combine([
 	'resources/assets/js/internal/healthcareprovider/*.js'],'public/js/healthcareprovider.js');

 mix.combine([
 	'resources/assets/js/internal/admin/*.js'],'public/js/admin.js');




 mix.browserSync('appointmentnotify.dev');
 