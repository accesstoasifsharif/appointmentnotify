<?php
define('APP_NAME', 'Appointment Notify');

define('Config_File_Name', 'appointmentnotify');

define('COMPANY_NAME', 'Appointment Notify Inc');

define('ASSET_FILES', 'assetfiles/');

define('ZIP_CODE_FILE', 'zipcode.csv');

define('HOME_PAGE_PAGINATE', 6);

define('HealthCareProvider', 'HealthCareProvider');

define('US_State_FILE', 'states.csv');

define('DEFAULT_PAGE_PAGINATION', 5);

define('SiteUser', 'SiteUser');

define('Admin', 'Admin');

define('Hospital', 'Hospital');
define('MedicalClinic', 'Medical Clinic');
define('SinglePractice', 'Single Practice');
define('Staff', 'Staff');
define('Department', 'Department');

//define ('APP_USERTYPE',['HealthCareProvider','SiteUser','Staff','MedicalClinic','Doctor','SuperAdmin','Admin']);
//static $MYARRAY = array('123', '234');