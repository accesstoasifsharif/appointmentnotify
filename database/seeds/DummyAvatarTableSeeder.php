<?php

use Illuminate\Database\Seeder;
use Model\DummyAvatar;

class DummyAvatarTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/defaultUser.JPG',
			'gender' => 'other',
			'default' => '0',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/defaultMale.png',
			'gender' => 'male',
			'default' => '0',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/user3.png',
			'gender' => 'male',
			'default' => '1',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/user4.png',
			'gender' => 'male',
			'default' => '1',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/user5.png',
			'gender' => 'male',
			'default' => '1',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/user6.png',
			'gender' => 'male',
			'default' => '1',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/user7.png',
			'gender' => 'male',
			'default' => '1',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/defaultFemale.png',
			'gender' => 'female',
			'default' => '0',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/user9.png',
			'gender' => 'female',
			'default' => '1',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/user10.png',
			'gender' => 'female',
			'default' => '1',
			'user_type' => config('appointmentnotify.model_identifiers.SiteUser'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/defaultFemaleDoctor.png',
			'gender' => 'female',
			'default' => '0',
			'user_type' => config('appointmentnotify.model_identifiers.Doctor'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/defaultMaleDoctor.png',
			'gender' => 'male',
			'default' => '0',
			'user_type' => config('appointmentnotify.model_identifiers.Doctor'),
		]);

		DummyAvatar::create([
			'avatar' => 'images/common/dummy_avatar/facility.png',
			'default' => '0',
			'gender' => 'other',
			'user_type' => config('appointmentnotify.model_identifiers.HealthCareFacility'),
		]);

	}
}
