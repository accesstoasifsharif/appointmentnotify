<?php

use App\Events\CityZipCodeSeedEvent;
use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	event(new CityZipCodeSeedEvent());
    }
}
