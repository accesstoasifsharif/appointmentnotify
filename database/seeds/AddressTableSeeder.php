<?php

use Illuminate\Database\Seeder;
use Model\Address;

class AddressTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Address::create([
			'address_line_1' => '12 fanning',
			'address_line_2' => 'William Street',
			'city_name' => 'Staten Island',
			'zip_code' => '10314',
			'state_id' => 1,
			'country_id' => 1,
			'addressable_id' => 1,
			'addressable_type' => 'Model\SiteUser',
		]);
		Address::create([
			'address_line_1' => '25 Fanning S',
			'address_line_2' => '',
			'city_name' => 'Staten Island',
			'zip_code' => '10314',
			'state_id' => 1,
			'country_id' => 1,
			'addressable_id' => 1,
			'addressable_type' => 'Model\HealthCareFacility',
		]);
		Address::create([
			'address_line_1' => '209 Steinway Ave',
			'address_line_2' => '',
			'city_name' => 'Staten Island',
			'zip_code' => '10314',
			'state_id' => 1,
			'country_id' => 1,
			'addressable_id' => 2,
			'addressable_type' => 'Model\HealthCareFacility',
		]);
		/*address detail of doctor*/
		Address::create([
			'address_line_1' => '209 Steinway Ave',
			'address_line_2' => 'block 2',
			'city_name' => 'Staten Island',
			'zip_code' => '10314',
			'state_id' => 1,
			'country_id' => 1,
			'addressable_id' => 1,
			'addressable_type' => 'Model\Doctor',
		]);
		Address::create([
			'address_line_1' => '314 Saint Mary',
			'address_line_2' => 'Apartment 2',
			'city_name' => 'Staten Island',
			'zip_code' => '10314',
			'state_id' => 1,
			'country_id' => 1,
			'addressable_id' => 2,
			'addressable_type' => 'Model\Doctor',
		]);
		Address::create([
			'address_line_1' => '123 levenshtein st',
			'address_line_2' => '',
			'city_name' => 'Staten Island',
			'zip_code' => '10314',
			'state_id' => 1,
			'country_id' => 1,
			'addressable_id' => 3,
			'addressable_type' => 'Model\Doctor',
		]);

	}
}
