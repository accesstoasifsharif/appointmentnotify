<?php

use Illuminate\Database\Seeder;
use Model\SiteUser;

class SiteUserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$siteUser1 = SiteUser::create([
			'user_id' => '9',
		]);
		$siteUser2 = SiteUser::create([
			'user_id' => '11',
		]);
		$siteUser3 = SiteUser::create([
			'user_id' => '12',
		]);
		$siteUser4 = SiteUser::create([
			'user_id' => '10',
		]);
		$siteUser1->doctors()->attach([1, 5, 2, 3]);
		$siteUser2->doctors()->attach([4, 5, 3]);
		$siteUser3->doctors()->attach([5, 1, 3]);
		$siteUser4->doctors()->attach([1, 5, 3]);
	}
}
