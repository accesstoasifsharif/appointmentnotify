<?php

use Illuminate\Database\Seeder;
use Model\Avatar;

class AvatarTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Avatar::create([
			'avatar' => 'images/common/dummy_avatar/user6.png',
			'avatarable_id' => 4,
			'avatarable_type' => 'Model\SiteUser',
		]);
		// Avatar::create([
		// 	'avatar' => 'images/avatar/male-doc.png',
		// 	'avatarable_id' => 1,
		// 	'avatarable_type' => 'Model\Doctor',
		// ]);
		// Avatar::create([
		// 	'avatar' => 'images/avatar/male-doc.png',
		// 	'avatarable_id' => 2,
		// 	'avatarable_type' => 'Model\Doctor',
		// ]);
		// Avatar::create([
		// 	'avatar' => 'images/avatar/male-doc.png',
		// 	'avatarable_id' => 3,
		// 	'avatarable_type' => 'Model\Doctor',
		// ]);
		// Avatar::create([
		// 	'avatar' => 'images/avatar/male-doc.png',
		// 	'avatarable_id' => 4,
		// 	'avatarable_type' => 'Model\Doctor',
		// ]);
		// Avatar::create([
		// 	'avatar' => 'images/avatar/male-doc.png',
		// 	'avatarable_id' => 5,
		// 	'avatarable_type' => 'Model\Doctor',
		// ]);

	}
}
