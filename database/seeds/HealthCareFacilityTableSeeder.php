<?php

use Illuminate\Database\Seeder;
use Model\HealthCareFacility;
use Model\NationalProviderIdentification;

class HealthCareFacilityTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$facility = new HealthCareFacility();
		$facility->facility_name = 'Clove Lake Health Care';
		$facility->longitude = -74.123028;
		$facility->latitude = 40.608343;
		$facility->honor_id = 1;
		$facility->creator_id = 1;
		$facility->save();
		$facility->NPI()->save(new NationalProviderIdentification(['NPI' => '1417952417']));

		$facility = new HealthCareFacility();
		$facility->facility_name = 'Richmond University Medical Center';
		$facility->longitude = -74.105962;
		$facility->latitude = 40.635792;
		$facility->honor_id = 1;
		$facility->creator_id = 1;
		$facility->save();
		$facility->NPI()->save(new NationalProviderIdentification(['NPI' => '1811054042']));

		$facility = new HealthCareFacility();
		$facility->facility_name = 'Eger Health Care';
		$facility->longitude = -74.129891;
		$facility->latitude = 40.579884;
		$facility->honor_id = 1;
		$facility->creator_id = 1;
		$facility->save();
		$facility->NPI()->save(new NationalProviderIdentification(['NPI' => '1356331565']));

		$facility = new HealthCareFacility();
		$facility->facility_name = 'Staten Island University Hospital';
		$facility->longitude = -74.08581;
		$facility->latitude = 40.584438;
		$facility->honor_id = 1;
		$facility->creator_id = 1;
		$facility->save();
		$facility->NPI()->save(new NationalProviderIdentification(['NPI' => '1912925199']));

		$facility = new HealthCareFacility();
		$facility->facility_name = 'Staten Island University Hospital';
		$facility->longitude = -74.075609;
		$facility->latitude = 40.640588;
		$facility->honor_id = 1;
		$facility->creator_id = 1;
		$facility->save();
		$facility->NPI()->save(new NationalProviderIdentification(['NPI' => '1407877137']));

		$facility = new HealthCareFacility();
		$facility->facility_name = 'Golden Gate Rehabilitation';
		$facility->longitude = -74.131363;
		$facility->latitude = 40.606724;
		$facility->honor_id = 1;
		$facility->creator_id = 1;
		$facility->save();
		$facility->NPI()->save(new NationalProviderIdentification(['NPI' => '1174510648']));

		$facility = new HealthCareFacility();
		$facility->facility_name = 'Eg Care Management';
		$facility->longitude = -74.162265;
		$facility->latitude = 40.592077;
		$facility->honor_id = 1;
		$facility->creator_id = 1;
		$facility->save();
		$facility->NPI()->save(new NationalProviderIdentification(['NPI' => '1811316516']));

	}
}
