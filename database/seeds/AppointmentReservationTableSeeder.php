<?php
use Illuminate\Database\Seeder;
use Model\AnEvent;
use Model\AppointmentReservation;

class AppointmentReservationTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-2-2',
			'start_time' => '12:45',
			'end_time' => '1:00',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '1',
			'health_care_provider_facility_id' => '1',
			'appointment_reason_id' => '1',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReservation::create(['site_user_id' => $an_event->site_user_id,
			'an_event_id' => $an_event->id]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-3-6',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '1',
			'health_care_provider_facility_id' => '1',
			'appointment_reason_id' => '2',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReservation::create(['site_user_id' => $an_event->site_user_id,
			'an_event_id' => $an_event->id]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2016-3-6',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '1',
			'health_care_provider_facility_id' => '1',
			'appointment_reason_id' => '3',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReservation::create(['site_user_id' => $an_event->site_user_id,
			'an_event_id' => $an_event->id]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-6-6',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '2',
			'health_care_provider_facility_id' => '2',
			'appointment_reason_id' => '4',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReservation::create(['site_user_id' => $an_event->site_user_id,
			'an_event_id' => $an_event->id]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-08-08',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '2',
			'health_care_provider_facility_id' => '2',
			'appointment_reason_id' => '4',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReservation::create(['site_user_id' => $an_event->site_user_id,
			'an_event_id' => $an_event->id]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-08-08',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '2',
			'health_care_provider_facility_id' => '2',
			'appointment_reason_id' => '4',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReservation::create(['site_user_id' => $an_event->site_user_id,
			'an_event_id' => $an_event->id]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-08-08',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '2',
			'health_care_provider_facility_id' => '2',
			'appointment_reason_id' => '4',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReservation::create(['site_user_id' => $an_event->site_user_id,
			'an_event_id' => $an_event->id]);

	}

}
