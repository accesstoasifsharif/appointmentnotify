<?php

use Illuminate\Database\Seeder;
use Model\HealthCareProviderFacility;

class HealthCareProviderFacilityTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		HealthCareProviderFacility::create([
			'health_care_facility_id' => 1,
			'health_care_provider_id' => 1,
		]);
		HealthCareProviderFacility::create([
			'health_care_facility_id' => 2,
			'health_care_provider_id' => 1,
		]);
		HealthCareProviderFacility::create([
			'health_care_facility_id' => 2,
			'health_care_provider_id' => 2,
		]);
		HealthCareProviderFacility::create([
			'health_care_facility_id' => 3,
			'health_care_provider_id' => 4,
		]);
		HealthCareProviderFacility::create([
			'health_care_facility_id' => 4,
			'health_care_provider_id' => 3,
		]);
	}
}
