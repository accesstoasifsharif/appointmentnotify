<?php

use Illuminate\Database\Seeder;
use Model\Department;
use Model\HealthCareProvider;
use Model\Hospital;

class HealthCareProviderTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		HealthCareProvider::create([
			'user_id' => '5',
			'provider_type' => '0',
		]);

		HealthCareProvider::create([
			'user_id' => '6',
			'provider_type' => '1',
		]);

		HealthCareProvider::create([
			'user_id' => '7',
			'provider_type' => '2',
		]);

		HealthCareProvider::create([
			'user_id' => '8',
			'provider_type' => '3',
		]);

		Hospital::create([
			'hospital_name' => 'National Hospital',
			'health_care_provider_id' => 1,
		]);

		$department = new Department();
		$department->department_name = "Cardiology";
		$department->health_care_provider_id = 3;
		$department->parent_id = 0;
		$department->save();

	}
}
