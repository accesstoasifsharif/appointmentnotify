<?php

use Illuminate\Database\Seeder;
use Model\HealthCareProviderDoctor;

class HealthCareProviderDoctorTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		HealthCareProviderDoctor::create([
			'hcp_facility_id' => 1,
			'doctor_id' => 1,
		]);
		HealthCareProviderDoctor::create([
			'hcp_facility_id' => 2,
			'doctor_id' => 1,
		]);
		HealthCareProviderDoctor::create([
			'hcp_facility_id' => 2,
			'doctor_id' => 2,
		]);
		HealthCareProviderDoctor::create([
			'hcp_facility_id' => 3,
			'doctor_id' => 2,
		]);
		HealthCareProviderDoctor::create([
			'hcp_facility_id' => 3,
			'doctor_id' => 3,
		]);
		HealthCareProviderDoctor::create([
			'hcp_facility_id' => 3,
			'doctor_id' => 3,
		]);
		HealthCareProviderDoctor::create([
			'hcp_facility_id' => 4,
			'doctor_id' => 4,
		]);
		HealthCareProviderDoctor::create([
			'hcp_facility_id' => 5,
			'doctor_id' => 4,
		]);
	}
}
