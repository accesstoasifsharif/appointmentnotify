<?php

use Illuminate\Database\Seeder;
use Model\Feature;

class FeaturesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		foreach (config('appointmentnotify.features') as $key => $value) {
			Feature::create([
				'feature_name' => $value,
			]);
		}
	}
}
