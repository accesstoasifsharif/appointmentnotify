<?php

use App\AppointmentHistory;
use Illuminate\Database\Seeder;

class AppointmentHistoriesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		AppointmentHistory::create([
			'calendar_type' => '0',
			'date' => '2017-08-09',
			'start_time' => '12:45',
			'end_time' => '01:00',
			'an_event_id' => '3',
			'site_user_id' => '4',
			'doctor_id' => '4',
			'health_care_facility_id' => '3',
			'health_care_provider_facility_id' => '5',
		]);
		AppointmentHistory::create([
			'calendar_type' => '0',
			'date' => '2017-08-10',
			'start_time' => '05:00',
			'end_time' => '06:30',
			'site_user_id' => '4',
			'doctor_id' => '4',
			'health_care_facility_id' => '7',
			'health_care_provider_facility_id' => '4',
		]);

	}
}
