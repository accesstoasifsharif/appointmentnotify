<?php

use App\BusinessType;
use App\PerspectiveLevel;
use App\Role;
use App\RoleLevel;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run() {

		foreach (config('appointmentnotify.' . HealthCareProvider . '.type') as $key => $value) {
			BusinessType::create([
				'business_name' => $key,
				'config_id' => $value,
			]);
		}

		foreach (config('appointmentnotify.role_levels') as $key => $value) {
			RoleLevel::create([
				'level_name' => $value,
			]);
		}

		// $role_levels = RoleLevel::all();
		// $business_types = BusinessType::all();
		// $features = Feature::all();

		// foreach ($role_levels as $role_level) {
		// 	foreach ($business_types as $business_type) {
		// 		foreach ($features as $feature) {
		// 			Role::create([
		// 				'name' => $role_level->level_name . '-' . $business_type->business_name . '-' . $feature->feature_name,
		// 				'label' => $role_level->level_name . '-' . $business_type->business_name . '-' . $feature->feature_name,
		// 				'business_type_id' => $business_type->id,
		// 				'role_level_id' => $role_level->id,
		// 				'feature_id' => $feature->id,
		// 			]);
		// 		}
		// 	}
		// }

		foreach (config('appointmentnotify.business_type_hospital_admin') as $key => $value) {
			Role::create($value);
		}
		foreach (config('appointmentnotify.business_type_hospital_non_admin') as $key => $value) {
			Role::create($value);
		}
		foreach (config('appointmentnotify.business_type_medical_clinic_admin') as $key => $value) {
			Role::create($value);
		}
		foreach (config('appointmentnotify.business_type_medical_clinic_non_admin') as $key => $value) {
			Role::create($value);
		}
		foreach (config('appointmentnotify.business_type_department_admin') as $key => $value) {
			Role::create($value);
		}
		foreach (config('appointmentnotify.business_type_department_non_admin') as $key => $value) {
			Role::create($value);
		}
		foreach (config('appointmentnotify.business_type_single_practice_admin') as $key => $value) {
			Role::create($value);
		}
		foreach (config('appointmentnotify.business_type_single_practice_non_admin') as $key => $value) {
			Role::create($value);
		}

		PerspectiveLevel::create([
			'user_id' => '5',
			'health_care_provider_id' => '1',
			'role_level_id' => '1',
		]);
		PerspectiveLevel::create([
			'user_id' => '6',
			'health_care_provider_id' => '2',
			'role_level_id' => '1',
		]);

		PerspectiveLevel::create([
			'user_id' => '7',
			'health_care_provider_id' => '3',
			'role_level_id' => '1',
		]);

		PerspectiveLevel::create([
			'user_id' => '8',
			'health_care_provider_id' => '1',
			'role_level_id' => '2',
		]);

		PerspectiveLevel::create([
			'user_id' => '8',
			'health_care_provider_id' => '4',
			'role_level_id' => '1',
		]);

	}

}
