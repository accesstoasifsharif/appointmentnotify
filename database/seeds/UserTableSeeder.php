<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

	public function run() {
		$email = '@cherry.com';

		User::create([
			'user_type' => '1',
			'email' => 'nationalhospital' . $email,
			'name' => 'National Hospital',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);

		User::create([
			'user_type' => '1',
			'name' => 'Island Kids',
			'email' => 'islandkids' . $email,
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);

		User::create([
			'user_type' => '1',
			'name' => 'Cardiology',
			'email' => 'cardio' . $email,
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);

		User::create([
			'user_type' => '1',
			'email' => 'glenda' . $email,
			'name' => 'Dr Glenda',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);

		User::create([
			'user_type' => '0',
			'email' => 'zubair' . $email,
			'name' => 'Zubair',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);
		User::create([
			'user_type' => '0',
			'email' => 'mocsy' . $email,
			'name' => 'Moc',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);
		User::create([
			'user_type' => '0',
			'email' => 'alexanderer' . $email,
			'name' => 'Alexanderer',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);
		User::create([
			'user_type' => '0',
			'email' => 'david' . $email,
			'name' => 'David',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);
	}

}
