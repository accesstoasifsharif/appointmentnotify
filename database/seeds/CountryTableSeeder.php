<?php

use Illuminate\Database\Seeder;
use Model\Country;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Country::create([
    		'country_name' => 'America',
    		'area_code' => '+1',
    		]);

        // \Models\Country::create([
        //     'country_name' => 'China',
        //     'area_code' => '+2',
        // ]);

        // \Models\Country::create([
        //     'country_name' => 'Pakistan',
        //     'area_code' => '+92',
        // ]);

        // \Models\Country::create([
        //     'country_name' => 'England',
        //     'area_code' => '+3',
        // ]);

        // \Models\Country::create([
        //     'country_name' => 'India',
        //     'area_code' => '+91',
        // ]);
    }
}
