<?php

use Illuminate\Database\Seeder;
use Model\AppointmentReason;

class AppointmentReasonTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		AppointmentReason::create([
			'reason_name' => 'Flu Shot',

		]);

		AppointmentReason::create([
			'reason_name' => 'Fever',

		]);

		AppointmentReason::create([
			'reason_name' => 'Surgery',

		]);

		AppointmentReason::create([
			'reason_name' => 'Neck surgery',

		]);

		AppointmentReason::create([
			'reason_name' => 'Heart Problem',

		]);

		AppointmentReason::create([
			'reason_name' => 'Skin Infection',

		]);
	}
}
