<?php

use Illuminate\Database\Seeder;
use Model\PersonalInformation;

class PersonalInformationTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		PersonalInformation::create([
			'first_name' => 'Dr Glenda',
			'middle_name' => '',
			'last_name' => 'Santa Maria, DMD',
			'psuedo_name' => 'glenda',
			// 'date_of_birth' => '',
			'gender' => 'female',
			'personalable_id' => 1,
			'personalable_type' => 'Model\Doctor',
		]);

		PersonalInformation::create([
			'first_name' => 'William',
			'middle_name' => 'm',
			'last_name' => 'Santa, MD',
			'psuedo_name' => 'santa',
			// 'date_of_birth' => '',
			'gender' => 'male',
			'personalable_id' => 2,
			'personalable_type' => 'Model\Doctor',
		]);

		PersonalInformation::create([
			'first_name' => 'Brain',
			'middle_name' => 'c',
			'last_name' => 'Lara, MD',
			'psuedo_name' => 'brain',
			// 'date_of_birth' => '',
			'gender' => 'male',
			'personalable_id' => 3,
			'personalable_type' => 'Model\Doctor',
		]);

		PersonalInformation::create([
			'first_name' => 'Alex',
			'middle_name' => 'd',
			'last_name' => 'Lara',
			'psuedo_name' => 'alex',
			// 'date_of_birth' => '',
			'gender' => 'female',
			'personalable_id' => 4,
			'personalable_type' => 'Model\Doctor',
		]);
		PersonalInformation::create([
			'first_name' => 'scarlet',
			'middle_name' => 'f',
			'last_name' => 'johnson',
			'psuedo_name' => 'scarlet',
			// 'date_of_birth' => '',
			'gender' => 'female',
			'personalable_id' => 5,
			'personalable_type' => 'Model\Doctor',
		]);

		PersonalInformation::create([
			'first_name' => 'Mocsy',
			'middle_name' => 'm',
			'last_name' => 'Landy',
			'psuedo_name' => 'mocsy',
			// 'date_of_birth' => '',
			'gender' => 'male',
			'personalable_id' => 4,
			'personalable_type' => 'Model\SiteUser',
		]);
		PersonalInformation::create([
			'first_name' => 'Zubair',
			'middle_name' => 'm',
			'last_name' => 'chuadhary',
			'psuedo_name' => 'zubi',
			// 'date_of_birth' => '',
			'gender' => 'male',
			'personalable_id' => 1,
			'personalable_type' => 'Model\SiteUser',
		]);
	}
}
