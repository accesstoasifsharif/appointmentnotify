<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$this->command->info('database seeding started ...');

		$this->call(AdminTableSeeder::class);
		$this->call(UserTableSeeder::class);
		$this->call(HealthCareProviderTableSeeder::class);
		$this->call(FeaturesTableSeeder::class);
		$this->call(RolesTableSeeder::class);
		$this->call(DummyAvatarTableSeeder::class);
		$this->call(SpecialtyTableSeeder::class);
		$this->call(CountryTableSeeder::class);
		$this->call(CityTableSeeder::class);
		$this->call(StateTableSeeder::class);
		$this->call(HealthCareFacilityTableSeeder::class);
		$this->call(DoctorTableSeeder::class);
		$this->call(SiteUserTableSeeder::class);
		$this->call(PersonalInformationTableSeeder::class);
		$this->call(AvatarTableSeeder::class);
		$this->call(HealthCareProviderFacilityTableSeeder::class);
		$this->call(HealthCareProviderDoctorTableSeeder::class);
		$this->call(AppointmentReasonTableSeeder::class);
		$this->call(ContactInformationTableSeeder::class);
		$this->call(TimeTableTableSeeder::class);
		$this->call(AddressTableSeeder::class);
		$this->call(AnEventTableSeeder::class);
		$this->call(AppointmentTableSeeder::class);
		$this->call(AppointmentReservationTableSeeder::class);
		$this->call(AppointmentRescheduleTableSeeder::class);
		$this->call(AppointmentCancelationRequestTableSeeder::class);
		$this->call(AppointmentHistoriesTableSeeder::class);

		$this->command->info('database seeding completed ...');

	}
}
