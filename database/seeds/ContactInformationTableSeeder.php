<?php

use Illuminate\Database\Seeder;
use Model\ContactInformation;

class ContactInformationTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$email = '@cherry.com';
		ContactInformation::create([
			'phone_number' => '(718) 982-2363',
			'fax_number' => '123-234-2423',
			'contactable_id' => 4,
			'contactable_type' => 'Model\SiteUser',
		]);

		ContactInformation::create([
			'phone_number' => '(718) 982-2363',
			'fax_number' => '123-234-2423',
			'contactable_id' => 1,
			'contactable_type' => 'Model\SiteUser',
		]);
		ContactInformation::create([
			'phone_number' => '(718) 982-2363',
			'fax_number' => '',
			'email' => '',
			'contactable_id' => 1,
			'contactable_type' => 'Model\Doctor',
		]);
		ContactInformation::create([
			'phone_number' => '(700) 900-2063',
			'fax_number' => '',
			'email' => '',
			'contactable_id' => 2,
			'contactable_type' => 'Model\Doctor',
		]);
		ContactInformation::create([
			'phone_number' => '(718) 967-276',
			'fax_number' => '',
			'email' => '',
			'contactable_id' => 3,
			'contactable_type' => 'Model\Doctor',
		]);
		ContactInformation::create([
			'phone_number' => '(503) 236-647',
			'fax_number' => '',
			'email' => '',
			'contactable_id' => 4,
			'contactable_type' => 'Model\Doctor',
		]);
		ContactInformation::create([
			'phone_number' => '(503) 236-647',
			'fax_number' => '(212) 213 2312',
			'email' => '',
			'contactable_id' => 5,
			'contactable_type' => 'Model\Doctor',
		]);

		ContactInformation::create([
			'phone_number' => '(782) 236-971',
			'fax_number' => '',
			'email' => 'clovelakehealthcare' . $email,
			'contactable_id' => 1,
			'contactable_type' => 'Model\HealthCareFacility',
		]);
		ContactInformation::create([
			'phone_number' => '(503) 95-733',
			'fax_number' => '',
			'email' => 'richmondmedicalcenter' . $email,
			'contactable_id' => 2,
			'contactable_type' => 'Model\HealthCareFacility',
		]);

	}
}
