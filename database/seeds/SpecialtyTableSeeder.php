<?php

use Illuminate\Database\Seeder;
use Model\Specialty;

class SpecialtyTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		Specialty::create([
			'specialty_name' => 'Acupuncture',
		]);
		Specialty::create([
			'specialty_name' => 'Adolescent Medicine',
		]);

		Specialty::create([
			'specialty_name' => 'Allergy & Immunology',
		]);

		Specialty::create([
			'specialty_name' => 'Anatomic Pathology',
		]);

		Specialty::create([
			'specialty_name' => 'Nuclear Medicine',
		]);

		Specialty::create([
			'specialty_name' => 'Nuclear Medicine',
		]);

		Specialty::create([
			'specialty_name' => 'Nursing (Clinical Nurse Specialist)',
		]);

		Specialty::create([
			'specialty_name' => 'Nursing (Registered Nurse)',
		]);

		Specialty::create([
			'specialty_name' => 'Nursing Services',
		]);

		Specialty::create([
			'specialty_name' => 'Occupational Therapy (Occupational Therapy Assistant)',
		]);

		Specialty::create([
			'specialty_name' => 'Ophthamology (Ophthalmic Assistant)',
		]);

		Specialty::create([
			'specialty_name' => 'Cardiac Surgery',
		]);

		Specialty::create([
			'specialty_name' => 'Clinical Informatics',
		]);

		Specialty::create([
			'specialty_name' => 'Clinical Molecular Genetics',
		]);

		Specialty::create([
			'specialty_name' => 'Oral & Maxillofacial Surgery',
		]);

		Specialty::create([
			'specialty_name' => 'Pediatric Emergency Medicine',
		]);

		Specialty::create([
			'specialty_name' => 'Pediatric Hematology & Oncology',
		]);

		Specialty::create([
			'specialty_name' => 'Colon & Rectal Surgery',
		]);

		Specialty::create([
			'specialty_name' => 'Dance Therapy',
		]);

		Specialty::create([
			'specialty_name' => 'Pediatric Neurodevelopmental Disabilities',
		]);

		Specialty::create([
			'specialty_name' => 'Pediatric Nursing (Nurse Practitioner)',
		]);

		Specialty::create([
			'specialty_name' => 'Pediatric Optometry',
		]);

		Specialty::create([
			'specialty_name' => 'Pediatric Orthopedic Surgery',
		]);

		Specialty::create([
			'specialty_name' => 'Diagnostic Ultrasound Imaging',
		]);

		Specialty::create([
			'specialty_name' => 'Epilepsy',
		]);

		Specialty::create([
			'specialty_name' => 'Pharmacotherapy',

		]);
		Specialty::create([
			'specialty_name' => 'Pediatric Plastic Surgery',

		]);
		Specialty::create([
			'specialty_name' => 'Pediatric Radiology',

		]);
		Specialty::create([
			'specialty_name' => 'Pediatric Neurosurgery',

		]);

		Specialty::create([
			'specialty_name' => 'Pediatric Pulmonology',

		]);

		Specialty::create([
			'specialty_name' => 'Pediatric Sports Medicine',

		]);

		Specialty::create([
			'specialty_name' => 'Personal Emergency Response Attendance',

		]);

		Specialty::create([
			'specialty_name' => 'Plastic Surgery-Head & Neck',

		]);

		Specialty::create([
			'specialty_name' => 'Hepatology',

		]);

		Specialty::create([
			'specialty_name' => 'Prosthetics & Orthotics',

		]);

		Specialty::create([
			'specialty_name' => 'Pediatric Neurodevelopmental Disabilities',

		]);

		Specialty::create([
			'specialty_name' => 'Radiation Oncology',

		]);

		Specialty::create([
			'specialty_name' => 'Laboratory Medicine',

		]);

		Specialty::create([
			'specialty_name' => 'Legal Medicine',

		]);

		Specialty::create([
			'specialty_name' => 'Marriage & Family Therapy',

		]);

		Specialty::create([
			'specialty_name' => 'Medical Biochemical Genetics',

		]);

		Specialty::create([
			'specialty_name' => 'Medical Ethics',

		]);

		Specialty::create([
			'specialty_name' => 'Speech-Language Pathology',

		]);

		Specialty::create([
			'specialty_name' => 'Spinal Cord Injury Medicine',

		]);

		Specialty::create([
			'specialty_name' => 'Substance Abuse Counseling',

		]);

		Specialty::create([
			'specialty_name' => 'Medical Microbiology',

		]);
		Specialty::create([
			'specialty_name' => 'Music Therapy',

		]);

		Specialty::create([
			'specialty_name' => 'Medical Toxicology',

		]);
		Specialty::create([
			'specialty_name' => 'Music Therapy',

		]);
		Specialty::create([
			'specialty_name' => 'Transplant Surgery',

		]);
		Specialty::create([
			'specialty_name' => 'Vascular & Interventional Radiology',

		]);
		Specialty::create([
			'specialty_name' => 'Vascular Surgery',

		]);
		Specialty::create([
			'specialty_name' => 'Vascular Medicine',

		]);
		Specialty::create([
			'specialty_name' => 'Research',

		]);
		Specialty::create([
			'specialty_name' => 'Pulmonology',

		]);
		Specialty::create([
			'specialty_name' => 'Psychosomatic Medicine',

		]);
		Specialty::create([
			'specialty_name' => 'Preventive Medicine',

		]);
		Specialty::create([
			'specialty_name' => 'Home Health (Aide)',

		]);
		Specialty::create([
			'specialty_name' => 'Pediatric Urology',

		]);

	}

}
