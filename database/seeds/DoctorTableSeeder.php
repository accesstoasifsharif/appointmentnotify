<?php

use App\User;
use Illuminate\Database\Seeder;
use Model\Doctor;
use Model\HealthCareProvider;
use Model\SinglePractice;

class DoctorTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$email = '@cherry.com';
		Doctor::create([
			'specialty_id' => 11,
			'verify_status' => 1,
		]);

		SinglePractice::create([
			'health_care_provider_id' => '4',
			'doctor_id' => '1',
		]);

		$user = User::create([
			'user_type' => '1',
			'email' => 'william' . $email,
			'name' => 'Dr William',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);

		$hcp = HealthCareProvider::create([
			'user_id' => $user->id,
			'provider_type' => '3',
		]);

		$doctor = Doctor::create([
			'specialty_id' => 12,
			'verify_status' => 1,
		]);
		SinglePractice::create([
			'health_care_provider_id' => $hcp->id,
			'doctor_id' => $doctor->id,
		]);

		$user = User::create([
			'user_type' => '1',
			'email' => 'brain' . $email,
			'name' => 'Dr Brain',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);

		$hcp = HealthCareProvider::create([
			'user_id' => $user->id,
			'provider_type' => '3',
		]);

		$doctor = Doctor::create([
			'specialty_id' => 14,
			'verify_status' => 1,
		]);
		SinglePractice::create([
			'health_care_provider_id' => $hcp->id,
			'doctor_id' => $doctor->id,
		]);

		$user = User::create([
			'user_type' => '1',
			'email' => 'alex' . $email,
			'name' => 'Dr Alex',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);

		$hcp = HealthCareProvider::create([
			'user_id' => $user->id,
			'provider_type' => '3',
		]);

		$doctor = Doctor::create([
			'specialty_id' => 16,
			'verify_status' => 1,
		]);
		SinglePractice::create([
			'health_care_provider_id' => $hcp->id,
			'doctor_id' => $doctor->id,
		]);

		$user = User::create([
			'user_type' => '1',
			'email' => 'scarlet' . $email,
			'name' => 'Dr Scarlet',
			'password' => bcrypt('cherry'),
			'profile_status' => '1',
		]);

		$hcp = HealthCareProvider::create([
			'user_id' => $user->id,
			'provider_type' => '3',
		]);

		$doctor = Doctor::create([
			'specialty_id' => 19,
			'verify_status' => 1,
		]);
		SinglePractice::create([
			'health_care_provider_id' => $hcp->id,
			'doctor_id' => $doctor->id,
		]);

	}
}
