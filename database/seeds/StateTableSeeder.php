<?php

use Illuminate\Database\Seeder;
use Model\State;

class StateTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$file_n = storage_path(ASSET_FILES . US_State_FILE);
		$file = fopen($file_n, "r");
		while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {
			if (!in_array(null, $data)) {
				$state = new State();
				$state->state_name = $data[0];
				$state->postal_code = $data[1];
				$state->country_id = 1;
				$state->save();
			}
		}
		fclose($file);
		// event(new StateSeedEvent());
	}
}
