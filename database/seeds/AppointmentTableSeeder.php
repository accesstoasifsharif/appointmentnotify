<?php

use Illuminate\Database\Seeder;
use Model\Appointment;

class AppointmentTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Appointment::create([
			'an_event_id' => 1,
		]);
		Appointment::create([
			'an_event_id' => 2,
		]);
		Appointment::create([
			'an_event_id' => 3,
		]);
		Appointment::create([
			'an_event_id' => 4,
		]);
		Appointment::create([
			'an_event_id' => 5,
		]);
		Appointment::create([
			'an_event_id' => 6,
		]);
		Appointment::create([
			'an_event_id' => 7,
		]);
	}
}
