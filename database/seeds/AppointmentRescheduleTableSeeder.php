<?php
use Illuminate\Database\Seeder;
use Model\AnEvent;
use Model\AppointmentReschedule;

class AppointmentRescheduleTableSeeder extends Seeder {
/**
 * Run the database seeds.
 *
 * @return void
 */
	public function run() {

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-2-2',
			'start_time' => '12:45',
			'end_time' => '1:00',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '1',
			'health_care_provider_facility_id' => '1',
			'appointment_reason_id' => '1',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReschedule::create([
			'an_event_id' => $an_event->id,
			'doctor_id' => $an_event->doctor_id,
			'prefer_time_1' => '12:45',
			'prefer_time_2' => '12:00',
			'prefer_date_1' => '2017-2-2',
			'prefer_date_2' => '2017-2-5',

		]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-3-6',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '1',
			'health_care_provider_facility_id' => '1',
			'appointment_reason_id' => '2',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReschedule::create([
			'an_event_id' => $an_event->id,
			'doctor_id' => $an_event->doctor_id,
			'prefer_time_1' => '13:45',
			'prefer_time_2' => '13:00',
			'prefer_date_1' => '2017-3-2',
			'prefer_date_2' => '2017-3-5',

		]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2016-3-6',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '1',
			'health_care_provider_facility_id' => '1',
			'appointment_reason_id' => '3',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReschedule::create([
			'an_event_id' => $an_event->id,
			'doctor_id' => $an_event->doctor_id,
			'prefer_time_1' => '13:45',
			'prefer_time_2' => '13:00',
			'prefer_date_1' => '2017-4-20',
			'prefer_date_2' => '2017-4-15',

		]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-6-6',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '2',
			'health_care_provider_facility_id' => '2',
			'appointment_reason_id' => '4',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReschedule::create([
			'an_event_id' => $an_event->id,
			'doctor_id' => $an_event->doctor_id,
			'prefer_time_1' => '13:45',
			'prefer_time_2' => '13:00',
			'prefer_date_1' => '2017-3-20',
			'prefer_date_2' => '2017-3-25',

		]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-08-08',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '2',
			'health_care_provider_facility_id' => '2',
			'appointment_reason_id' => '4',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReschedule::create([
			'an_event_id' => $an_event->id,
			'doctor_id' => $an_event->doctor_id,
			'prefer_time_1' => '13:45',
			'prefer_time_2' => '13:00',
			'prefer_date_1' => '2017-5-2',
			'prefer_date_2' => '2017-5-5',

		]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-08-08',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '2',
			'health_care_provider_facility_id' => '2',
			'appointment_reason_id' => '4',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReschedule::create([
			'an_event_id' => $an_event->id,
			'doctor_id' => $an_event->doctor_id,
			'prefer_time_1' => '13:45',
			'prefer_time_2' => '13:00',
			'prefer_date_1' => '2017-7-2',
			'prefer_date_2' => '2017-8-5',

		]);

		$an_event = AnEvent::create([
			'calendar_type' => '0',
			'date' => '2017-08-08',
			'start_time' => '12:30',
			'end_time' => '1:30',
			'site_user_id' => '1',
			'doctor_id' => '1',
			'health_care_facility_id' => '2',
			'health_care_provider_facility_id' => '2',
			'appointment_reason_id' => '4',
			'appointment_status' => '0',
			'appointment_current_state' => '0',
		]);

		AppointmentReschedule::create([
			'an_event_id' => $an_event->id,
			'doctor_id' => $an_event->doctor_id,
			'prefer_time_1' => '13:45',
			'prefer_time_2' => '13:00',
			'prefer_date_1' => '2017-12-2',
			'prefer_date_2' => '2017-12-5',
		]);

	}

}
