<?php

use Illuminate\Database\Seeder;
use Model\TimeTable;

class TimeTableTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		TimeTable::create([
			'time_from' => '08:00:00',
			'time_to' => '05:00:00',
			'day' => 'monday',
			'timetableable_id' => 1,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);

		TimeTable::create([
			'time_from' => '08:00:00',
			'time_to' => '05:00:00',
			'day' => 'tuesday',
			'timetableable_id' => 1,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);
		TimeTable::create([
			'time_from' => '08:00:00',
			'time_to' => '05:00:00',
			'day' => 'wednesday',
			'timetableable_id' => 1,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);
		TimeTable::create([
			'time_from' => '08:00:00',
			'time_to' => '05:00:00',
			'day' => 'thursday',
			'timetableable_id' => 1,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);
		TimeTable::create([
			'time_from' => '08:00:00',
			'time_to' => '05:00:00',
			'day' => 'friday',
			'timetableable_id' => 1,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);
		TimeTable::create([
			'time_from' => '08:00:00',
			'time_to' => '05:00:00',
			'day' => 'saturday',
			'timetableable_id' => 1,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);

		TimeTable::create([
			'time_from' => '09:00:00',
			'time_to' => '04:00:00',
			'day' => 'monday',
			'timetableable_id' => 2,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);

		TimeTable::create([
			'time_from' => '09:00:00',
			'time_to' => '04:00:00',
			'day' => 'tuesday',
			'timetableable_id' => 2,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);
		TimeTable::create([
			'time_from' => '09:00:00',
			'time_to' => '04:00:00',
			'day' => 'wednesday',
			'timetableable_id' => 2,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);
		TimeTable::create([
			'time_from' => '09:00:00',
			'time_to' => '04:00:00',
			'day' => 'thursday',
			'timetableable_id' => 2,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);
		TimeTable::create([
			'time_from' => '09:00:00',
			'time_to' => '04:00:00',
			'day' => 'friday',
			'timetableable_id' => 2,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);
		TimeTable::create([
			'time_from' => '09:00:00',
			'time_to' => '04:00:00',
			'day' => 'saturday',
			'timetableable_id' => 2,
			'timetableable_type' => 'Model\HealthCareFacility',
		]);

	}
}
