<?php
use App\Admin;
use App\User;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$user = new User();
		$user = User::create([
			'email' => 'adnanmoghal@gmail.com',
			'name' => 'Adnan Moghal',

			'user_type' => '9',
			'password' => bcrypt('cherry'),
		]);
		Admin::create([
			'user_id' => $user->id,
			'job_title' => 'Senior Software Architect',
		]);

		$user = User::create([
			'email' => 'alihaider123go@gmail.com',
			'name' => 'Ali Haider',
			'user_type' => '9',
			'password' => bcrypt('cherry'),
		]);
		Admin::create([
			'user_id' => $user->id,
			'job_title' => 'Laravel guru',
		]);

		$user = User::create([
			'email' => 'aliraza@gmail.com',
			'name' => 'Ali Raza',
			'user_type' => '9',
			'password' => bcrypt('cherry'),
		]);
		Admin::create([
			'user_id' => $user->id,
			'job_title' => 'Front End as well as Back End Developer',
		]);

		$user = User::create([
			'email' => 'asifsharif@gmail.com',
			'name' => 'Asif Sharif',
			'user_type' => '9',
			'password' => bcrypt('cherry'),
		]);

		Admin::create([
			'user_id' => $user->id,
			'job_title' => 'Front End Vue Js Cheeta',
		]);

	}

}
