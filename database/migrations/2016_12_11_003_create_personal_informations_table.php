<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonalInformationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('personal_informations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('first_name', 50)->nullable();
			$table->string('middle_name', 30)->nullable();
			$table->string('last_name', 50)->nullable();
			$table->string('psuedo_name', 50)->nullable();
			$table->date('date_of_birth')->nullable();
			$table->enum('gender', ['male', 'female', 'other']);
			$table->integer('personalable_id');
			$table->string('personalable_type');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('personal_informations');
	}

}
