<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryContactNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secondary_contact_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_number');
            $table->tinyInteger('contact_type');
            // $table->enum('contact_type',['Home','Work','Fax']);
            $table->integer('secondarycontactable_id');
            $table->string('secondarycontactable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secondary_contact_numbers');
    }
}
