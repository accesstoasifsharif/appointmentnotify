<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentOtherReasonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('appointment_other_reasons', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('reason_name',100);
			$table->timestamps();
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')
			->references('id')
			->on('users')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('appointment_other_reasons');
	}

}
