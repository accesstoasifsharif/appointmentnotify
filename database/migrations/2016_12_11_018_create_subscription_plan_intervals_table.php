<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPlanIntervalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('subscription_plan_intervals', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('subscription_plan_name');           
            $table->integer('subscription_plan_id')->unsigned();
            $table->foreign('subscription_plan_id')
                           ->references('id')
                           ->on('subscription_plans')
                           ->onDelete('cascade');

            $table->integer('interval_period_id')->unsigned();
            $table->foreign('interval_period_id')
                           ->references('id')
                           ->on('interval_periods')
                           ->onDelete('cascade');
            $table->string('amount');                                 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('subscription_plan_intervals');
    }
}
