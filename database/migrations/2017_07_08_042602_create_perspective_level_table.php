<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerspectiveLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perspective_levels', function(Blueprint $table)
        {
            $table->increments('id');
            
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

            $table->integer('health_care_provider_id')->unsigned();
            $table->foreign('health_care_provider_id')
            ->references('id')
            ->on('health_care_providers')
            ->onDelete('cascade');

            $table->integer('role_level_id')->unsigned();
            $table->foreign('role_level_id')
            ->references('id')
            ->on('role_levels')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perspective_levels');
    }
}
