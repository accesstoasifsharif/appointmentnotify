<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('appointment_notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('description',250)->nullable();
			$table->integer('appointment_id')->unsigned();
			$table->foreign('appointment_id')
			->references('id')
			->on('appointments');
			$table->timestamps();                
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('appointment_notes');
	}

}
