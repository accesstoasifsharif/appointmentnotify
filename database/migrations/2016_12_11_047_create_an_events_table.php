<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('an_events', function(Blueprint $table)
        {
            $table->increments('id');
            $table->tinyInteger('calendar_type');
            $table->date('date');
            $table->time('start_time');
            $table->time('end_time')->nullable();

            $table->integer('site_user_id')->unsigned()->nullable();
            $table->foreign('site_user_id')
            ->references('id')
            ->on('site_users')
            ->onDelete('cascade');

            $table->integer('doctor_id')->unsigned()->nullable();
            $table->foreign('doctor_id')
            ->references('id')
            ->on('doctors')
            ->onDelete('cascade');

            $table->integer('health_care_facility_id')->unsigned()->nullable();
            $table->foreign('health_care_facility_id')
            ->references('id')
            ->on('health_care_facilities')
            ->onDelete('cascade');

            $table->integer('health_care_provider_facility_id')->unsigned()->nullable();
            $table->foreign('health_care_provider_facility_id')
            ->references('id')
            ->on('health_care_provider_facilities')
            ->onDelete('cascade');

            $table->integer('appointment_reason_id')->unsigned()->nullable();
            $table->foreign('appointment_reason_id')
            ->references('id')
            ->on('appointment_reasons')
            ->onDelete('cascade');

            $table->tinyInteger('appointment_status')->nullable();
            $table->tinyInteger('appointment_current_state')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('an_events');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }

}