<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDummyAvatars extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('dummy_avatars', function (Blueprint $table) {
			$table->increments('id');
			$table->string('avatar');
			$table->enum('gender', ['male', 'female', 'other'])->nullable();
			$table->tinyInteger('default', 0); // defaults to 0
			$table->tinyInteger('user_type');
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('dummy_avatars');
	}
}
