<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentReschedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('appointment_reschedules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('an_event_id')->unsigned();
			$table->foreign('an_event_id')
			->references('id')
			->on('an_events')
			->onDelete('cascade');

			$table->integer('doctor_id')->unsigned();
			$table->foreign('doctor_id')
			->references('id')
			->on('doctors')
			->onDelete('cascade');
			
			$table->time('prefer_time_1')->nullable();
			$table->time('prefer_time_2')->nullable();
			$table->date('prefer_date_1')->nullable();
			$table->date('prefer_date_2')->nullable();

			$table->string('reason',30)->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('appointment_reschedules');
	}

}
