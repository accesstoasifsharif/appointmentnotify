<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHealthCareFacilitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('health_care_facilities', function (Blueprint $table) {
			$table->increments('id');
			$table->string('facility_name', 70);
			//creator_id = user_id
			$table->integer('creator_id')->nullable();
			//honor_id = hcp_id
			$table->integer('honor_id')->nullable();
			$table->decimal('latitude', 9, 6)->nullable();
			$table->decimal('longitude', 9, 6)->nullable();
			$table->tinyInteger('model_identifier')->default(config('appointmentnotify.model_identifiers.HealthCareFacility'));
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('health_care_facilities');
	}

}
