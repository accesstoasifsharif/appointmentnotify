<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDoctorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('doctors', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('specialty_id')->unsigned()->nullable();
			$table->tinyInteger('model_identifier')->default(config('appointmentnotify.model_identifiers.Doctor'));
			$table->tinyInteger('profile_status')->default(0);
			$table->foreign('specialty_id')
				->references('id')
				->on('specialties')
				->onDelete('cascade');
			$table->tinyInteger('verify_status')->default(0);
			$table->timestamps();
		});

		Schema::create('doctor_site_user', function (Blueprint $table) {
			$table->integer('doctor_id')->unsigned();
			$table->foreign('doctor_id')
				->references('id')
				->on('doctors')
				->onDelete('cascade');

			$table->integer('site_user_id')->unsigned();
			$table->foreign('site_user_id')
				->references('id')
				->on('site_users')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('doctor_site_user');
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::dropIfExists('doctors');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

	}

}
