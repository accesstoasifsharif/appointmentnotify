<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('hospitals', function (Blueprint $table) {
			$table->increments('id');
			$table->string('hospital_name', 100)->nullable();
			$table->integer('parent_id')->default(0);
			$table->integer('health_care_provider_id')->unsigned();
			$table->foreign('health_care_provider_id')
				->references('id')
				->on('health_care_providers')
				->onDelete('cascade');

			$table->timestamps();
		});

		// Schema::create('hospital_branches', function(Blueprint $table)
		// {
		//     $table->increments('id');
		//     $table->string('hospital_branch_name',100);
		//     $table->integer('hospital_id')->unsigned();
		//     $table->foreign('hospital_id')
		//     ->references('id')
		//     ->on('hospitals')
		//     ->onDelete('cascade');
		//     $table->timestamps();
		// });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		// Schema::dropIfExists('hospital_branches');
		Schema::dropIfExists('hospitals');
	}

}
