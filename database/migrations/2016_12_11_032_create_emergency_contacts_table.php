<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmergencyContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('emergency_contacts', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('person_name',50);
            $table->string('phone_number',15);
            $table->string('email',62);
            $table->string('relation');
            $table->tinyInteger('contact_index');
            
            $table->integer('emergencycontactable_id');
            $table->string('emergencycontactable_type');                   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergency_contacts');
    }
}
