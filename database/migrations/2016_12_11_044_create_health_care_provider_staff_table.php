<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHealthCareProviderStaffTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('health_care_provider_staff', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('hcp_facility_id')->unsigned();
			$table->foreign('hcp_facility_id')
				->references('id')
				->on('health_care_provider_facilities')
				->onDelete('cascade');

			$table->integer('staff_id')->unsigned()->nullable();
			$table->foreign('staff_id')
				->references('id')
				->on('staff')
				->onDelete('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('health_care_provider_staff');
	}

}