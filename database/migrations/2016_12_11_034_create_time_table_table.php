<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('time_tables', function(Blueprint $table)
       {
        $table->increments('id');
        $table->time('time_from');
        $table->time('time_to');
        $table->enum('day',['monday','tuesday','wednesday','thursday','friday','saturday','sunday']);
        $table->integer('timetableable_id');
        $table->string('timetableable_type');
        $table->timestamps();
    });


   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_tables');
    }

}
