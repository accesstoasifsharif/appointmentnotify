<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProviderOrgNameTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('provider_organization_names', function (Blueprint $table) {
			$table->increments('id');
			$table->string('pon_name', 100);
			$table->integer('ponnameable_id');
			$table->string('ponnameable_type');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('provider_organization_names');
	}
}
