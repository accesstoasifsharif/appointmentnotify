<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalClinicTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('medical_clinics', function (Blueprint $table) {
			$table->increments('id');
			$table->string('provider_organization_name', 50)->nullable();
			$table->string('contact_person', 50)->nullable();
			$table->integer('health_care_provider_id')->unsigned();
			$table->foreign('health_care_provider_id')
				->references('id')
				->on('health_care_providers')
				->onDelete('cascade');

			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('medical_clinics');
	}
}
