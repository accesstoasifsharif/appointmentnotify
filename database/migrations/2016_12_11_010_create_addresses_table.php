<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('addresses', function (Blueprint $table) {
			$table->increments('id');
			$table->string('address_line_1', 95)->nullable();
			$table->string('address_line_2', 95)->nullable();
			$table->string('city_name', 40)->nullable();
			$table->string('zip_code', 5)->nullable();
			$table->integer('state_id')->unsigned();
			$table->integer('country_id')->unsigned();
			$table->tinyInteger('address_type')->nullable();
			$table->integer('addressable_id');
			$table->string('addressable_type');
			$table->foreign('state_id')
				->references('id')
				->on('states')
				->onDelete('cascade');
			$table->foreign('country_id')
				->references('id')
				->on('countries')
				->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('addresses');
	}

}
