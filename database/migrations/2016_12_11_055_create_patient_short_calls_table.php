<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientShortCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_short_calls', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('doctor_id')->unsigned();
            $table->foreign('doctor_id')
            ->references('id')
            ->on('doctors')
            ->onDelete('cascade');

            $table->integer('health_care_provider_facility_id')->unsigned();
            $table->foreign('health_care_provider_facility_id')
            ->references('id')
            ->on('health_care_provider_facilities')
            ->onDelete('cascade');                

            $table->integer('site_user_id')->unsigned()->nullable();
            $table->foreign('site_user_id')
            ->references('id')
            ->on('site_users')
            ->onDelete('cascade');                
            $table->string('site_user_email' , 62 )->nullable();
            $table->string('time_span');                
            $table->timestamp('expiry_date_time');                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_short_calls');
    }
}
