<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPlanSelect extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_plan_select', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('health_care_provider_id')->unsigned();
            $table->foreign('health_care_provider_id')
                           ->references('id')
                           ->on('health_care_providers')
                           ->onDelete('cascade');
            $table->integer('subscription_plan_interval_id')->unsigned();
            $table->foreign('subscription_plan_interval_id')
                           ->references('id')
                           ->on('subscription_plan_intervals')
                           ->onDelete('cascade');
            $table->string('status');               
            $table->timestamp('end_at');                           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_plan_select');
    }
}
