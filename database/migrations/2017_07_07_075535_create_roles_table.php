<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('business_types', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('business_name' , 30);
            $table->integer('config_id')->nullable();
            $table->timestamps();
        });


        Schema::create('role_levels', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('level_name' , 30);
            $table->timestamps();
        });


        Schema::create('roles', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name' , 50);
            $table->string('label' , 50)->nullable();

            $table->integer('business_type_id')->unsigned();
            $table->foreign('business_type_id')
            ->references('id')
            ->on('business_types')
            ->onDelete('cascade');

            $table->integer('role_level_id')->unsigned();
            $table->foreign('role_level_id')
            ->references('id')
            ->on('role_levels')
            ->onDelete('cascade');

            $table->integer('feature_id')->unsigned();
            $table->foreign('feature_id')
            ->references('id')
            ->on('features')
            ->onDelete('cascade');

            $table->timestamps();
        });





        Schema::create('provider_role', function(Blueprint $table)
        {
            $table->integer('health_care_provider_id')->unsigned();
            $table->foreign('health_care_provider_id')
            ->references('id')
            ->on('health_care_providers')
            ->onDelete('cascade');

            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')
            ->references('id')
            ->on('roles')
            ->onDelete('cascade');
            $table->primary(['health_care_provider_id', 'role_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { 
        Schema::dropIfExists('provider_role');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('role_levels');
        Schema::dropIfExists('business_types');
    }

}