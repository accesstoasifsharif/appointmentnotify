<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 30)->unique()->nullable();
			$table->tinyInteger('user_type');
			$table->string('email', 62)->unique();
			$table->string('password', 100);
			$table->string('slug')->unique()->nullable();
			$table->tinyInteger('profile_status')->default(0);
			$table->rememberToken();
			$table->timestamps();
			$table->timestamp('current_login_date_time')->nullable();
			$table->timestamp('last_login_date_time')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::dropIfExists('users');

	}
}
