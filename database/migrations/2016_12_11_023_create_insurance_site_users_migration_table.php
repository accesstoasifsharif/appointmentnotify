<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceSiteUsersMigrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_site_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_id')->unsigned();
            $table->integer('site_user_id')->unsigned();
            
            $table->foreign('insurance_id')
            ->references('id')
            ->on('insurances')
            ->onDelete('cascade');
           
            $table->foreign('site_user_id')
            ->references('id')
            ->on('site_users')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_site_user');
    }
}
