<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProviderOrganizationNumberTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('provider_organization_numbers', function (Blueprint $table) {
			$table->increments('id');
			$table->string('pon_number', 10)->unique();
			$table->integer('ponnumberable_id');
			$table->string('ponnumberable_type');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('provider_organization_numbers');
	}
}
