<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('departments', function (Blueprint $table) {
			$table->increments('id');
			$table->string('department_name', 50)->nullable();
			$table->integer('parent_id')->default(0);
			$table->integer('health_care_provider_id')->unsigned()->nullable();
			$table->foreign('health_care_provider_id')
				->references('id')
				->on('health_care_providers')
				->onDelete('cascade');

			$table->integer('other_hcp_id')->unsigned()->nullable();
			$table->foreign('other_hcp_id')
				->references('id')
				->on('health_care_providers')
				->onDelete('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('departments');
	}

}
