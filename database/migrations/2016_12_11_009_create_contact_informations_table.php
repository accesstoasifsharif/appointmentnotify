<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactInformationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('contact_informations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('phone_number', 15)->nullable();
			$table->string('fax_number', 15)->nullable();
			$table->string('email')->nullable();
			$table->string('url')->nullable();
			$table->integer('contactable_id');
			$table->string('contactable_type');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('contact_informations');
	}

}
