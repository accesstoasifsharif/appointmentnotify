<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNationalProviderIdentificationTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('national_provider_identifications', function (Blueprint $table) {
			$table->increments('id');
			$table->string('npi', 10)->unique();
			$table->integer('npinumberable_id');
			$table->string('npinumberable_type');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('national_provider_identifications');
	}
}
