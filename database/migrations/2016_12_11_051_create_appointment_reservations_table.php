<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentReservationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('appointment_reservations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('site_user_id')->unsigned();
			$table->foreign('site_user_id')
			->references('id')
			->on('site_users')
			->onDelete('cascade');

			$table->integer('an_event_id')->unsigned();
			$table->foreign('an_event_id')
			->references('id')
			->on('an_events')
			->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('appointment_reservations');
	}

}
