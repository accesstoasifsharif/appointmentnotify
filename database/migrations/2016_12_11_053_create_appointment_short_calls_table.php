<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentShortCallsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('appointment_short_calls', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('an_event_id')->unsigned();
			$table->foreign('an_event_id')
				->references('id')
				->on('an_events')
				->onDelete('cascade');

			// $table->integer('site_user_id')->unsigned();
			// $table->foreign('site_user_id')
			// ->references('id')
			// ->on('site_users')
			// ->onDelete('cascade');

			$table->timestamp('expiry_date_time');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('appointment_short_calls');
	}
}
