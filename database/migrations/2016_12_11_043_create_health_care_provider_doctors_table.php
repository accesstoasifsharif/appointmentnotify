<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHealthCareProviderDoctorsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('health_care_provider_doctors', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('hcp_facility_id')->unsigned();
			$table->foreign('hcp_facility_id')
				->references('id')
				->on('health_care_provider_facilities')
				->onDelete('cascade');

			$table->integer('doctor_id')->unsigned()->nullable();
			$table->foreign('doctor_id')
				->references('id')
				->on('doctors')
				->onDelete('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('health_care_provider_doctors');
	}

}
