<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHealthCareProviderFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_care_provider_facilities', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('health_care_facility_id')->unsigned();
            $table->foreign('health_care_facility_id')
            ->references('id')
            ->on('health_care_facilities')
            ->onDelete('cascade');

            $table->integer('health_care_provider_id')->unsigned();
            $table->foreign('health_care_provider_id')
            ->references('id')
            ->on('health_care_providers')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_care_provider_facilities');
    }

}
