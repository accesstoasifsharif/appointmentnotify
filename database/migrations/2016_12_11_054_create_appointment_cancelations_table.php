<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentCancelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_cancelations', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('an_event_id')->unsigned();
            $table->foreign('an_event_id')
            ->references('id')
            ->on('an_events')
            ->onDelete('cascade');
            
            $table->integer('doctor_id')->unsigned();
            $table->foreign('doctor_id')
            ->references('id')
            ->on('doctors')
            ->onDelete('cascade');                

            $table->timestamp('expiry_date_time');                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_cancelations');
    }
}
