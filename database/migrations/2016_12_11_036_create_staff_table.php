<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('staff', function (Blueprint $table) {
			$table->increments('id');
			$table->string('designation', 15)->nullable();
			$table->tinyInteger('model_identifier')->default(config('appointmentnotify.model_identifiers.Staff'));
			$table->tinyInteger('profile_status')->default(0);
			$table->integer('health_care_provider_id')->unsigned()->nullable();
			$table->foreign('health_care_provider_id')
				->references('id')
				->on('health_care_providers')
				->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('staff');
	}

}
