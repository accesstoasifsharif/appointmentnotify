var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var Redis = require('ioredis');
var redis = new Redis();
redis.subscribe(['message-conversation','patient-to-provider','provider-to-patient']);
redis.on('message',function(channel,message){
	message = JSON.parse(message);
	console.log(channel);
	console.log(message.data);
	io.emit(channel, message.data);
})

server.listen(3002);
